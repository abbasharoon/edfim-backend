<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\AbsentReason
 *
 * @property int $id
 * @property string $reason
 * @property int $tenant_id
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbsentReason whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbsentReason whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbsentReason whereReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AbsentReason whereTenantId($value)
 */
	class AbsentReason extends \Eloquent {}
}

namespace App{
/**
 * App\AssessmentExam
 *
 * @property int $id
 * @property string $name
 * @property string|null $detail
 * @property int $schedule 1=daily,2=weekly,3=monthly,4=quarterly,5=triannual,6=biannually,7=annually
 * @property int $status 0=draft, 1 =due, 2=taken, 3 =published, 4=cancelled
 * @property int $batch_id
 * @property int $class_Id
 * @property int $tenant_id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ExamPaper[] $papers
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentExam whereBatchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentExam whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentExam whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentExam whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentExam whereDetail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentExam whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentExam whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentExam whereSchedule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentExam whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentExam whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentExam whereUpdatedAt($value)
 */
	class AssessmentExam extends \Eloquent {}
}

namespace App{
/**
 * App\AssessmentTest
 *
 * @property int $id
 * @property string $name
 * @property string|null $detail
 * @property string|null $type
 * @property int $schedule 1=daily,2=weekly,3=monthly,4=quarterly,5=triannual,6=biannually,7=annually
 * @property int $status 0=draft, 1 =due, 2=take, 3 =published, 4=cancelled
 * @property int $score
 * @property string $due_date
 * @property int $subject_id
 * @property int $batch_id
 * @property int $tenant_id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\AssessmentTestScore[] $student_score
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentTest whereBatchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentTest whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentTest whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentTest whereDetail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentTest whereDueDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentTest whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentTest whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentTest whereSchedule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentTest whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentTest whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentTest whereSubjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentTest whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentTest whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentTest whereUpdatedAt($value)
 */
	class AssessmentTest extends \Eloquent {}
}

namespace App{
/**
 * App\AssessmentTestScore
 *
 * @property int $id
 * @property int $assessment_test_id
 * @property int $user_id
 * @property int $score
 * @property int $tenant_id
 * @property-read \App\AssessmentTest $assessmentTest
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentTestScore whereAssessmentTestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentTestScore whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentTestScore whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentTestScore whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssessmentTestScore whereUserId($value)
 */
	class AssessmentTestScore extends \Eloquent {}
}

namespace App{
/**
 * App\Assignment
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $batch_id
 * @property int $subject_id
 * @property \Carbon\Carbon $due_date
 * @property int $status 0=draft, 1 =due, 2 =published, 3 =cancelled
 * @property int $score 0 for done/not done score, >0 for marks per like system
 * @property int $tenant_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\AssignmentScore[] $student_score
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assignment whereBatchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assignment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assignment whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assignment whereDueDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assignment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assignment whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assignment whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assignment whereSubjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assignment whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assignment whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Assignment whereUpdatedAt($value)
 */
	class Assignment extends \Eloquent {}
}

namespace App{
/**
 * App\AssignmentScore
 *
 * @property int $id
 * @property int $assignment_id
 * @property int $user_id Student id
 * @property int $score 0/1 for done/not done in case score is zero for assignment, else achieved score
 * @property int $tenant_id
 * @property-read \App\Assignment $assignment
 * @property-read \App\User $student
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssignmentScore whereAssignmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssignmentScore whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssignmentScore whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssignmentScore whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AssignmentScore whereUserId($value)
 */
	class AssignmentScore extends \Eloquent {}
}

namespace App{
/**
 * App\Attendance
 *
 * @property int $id
 * @property \Carbon\Carbon $date
 * @property int $batch_id
 * @property int $status 0=started,1=completed
 * @property int $tenant_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\AttendanceAbsent[] $absent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attendance whereBatchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attendance whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attendance whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attendance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attendance whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attendance whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attendance whereUpdatedAt($value)
 */
	class Attendance extends \Eloquent {}
}

namespace App{
/**
 * App\AttendanceAbsent
 *
 * @property int $id
 * @property int $attendance_id
 * @property int $user_id
 * @property int $reason_id
 * @property int $absent
 * @property-read \App\Attendance $attendance
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttendanceAbsent whereAttendanceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttendanceAbsent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttendanceAbsent whereReasonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AttendanceAbsent whereUserId($value)
 */
	class AttendanceAbsent extends \Eloquent {}
}

namespace App{
/**
 * App\Batch
 *
 * @property int $id
 * @property string $name
 * @property int $class_id
 * @property int $tenant_id
 * @property \Carbon\Carbon|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Classes[] $batch_history
 * @property-read \App\Classes $class
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $student
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Batch onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Batch whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Batch whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Batch whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Batch whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Batch whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Batch whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Batch whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Batch withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Batch withoutTrashed()
 */
	class Batch extends \Eloquent {}
}

namespace App{
/**
 * App\BatchClassHistory
 *
 * @property int $id
 * @property int $batch_id
 * @property int $from_class_id
 * @property int $to_class_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BatchClassHistory whereBatchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BatchClassHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BatchClassHistory whereFromClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BatchClassHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BatchClassHistory whereToClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BatchClassHistory whereUpdatedAt($value)
 */
	class BatchClassHistory extends \Eloquent {}
}

namespace App{
/**
 * App\BatchUserHistory
 *
 * @property int $id
 * @property int $user_id
 * @property int $from_batch_id
 * @property int $to_batch_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BatchUserHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BatchUserHistory whereFromBatchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BatchUserHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BatchUserHistory whereToBatchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BatchUserHistory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BatchUserHistory whereUserId($value)
 */
	class BatchUserHistory extends \Eloquent {}
}

namespace App{
/**
 * App\Classes
 *
 * @property int $id
 * @property string $name
 * @property int $tenant_id
 * @property \Carbon\Carbon|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Batch[] $batch
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $class_staff
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ClassFee[] $fee
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $student
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Subject[] $subject
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Classes onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classes whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classes whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classes whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classes whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classes whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classes whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Classes withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Classes withoutTrashed()
 */
	class Classes extends \Eloquent {}
}

namespace App{
/**
 * App\ClassesTimetable
 *
 * @property int $id
 * @property int $class_id
 * @property int $timetable_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassTimetable whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassTimetable whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassTimetable whereTimetableId($value)
 */
	class ClassesTimetable extends \Eloquent {}
}

namespace App{
/**
 * App\ClassFee
 *
 * @property int $id
 * @property int $class_id
 * @property int $fee_id
 * @property int|null $target_fee_id
 * @property int $tenant_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Classes $class
 * @property-read \App\Fee|null $targetFee
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassFee whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassFee whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassFee whereFeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassFee whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassFee whereTargetFeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassFee whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassFee whereUpdatedAt($value)
 */
	class ClassFee extends \Eloquent {}
}

namespace App{
/**
 * App\ClassInvoice
 *
 * @property int $id
 * @property float $amount_paid
 * @property int $class_id
 * @property int|null $batch_id
 * @property int $status Status of invoice, 1=due,2=paid,3=cancelled
 * @property string|null $due_date
 * @property int $tenant_id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ClassInvoiceFee[] $fee
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Fee[] $pivotFee
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UserInvoice[] $userInvoice
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\ClassInvoice onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassInvoice whereAmountPaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassInvoice whereBatchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassInvoice whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassInvoice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassInvoice whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassInvoice whereDueDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassInvoice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassInvoice whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassInvoice whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassInvoice whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ClassInvoice withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\ClassInvoice withoutTrashed()
 */
	class ClassInvoice extends \Eloquent {}
}

namespace App{
/**
 * App\ClassInvoiceFee
 *
 * @property int $id
 * @property int $class_invoice_id
 * @property int $fee_id
 * @property int|null $target_fee_id
 * @property int $tenant_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Fee $fee
 * @property-read \App\Fee|null $targetFee
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassInvoiceFee whereClassInvoiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassInvoiceFee whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassInvoiceFee whereFeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassInvoiceFee whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassInvoiceFee whereTargetFeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassInvoiceFee whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ClassInvoiceFee whereUpdatedAt($value)
 */
	class ClassInvoiceFee extends \Eloquent {}
}

namespace App{
/**
 * App\ClassUser
 *
 */
	class ClassUser extends \Eloquent {}
}

namespace App{
/**
 * App\ExamPaper
 *
 * @property int $id
 * @property string $name
 * @property string|null $detail
 * @property string|null $type
 * @property int $status 0=draft, 1 =due, 2=take, 3 =published, 4=cancelled
 * @property int $score
 * @property string $due_date
 * @property int $subject_id
 * @property int $assessment_exam_id
 * @property int $tenant_id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\AssessmentExam $assessmentExam
 * @property-read \App\ExamPaper $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ExamPaper[] $parts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ExamPaperScore[] $student_score
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamPaper whereAssessmentExamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamPaper whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamPaper whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamPaper whereDetail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamPaper whereDueDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamPaper whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamPaper whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamPaper whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamPaper whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamPaper whereSubjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamPaper whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamPaper whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamPaper whereUpdatedAt($value)
 */
	class ExamPaper extends \Eloquent {}
}

namespace App{
/**
 * App\ExamPaperScore
 *
 * @property int $id
 * @property int $exam_paper_id
 * @property int $user_id
 * @property int $score
 * @property int $tenant_id
 * @property-read \App\ExamPaper $examPaper
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamPaperScore whereExamPaperId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamPaperScore whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamPaperScore whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamPaperScore whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamPaperScore whereUserId($value)
 */
	class ExamPaperScore extends \Eloquent {}
}

namespace App{
/**
 * App\Fee
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property float $amount
 * @property int $schedule 1=daily,2=weekly,3=monthly,4=quarterly,5=triannual,6=biannually,7=annually
 * @property int $type 1=whole number,2=percentage
 * @property int $addition 0=subtract the fee from total or w.e/1=add the fee
 * @property int|null $fee_category_id
 * @property int $tenant_id
 * @property \Carbon\Carbon|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\FeeCategory|null $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ClassFee[] $class
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Classes[] $classFee
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UserFee[] $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $userFee
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Fee onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Fee whereAddition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Fee whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Fee whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Fee whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Fee whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Fee whereFeeCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Fee whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Fee whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Fee whereSchedule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Fee whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Fee whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Fee whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Fee withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Fee withoutTrashed()
 */
	class Fee extends \Eloquent {}
}

namespace App{
/**
 * App\FeeCategory
 *
 * @property int $id
 * @property string $name
 * @property int $tenant_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Fee[] $fee
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FeeCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FeeCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FeeCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FeeCategory whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FeeCategory whereUpdatedAt($value)
 */
	class FeeCategory extends \Eloquent {}
}

namespace App{
/**
 * App\Reputation
 *
 * @property int $id
 * @property int $badge_id
 * @property int $user_id
 * @property string|null $detail
 * @property int $tenant_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\ReputationBadge $badge
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Reputation whereBadgeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Reputation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Reputation whereDetail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Reputation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Reputation whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Reputation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Reputation whereUserId($value)
 */
	class Reputation extends \Eloquent {}
}

namespace App{
/**
 * App\ReputationBadge
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string|null $image
 * @property string|null $color
 * @property int $score
 * @property int $tenant_id
 * @property \Carbon\Carbon|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\ReputationBadge onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReputationBadge whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReputationBadge whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReputationBadge whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReputationBadge whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReputationBadge whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReputationBadge whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReputationBadge whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReputationBadge whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReputationBadge whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReputationBadge whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ReputationBadge withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\ReputationBadge withoutTrashed()
 */
	class ReputationBadge extends \Eloquent {}
}

namespace App{
/**
 * App\Role
 *
 * @property int $id
 * @property string $name
 * @property string|null $title
 * @property int|null $level
 * @property int|null $scope
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereScope($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereUpdatedAt($value)
 */
	class Role extends \Eloquent {}
}

namespace App{
/**
 * App\Setting
 *
 * @property int $id
 * @property string $key
 * @property string $value
 * @property int $tenant_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereValue($value)
 */
	class Setting extends \Eloquent {}
}

namespace App{
/**
 * App\Subject
 *
 * @property int $id
 * @property string $name
 * @property string $class_id
 * @property int $tenant_id
 * @property \Carbon\Carbon|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Classes $class
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $subject_staff
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Subject onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subject whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subject whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subject whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subject whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subject whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subject whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subject whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Subject withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Subject withoutTrashed()
 */
	class Subject extends \Eloquent {}
}

namespace App{
/**
 * App\SubjectUser
 *
 */
	class SubjectUser extends \Eloquent {}
}

namespace App{
/**
 * App\Tenant
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property int $user_id
 * @property string $settings
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tenant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tenant whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tenant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tenant whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tenant whereSettings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tenant whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tenant whereUserId($value)
 */
	class Tenant extends \Eloquent {}
}

namespace App{
/**
 * App\TenantHostname
 *
 * @property int $id
 * @property int $tenant_id
 * @property string $hostname
 * @property int $status 0 = cancelled, 1=active
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TenantHostname whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TenantHostname whereHostname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TenantHostname whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TenantHostname whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TenantHostname whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TenantHostname whereUpdatedAt($value)
 */
	class TenantHostname extends \Eloquent {}
}

namespace App{
/**
 * App\Timetable
 *
 * @property int $id
 * @property string $title
 * @property string|null $description
 * @property string|null $start_date
 * @property string|null $end_date
 * @property int $tenant_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Classes[] $classTimetable
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Timetable whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Timetable whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Timetable whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Timetable whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Timetable whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Timetable whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Timetable whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Timetable whereUpdatedAt($value)
 */
	class Timetable extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $first_name
 * @property string|null $middle_name
 * @property string $last_name
 * @property string|null $username
 * @property string|null $nic
 * @property string|null $email
 * @property string $password
 * @property string|null $phone_no
 * @property int|null $parent_id To associate child to parent
 * @property int|null $batch_id Nullable for users and parents
 * @property string|null $roll_no
 * @property int $tenant_id
 * @property string|null $remember_token
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Silber\Bouncer\Database\Ability[] $abilities
 * @property-read \App\Batch|null $batch
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $children
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UserFee[] $fee
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \App\User|null $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Role[] $roles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Subject[] $subject
 * @property-read \App\UserData $user_data
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\User onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereBatchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIs($role)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIsAll($role)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIsNot($role)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereMiddleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereNic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhoneNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRollNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\User withoutTrashed()
 */
	class User extends \Eloquent {}
}

namespace App{
/**
 * App\UserData
 *
 * @property int $user_id
 * @property int $gender 1 = male, 2 = female, 3 = other
 * @property string|null $picture_url
 * @property string|null $dob
 * @property string|null $qualification
 * @property string|null $bio_data
 * @property string|null $job_title
 * @property string|null $address
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\User $student
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserData whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserData whereBioData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserData whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserData whereDob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserData whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserData whereJobTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserData wherePictureUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserData whereQualification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserData whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserData whereUserId($value)
 */
	class UserData extends \Eloquent {}
}

namespace App{
/**
 * App\UserFee
 *
 * @property int $id
 * @property int $user_id
 * @property int $fee_id
 * @property int|null $target_fee_id
 * @property int $tenant_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Fee $fee
 * @property-read \App\Fee|null $targetFee
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserFee whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserFee whereFeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserFee whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserFee whereTargetFeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserFee whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserFee whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserFee whereUserId($value)
 */
	class UserFee extends \Eloquent {}
}

namespace App{
/**
 * App\UserInvoice
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $class_invoice_id invoice of a class are added to student, this relation may be userful in future
 * @property float $amount_paid Total paid amount
 * @property int $status Status of invoice, 1=due,2=paid,3=cancelled
 * @property string|null $due_date
 * @property int $tenant_id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\ClassInvoice|null $classInvoice
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\UserInvoiceFee[] $fee
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Fee[] $pivotFee
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\UserInvoice onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInvoice whereAmountPaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInvoice whereClassInvoiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInvoice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInvoice whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInvoice whereDueDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInvoice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInvoice whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInvoice whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInvoice whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInvoice whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\UserInvoice withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\UserInvoice withoutTrashed()
 */
	class UserInvoice extends \Eloquent {}
}

namespace App{
/**
 * App\UserInvoiceFee
 *
 * @property int $id
 * @property int $user_invoice_id
 * @property int $fee_id
 * @property int|null $target_fee_id
 * @property int $tenant_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Fee $fee
 * @property-read \App\Fee|null $targetFee
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInvoiceFee whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInvoiceFee whereFeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInvoiceFee whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInvoiceFee whereTargetFeeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInvoiceFee whereTenantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInvoiceFee whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInvoiceFee whereUserInvoiceId($value)
 */
	class UserInvoiceFee extends \Eloquent {}
}

