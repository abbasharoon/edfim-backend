<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['web']], function () {
    Route::get('admin', 'CP\TenantController@index')->name('admin');
    Route::get('admin/login', 'CP\TenantController@getLogin')->name('adminGetLogin');
    Route::post('admin/login', 'CP\TenantController@postLogin')->name('adminPostLogin');
    Route::get('admin/create', 'CP\TenantController@create')->name('adminGetCreate');
    Route::post('admin/create', 'CP\TenantController@store')->name('adminPostCreate');
});