<?php

use Illuminate\Database\Seeder;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            //        Nursery
            [
                'name' => 'English',
                'class_id' => 1,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Urdu',
                'class_id' => 1,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ], 
            [
                'name' => 'Numbers',
                'class_id' => 1,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
//             Prep
            [
                'name' => 'English',
                'class_id' => 2,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Urdu',
                'class_id' => 2,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],[
                'name' => 'Math',
                'class_id' => 2,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],[
                'name' => 'Science',
                'class_id' => 2,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],

//            Class I
            [
                'name' => 'English',
                'class_id' => 3,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Urdu',
                'class_id' => 3,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Math',
                'class_id' => 3,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Science',
                'class_id' => 3,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            
            
//            Class II
            [
                'name' => 'English',
                'class_id' => 4,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Urdu',
                'class_id' => 4,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Math',
                'class_id' => 4,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Science',
                'class_id' => 4,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            
//            Class 3
            [
                'name' => 'English',
                'class_id' => 5,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Urdu',
                'class_id' => 5,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Math',
                'class_id' => 5,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Science',
                'class_id' => 5,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            
//            Class 4
            [
                'name' => 'English',
                'class_id' => 6,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Urdu',
                'class_id' => 6,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Math',
                'class_id' => 6,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Science',
                'class_id' => 6,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            
//            Class 5
            [
                'name' => 'English',
                'class_id' => 7,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Urdu',
                'class_id' => 7,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Math',
                'class_id' => 7,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Science',
                'class_id' => 7,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            
//            Class 6
            [
                'name' => 'English',
                'class_id' => 8,
                'tenant_id' => 1,
                'created_at' => '2018-12-30',
                'updated_at' => '2018-12-30',
            ],
            [
                'name' => 'Urdu',
                'class_id' => 8,
                'tenant_id' => 1,
                'created_at' => '2018-12-30',
                'updated_at' => '2018-12-30',
            ],
            [
                'name' => 'Math',
                'class_id' => 8,
                'tenant_id' => 1,
                'created_at' => '2018-12-30',
                'updated_at' => '2018-12-30',
            ],
            [
                'name' => 'Science',
                'class_id' => 8,
                'tenant_id' => 1,
                'created_at' => '2018-12-30',
                'updated_at' => '2018-12-30',
            ],
            
//            Class 7
            [
                'name' => 'English',
                'class_id' => 9,
                'tenant_id' => 1,
                'created_at' => '2019-12-30',
                'updated_at' => '2019-12-30',
            ],
            [
                'name' => 'Urdu',
                'class_id' => 9,
                'tenant_id' => 1,
                'created_at' => '2019-12-30',
                'updated_at' => '2019-12-30',
            ],
            [
                'name' => 'Math',
                'class_id' => 9,
                'tenant_id' => 1,
                'created_at' => '2019-12-30',
                'updated_at' => '2019-12-30',
            ],
            [
                'name' => 'Science',
                'class_id' => 9,
                'tenant_id' => 1,
                'created_at' => '2019-12-30',
                'updated_at' => '2019-12-30',
            ],

//            8
            [
                'name' => 'Biology',
                'class_id' => 10,
                'tenant_id' => 1,
                'created_at' => '2017-10-30',
                'updated_at' => '2017-10-30',
            ],
            [
                'name' => 'Chemistry',
                'class_id' => 10,
                'tenant_id' => 1,
                'created_at' => '2017-10-30',
                'updated_at' => '2017-10-30',
            ], [
                'name' => 'Computer Science',
                'class_id' => 10,
                'tenant_id' => 1,
                'created_at' => '2017-10-30',
                'updated_at' => '2017-10-30',
            ],[
                'name' => 'English',
                'class_id' => 10,
                'tenant_id' => 1,
                'created_at' => '2017-10-30',
                'updated_at' => '2017-10-30',
            ],
            [
                'name' => 'Math',
                'class_id' => 10,
                'tenant_id' => 1,
                'created_at' => '2017-10-30',
                'updated_at' => '2017-10-30',
            ],
           [
               'name' => 'Physics',
               'class_id' => 10,
                'tenant_id' => 1,
                'created_at' => '2017-10-30',
                'updated_at' => '2017-10-30',
            ],
            [
                'name' => 'Social Studies',
                'class_id' => 10,
                'tenant_id' => 1,
                'created_at' => '2017-10-30',
                'updated_at' => '2017-10-30',
            ], [
                'name' => 'Urdu',
                'class_id' => 10,
                'tenant_id' => 1,
                'created_at' => '2017-10-30',
                'updated_at' => '2017-10-30',
            ],
//          9
            [
                'name' => 'Biology',
                'class_id' => 11,
                'tenant_id' => 1,
                'created_at' => '2017-11-30',
                'updated_at' => '2017-11-30',
            ],
            [
                'name' => 'Chemistry',
                'class_id' => 11,
                'tenant_id' => 1,
                'created_at' => '2017-11-30',
                'updated_at' => '2017-11-30',
            ], [
                'name' => 'Computer Science',
                'class_id' => 11,
                'tenant_id' => 1,
                'created_at' => '2017-11-30',
                'updated_at' => '2017-11-30',
            ],[
                'name' => 'English',
                'class_id' => 11,
                'tenant_id' => 1,
                'created_at' => '2017-11-30',
                'updated_at' => '2017-11-30',
            ],
            [
                'name' => 'Math',
                'class_id' => 11,
                'tenant_id' => 1,
                'created_at' => '2017-11-30',
                'updated_at' => '2017-11-30',
            ],
           [
               'name' => 'Physics',
               'class_id' => 11,
                'tenant_id' => 1,
                'created_at' => '2017-11-30',
                'updated_at' => '2017-11-30',
            ],
            [
                'name' => 'Social Studies',
                'class_id' => 11,
                'tenant_id' => 1,
                'created_at' => '2017-11-30',
                'updated_at' => '2017-11-30',
            ], [
                'name' => 'Urdu',
                'class_id' => 11,
                'tenant_id' => 1,
                'created_at' => '2017-11-30',
                'updated_at' => '2017-11-30',
            ],
//          10
            [
                'name' => 'Biology',
                'class_id' => 12,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Chemistry',
                'class_id' => 12,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ], [
                'name' => 'Computer Science',
                'class_id' => 12,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],[
                'name' => 'English',
                'class_id' => 12,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Math',
                'class_id' => 12,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
           [
               'name' => 'Physics',
               'class_id' => 12,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Social Studies',
                'class_id' => 12,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ], [
                'name' => 'Urdu',
                'class_id' => 12,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],


//            11
            [
                'name' => 'Biology',
                'class_id' => 13,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Chemistry',
                'class_id' => 13,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ], [
                'name' => 'Computer Science',
                'class_id' => 13,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],[
                'name' => 'English',
                'class_id' => 13,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Math',
                'class_id' => 13,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
           [
               'name' => 'Physics',
               'class_id' => 13,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Social Studies',
                'class_id' => 13,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ], [
                'name' => 'Urdu',
                'class_id' => 13,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ] ,
//            13
            [
                'name' => 'Biology',
                'class_id' => 14,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Chemistry',
                'class_id' => 14,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ], [
                'name' => 'Computer Science',
                'class_id' => 14,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],[
                'name' => 'English',
                'class_id' => 14,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Math',
                'class_id' => 14,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
           [
               'name' => 'Physics',
               'class_id' => 14,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Social Studies',
                'class_id' => 14,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ], [
                'name' => 'Urdu',
                'class_id' => 14,
                'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ]
        ];

        DB::table('subjects')->insert($data);
    }
}
