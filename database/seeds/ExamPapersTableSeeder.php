<?php

use Illuminate\Database\Seeder;

class ExamPapersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('exam_papers')->insert([
            'name' => 'Biology Paper',
            'detail' => 'Biology paper will include two parts:
            
            Part A:
            It will consist of 20 MCQs carrying 20 marks.
            Time to complete Part A will be 20 minutes.
            
            Part B: 
            Part B will consist of 12 SEQs out of which 8 must be answered.
            Each answer will carry 10 marks. Passing percentage is 75%.
            Time for Part B will be 2 hours and 40 minutes.
            
            
            If any diagrams are required, they must be drawn only by pencil only.
            
            More details by teacher here
            ',
            'type'=>'Written',
            'status' => 1,
            'score' => 100,
            'due_date' => \Carbon\Carbon::now()->addDays(4)->toDateTimeString(),
            'subject_id'=> 36,
            'assessment_exam_id' => 1,
            'tenant_id' => 1,
        ]);
        DB::table('exam_papers')->insert([
            'name' => 'Biology Viva',
            'detail' => 'Practical copies need to be completed and checked for Exam Viva which will consist of following parts:
            
            1. Copy Checking
            2. Performing asked practical and writing it down
            3. Answering Viva Questions
            
            More details by teacher here
            ',
            'type'=>'Viva',
            'status' => 1,
            'score' => 100,
            'due_date' => \Carbon\Carbon::now()->addDays(13)->toDateTimeString(),
            'subject_id'=> 36,
            'assessment_exam_id' => 1,
            'tenant_id' => 1,
        ]);

        DB::table('exam_papers')->insert([
            'name' => 'Chemistry Paper',
            'detail' => 'Chemistry paper will include two parts:
            
            Part A:
            It will consist of 20 MCQs carrying 20 marks.
            Time to complete Part A will be 20 minutes.
            
            Part B: 
            Part B will consist of 12 SEQs out of which 8 must be answered.
            Each answer will carry 10 marks. Passing percentage is 75%.
            Time for Part B will be 2 hours and 40 minutes.
            
            
            If any diagrams are required, they must be drawn only by pencil only.
            
            More details by teacher here
            ',
            'type'=>'Written',
            'status' => 1,
            'score' => 100,
            'due_date' => \Carbon\Carbon::now()->addDays(5)->toDateTimeString(),
            'subject_id'=> 37,
            'assessment_exam_id' => 1,
            'tenant_id' => 1,
        ]);
        DB::table('exam_papers')->insert([
            'name' => 'Chemistry Viva',
            'detail' => 'Practical copies need to be completed and checked for Exam Viva which will consist of following parts:
            
            1. Copy Checking
            2. Performing the asked practical and writing it down
            3. Answering Viva Questions
            
            More details by teacher here
            ',
            'type'=>'Viva',
            'status' => 1,
            'score' => 100,
            'due_date' => \Carbon\Carbon::now()->addDays(14)->toDateTimeString(),
            'subject_id'=> 37,
            'assessment_exam_id' => 1,
            'tenant_id' => 1,
        ]);

        DB::table('exam_papers')->insert([
            'name' => 'Physics Paper',
            'detail' => 'Physics paper will include two parts:
            
            Part A:
            It will consist of 20 MCQs carrying 20 marks.
            Time to complete Part A will be 20 minutes.
            
            Part B: 
            Part B will consist of 12 SEQs out of which 8 must be answered.
            Each answer will carry 10 marks. Passing percentage is 75%.
            Time for Part B will be 2 hours and 40 minutes.
            
            
            If any diagrams are required, they must be drawn only by pencil only.
            
            More details by teacher here
            ',
            'type'=>'Written',
            'status' => 1,
            'score' => 100,
            'due_date' => \Carbon\Carbon::now()->addDays(6)->toDateTimeString(),
            'subject_id'=> 41,
            'assessment_exam_id' => 1,
            'tenant_id' => 1,
        ]);
        DB::table('exam_papers')->insert([
            'name' => 'Physics Viva',
            'detail' => 'Practical copies need to be completed and checked for Exam Viva which will consist of following parts:
            
            1. Copy Checking
            2. Performing asked practical and writing it down
            3. Answering Viva Questions
            
            More details by teacher here
            ',
            'type'=>'Viva',
            'status' => 1,
            'score' => 100,
            'due_date' => \Carbon\Carbon::now()->addDays(15)->toDateTimeString(),
            'subject_id'=> 41,
            'assessment_exam_id' => 1,
            'tenant_id' => 1,
        ]);

        DB::table('exam_papers')->insert([
            'name' => 'Maths Paper',
            'detail' => 'Maths paper will include two parts:
            
            Part A:
            It will consist of 20 MCQs carrying 20 marks.
            Time to complete Part A will be 20 minutes.
            
            Part B: 
            Part B will consist of 12 SEQs out of which 8 must be answered.
            Each answer will carry 10 marks. Passing percentage is 75%.
            Time for Part B will be 2 hours and 40 minutes.
            
            
            Calculator is not allowed in exam hall. Rough work for each question needs to be done beside it
            
            More details by teacher here
            ',
            'type'=>'Written',
            'status' => 1,
            'score' => 100,
            'due_date' => \Carbon\Carbon::now()->addDays(7)->toDateTimeString(),
            'subject_id'=> 40,
            'assessment_exam_id' => 1,
            'tenant_id' => 1,
        ]);

        DB::table('exam_papers')->insert([
            'name' => 'English A',
            'detail' => 'English paper will include two parts:
            
            Part A:
            It will consist of 20 MCQs carrying 20 marks.
            Time to complete Part A will be 20 minutes.
            
            Part B: 
            Part B will consist of 12 SEQs out of which 8 must be answered.
            Each answer will carry 10 marks. Passing percentage is 75%.
            Time for Part B will be 2 hours and 40 minutes.
            
            
            More details by teacher here
            ',
            'type'=>'Written',
            'status' => 1,
            'score' => 100,
            'due_date' => \Carbon\Carbon::now()->addDays(8)->toDateTimeString(),
            'subject_id'=> 39,
            'assessment_exam_id' => 1,
            'tenant_id' => 1,
        ]);
        DB::table('exam_papers')->insert([
            'name' => 'English B',
            'detail' => 'English paper will include two parts:
            
            Part A:
            It will consist of 20 MCQs carrying 20 marks.
            Time to complete Part A will be 20 minutes.
            
            Part B: 
            Part B will consist of 12 SEQs out of which 8 must be answered.
            Each answer will carry 10 marks. Passing percentage is 75%.
            Time for Part B will be 2 hours and 40 minutes.
            
            
            More details by teacher here
            ',
            'type'=>'Written',
            'status' => 1,
            'score' => 100,
            'due_date' => \Carbon\Carbon::now()->addDays(8)->toDateTimeString(),
            'subject_id'=> 39,
            'assessment_exam_id' => 1,
            'tenant_id' => 1,
        ]);

        DB::table('exam_papers')->insert([
            'name' => 'Urdu A',
            'detail' => 'Urdu paper will include two parts:
            
            Part A:
            It will consist of 20 MCQs carrying 20 marks.
            Time to complete Part A will be 20 minutes.
            
            Part B: 
            Part B will consist of 12 SEQs out of which 8 must be answered.
            Each answer will carry 10 marks. Passing percentage is 75%.
            Time for Part B will be 2 hours and 40 minutes.
            
            
            More details by teacher here
            ',
            'type'=>'Written',
            'status' => 1,
            'score' => 100,
            'due_date' => \Carbon\Carbon::now()->addDays(9)->toDateTimeString(),
            'subject_id'=> 43,
            'assessment_exam_id' => 1,
            'tenant_id' => 1,
        ]);
        DB::table('exam_papers')->insert([
            'name' => 'Urdu B',
            'detail' => 'Urdu paper will include two parts:
            
            Part A:
            It will consist of 20 MCQs carrying 20 marks.
            Time to complete Part A will be 20 minutes.
            
            Part B: 
            Part B will consist of 12 SEQs out of which 8 must be answered.
            Each answer will carry 10 marks. Passing percentage is 75%.
            Time for Part B will be 2 hours and 40 minutes.
            
            
            More details by teacher here
            ',
            'type'=>'Written',
            'status' => 1,
            'score' => 100,
            'due_date' => \Carbon\Carbon::now()->addDays(9)->toDateTimeString(),
            'subject_id'=> 43,
            'assessment_exam_id' => 1,
            'tenant_id' => 1,
        ]);

    }
}
