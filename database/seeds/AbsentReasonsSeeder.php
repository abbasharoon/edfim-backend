<?php

use Illuminate\Database\Seeder;

class AbsentReasonsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'reason' => 'Fever',
                'tenant_id' => 1,
                'notify' => 0,
            ], [
                'reason' => 'Family Event',
                'tenant_id' => 1,
                'notify' => 0,
            ], [
                'reason' => 'Religious Event',
                'tenant_id' => 1,
                'notify' => 0,
            ], [
                'reason' => 'Not Given',
                'tenant_id' => 1,
                'notify' => 0,
            ],
        ];

        DB::table('absent_reasons')->insert($data);
    }
}
