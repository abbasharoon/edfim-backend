<?php

use Illuminate\Database\Seeder;

class AttendanceRegisterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Primary',
                'batch_id' => 1,
                'class_id' => 1,
                'tenant_id' => 1,
                'start_date' => '2018-01-01',
                'end_date' => '2018-12-30',

            ],            [
                'name' => 'Evening',
                'batch_id' => 1,
                'class_id' => 1,
                'tenant_id' => 1,
                'start_date' => '2018-06-01',
                'end_date' => '2018-12-30',

            ],            [
                'name' => 'Primary',
                'batch_id' => 3,
                'class_id' => 2,
                'tenant_id' => 1,
                'start_date' => '2018-01-01',
                'end_date' => '2018-12-30',

            ],
        ];

        DB::table('attendance_register')->insert($data);
    }
}
