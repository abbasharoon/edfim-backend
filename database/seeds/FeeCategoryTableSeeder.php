<?php

use Illuminate\Database\Seeder;

class FeeCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Tution Fee',
               'tenant_id' => 1,
            ], [
                'name' => 'Promotion',
               'tenant_id' => 1,

            ], [
                'name' => 'Exam Fee',
               'tenant_id' => 1,

            ], [
                'name' => 'Fine',
               'tenant_id' => 1,

            ],
        ];

        DB::table('fee_categories')->insert($data);
    }
}
