<?php

use Illuminate\Database\Seeder;

class ClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $classesData = [
            [
                'name' => 'Nursery',
                'tenant_id' => 1,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ], [
                'name' => 'Prep',
                'tenant_id' => 1,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ], [
                'name' => 'Class I',
                'tenant_id' => 1,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ], [
                'name' => 'Class II',
                'tenant_id' => 1,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ], [
                'name' => 'Class III',
                'tenant_id' => 1,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],[
                'name' => 'Class IV',
                'tenant_id' => 1,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],[
                'name' => 'Class V',
                'tenant_id' => 1,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],[
                'name' => 'Class VI',
                'tenant_id' => 1,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],[
                'name' => 'Class VII',
                'tenant_id' => 1,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],[
                'name' => 'Class VIII',
                'tenant_id' => 1,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],[
                'name' => 'Class IX',
                'tenant_id' => 1,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],[
                'name' => 'Class X',
                'tenant_id' => 1,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],[
                'name' => 'Class XI',
                'tenant_id' => 1,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],[
                'name' => 'Class XII',
                'tenant_id' => 1,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
        ];


        DB::table('classes')->insert($classesData);

        $data = [
            [
                'class_id' => 1,
                'user_id' => 1,
            ], [
                'class_id' => 1,
                'user_id' => 2,
            ], [
                'class_id' => 1,
                'user_id' => 4,
            ], [
                'class_id' => 1,
                'user_id' => 5,
            ],
        ];
        DB::table('class_user')->insert($data);
        DB::table('class_user')->insert([
            'class_id' => 1,
            'user_id' => 3,
            'class_teacher' => true,
        ]);
    }
}
