<?php
/**
 * @file    UsersTableSeeder.php
 *
 *
 * PHP Version 7
 *
 * @author  <Sandun Dissanayake> <java2012@gmail.com>
 *
 */

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $pre_configured_user_data = [
            'first_name' => 'System',
            'middle_name' => '',
            'last_name' => 'Admin',
            'username' => 'systemadmin',
            'gender' => rand(1, 3),
            'picture_url' => $faker->imageUrl(),
            'nic' => '860443880v',
            'email' => 'test@example.org',
            'password' => Hash::make('123456'),
            'tenant_id' => 1,
            'remember_token' => str_random(10),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ];
        DB::table('users')->insert($pre_configured_user_data);
        $pre_configured_user_data = [
            'first_name' => 'User',
            'middle_name' => '',
            'last_name' => 'Admin',
            'gender' => rand(1, 3),
            'picture_url' => $faker->imageUrl(),
            'nic' => '860443880v',
            'email' => 'user@example.org',
            'password' => Hash::make('123456'),
            'tenant_id' => 1,
            'remember_token' => str_random(10),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ];
        DB::table('users')->insert($pre_configured_user_data);
        $pre_configured_user_data = [
            'first_name' => 'Classes',
            'middle_name' => '',
            'last_name' => 'Admin',
            'gender' => rand(1, 3),
            'picture_url' => $faker->imageUrl(),
            'nic' => '860443880v',
            'email' => 'class@example.org',
            'password' => Hash::make('123456'),
            'tenant_id' => 1,
            'remember_token' => str_random(10),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ];
        DB::table('users')->insert($pre_configured_user_data);
        $pre_configured_user_data = [
            'first_name' => 'Class',
            'middle_name' => '',
            'last_name' => 'Teacher',
            'gender' => rand(1, 3),
            'picture_url' => $faker->imageUrl(), 'nic' => '860443880v',
            'email' => 'classteacher@example.org',
            'password' => Hash::make('123456'),
            'tenant_id' => 1,
            'remember_token' => str_random(10),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ];
        DB::table('users')->insert($pre_configured_user_data);
        $pre_configured_user_data = [
            'first_name' => 'Only',
            'middle_name' => '',
            'last_name' => 'Teacher',
            'gender' => rand(1, 3),
            'picture_url' => $faker->imageUrl(),
            'nic' => '860443880v',
            'email' => 'teacher@example.org',
            'password' => Hash::make('123456'),
            'tenant_id' => 1,
            'remember_token' => str_random(10),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ];
        DB::table('users')->insert($pre_configured_user_data);
        $pre_configured_user_data = [
            'first_name' => 'Attendance',
            'middle_name' => '',
            'last_name' => 'Admin',
            'gender' => rand(1, 3),
            'picture_url' => $faker->imageUrl(),
            'nic' => '860443880v',
            'email' => 'attendance@example.org',
            'password' => Hash::make('123456'),
            'tenant_id' => 1,
            'remember_token' => str_random(10),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ];
        DB::table('users')->insert($pre_configured_user_data);
        $pre_configured_user_data = [
            'first_name' => 'Assignment',
            'middle_name' => '',
            'last_name' => 'Admin',
            'gender' => rand(1, 3),
            'picture_url' => $faker->imageUrl(),
            'nic' => '860443880v',
            'email' => 'assignment@example.org',
            'password' => Hash::make('123456'),
            'tenant_id' => 1,
            'remember_token' => str_random(10),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ];
        DB::table('users')->insert($pre_configured_user_data);
        $pre_configured_user_data = [
            'first_name' => 'Assessment',
            'middle_name' => '',
            'last_name' => 'Admin',
            'gender' => rand(1, 3),
            'picture_url' => $faker->imageUrl(),
            'nic' => '860443880v',
            'email' => 'assessment@example.org',
            'password' => Hash::make('123456'),
            'tenant_id' => 1,
            'remember_token' => str_random(10),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ];
        DB::table('users')->insert($pre_configured_user_data);
        $pre_configured_user_data = [
            'first_name' => 'Reputation',
            'middle_name' => '',
            'last_name' => 'Admin',
            'gender' => rand(1, 3),
            'picture_url' => $faker->imageUrl(),
            'nic' => '860443880v',
            'email' => 'reputation@example.org',
            'password' => Hash::make('123456'),
            'tenant_id' => 1,
            'remember_token' => str_random(10),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ];
        DB::table('users')->insert($pre_configured_user_data);
        $pre_configured_user_data = [
            'first_name' => 'Finance',
            'middle_name' => '',
            'last_name' => 'Admin',
            'gender' => rand(1, 3),
            'picture_url' => $faker->imageUrl(),
            'nic' => '860443880v',
            'email' => 'finance@example.org',
            'password' => Hash::make('123456'),
            'tenant_id' => 1,
            'remember_token' => str_random(10),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ];
        DB::table('users')->insert($pre_configured_user_data);


        $faker = Faker::create();

        $pre_configured_user_data = [
            'first_name' => 'Ummer',
            'middle_name' => '',
            'last_name' => 'Sultan',
            'gender' => rand(1, 3),
            'picture_url' => $faker->imageUrl(),
            'username' => 'ummer.sultan',
            'nic' => $faker->numerify('#########v'),
            'email' => 'ummer.sultan@example.org',
            'password' => Hash::make('123456'),
            'phone_no' => $faker->phoneNumber,
            'tenant_id' => 1,
            'batch_id' => 1,
            'parent_id' => rand(5, 10),
            'remember_token' => str_random(10),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ];
        DB::table('users')->insert($pre_configured_user_data);

        $pre_configured_user_data = [
            'first_name' => 'Amir',
            'middle_name' => '',
            'last_name' => 'Ali',
            'gender' => rand(1, 3),
            'picture_url' => $faker->imageUrl(),
            'username' => 'amir.ali',
            'nic' => $faker->numerify('#########v'),
            'email' => 'amir.ali@example.org',
            'password' => Hash::make('123456'),
            'phone_no' => $faker->phoneNumber,
            'tenant_id' => 1,
            'batch_id' => 1,
            'parent_id' => rand(5, 10),
            'remember_token' => str_random(10),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ];
        DB::table('users')->insert($pre_configured_user_data);
        $pre_configured_user_data = [
            'first_name' => 'Talha',
            'middle_name' => '',
            'last_name' => 'Baig',
            'gender' => rand(1, 3),
            'picture_url' => $faker->imageUrl(),
            'username' => $faker->userName,
            'nic' => $faker->numerify('#########v'),
            'email' => 'amir2.ali@example.org',
            'password' => Hash::make('123456'),
            'phone_no' => $faker->phoneNumber,
            'tenant_id' => 1,
            'batch_id' => 1,
            'parent_id' => rand(5, 10),
            'remember_token' => str_random(10),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ];
        DB::table('users')->insert($pre_configured_user_data);

        $pre_configured_user_data = [
            'first_name' => 'Asad',
            'middle_name' => '',
            'last_name' => 'Khan',
            'gender' => 1,
            'picture_url' => $faker->imageUrl(),
            'username' => $faker->userName,
            'nic' => $faker->numerify('#########v'),
            'email' => 'asad.ali@example.org',
            'password' => Hash::make('123456'),
            'phone_no' => $faker->phoneNumber,
            'tenant_id' => 1,
            'batch_id' => 1,
            'parent_id' => rand(5, 10),
            'remember_token' => str_random(10),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ];
        DB::table('users')->insert($pre_configured_user_data);

        $pre_configured_user_data = [
            'first_name' => 'Akas',
            'middle_name' => '',
            'last_name' => 'Khan',
            'gender' => 1,
            'picture_url' => $faker->imageUrl(),
            'username' => $faker->userName,
            'nic' => $faker->numerify('#########v'),
            'email' => 'akas.khan@example.org',
            'password' => Hash::make('123456'),
            'phone_no' => $faker->phoneNumber,
            'tenant_id' => 1,
            'batch_id' => 1,
            'parent_id' => rand(5, 10),
            'remember_token' => str_random(10),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ];
        DB::table('users')->insert($pre_configured_user_data);

        $pre_configured_user_data = [
            'first_name' => 'Masroor',
            'middle_name' => '',
            'last_name' => 'Ahmed',
            'gender' => 1,
            'picture_url' => $faker->imageUrl(),
            'username' => $faker->userName,
            'nic' => $faker->numerify('#########v'),
            'email' => 'masroor.khan@example.org',
            'password' => Hash::make('123456'),
            'phone_no' => $faker->phoneNumber,
            'tenant_id' => 1,
            'batch_id' => 1,
            'parent_id' => rand(5, 10),
            'remember_token' => str_random(10),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ];
        DB::table('users')->insert($pre_configured_user_data);

        $pre_configured_user_data = [
            'first_name' => 'Saqlain',
            'middle_name' => '',
            'last_name' => 'Jehan',
            'gender' => 1,
            'picture_url' => $faker->imageUrl(),
            'username' => $faker->userName,
            'nic' => $faker->numerify('#########v'),
            'email' => 'saqlain.khan@example.org',
            'password' => Hash::make('123456'),
            'phone_no' => $faker->phoneNumber,
            'tenant_id' => 1,
            'batch_id' => 1,
            'parent_id' => rand(5, 10),
            'remember_token' => str_random(10),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ];
        DB::table('users')->insert($pre_configured_user_data);

        $pre_configured_user_data = [
            'first_name' => 'Faisal',
            'middle_name' => '',
            'last_name' => 'Ali',
            'gender' => 1,
            'picture_url' => $faker->imageUrl(),
            'username' => $faker->userName,
            'nic' => $faker->numerify('#########v'),
            'email' => 'faisal.khan@example.org',
            'password' => Hash::make('123456'),
            'phone_no' => $faker->phoneNumber,
            'tenant_id' => 1,
            'batch_id' => 1,
            'parent_id' => rand(5, 10),
            'remember_token' => str_random(10),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ];
        DB::table('users')->insert($pre_configured_user_data);

        $pre_configured_user_data = [
            'first_name' => 'Hilal',
            'middle_name' => '',
            'last_name' => 'Khan',
            'gender' => 1,
            'picture_url' => $faker->imageUrl(),
            'username' => $faker->userName,
            'nic' => $faker->numerify('#########v'),
            'email' => 'hilal.khan@example.org',
            'password' => Hash::make('123456'),
            'phone_no' => $faker->phoneNumber,
            'tenant_id' => 1,
            'batch_id' => 1,
            'parent_id' => rand(5, 10),
            'remember_token' => str_random(10),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ];
        DB::table('users')->insert($pre_configured_user_data);

        $pre_configured_user_data = [
            'first_name' => 'Hafeez',
            'middle_name' => '',
            'last_name' => 'Sardar',
            'gender' => 1,
            'picture_url' => $faker->imageUrl(),
            'username' => $faker->userName,
            'nic' => $faker->numerify('#########v'),
            'email' => 'hafeez.khan@example.org',
            'password' => Hash::make('123456'),
            'phone_no' => $faker->phoneNumber,
            'tenant_id' => 1,
            'batch_id' => 1,
            'parent_id' => rand(5, 10),
            'remember_token' => str_random(10),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ];
        DB::table('users')->insert($pre_configured_user_data);


        for ($i = 0; $i < 9; $i++) {

            $user_data = [
                'first_name' => $faker->firstName,
                'middle_name' => $faker->firstName,
                'last_name' => $faker->firstName,
                'gender' => rand(1, 3),
                'picture_url' => $faker->imageUrl(),
                'username' => $faker->userName,
                'nic' => $faker->numerify('#########v'),
                'email' => $faker->unique()->safeEmail,
                'password' => Hash::make('123456'),
                'phone_no' => $faker->phoneNumber,
                'tenant_id' => 1,
                'batch_id' => 1,
                'roll_no' => $faker->randomNumber(4),
                'parent_id' => rand(5, 21),
                'remember_token' => str_random(10),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ];
            DB::table('users')->insert($user_data);
        }
        $pre_configured_user_data = [
            'first_name' => 'Father',
            'middle_name' => '',
            'last_name' => 'Account',
            'gender' => rand(1, 3),
            'picture_url' => $faker->imageUrl(),
            'username' => $faker->userName,
            'nic' => $faker->numerify('#########v'),
            'email' => 'father@example.org',
            'password' => Hash::make('123456'),
            'phone_no' => $faker->phoneNumber,
            'tenant_id' => 1,
            'remember_token' => str_random(10),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ];
        DB::table('users')->insert($pre_configured_user_data);

        $data = [];
        for ($i = 1; $i < 30; $i++) {
            $data = [
                'user_id' => $i,
                'email' => false,
                'sms' => false,
                'web' => true,
                'app' => true,
            ];
            DB::table('notification_preferences')->insert($data);
        }


    }
}
