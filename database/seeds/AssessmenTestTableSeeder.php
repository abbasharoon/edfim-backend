<?php

use Illuminate\Database\Seeder;

class AssessmenTestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('assessment_tests')->insert([
            'name' => 'Monthly Maths Test',
            'detail' => 'Maths test will be taken in Unit 3 and 4. 
            Four questions will be given from each unit to solve.
            Use of calculator is allowed. Rough sheets will be provided
            Time for test will be 30 minutes.            
            Passing percentage is avoce 80% ',
            'batch_id' => 1,
            'type'=>'Written',
            'schedule'=>'3',
            'class_id' => 10,
            'subject_id' => 40,
            'status' => 1,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);
        DB::table('assessment_tests')->insert([
            'name' => 'Urdu Dictation',
            'detail' => 'Prepare Chapter 3 for dication. 
            20 words will be dictated, each required to be written grammatically correct
            Each word will carry 1 Mark
            15 our of 20 marks are required to pass the test
            Failed students will be fined and required to write the dictation 20 times as assignment',
            'batch_id' => 1,
            'type'=>'Written',
            'schedule'=>'1',
            'class_id' => 10,
            'subject_id' => 43,
            'status' => 2,
            'score' => 20,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(-4)->toDateTimeString(),
        ]);
        DB::table('assessment_tests')->insert([
            'name' => 'English Verbal Test',
            'detail' => 'Verbal Test will be taken in chapter 6 of English book. Students are required to prepare the following parts
            Chapter Reading with meaning of difficult words
            Questions/Answers
            Fill in the blanks
            
            Correct pronounciation of all words will carry extra marks',
            'batch_id' => 1,
            'type'=>'Written',
            'schedule'=>'2',
            'class_id' => 10,
            'subject_id' => 39,
            'status' => 4,
            'score' => 50,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(-4)->toDateTimeString(),
        ]);
        DB::table('assessment_tests')->insert([
            'name' => 'Biology Viva',
            'detail' => 'Viva will be taken in Chapter Kingdom Anamalia
            All students are instructed to memorize the classification of animal kingdom with atleast 3 species example for each class
            ',
            'batch_id' => 1,
            'type'=>'Written',
            'schedule'=>'3',
            'class_id' => 10,
            'subject_id' => 36,
            'status' => 2,
            'score' => 50,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(17)->toDateTimeString(),
        ]);

        DB::table('assessment_tests')->insert([
            'name' => 'Monthly Maths Test',
            'detail' => 'Maths test will be taken in Unit 3 and 4. 
            Four questions will be given from each unit to solve.
            Use of calculator is allowed. Rough sheets will be provided
            Time for test will be 30 minutes.            
            Passing percentage is avoce 80% ',
            'batch_id' => 1,
            'type'=>'Written',
            'schedule'=>'3',
            'class_id' => 10,
            'subject_id' => 40,
            'status' => 3,
            'score' => 300,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(6)->toDateTimeString(),
        ]);
        DB::table('assessment_tests')->insert([
            'name' => 'Urdu Dictation',
            'detail' => 'Prepare Chapter 3 for dication. 
            20 words will be dictated, each required to be written grammatically correct
            Each word will carry 1 Mark
            15 our of 20 marks are required to pass the test
            Failed students will be fined and required to write the dictation 20 times as assignment',
            'batch_id' => 1,
            'type'=>'Written',
            'schedule'=>'1',
            'class_id' => 10,
            'subject_id' => 43,
            'status' => 1,
            'score' => 40,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(6)->toDateTimeString(),
        ]);
        DB::table('assessment_tests')->insert([
            'name' => 'Biology Viva',
            'detail' => 'Viva will be taken in Chapter Kingdom Anamalia
            All students are instructed to memorize the classification of animal kingdom with atleast 3 species example for each class
            ',
            'batch_id' => 1,
            'type'=>'Written',
            'schedule'=>'3',
            'class_id' => 10,
            'subject_id' => 36,
            'status' => 2,
            'score' => 40,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(17)->toDateTimeString(),
        ]);
        DB::table('assessment_tests')->insert([
            'name' => 'English Verbal Test',
            'detail' => 'Verbal Test will be taken in chapter 6 of English book. Students are required to prepare the following parts
            Chapter Reading with meaning of difficult words
            Questions/Answers
            Fill in the blanks
            
            Correct pronounciation of all words will carry extra marks',
            'batch_id' => 1,
            'type'=>'Written',
            'schedule'=>'2',
            'class_id' => 10,
            'subject_id' => 39,
            'status' => 0,
            'score' => 10,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(15)->toDateTimeString(),
        ]);
        DB::table('assessment_tests')->insert([
            'name' => 'Biology Viva',
            'detail' => 'Viva will be taken in Chapter Kingdom Anamalia
            All students are instructed to memorize the classification of animal kingdom with atleast 3 species example for each class
            ',
            'batch_id' => 1,
            'type'=>'Written',
            'schedule'=>'3',
            'class_id' => 10,
            'subject_id' => 36,
            'status' => 4,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(-6)->toDateTimeString(),
        ]);       DB::table('assessment_tests')->insert([
            'name' => 'Monthly Maths Test',
            'detail' => 'Maths test will be taken in Unit 3 and 4. 
            Four questions will be given from each unit to solve.
            Use of calculator is allowed. Rough sheets will be provided
            Time for test will be 30 minutes.            
            Passing percentage is avoce 80% ',
            'batch_id' => 1,
            'type'=>'Written',
            'schedule'=>'3',
            'class_id' => 10,
            'subject_id' => 40,
            'status' => 3,
            'score' => 300,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(6)->toDateTimeString(),
        ]);
        DB::table('assessment_tests')->insert([
            'name' => 'Urdu Dictation',
            'detail' => 'Prepare Chapter 3 for dication. 
            20 words will be dictated, each required to be written grammatically correct
            Each word will carry 1 Mark
            15 our of 20 marks are required to pass the test
            Failed students will be fined and required to write the dictation 20 times as assignment',
            'batch_id' => 1,
            'type'=>'Written',
            'schedule'=>'1',
            'class_id' => 10,
            'subject_id' => 43,
            'status' => 1,
            'score' => 40,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(17)->toDateTimeString(),
        ]);
        DB::table('assessment_tests')->insert([
            'name' => 'Biology Viva',
            'detail' => 'Viva will be taken in Chapter Kingdom Anamalia
            All students are instructed to memorize the classification of animal kingdom with atleast 3 species example for each class
            ',
            'batch_id' => 1,
            'type'=>'Written',
            'schedule'=>'3',
            'class_id' => 10,
            'subject_id' => 36,
            'status' => 2,
            'score' => 40,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(15)->toDateTimeString(),
        ]);
        DB::table('assessment_tests')->insert([
            'name' => 'English Verbal Test',
            'detail' => 'Verbal Test will be taken in chapter 6 of English book. Students are required to prepare the following parts
            Chapter Reading with meaning of difficult words
            Questions/Answers
            Fill in the blanks
            
            Correct pronounciation of all words will carry extra marks',
            'batch_id' => 1,
            'type'=>'Written',
            'schedule'=>'2',
            'class_id' => 10,
            'subject_id' => 39,
            'status' => 0,
            'score' => 10,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(20)->toDateTimeString(),
        ]);
        DB::table('assessment_tests')->insert([
            'name' => 'Biology Viva',
            'detail' => 'Viva will be taken in Chapter Kingdom Anamalia
            All students are instructed to memorize the classification of animal kingdom with atleast 3 species example for each class
            ',
            'batch_id' => 1,
            'type'=>'Written',
            'schedule'=>'3',
            'class_id' => 10,
            'subject_id' => 36,
            'status' => 4,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(17)->toDateTimeString(),
        ]);


        DB::table('assessment_tests')->insert([
            'name' => 'Monthly Maths Test',
            'detail' => 'Maths test will be taken in Unit 3 and 4. 
            Four questions will be given from each unit to solve.
            Use of calculator is allowed. Rough sheets will be provided
            Time for test will be 30 minutes.            
            Passing percentage is avoce 80% ',
            'batch_id' => 1,
            'type'=>'Written',
            'schedule'=>'3',
            'class_id' => 10,
            'subject_id' => 40,
            'status' => 3,
            'score' => 200,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(9)->toDateTimeString(),
        ]);
        DB::table('assessment_tests')->insert([
            'name' => 'Urdu Dictation',
            'detail' => 'Prepare Chapter 3 for dication. 
            20 words will be dictated, each required to be written grammatically correct
            Each word will carry 1 Mark
            15 our of 20 marks are required to pass the test
            Failed students will be fined and required to write the dictation 20 times as assignment',
            'batch_id' => 1,
            'type'=>'Written',
            'schedule'=>'1',
            'class_id' => 10,
            'subject_id' => 43,
            'status' => 2,
            'score' => 70,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(9)->toDateTimeString(),
        ]);
        DB::table('assessment_tests')->insert([
            'name' => 'Biology Viva',
            'detail' => 'Viva will be taken in Chapter Kingdom Anamalia
            All students are instructed to memorize the classification of animal kingdom with atleast 3 species example for each class
            ',
            'batch_id' => 1,
            'type'=>'Written',
            'schedule'=>'3',
            'class_id' => 10,
            'subject_id' => 36,
            'status' => 2,
            'score' => 40,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(17)->toDateTimeString(),
        ]);
        DB::table('assessment_tests')->insert([
            'name' => 'English Verbal Test',
            'detail' => 'Verbal Test will be taken in chapter 6 of English book. Students are required to prepare the following parts
            Chapter Reading with meaning of difficult words
            Questions/Answers
            Fill in the blanks
            
            Correct pronounciation of all words will carry extra marks',
            'batch_id' => 1,
            'type'=>'Written',
            'schedule'=>'2',
            'class_id' => 10,
            'subject_id' => 39,
            'status' => 1,
            'score' => 20,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(-8)->toDateTimeString(),
        ]);
        DB::table('assessment_tests')->insert([
            'name' => 'Biology Viva',
            'detail' => 'Viva will be taken in Chapter Kingdom Anamalia
            All students are instructed to memorize the classification of animal kingdom with atleast 3 species example for each class
            ',
            'batch_id' => 1,
            'type'=>'Written',
            'schedule'=>'3',
            'class_id' => 10,
            'subject_id' => 36,
            'status' => 3,
            'score' => 30,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(-3)->toDateTimeString(),
        ]);

        DB::table('assessment_tests')->insert([
            'name' => 'Monthly Maths Test',
            'detail' => 'Maths test will be taken in Unit 3 and 4. 
            Four questions will be given from each unit to solve.
            Use of calculator is allowed. Rough sheets will be provided
            Time for test will be 30 minutes.            
            Passing percentage is avoce 80% ',
            'batch_id' => 1,
            'type'=>'Written',
            'schedule'=>'3',
            'class_id' => 10,
            'subject_id' => 40,
            'status' => 2,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(20)->toDateTimeString(),
        ]);
        DB::table('assessment_tests')->insert([
            'name' => 'Urdu Dictation',
            'detail' => 'Prepare Chapter 3 for dication. 
            20 words will be dictated, each required to be written grammatically correct
            Each word will carry 1 Mark
            15 our of 20 marks are required to pass the test
            Failed students will be fined and required to write the dictation 20 times as assignment',
            'batch_id' => 1,
            'type'=>'Written',
            'schedule'=>'1',
            'class_id' => 10,
            'subject_id' => 43,
            'status' => 0,
            'score' => 30,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(17)->toDateTimeString(),
        ]);
        DB::table('assessment_tests')->insert([
            'name' => 'English Verbal Test',
            'detail' => 'Verbal Test will be taken in chapter 6 of English book. Students are required to prepare the following parts
            Chapter Reading with meaning of difficult words
            Questions/Answers
            Fill in the blanks
            
            Correct pronounciation of all words will carry extra marks',
            'batch_id' => 1,
            'type'=>'Written',
            'schedule'=>'2',
            'class_id' => 10,
            'subject_id' => 39,
            'status' => 1,
            'score' => 10,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(20)->toDateTimeString(),
        ]);
        DB::table('assessment_tests')->insert([
            'name' => 'Biology Viva',
            'detail' => 'Viva will be taken in Chapter Kingdom Anamalia
            All students are instructed to memorize the classification of animal kingdom with atleast 3 species example for each class
            ',
            'batch_id' => 1,
            'type'=>'Written',
            'schedule'=>'3',
            'class_id' => 10,
            'subject_id' => 36,
            'status' => 2,
            'score' => 40,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(17)->toDateTimeString(),
        ]);



    }
}
