<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Model::unguard();

        $this->call('TenantHostNamesTableSeeder');
        $this->call('UsersTableSeeder');
        $this->call('UsersDataTableSeeder');
        $this->call('SettingTableSeeder');
        $this->call('BouncerSeeder');
        $this->call('AssignedRoleTableSeeder');
        $this->call('AssessmenTestTableSeeder');
        $this->call('AssessmentExamsTableSeeder');
        $this->call('ExamPapersTableSeeder');
        $this->call('ClassesTableSeeder');
        $this->call('BatchesTableSeeder');
        $this->call('SubjectsTableSeeder');
        $this->call('TimetableTableSeeder');
        $this->call('AssignmentTableSeeder');
//        $this->call('TaxesSeeder');
        $this->call('FeeTableSeeder');
        $this->call('FeeCategoryTableSeeder');
        $this->call('FeeCategoryTableSeeder');
        $this->call('ClassFeeTableSeeder');
        $this->call('UserFeeTableSeeder');
        $this->call('AttendanceRegisterSeeder');
        $this->call('AbsentReasonsSeeder');
        $this->call('ReputationBadgesSeeder');
        $this->call('ClassroomTableSeeder');
        $this->call('ApiServicesSeeder');

        Model::reguard();
    }
}
