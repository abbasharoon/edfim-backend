<?php

use Illuminate\Database\Seeder;

class AssignedRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 2,
                'role_id' => 2,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 2,
                'role_id' => 3,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 2,
                'role_id' => 4,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 2,
                'role_id' => 5,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 2,
                'role_id' => 6,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 2,
                'role_id' => 7,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 2,
                'role_id' => 1,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 3,
                'role_id' => 4,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 4,
                'role_id' => 5,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 5,
                'role_id' => 6,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 6,
                'role_id' => 7,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 7,
                'role_id' => 8,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 8,
                'role_id' => 9,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 9,
                'role_id' => 10,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 10,
                'role_id' => 11,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 11,
                'role_id' => 12,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 12,
                'role_id' => 12,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 13,
                'role_id' => 12,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 14,
                'role_id' => 12,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 15,
                'role_id' => 12,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 16,
                'role_id' => 12,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 17,
                'role_id' => 12,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 18,
                'role_id' => 12,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 19,
                'role_id' => 12,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 20,
                'role_id' => 12,
            ], [
                'entity_type' => 'App\User', 'scope' => 1, 'entity_id' => 21,
                'role_id' => 13,
            ]
        ];
        DB::table('assigned_roles')->insert($data);
    }
}
