<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;


class AssignmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();

        DB::table('assignments')->insert([
            'name' => 'Vaccations Homework',
            'detail' => 'Each student is required to perform following tasks daily during vaccations:
            
            - Solve questions of one unit and write there answers neatly totalling 30 units
            - Wite one table of multiplication daily starting from 2 to 12 and then repeating them
            
            
            Note to Parents: Please ensure that your child completes his daily homework and not to rush it in the end.',
            'batch_id' => 1,
            'upload' => 5,
            'class_id' => 10,
            'subject_id' => 40,
            'status' => 1,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Summer Tasks',
            'detail' => 'Summer tasks were givent to each students group and are required to be completed according to the instructions.
            
            Summer task marks will add towards the Annual Exam marks of paprt',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 36,
            'status' => 3,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(30)->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Winter Tasks',
            'detail' => 'Winter tasks were givent to each students group and are required to be completed according to the instructions.
            
            Summer task marks will add towards the Annual Exam marks of paprt',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 36,
            'status' => 3,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(4)->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Weekend Homework',
            'detail' => 'Each student is required to perform following tasks daily during the weekend:
            
            - Solve questions of one unit and write there answers neatly totalling 30 units
            - Wite one table of multiplication daily starting from 2 to 12 and then repeating them',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 41,
            'status' => 1,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Social Studies Chapter Revision',
            'detail' => 'Chapter 3 with 4 questions should be memorized and then written 3 times. ' .
                'Written notes will be checked on due date',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 42,
            'status' => 2,
            'score' => 0,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(6)->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Build "Hello World!" Program',
            'detail' => 'Create an html page with "Hello World" between heading tags, colored orange and centered.
            Copy your code to your notes after successfull completion.',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 38,
            'status' => 1,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(30)->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Winter Tasks',
            'detail' => 'Winter tasks were givent to each students group and are required to be completed according to the instructions.
            
            Summer task marks will add towards the Annual Exam marks of paprt',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 36,
            'status' => 2,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(10)->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Summer Tasks',
            'detail' => 'Summer tasks were givent to each students group and are required to be completed according to the instructions.
            
            Summer task marks will add towards the Annual Exam marks of paprt',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 36,
            'status' => 3,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(10)->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Vaccations Homework',
            'detail' => 'Each student is required to perform following tasks daily during vaccations:
            
            - Solve questions of one unit and write there answers neatly totalling 30 units
            - Wite one table of multiplication daily starting from 2 to 12 and then repeating them
            
            
            Note to Parents: Please ensure that your child completes his daily homework and not to rush it in the end.',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 40,
            'status' => 1,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Weekend Homework',
            'detail' => 'Each student is required to perform following tasks daily during the weekend:
            
            - Solve questions of one unit and write there answers neatly totalling 30 units
            - Wite one table of multiplication daily starting from 2 to 12 and then repeating them',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 41,
            'status' => 1,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Social Studies Chapter Revision',
            'detail' => 'Chapter 3 with 4 questions should be memorized and then written 3 times. ' .
                'Written notes will be checked on due date',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 42,
            'status' => 3,
            'score' => 0,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(6)->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Copy Completion',
            'detail' => 'Copy today\'s classwork to your neat books. 
            Pay attention to grammer ans spellings. 
            Use only blue/black pens. 
            Incomplete copies will result in fines.',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 39,
            'status' => 0,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(19)->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Social Studies Chapter Revision',
            'detail' => 'Chapter 3 with 4 questions should be memorized and then written 3 times. ' .
                'Written notes will be checked on due date',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 42,
            'status' => 1,
            'score' => 0,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(6)->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Build "Hello World!" Program',
            'detail' => 'Create an html page with "Hello World" between heading tags, colored orange and centered.
            Copy your code to your notes after successfull completion.',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 38,
            'status' => 2,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(4)->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Weekend Homework',
            'detail' => 'Each student is required to perform following tasks daily during the weekend:
            
            - Solve questions of one unit and write there answers neatly totalling 30 units
            - Wite one table of multiplication daily starting from 2 to 12 and then repeating them',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 41,
            'status' => 1,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Vaccations Homework',
            'detail' => 'Each student is required to perform following tasks daily during vaccations:
            
            - Solve questions of one unit and write there answers neatly totalling 30 units
            - Wite one table of multiplication daily starting from 2 to 12 and then repeating them
            
            
            Note to Parents: Please ensure that your child completes his daily homework and not to rush it in the end.',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 40,
            'status' => 1,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Build "Hello World!" Program',
            'detail' => 'Create an html page with "Hello World" between heading tags, colored orange and centered.
            Copy your code to your notes after successfull completion.',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 38,
            'status' => 2,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(4)->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Physics Assignment',
            'detail' => 'Physics assignment must be submitted by the due data. Assignment consists of:
             
             Drawing a neat and clean diagram of Screw Gauge
             Labeling the diagram
             Describing parts of Screw Gauge on page opposite to the diagram
             Writing notes of measurement experements performed today',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 41,
            'status' => 2,
            'score' => 0,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(6)->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Physics Assignment',
            'detail' => 'Physics assignment must be submitted by the due data. Assignment consists of:
             
             Drawing a neat and clean diagram of Screw Gauge
             Labeling the diagram
             Describing parts of Screw Gauge on page opposite to the diagram
             Writing notes of measurement experements performed today',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 41,
            'status' => 3,
            'score' => 0,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(6)->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Copy Completion',
            'detail' => 'Copy today\'s classwork to your neat books. 
            Pay attention to grammer ans spellings. 
            Use only blue/black pens. 
            Incomplete copies will result in fines.',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 39,
            'status' => 0,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(13)->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Physics Assignment',
            'detail' => 'Physics assignment must be submitted by the due data. Assignment consists of:
             
             Drawing a neat and clean diagram of Screw Gauge
             Labeling the diagram
             Describing parts of Screw Gauge on page opposite to the diagram
             Writing notes of measurement experements performed today',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 41,
            'status' => 3,
            'score' => 0,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(6)->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Copy Completion',
            'detail' => 'Copy today\'s classwork to your neat books. 
            Pay attention to grammer ans spellings. 
            Use only blue/black pens. 
            Incomplete copies will result in fines.',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 39,
            'status' => 2,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(10)->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Physics Assignment',
            'detail' => 'Physics assignment must be submitted by the due data. Assignment consists of:
             
             Drawing a neat and clean diagram of Screw Gauge
             Labeling the diagram
             Describing parts of Screw Gauge on page opposite to the diagram
             Writing notes of measurement experements performed today',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 41,
            'status' => 1,
            'score' => 0,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(6)->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Social Studies Chapter Revision',
            'detail' => 'Chapter 3 with 4 questions should be memorized and then written 3 times. ' .
                'Written notes will be checked on due date',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 42,
            'status' => 1,
            'score' => 0,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(6)->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Summer Tasks',
            'detail' => 'Summer tasks were givent to each students group and are required to be completed according to the instructions.
            
            Summer task marks will add towards the Annual Exam marks of paprt',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 36,
            'status' => 3,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(19)->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Winter Tasks',
            'detail' => 'Winter tasks were givent to each students group and are required to be completed according to the instructions.
            
            Summer task marks will add towards the Annual Exam marks of paprt',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 36,
            'status' => 0,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(19)->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Copy Completion',
            'detail' => 'Copy today\'s classwork to your neat books. 
            Pay attention to grammer ans spellings. 
            Use only blue/black pens. 
            Incomplete copies will result in fines.',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 39,
            'status' => 0,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(30)->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Winter Tasks',
            'detail' => 'Winter tasks were givent to each students group and are required to be completed according to the instructions.
            
            Summer task marks will add towards the Annual Exam marks of paprt',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 36,
            'status' => 2,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(10)->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Build "Hello World!" Program',
            'detail' => 'Create an html page with "Hello World" between heading tags, colored orange and centered.
            Copy your code to your notes after successfull completion.',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 38,
            'status' => 1,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->addDays(13)->toDateTimeString()
        ]);
        DB::table('assignments')->insert([
            'name' => 'Weekend Homework',
            'detail' => 'Each student is required to perform following tasks daily during the weekend:
            
            - Solve questions of one unit and write there answers neatly totalling 30 units
            - Wite one table of multiplication daily starting from 2 to 12 and then repeating them',
            'batch_id' => 1,
            'class_id' => 10,
            'subject_id' => 41,
            'status' => 1,
            'score' => 100,
            'tenant_id' => 1,
            'due_date' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
    }
}
