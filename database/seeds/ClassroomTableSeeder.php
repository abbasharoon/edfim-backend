<?php

use Illuminate\Database\Seeder;

class ClassroomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'New Classroom',
                'class_id' => 1,
                'batch_id' => 1,
                'settings' => null,
                'status' => 1,
                'created_at' => '2020-07-14 17:32:48',
                'updated_at' => '2020-07-14 17:32:48',
                'tenant_id' => 1,
            ]
        ];
        DB::table('classrooms')->insert($data);
    }
}
