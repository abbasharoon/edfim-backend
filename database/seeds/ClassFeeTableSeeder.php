<?php

use Illuminate\Database\Seeder;

class ClassFeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'fee_id' => 3,
                'class_id' => 1,
                'tenant_id' => 1,
                'target_fee_id' => 2,

            ], [
                'fee_id' => 1,
                'class_id' => 1,
                'tenant_id' => 1,
                'target_fee_id' => 3,

            ], [
                'fee_id' => 4,
                'class_id' => 1,
                'tenant_id' => 1,
                'target_fee_id' => null,

            ], [
                'fee_id' => 1,
                'class_id' => 7,
                'tenant_id' => 1,
                'target_fee_id' => null,
            ],
        ];

        DB::table('class_fee')->insert($data);
    }
}
