<?php

use Illuminate\Database\Seeder;

class ApiServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'service_id' => '1',
                'service_user_id' => '1',
                'user_id' => '1',
                'service_username' => 'edfimbooks@edfim.com',
            ],
        ];

        DB::table('api_services')->insert($data);
    }
}
