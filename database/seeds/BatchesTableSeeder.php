<?php

use Illuminate\Database\Seeder;

class BatchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()

    {
        $data = [
            [
                'name' => 'Batch 2012',
                'class_id' => 1,
               'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Batch 2013',
                'class_id' => 1,
               'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Batch 2014',
                'class_id' => 1,
               'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Batch 2015',
                'class_id' => 2,
               'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ],
            [
                'name' => 'Batch 2014',
                'class_id' => 2,
               'tenant_id' => 1,
                'created_at' => '2017-12-30',
                'updated_at' => '2017-12-30',
            ]
        ];

        DB::table('batches')->insert($data);
    }
}
