<?php

use Illuminate\Database\Seeder;

class BouncerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bouncer::scope()->to(1);
        Bouncer::allow('user-admin')->to([
            'manage-all-staff',
            'view-all-staff',
            'manage-all-student',
            'view-all-student',
            'manage-all-parent',
            'view-all-parent',
            'view-all-role',
            'change-all-student-batch',
        ]);
        Bouncer::allow('student-admin')->to([
            'manage-all-student',
            'view-all-student',
            'manage-all-parent',
            'view-all-parent',
            'change-all-student-batch']);
        Bouncer::allow('staff-admin')->to([
            'manage-all-staff',
            'view-all-staff',
            'view-all-role']);
        Bouncer::allow('class-admin')->to([
            'manage-all-class',
            'view-all-class',
            'manage-all-batch',
            'view-all-batch',
            'manage-all-subject',
            'view-all-subject',
            'change-all-batch-class']);
        Bouncer::allow('class-teacher')->to([
            'view-timetable',
            'view-class',
            'view-batch',
            'view-subject',
            'view-student',
            'view-attendance', 'manage-attendance',
            'view-assignment', 'manage-assignment',
            'view-test', 'manage-test',
            'view-exam', 'manage-exam',
            'view-paper', 'manage-paper',
            'view-reputation', 'manage-reputation',
            'view-reputation-badge',
            'view-fine',
            'manage-fine',
        ]);
        Bouncer::allow('teacher')->to([
            'view-timetable',
            'view-class',
            'view-batch',
            'view-subject',
            'view-student',
            'view-attendance',
            'view-assignment', 'manage-subject-assignment',
            'view-test', 'manage-subject-test',
            'view-paper', 'manage-subject-paper',
            'view-exam',
            'view-reputation', 'manage-reputation',
            'view-reputation-badge',
            'view-fine',
            'manage-fine',
        ]);
        Bouncer::allow('attendance-admin')->to([
            'manage-all-timetable',
            'view-all-timetable',
            'manage-all-attendance',
            'view-all-attendance',
            'view-all-class',
            'view-all-batch',
            'view-all-student',
        ]);
        Bouncer::allow('assignment-admin')->to(['manage-all-assignment',
            'view-all-timetable',
            'view-all-assignment',
            'view-all-class',
            'view-all-batch',
            'view-all-subject',
            'view-all-student',
        ]);
        Bouncer::allow('assessment-admin')->to([
            'view-all-timetable',
            'manage-all-test',
            'view-all-test',
            'manage-all-exam',
            'view-all-paper', 'add-all-paper',
            'view-all-exam',
            'view-class',
            'view-batch',
            'view-subject',
            'view-all-student',
        ]);
        Bouncer::allow('reputation-admin')->to([
            'manage-all-reputation-badge',
            'view-reputation-badge'
        ]);
        Bouncer::allow('finance-admin')->to([
            'manage-all-fee',
            'view-all-fee',
            'manage-all-class-invoice',
            'view-all-class-invoice',
            'manage-all-student-invoice',
            'view-all-student-invoice',
            'view-all-class',
            'view-all-batch',
            'view-all-student']);
        Bouncer::allow('student')->to([]);
        Bouncer::allow('parent')->to([]);
        for ($i = 11; $i < 30; $i++) {
            \App\User::find($i)->assign('student');
        }
        Bouncer::allow('system-admin')->everything();
        Bouncer::assign('system-admin')->to(\App\User::find(1));
    }
}
