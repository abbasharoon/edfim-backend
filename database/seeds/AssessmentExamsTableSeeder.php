<?php

use Illuminate\Database\Seeder;

class AssessmentExamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('assessment_exams')->insert([
            'name' => 'Detention Exam',
            'detail' => 'Detention exam will be taken from 25th Nov to 2nd December. 
            Any Student who fails in detention exam will not be allowed to set in board exams.
            
            
            
            More details by teacher here
            ',
            'batch_id' => 1,
            'schedule'=>6,
            'class_id' => 10,
            'status' => 1,
            'tenant_id' => 1,
        ]);
        DB::table('assessment_exams')->insert([
            'name' => '1st Term Exam',
            'detail' => 'First term exam will be taken from 25th Nov to 2nd December.
            
            
            
            More details by teacher here
            ',
            'batch_id' => 1,
            'schedule'=>2,
            'class_id' => 10,
            'status' => 2,
            'tenant_id' => 1,
        ]);
        DB::table('assessment_exams')->insert([
            'name' => '2nd Term Exam',
            'detail' => '2nd term exam will be taken from 20th March to 28th Match.
            
            
            
            More details by teacher here
            ',
            'batch_id' => 1,
            'schedule'=>3,
            'class_id' => 10,
            'status' => 3,
            'tenant_id' => 1,
        ]);
        DB::table('assessment_exams')->insert([
            'name' => 'Monthly Exam',
            'detail' => 'Monthly exam will for month of August will start next week.
            
            
            
            More details by teacher here
            ',
            'batch_id' => 1,
            'schedule'=>2,
            'class_id' => 10,
            'status' => 0,
            'tenant_id' => 1,
        ]);
        DB::table('assessment_exams')->insert([
            'name' => 'Monthly Exam',
            'detail' => 'Monthly exam will be taken from 25th Oct to 2nd November.
            
            
            
            More details by teacher here
            ',
            'batch_id' => 1,
            'schedule'=>3,
            'class_id' => 10,
            'status' => 4,
            'tenant_id' => 1,
        ]);
        DB::table('assessment_exams')->insert([
            'name' => 'Detention Exam',
            'detail' => 'Detention exam will be taken from 25th Nov to 2nd December.
            
            
            
            More details by teacher here
            ',
            'batch_id' => 1,
            'schedule'=>3,
            'class_id' => 11,
            'status' => 1,
            'tenant_id' => 1,
        ]);
        DB::table('assessment_exams')->insert([
            'name' => 'First Term Exam',
            'detail' => 'First term exam will be taken from 25th Nov to 2nd December.
            
            
            
            More details by teacher here
            ',
            'batch_id' => 1,
            'schedule'=>2,
            'class_id' => 10,
            'status' => 3,
            'tenant_id' => 1,
        ]);
        DB::table('assessment_exams')->insert([
            'name' => 'Third Term Exams',
            'detail' => 'First term exam will be taken from 25th Nov to 2nd December.
            
            
            
            More details by teacher here
            
            ',
            'batch_id' => 1,
            'schedule'=>3,
            'class_id' => 10,
            'status' => 2,
            'tenant_id' => 1,
        ]);
    }
}
