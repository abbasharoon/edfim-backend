<?php

use Illuminate\Database\Seeder;

class UserFeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'fee_id' => 3,
                'user_id' => 11,
               'tenant_id' => 1,
                'target_fee_id' => 2,

            ], [
                'fee_id' => 2,
                'user_id' => 11,
               'tenant_id' => 1,
                'target_fee_id' => null,

            ], [
                'fee_id' => 1,
                'user_id' => 11,
               'tenant_id' => 1,
                'target_fee_id' => 3,

            ], [
                'fee_id' => 1,
                'user_id' => 11,
               'tenant_id' => 1,
                'target_fee_id' => null,
            ],
        ];

        DB::table('user_fee')->insert($data);
    }
}
