<?php

use Illuminate\Database\Seeder;

class FeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Class 1 Tuition Fee',
                'detail' => '',
                'amount' => 400,
                'schedule' => 3,
                'type' => 1,
                'fee_category_id' => 1,
               'tenant_id' => 1,
            ], [
                'name' => 'Class 2 Tuition Fee',
                'detail' => '',
                'amount' => 300,
                'schedule' => 3,
                'type' => 1,
                'fee_category_id' => 1,
               'tenant_id' => 1,
            ], [
                'name' => 'Promotion Fee',
                'detail' => '',
                'amount' => 200,
                'schedule' => 3,
                'type' => 1,
                'fee_category_id' => 2,
               'tenant_id' => 1,
            ], [
                'name' => 'Fine',
                'detail' => '',
                'amount' => 100,
                'schedule' => 3,
                'type' => 1,
                'fee_category_id' => 4,
               'tenant_id' => 1,
            ], [
                'name' => 'Exam Fee',
                'detail' => '',
                'amount' => 100,
                'schedule' => 3,
                'type' => 1,
                'fee_category_id' => 3,
               'tenant_id' => 1,
            ]
        ];

        DB::table('fees')->insert($data);
    }
}
