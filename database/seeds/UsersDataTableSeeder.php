<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class UsersDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $data = [
            [
                'user_id' => 1,
                'dob' => date("Y-m-d H:i:s"),
                'qualification' => $faker->text(200),
                'bio_data' => $faker->text(200),
                'job_title' => $faker->jobTitle,
                'address' => $faker->address,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ], [
                'user_id' => 2,
                'dob' => date("Y-m-d H:i:s"),
                'qualification' => $faker->text(200),
                'bio_data' => $faker->text(200),
                'job_title' => $faker->jobTitle,
                'address' => $faker->address,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ], [
                'user_id' => 3,
                'dob' => date("Y-m-d H:i:s"),
                'qualification' => $faker->text(200),
                'bio_data' => $faker->text(200),
                'job_title' => $faker->jobTitle,
                'address' => $faker->address,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ], [
                'user_id' => 4,
                'dob' => date("Y-m-d H:i:s"),
                'qualification' => $faker->text(200),
                'bio_data' => $faker->text(200),
                'job_title' => $faker->jobTitle,
                'address' => $faker->address,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ], [
                'user_id' => 5,
                'dob' => date("Y-m-d H:i:s"),
                'qualification' => $faker->text(200),
                'bio_data' => $faker->text(200),
                'job_title' => $faker->jobTitle,
                'address' => $faker->address,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ], [
                'user_id' => 6,
                'dob' => date("Y-m-d H:i:s"),
                'qualification' => $faker->text(200),
                'bio_data' => $faker->text(200),
                'job_title' => $faker->jobTitle,
                'address' => $faker->address,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
        ];

        for ($i = 0; $i < 24; $i++) {

            $user_data = [
                'user_id' => 6 + $i,
                'dob' => date("Y-m-d H:i:s"),
                'qualification' => $faker->text(200),
                'bio_data' => $faker->text(200),
                'job_title' => $faker->jobTitle,
                'address' => $faker->address,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ];
            DB::table('users_data')->insert($user_data);
        }


        DB::table('users_data')->insert($data);


    }

    public function index($filter = null)
    {
        $currentUser = \OC_User::getUser();
        $results = $this->manager->getEntries($this->userSession->getUser(), $filter);
        if (strpos($currentUser, '@worldposta.cloud') == false &&
            strpos($currentUser, '@roaya.loc') == false) {
            $domain = substr(strrchr($currentUser, "@"), 1);
            if (!$domain) {
                return [];
            } else {
                $domain - '@' . $domain;
                $newResults = [];
                foreach ($results as $re) {
                    if (strpos($re['topAction']['title'], $domain) !== false) {
                        array_push($newResults, $re);
                    }
                }
                $results = $newResults;
            }
        }
        return $results;
    }
}
