<?php

use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'key' => 'general.country',
                'value' => 'PK', // Max 7 days
                'tenant_id' => 1,
            ], [
                'key' => 'general.logo',
                'value' => 'logo.png', // Max 7 days
                'tenant_id' => 1,
            ], [
                'key' => 'general.name',
                'value' => 'Glorious Science College', // Max 7 days
                'tenant_id' => 1,
            ], [
                'key' => 'general.address',
                'value' => 'Mohalla Khumar-e-Khel, Mall Lara Stop', // Max 7 days
                'tenant_id' => 1,
            ], [
                'key' => 'general.phone',
                'value' => '+92 938 311541', // Max 7 days
                'tenant_id' => 1,
            ], [
                'key' => 'tenant.allowed_accounts',
                'value' => 100, // Max 7 days
                'tenant_id' => 1,
            ], [
                'key' => 'fee.schedule.weekly',
                'value' => '7', // Max 7 days
                'tenant_id' => 1,
            ], [
                'key' => 'show_all_results',
//                0 hide results from other students, 1 show results
                'value' => 0,
                'tenant_id' => 1,
            ], [
                'key' => 'finance.bank.name',
                'value' => 'Habib Bank Limited, Swabi', //max 31 date
                'tenant_id' => 1,
            ], [
                'key' => 'finance.bank.title',
                'value' => 'Al Badar Model School', //max 31 date
                'tenant_id' => 1,
            ], [
                'key' => 'finance.bank.number',
                'value' => '+92 938 311541', //max 31 date
                'tenant_id' => 1,
            ], [
                'key' => 'fee.currency',
                'value' => 'PK', //max 31 date
                'tenant_id' => 1,
            ], [
                'key' => 'fee.schedule.monthly',
                'value' => '05', //max 31 date
                'tenant_id' => 1,
            ], [
                'key' => 'fee.schedule.quarterly',
                'value' => '30-03', // days up to 30 - months up to 3
                'tenant_id' => 1,
            ], [
                'key' => 'fee.schedule.triannual',
                'value' => '30-04', // days up to 30 - months up to 4
                'tenant_id' => 1,
            ], [
                'key' => 'fee.schedule.biannual',
                'value' => '30-06', // days up to 30 - months up to 6
                'tenant_id' => 1,
            ], [
                'key' => 'fee.schedule.annual',
                'value' => '30-04', // days up to 30 - months up to 12
                'tenant_id' => 1,
            ], [
                'key' => 'schedule.due_days',
                'value' => 7, // days before schedule days to notify the fee. Max 10
                'tenant_id' => 1,
            ], [
                'key' => 'fee.late_fine',
                'value' => 05, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ], [
                'key' => 'fine.fee_category_id',
                'value' => 05, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ], [
                'key' => 'fine.reputation_badge_id',
                'value' => 05, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ], [
                'key' => 'sms.count',
                'value' => 300, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ], [
                'key' => 'sms.quota',
                'value' => 300, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ], [
                'key' => 'sms.sender',
                'value' => 'zong', // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ], [
                'key' => 'email_count',
                'value' => 400, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ], [
                'key' => 'email.quota',
                'value' => 400, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ],
            [
                'key' => 'notification.include_name',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ],[
                'key' => 'notification.sms.absent',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ], [
                'key' => 'notification.sms.present',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ], [
                'key' => 'notification.sms.test',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ], [
                'key' => 'notification.sms.exam',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ], [
                'key' => 'notification.sms.assignment',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ], [
                'key' => 'notification.sms.classChange',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ], [
                'key' => 'notification.sms.noticeboard',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ],
            [
                'key' => 'module.users',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ],
            [
                'key' => 'module.classes',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ],
            [
                'key' => 'module.attendance',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ], [
                'key' => 'module.assignment',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ], [
                'key' => 'module.assessment',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ], [
                'key' => 'module.fee',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ], [
                'key' => 'user.student.fields',
                'value' => '', // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ], [
                'key' => 'user.parent.fields',
                'value' => '', // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ], [
                'key' => 'user.paren.fields',
                'value' => '', // Fine applied to invoice per day in case of late fine.
                'tenant_id' => 1,
            ], [
                'key' => 'academic.grades_status',
                'value' => 1, // Max 7 days
                'tenant_id' => 1,
            ], [
                'key' => 'academic.grades',
                'value' => '{}', // Max 7 days
                'tenant_id' => 1,
            ],
        ];

        DB::table('settings')->insert($data);
    }
}
