<?php

use Illuminate\Database\Seeder;

class ReputationBadgesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Book Reader',
                'icon' => 'book-reader',
                'icon_color' => '#ffffff',
                'bg_color' => '#fe8493',
                'score' => 5,
                'tenant_id' => 1,
            ],
            [
                'name' => 'Kind',
                'icon' => 'crow',
                'icon_color' => '#ffffff',
                'bg_color' => '#c49d23',
                'score' => 4,
                'tenant_id' => 1,
            ],
            [
                'name' => 'Light of Hope',
                'icon' => 'candle-holder',
                'icon_color' => '#ffffff',
                'bg_color' => '#3ebe95',
                'score' => 6,
                'tenant_id' => 1,
            ],
            [
                'name' => 'Friendly',
                'icon' => 'chart-network',
                'icon_color' => '#ffffff',
                'bg_color' => '#3e77be',
                'score' => 7,
                'tenant_id' => 1,
            ],
            [
                'name' => 'Mistake Fixer',
                'icon' => 'eraser',
                'icon_color' => '#ffffff',
                'bg_color' => '#7b3ebe',
                'score' => 8,
                'tenant_id' => 1,
            ],
            [
                'name' => 'Test Chewer',
                'icon' => 'medal',
                'icon_color' => '#ffffff',
                'bg_color' => '#d31212',
                'score' => 5,
                'tenant_id' => 1,
            ],
            [
                'name' => 'Knowledge Digger',
                'icon' => 'shovel',
                'icon_color' => '#ffffff',
                'bg_color' => '#789340',
                'score' => 6,
                'tenant_id' => 1,
            ],
            [
                'name' => 'Bully Badge',
                'icon' => 'bully',
                'icon_color' => '#ffffff',
                'bg_color' => '#f48d93',
                'score' => -10,
                'tenant_id' => 1,
            ],
            [
                'name' => 'Lazy',
                'icon' => 'turtle',
                'icon_color' => '#ffffff',
                'bg_color' => '#d7390f',
                'score' => -3,
                'tenant_id' => 1,
            ],
            [
                'name' => 'Punctual',
                'icon' => 'user-clock',
                'icon_color' => '#ffffff',
                'bg_color' => '#e48591',
                'score' => 3,
                'tenant_id' => 1,
            ],
            [
                'name' => 'Test Hatrick',
                'icon' => 'stars',
                'icon_color' => '#ffffff',
                'bg_color' => '#f38d21',
                'score' => 3,
                'tenant_id' => 1,
            ],
        ];

        DB::table('reputation_badges')->insert($data);

        $data = [
            [
                'class_id' => 1,
                'user_id' => 16,
                'badge_id' => 1,
                'tenant_id' => 1,
            ],
            [
                'class_id' => 1,
                'user_id' => 16,
                'badge_id' => 2,
                'tenant_id' => 1,
            ],
            [
                'class_id' => 1,
                'user_id' => 16,
                'badge_id' => 3,
                'tenant_id' => 1,
            ],
            [
                'class_id' => 1,
                'user_id' => 16,
                'badge_id' => 4,
                'tenant_id' => 1,
            ],
            [
                'class_id' => 1,
                'user_id' => 16,
                'badge_id' => 5,
                'tenant_id' => 1,
            ],
            [
                'class_id' => 1,
                'user_id' => 16,
                'badge_id' => 6,
                'tenant_id' => 1,
            ],
            [
                'class_id' => 1,
                'user_id' => 16,
                'badge_id' => 7,
                'tenant_id' => 1,
            ],
            [
                'class_id' => 1,
                'user_id' => 16,
                'badge_id' => 8,
                'tenant_id' => 1,
            ],
            [
                'class_id' => 1,
                'user_id' => 16,
                'badge_id' => 9,
                'tenant_id' => 1,
            ],
            [
                'class_id' => 1,
                'user_id' => 16,
                'badge_id' => 10,
                'tenant_id' => 1,
            ], [
                'class_id' => 1,
                'user_id' => 16,
                'badge_id' => 1,
                'tenant_id' => 1,
            ],
        ];

        DB::table('reputations')->insert($data);
    }
}
