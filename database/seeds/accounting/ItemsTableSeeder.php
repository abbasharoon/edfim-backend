<?php

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'description' => "Sample Description",
                'enabled' => 1,
                'tenant_id' => 1,
                'name' => "Test Item",
                'quantity' => 40,
                'reorder_point' => 20,
                'sku' => "abcde123"
            ],
        ];

        DB::connection('accounting')->table('items')->insert($data);

        $itemPrices = [
            [
                'account_id' => 3,
                'tenant_id' => 1,
                'item_id' => 1,
                'amount' => 50,
                'description' => "Some purchase Information provided here",
                'tax_inclusive' => false,
                'type' => false
            ],
            [
                'account_id' => 2,
                'item_id' => 1,
                'tenant_id' => 1,
                'amount' => 60,
                'description' => "Some Sales Information provided here",
                'tax_inclusive' => false,
                'type' => true
            ],
        ];
        DB::connection('accounting')->table('item_prices')->insert($itemPrices);

    }
}
