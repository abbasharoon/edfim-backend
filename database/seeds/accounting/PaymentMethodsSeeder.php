<?php


use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PaymentMethodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->create();

        Model::reguard();
    }

    private function create()
    {

        $methods = [];

        $methods[] = [
            'code' => 'cash.1',
            'name' => 'Cash',
            'order' => '1',
            'description' => null,
            'tenant_id' => 1,
        ];

        $methods[] = [
            'code' => 'bank_transfer.2',
            'name' => 'Bank Transfer',
            'order' => '2',
            'description' => null,
            'tenant_id' => 1,
        ];

        \DB::connection('accounting')->table('payment_methods')->insert($methods);
    }
}
