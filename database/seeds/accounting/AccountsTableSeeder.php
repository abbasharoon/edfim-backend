<?php

use Illuminate\Database\Seeder;

class AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $account = [
            [
                'tenant_id' => 1,
                'name' => 'Undeposited Funds',
                'description' => 'Not available',
                'number' => 3000,
                'type_id' => 1,
                'tax_id' => 1,
            ],
            [
                'tenant_id' => 1,
                'name' => 'Inventor Assets',
                'description' => 'Not available',
                'number' => 3002,
                'type_id' => 86,
                'tax_id' => 1,
            ],
            [
                'tenant_id' => 1,
                'name' => 'Cost of Sales',
                'description' => 'Purchased Items Account',
                'number' => 3001,
                'type_id' => 95,
                'tax_id' => 1,
            ],
        ];
        \DB::connection('accounting')->table('accounts')->insert($account);
    }
}
