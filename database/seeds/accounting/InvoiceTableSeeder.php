<?php

use Illuminate\Database\Seeder;

class InvoiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $invoice = [
            'tenant_id' => 1,
            'invoice_number' => '00234',
            'invoiced_at' => '2019-05-06',
            'due_at' => '2019-05-06',
            'amount' => 99,
            'currency_code' => 'PKR',
            'currency_rate' => 1,
            'customer_id' => 1,
            'customer_name' => 'Abbas Haroon',
            'category_id' => '1',
            'status' => 0
        ];
        \DB::connection('accounting')->table('invoices')->insert($invoice);

        $invoiceItem = [
            'tenant_id' => 1,
            'invoice_id' => 1,
            'item_id' => null,
            'name' => 'Test Item',
            'description' => 'None Provided',
            'quantity' => 9,
            'rate' => 10,
            'amount' => 90,
            'tax_id' => 1,
        ];
        \DB::connection('accounting')->table('invoice_items')->insert($invoiceItem);
        $invoiceTax = ['tenant_id' => 1,
            'invoice_id' => 1,
            'tax_id' => 1,
            'rate' => 10,
            'name' => 'Special Tax',
            'taxable_amount' => 90,
            'tax_amount'=>9,
        ];
        \DB::connection('accounting')->table('invoice_taxes')->insert($invoiceTax);
    }

}
