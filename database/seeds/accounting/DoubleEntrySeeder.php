<?php


use Illuminate\Database\Seeder;

class DoubleEntrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // All Company create accounts

        // Just once create classes and types
        $this->call(CategoriesTableSeeder::class);
        $this->call(CurrenciesTableSeeder::class);
        $this->call(CustomersSeeder::class);
        $this->call(AccountsTableSeeder::class);
        $this->call(TaxesSeeder::class);
        $this->call(Types::class);
        $this->call(InvoiceTableSeeder::class);
        $this->call(PaymentMethodsSeeder::class);
        $this->call(ItemsTableSeeder::class);
    }
}
