<?php

use Illuminate\Database\Seeder;

class CustomersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Test Customer',
                'email' => 'admin@mobspider.tk',
                'currency_code' => 'PKR',
                'address' => 'JP II, Abbottabad',
                'tenant_id' => 1,
            ]
        ];

        DB::connection('accounting')->table('customers')->insert($data);
    }
}
