<?php

use Illuminate\Database\Seeder;

class TaxesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tax = [
            'tenant_id' => '1',
            'tax_agency_id' => '1',
            'name' => 'Test Tax',
            'description' => 'Test',
            'sales_rate' => 10,
            'sales_account_id' => 1,
            'sales_tax_amount_return' => 1,
            'sales_net_amount_return' => 1,
            'purchase_rate' => 20,
            'purchase_account_id' => 2,
            'purchase_tax_amount_return' => 1,
            'purchase_net_amount_return' => 1,
            'enabled' => '1',
            'type' => '1'];

        DB::connection('accounting')->table('taxes')->insert($tax);


        $agency = [
            'tenant_id' => 1,
            'name' => 'Test Agency',
            'registration_number' => 'jdflksjdlfjsldkfj',
            'start_month' => 1,
            'filing_frequency' => 1,
            'reporting_method' => 1,
        ];

        DB::connection('accounting')->table('tax_agencies')->insert($agency);
    }
}
