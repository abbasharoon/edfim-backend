<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Invoice Category',
                'tenant_id' => 1,
                'type' => 'invoice',
            ],
            [
                'name' => 'Item Category',
                'tenant_id' => 1,
                'type' => 'item',
            ]
        ];

        DB::connection('accounting')->table('categories')->insert($data);
    }
}
