<?php

use Illuminate\Database\Seeder;

class TimetableTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title' => 'National Holiday1',
                'detail' => 'It\'s just a national holiday.',
                'start_date' => '2018-06-13',
                'end_date' => null,
                'tenant_id' => 1,
            ], [
                'title' => 'Summer Vacations2',
                'detail' => 'It\'s just a national holiday.',
                'start_date' => '2018-05-13',
                'end_date' => '2018-06-20',
                'tenant_id' => 1,
            ], [
                'title' => 'National Holiday3',
                'detail' => 'It\'s just a national holiday.',
                'start_date' => '2018-08-15',
                'end_date' => null,
                'tenant_id' => 1,
            ], [
                'title' => 'Eid Vacations4',
                'detail' => 'It\'s just a national holiday.',
                'start_date' => '2018-04-13',
                'end_date' => null,
                'tenant_id' => 1,
            ], [
                'title' => 'Iqbal Day5',
                'detail' => 'It\'s just a national holiday.',
                'start_date' => '2018-06-22',
                'end_date' => null,
                'tenant_id' => 1,
            ],
        ];

        DB::table('timetables')->insert($data);
        $data = [
            [
                'class_id' => 1,
                'timetable_id' => 1,
                'tenant_id' => 1,
            ], [
                'class_id' => 1,
                'timetable_id' => 2,
                'tenant_id' => 1,
            ], [
                'class_id' => 1,
                'timetable_id' => 3,
                'tenant_id' => 1,
            ], [
                'class_id' => 1,
                'timetable_id' => 4,
                'tenant_id' => 1,
            ], [
                'class_id' => 3,
                'timetable_id' => 3,
                'tenant_id' => 1,
            ], [
                'class_id' => 3,
                'timetable_id' => 1,
                'tenant_id' => 1,
            ], [
                'class_id' => 2,
                'timetable_id' => 1,
                'tenant_id' => 1,
            ], [
                'class_id' => 4,
                'timetable_id' => 1,
                'tenant_id' => 1,
            ], [
                'class_id' => 2,
                'timetable_id' => 3,
                'tenant_id' => 1,
            ], [
                'class_id' => 2,
                'timetable_id' => 4,
                'tenant_id' => 1,
            ],
        ];
        DB::table('class_timetable')->insert($data);

    }
}
