<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssessmentTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessment_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 256);
            $table->string('detail', 1024)->nullable();
            $table->string('type', 64)->nullable();
            $table->tinyInteger('schedule')->comment = '1=daily,2=weekly,3=monthly,4=quarterly,5=triannual,6=biannually,7=annually';
            $table->tinyInteger('status')->comment = "0=draft, 1 =due, 2=taken, 3 =published, 4=cancelled";
            $table->integer('score');
            $table->date('due_date');
            $table->integer('subject_id');
            $table->integer('class_id');
            $table->integer('batch_id');
            $table->integer('tenant_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessment_tests');
    }
}
