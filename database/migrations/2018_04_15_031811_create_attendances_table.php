<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendances', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('batch_id');
            $table->integer('class_id')->comment = 'For refrence on front end';
            $table->tinyInteger('status')->default(0)->comment = '0=started,1=completed';
            $table->integer('attendance_register_id');
            $table->integer('tenant_id');
            $table->timestamps();
            $table->unique(['date', 'batch_id','attendance_register_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendances');
    }
}
