<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('detail', 1000);
            $table->integer('batch_id');
            $table->integer('class_id');
            $table->integer('subject_id');
            $table->date('due_date');
            $table->tinyInteger('status')->comment = "0=draft, 1 =due, 2 =published, 3 =cancelled";
            $table->integer('score')->comment = "0 for done/not done score, >0 for marks per like system";
            $table->integer('tenant_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignments');
    }
}
