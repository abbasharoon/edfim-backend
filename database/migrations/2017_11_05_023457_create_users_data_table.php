<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_data', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->date('dob')->nullable();
            $table->string('qualification', 250)->nullable();
            $table->string('bio_data', 250)->nullable();
            $table->string('job_title', 250)->nullable();
            $table->string('address')->nullable();
            $table->timestamps();
        });

        Schema::table('users_data', function ($table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_data');
    }
}
