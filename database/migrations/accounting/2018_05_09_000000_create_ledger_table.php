<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLedgerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting')->create('ledger', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id');
            $table->integer('account_id');
            $table->morphs('ledgerable');
            $table->date('issued_at');
            $table->string('entry_type');
            $table->double('debit', 15, 4)->nullable();
            $table->double('credit', 15, 4)->nullable();
            $table->string('reference')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->index('tenant_id');
            $table->index('account_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting')->drop('ledger');
    }
}
