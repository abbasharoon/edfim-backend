<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting')->create('item_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id');
            $table->integer('item_id');
            $table->boolean('type')->default(true)->comment = 'true=sales & false=purchase';
            $table->double('amount', 15, 4);
            $table->string('description', 512)->nullable();
            $table->integer('account_id');
            $table->boolean('tax_inclusive')->default(false);
            $table->integer('tax_id')->nullable();
            $table->integer('user_id')->nullable()->comment = 'Supplier/Customer';

            $table->index('tenant_id');
            $table->unique(['type', 'item_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting')->dropIfExists('item_prices');
    }
}
