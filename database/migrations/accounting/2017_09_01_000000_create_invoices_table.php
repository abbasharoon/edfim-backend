<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting')->create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id');
            $table->string('invoice_number');
            $table->dateTime('invoiced_at');
            $table->dateTime('due_at');
            $table->double('amount', 15, 4);
            $table->string('currency_code');
            $table->double('currency_rate', 15, 8);
            $table->integer('customer_id');
            $table->string('customer_email')->nullable();
            $table->string('customer_name');
            $table->string('customer_address')->nullable();
            $table->integer('category_id');
            $table->text('notes')->nullable();
            $table->string('attachment')->nullable();
            $table->integer('parent_id')->default(0);
            $table->tinyInteger('type')->default(0);
            $table->tinyInteger('status')->commment = '0=draft,1=due,2=partial,3=paid,4=canceled';

            $table->timestamps();
            $table->softDeletes();

            $table->index('tenant_id');
            $table->unique(['tenant_id', 'invoice_number', 'deleted_at']);
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting')->drop('invoices');
    }
}
