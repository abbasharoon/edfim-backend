<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting')->create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('number', 7)->nullable();
//            $table->boolean('system')->default(0);
            $table->integer('type_id');
            $table->integer('tax_id')->nullable();
            $table->integer('parent_account_id')->nullable();
            $table->boolean('enabled')->default(1);
            $table->timestamps();
            $table->softDeletes();

            $table->index('tenant_id');
            $table->index('type_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting')->drop('double_entry_accounts');
    }
}
