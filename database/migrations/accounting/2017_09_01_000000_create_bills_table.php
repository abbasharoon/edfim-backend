<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting')->create('bills', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id');
            $table->string('bill_number');
            $table->string('order_number')->nullable();
            $table->string('bill_status_code');
            $table->dateTime('billed_at');
            $table->dateTime('due_at');
            $table->double('amount', 15, 4);
            $table->string('currency_code');
            $table->double('currency_rate', 15, 8);
            $table->integer('vendor_id');
            $table->string('vendor_name');
            $table->string('vendor_email')->nullable();
            $table->string('vendor_tax_number')->nullable();
            $table->string('vendor_phone')->nullable();
            $table->text('vendor_address')->nullable();
            $table->text('notes')->nullable();
            $table->string('attachment')->nullable();
            $table->integer('category_id')->default();
            $table->integer('parent_id')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->index('tenant_id');
            $table->unique(['tenant_id', 'bill_number', 'deleted_at']);
        });

        Schema::connection('accounting')->create('bill_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id');
            $table->integer('bill_id');
            $table->integer('item_id')->nullable();
            $table->string('name');
            $table->string('sku')->nullable();
            $table->double('quantity', 7, 2);
            $table->double('price', 15, 4);
            $table->double('total', 15, 4);
            $table->float('tax', 15, 4)->default('0.0000');
            $table->timestamps();
            $table->softDeletes();

            $table->index('tenant_id');
        });

        Schema::connection('accounting')->create('bill_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id');
            $table->string('name');
            $table->string('code');
            $table->timestamps();
            $table->softDeletes();

            $table->index('tenant_id');
        });

        Schema::connection('accounting')->create('bill_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id');
            $table->integer('bill_id');
            $table->integer('account_id');
            $table->dateTime('paid_at');
            $table->double('amount', 15, 4);
            $table->string('currency_code');
            $table->double('currency_rate', 15, 8);
            $table->text('description')->nullable();
            $table->string('payment_method');
            $table->string('reference')->nullable();
            $table->string('attachment')->nullable();
            $table->boolean('reconciled')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->index('tenant_id');
        });

        Schema::connection('accounting')->create('bill_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id');
            $table->integer('bill_id');
            $table->string('status_code');
            $table->boolean('notify');
            $table->text('description')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->index('tenant_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting')->drop('bills');
        Schema::connection('accounting')->drop('bill_items');
        Schema::connection('accounting')->drop('bill_statuses');
        Schema::connection('accounting')->drop('bill_payments');
        Schema::connection('accounting')->drop('bill_histories');
    }
}
