<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Models\Accounting\AccountingModel;
use App\Models\Accounting\Company\Company;
use App\Models\Accounting\Income\Invoice;
use App\Models\Accounting\Income\InvoiceItem;
use App\Models\Accounting\Income\InvoiceTotal;
use App\Models\Accounting\Setting\Tax;

class CreateInvoiceTotalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting')->create('invoice_totals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id');
            $table->integer('invoice_id');
            $table->string('code')->nullable();
            $table->string('name');
            $table->double('amount', 15, 4);
            $table->integer('sort_order');
            $table->timestamps();
            $table->softDeletes();

            $table->index('tenant_id');
        });

        AccountingModel::unguard();

        $companies = Company::all();

        foreach ($companies as $company) {
            $invoices = Invoice::where('tenant_id', $company->id)->get();

            foreach ($invoices as $invoice) {
                $invoice_items = InvoiceItem::where('tenant_id', $company->id)->where('invoice_id', $invoice->id)->get();

                $taxes = [];
                $tax_total = 0;
                $sub_total = 0;

                foreach ($invoice_items as $invoice_item) {
                    unset($tax_object);

                    $invoice_item->total = $invoice_item->price * $invoice_item->quantity;

                    if (!empty($invoice_item->tax_id)) {
                        $tax_object = Tax::where('tenant_id', $company->id)->where('id', $invoice_item->tax_id)->first();

                        $invoice_item->tax = (($invoice_item->price * $invoice_item->quantity) / 100) * $tax_object->rate;
                    }

                    $invoice_item->update();

                    if (isset($tax_object)) {
                        if (array_key_exists($invoice_item->tax_id, $taxes)) {
                            $taxes[$invoice_item->tax_id]['amount'] += $invoice_item->tax;
                        } else {
                            $taxes[$invoice_item->tax_id] = [
                                'name' => $tax_object->name,
                                'amount' => $invoice_item->tax
                            ];
                        }
                    }

                    $tax_total += $invoice_item->tax;
                    $sub_total += $invoice_item->price * $invoice_item->quantity;
                }

                $invoice->amount = $sub_total + $tax_total;

                $invoice->update();

                // Added invoice total sub total
                $invoice_sub_total = [
                    'tenant_id' => $company->id,
                    'invoice_id' => $invoice->id,
                    'code' => 'sub_total',
                    'name' => 'invoices.sub_total',
                    'amount' => $sub_total,
                    'sort_order' => 1,
                ];

                InvoiceTotal::create($invoice_sub_total);

                $sort_order = 2;

                // Added invoice total taxes
                if ($taxes) {
                    foreach ($taxes as $tax) {
                        $invoice_tax_total = [
                            'tenant_id' => $company->id,
                            'invoice_id' => $invoice->id,
                            'code' => 'tax',
                            'name' => $tax['name'],
                            'amount' => $tax['amount'],
                            'sort_order' => $sort_order,
                        ];

                        InvoiceTotal::create($invoice_tax_total);

                        $sort_order++;
                    }
                }

                // Added invoice total total
                $invoice_total = [
                    'tenant_id' => $company->id,
                    'invoice_id' => $invoice->id,
                    'code' => 'total',
                    'name' => 'invoices.total',
                    'amount' => $sub_total + $tax_total,
                    'sort_order' => $sort_order,
                ];

                InvoiceTotal::create($invoice_total);
            }
        }

        AccountingModel::reguard();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting')->drop('invoice_totals');
    }
}
