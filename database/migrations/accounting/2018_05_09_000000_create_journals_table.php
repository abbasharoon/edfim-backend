<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJournalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting')->create('journals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id');
            $table->date('paid_at');
            $table->double('amount', 15, 4);
            $table->text('description');
            $table->string('reference')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->index('tenant_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting')->drop('journals');
    }
}
