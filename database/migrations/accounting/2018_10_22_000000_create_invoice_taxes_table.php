<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvoiceTaxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting')->create('invoice_taxes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id');
            $table->integer('invoice_id');
            $table->integer('tax_id');
            $table->double('rate');
            $table->string('name');
            $table->double('taxable_amount', 15, 4)->default('0.0000');
            $table->double('tax_amount', 15, 4)->default('0.0000');
            $table->timestamps();
            $table->softDeletes();

            $table->index('tenant_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting')->drop('invoice_taxes');
    }
}
