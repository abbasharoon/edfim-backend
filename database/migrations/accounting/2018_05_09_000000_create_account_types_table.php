<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccountTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting')->create('account_types', function (Blueprint $table) {
            $table->increments('id');
//            $table->tinyInteger('class')->comment = '0=assets,1=liabilities,2=expenses,3=income,4=equity';
            $table->tinyInteger('type')->comment = '0=COC equivalents
                                                            1=A/R
                                                            2=CurAsts
                                                            3=FixAsts
                                                            4=n-currAsts
                                                            5=A/P
                                                            6=CC
                                                            7=Currliab
                                                            8=n-currLiab
                                                            9=Ownerequity
                                                            10=Income
                                                            11=COS
                                                            12=Expenses
                                                            13=Other income
                                                            14=Other expense';
//            $table->tinyInteger('type')->comment = '0=Cash and cash equivalents3
//                                                            1Accounts receivable (A/R)
//                                                            2=Current assets
//                                                            3=Fixed assets
//                                                            4=Non-current assets
//                                                            5=Accounts payable (A/P)
//                                                            6=Credit card
//                                                            7=Current liabilities
//                                                            8=Non-current liabilities
//                                                            9=Owner\'s equity
//                                                            10=Income
//                                                            11=Cost of sales
//                                                            12=Expenses
//                                                            13=Other income
//                                                            14=Other expense';
            $table->integer('tenant_id');
            $table->string('name');
            $table->string('key');
            $table->timestamps();
            $table->softDeletes();

            $table->index('type');
            $table->index('tenant_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting')->drop('account_types');
    }
}
