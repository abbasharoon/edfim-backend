<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting')->create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id');
            $table->string('name', 256);
            $table->string('sku', 256);
            $table->string('description', 512)->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('account_id')->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('reorder_point')->nullable();
            $table->boolean('enabled')->default(1);
            $table->timestamps();
            $table->softDeletes();

            $table->index('tenant_id');
            $table->unique(['tenant_id', 'sku', 'deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting')->drop('items');
    }
}
