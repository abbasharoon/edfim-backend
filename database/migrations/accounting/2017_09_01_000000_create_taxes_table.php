<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTaxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting')->create('taxes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id');
            $table->integer('tax_agency_id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->double('sales_rate', 15, 4)->nullable();
            $table->tinyInteger('sales_account_id')->nullable();
            $table->tinyInteger('sales_tax_amount_return')->nullable();
            $table->tinyInteger('sales_net_amount_return')->nullable();
            $table->double('purchase_rate', 15, 4)->nullable();
            $table->tinyInteger('purchase_account_id')->nullable();
            $table->tinyInteger('purchase_tax_amount_return')->nullable();
            $table->tinyInteger('purchase_net_amount_return')->nullable();
            $table->boolean('enabled')->default(1);
            $table->tinyInteger('type')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->index('tenant_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting')->drop('taxes');
    }
}
