<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('accounting')->create('invoice_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id');
            $table->integer('invoice_id');
            $table->integer('item_id')->nullable();
            $table->string('name');
            $table->string('description')->nullable();
            $table->double('quantity', 7, 2);
            $table->double('rate', 15, 4);
            $table->double('amount', 15, 4);
            $table->integer('tax_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->index('tenant_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('accounting')->dropIfExists('invoice_items');
    }
}
