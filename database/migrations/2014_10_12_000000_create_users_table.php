<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 255);
            $table->string('middle_name', 255)->nullable();
            $table->string('last_name', 255);
            $table->tinyInteger('gender')->comment = "1 = male, 2 = female, 3 = other";
            $table->string('picture_url', 250)->nullable();
            $table->string('username')->nullable();
            $table->string('nic', 255)->nullable();
            $table->string('email')->nullable();
            $table->string('password');
            $table->string('phone_no')->nullable();
            $table->integer('parent_id')->nullable()->comment = "To associate child to parent";
            $table->integer('batch_id')->nullable()->comment = "Nullable for users and parents";
            $table->string('roll_no')->nullable();
            $table->integer('tenant_id');
            $table->boolean('frozen')->default(false)->comment = "whether account is frozen or not";
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
            $table->unique(['email', 'tenant_id']);
            $table->unique(['username', 'tenant_id']);
            $table->unique(['phone_no', 'tenant_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
