<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTotalAmountToUserInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_invoices', function (Blueprint $table) {
            $table->decimal('total_amount', 13, 4)->after('class_invoice_id')->default(false);
            $table->decimal('amount_paid',13,4)->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_invoices', function ($table) {
            $table->dropColumn('total_amount');
            $table->float('amount_paid')->change();
        });
    }
}
