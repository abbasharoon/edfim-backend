<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meetings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 256);
            $table->smallInteger('duration')->default(30);
            $table->uuid('meeting_id');
            $table->string('attendee_password', 512);
            $table->string('presenter_password', 512);
            $table->bigInteger('classroom_id')->default(0);
            $table->string('recorded')->nullable();
            $table->string('welcome_message', 5000)->nullable();
            $table->bigInteger('creator_id');
            $table->boolean('status');
            $table->dateTime('start_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meeting');
    }
}
