<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaints', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description', 1024);
            $table->bigInteger('user_id')->nullable()->comment = 'Complainant User Id';
            $table->string('comment', 1024)->nullable();
            $table->tinyInteger('status')->default(0);
            $table->bigInteger('tenant_id');
            $table->dateTime('occurred_at');
            $table->timestamps();
        });

        Schema::create('complaint_user', function (Blueprint $table) {
            $table->bigInteger('complaint_id');
            $table->bigInteger('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaints');
        Schema::dropIfExists('complaint_user');
    }
}
