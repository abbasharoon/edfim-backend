<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBatchesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('class_id')->nullable();
            $table->integer('tenant_id');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('batch_class_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('batch_id');
            $table->integer('from_class_id');
            $table->integer('to_class_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batches');
        Schema::dropIfExists('batch_class_history');
    }
}
