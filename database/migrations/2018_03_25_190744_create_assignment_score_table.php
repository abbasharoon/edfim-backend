<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentScoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignment_score', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('assignment_id');
            $table->integer('user_id')->comment = "Student id";
            $table->integer('score')->comment = "0/1 for done/not done in case score is zero for assignment, else achieved score";
            $table->integer('tenant_id');
            $table->unique(['assignment_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignment_score');
    }
}
