    <?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssessmentTestScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessment_test_scores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('assessment_test_id');
            $table->integer('user_id');
            $table->integer('score');
            $table->integer('tenant_id');
            $table->unique(['assessment_test_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessment_test_scores');
    }
}
