<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 256);
            $table->string('detail', 512)->nullable();
            $table->float('amount');
            $table->tinyInteger('schedule')->comment = '1=daily,2=weekly,3=monthly,4=quarterly,5=triannual,6=biannually,7=annually';
            $table->tinyInteger('type')->comment = "1=whole number,2=percentage";
//            $table->tinyInteger('addition')->comment = "0=subtract the fee from total or w.e/1=add the fee";
            $table->integer('fee_category_id')->nullable();
            $table->integer('old_fee_id')->nullable();
            $table->integer('tenant_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fees');
    }
}
