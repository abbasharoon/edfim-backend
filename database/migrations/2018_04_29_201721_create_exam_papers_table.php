<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamPapersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_papers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 256);
            $table->string('detail', 1024)->nullable();
            $table->string('type', 64)->nullable();
            $table->tinyInteger('status')->comment = "0=draft, 1 =due, 2=take, 3 =published, 4=cancelled";
            $table->integer('score');
            $table->date('due_date');
            $table->integer('subject_id');
            $table->integer('assessment_exam_id');
            $table->integer('tenant_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_papers');
    }
}
