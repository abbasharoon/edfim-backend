<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->float('amount_paid');
            $table->integer('class_id');
            $table->integer('batch_id')->nullable();
            $table->tinyInteger('status')->comment = "Status of invoice, 1=due,2=paid,3=cancelled";
            $table->date('due_date')->nullable();
            $table->tinyInteger('schedule')->comment = '0=custom,1=daily,2=weekly,3=monthly,4=quarterly,5=triannual,6=biannually,7=annually';
            $table->string('schedule_duration')->nullable();
            $table->integer('tenant_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_invoices');
    }
}
