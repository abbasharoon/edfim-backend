<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamPaperScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_paper_scores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('exam_paper_id');
            $table->integer('user_id');
            $table->integer('score');
            $table->integer('tenant_id');
            $table->unique(['exam_paper_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_paper_scores');
    }
}
