<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendanceAbsentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance_absents', function (Blueprint $table) {
            $table->integer('attendance_id');
            $table->integer('user_id');
            $table->integer('reason_id')->nullable();
            $table->integer('tenant_id');
            $table->unique(['attendance_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance_absents');
    }
}
