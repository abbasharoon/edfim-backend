<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;

class ExamPaperScore extends Model
{
    use BelongsToTenants;
    public $timestamps = false;
    protected $hidden = ['tenant_id'];
    protected $fillable = ['user_id', 'score'];

    public function examPaper()
    {
        return $this->belongsTo('App\ExamPaper');
    }

    public function student()
    {
        return $this->belongsTo('App\User')->withTrashed();
    }
}
