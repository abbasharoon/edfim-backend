<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Media as PMedia;

class Media extends PMedia
{
//    use SoftDeletes;
//    protected $connection = 'accounting';
    protected $dates = ['deleted_at'];

}
