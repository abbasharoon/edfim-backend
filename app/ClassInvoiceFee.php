<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;

class ClassInvoiceFee extends Model
{
    use BelongsToTenants;
    protected $hidden = ['tenant_id'];
    protected $table = 'class_invoice_fee';

    public function targetFee()
    {
        return $this->belongsTo('App\Fee', 'target_fee_id', 'id');
    }

    public function fee()
    {
        return $this->belongsTo('App\Fee', 'fee_id', 'id');
    }

}
