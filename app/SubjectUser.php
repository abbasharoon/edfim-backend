<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubjectUser extends Model
{
    public $timestamps = false;

	/**
	 * The table associated with the model
	 * @var string
	 */
    protected $table = 'subject_user';

    /**
     * The attributes that are mass-assignable
     * @var [type]
     */
    protected $fillable = ['users_id'];

}
