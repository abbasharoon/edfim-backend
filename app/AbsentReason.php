<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;

class AbsentReason extends Model
{
    use BelongsToTenants;
    protected $hidden = ['tenant_id', 'deleted_at'];
    public $timestamps = false;

    public function absent(){
        return $this->hasMany('App\AttendanceAbsent','reason_id','id');
    }
}
