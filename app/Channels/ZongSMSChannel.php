<?php

namespace App\Channels;

use anlutro\LaravelSettings\Facade as SettingFacade;
use App\Helpers\SMSHelper;
use App\Setting;
use App\User;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use HipsterJazzbo\Landlord\Facades\Landlord;
use Illuminate\Notifications\Notification;
use Mockery\Exception;

class ZongSMSChannel
{
    /**
     * Send the given notification.
     *
     * @param mixed                                  $notifiable
     * @param \Illuminate\Notifications\Notification $notification
     *
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {        try {

        $tenant_id = User::findOrFail($notifiable->id)->makeVisible('tenant_id')->tenant_id;
        $setting = Setting::where('tenant_id', $tenant_id)->get()->makeVisible('tenant_id')->keyBy('key')->toArray();
        Landlord::addTenant('tenant_id', $tenant_id);
        SettingFacade::setExtraColumns(['tenant_id' => $tenant_id]);
        $smsHelper = new SMSHelper();
        $message = $notification->toSms($notifiable);
        if ($setting['notification.include_name']['value'] || is_array($message)) {
            $message = is_array($message) ? $message[0] : $message;
            $smsHelper->addLine($message);
            $smsHelper->addLine($setting['general.name']['value']);
            $message = $smsHelper->getMessage();
        }
        $smsCounts = $smsHelper->smsCounts($message);
        $client = new Client();
        $settingUpdate = Setting::where('tenant_id', $tenant_id)->where('key', 'sms.count')->first();
        $settingUpdate->value = $setting['sms.count']['value'] - $smsCounts['totalSms'];
        $settingUpdate->save();
//        \DB::table('settings')->where('tenant_id', $tenant_id)
//            ->where('key', 'sms.count')
//            ->update(['value' => $setting['sms.count']['value'] - $smsCounts['totalSms']]);
//            ->update(['sms.count' => $setting['sms.count'] - $smsCounts['totalSms']);
//        setting(])->save();
            $response = $client->request('POST', env('ZONGCBS'), [
                'form_params' => [
                    'login' => env('CBSLOGIN'),
                    'pass' => env('CBSPASS'),
                    'message' => $message,
                    'unicode' => $smsCounts['unicode'],
                    'hostname' => uniqid(),
                    'numbers' => str_replace("+", "", str_replace(" ", "", $notifiable->phone_no)),
                ],
                'headers' => ['Content-Type' => 'application/x-www-form-urlencoded'],
            ]);
        } catch (Exception $e) {
//            print_r($e);
        } catch (GuzzleException $e) {
//            print_r($e->getMessage());
        }
    }
}