<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    protected $hidden = ['attendee_password', 'presenter_password', 'meeting_id'];

    public function classroom()
    {
        return $this->belongsTo('App\Classroom');
    }

    public function participant()
    {
        return $this->classroom()->participant;
    }
}
