<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationPreference extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'user_id';

}
