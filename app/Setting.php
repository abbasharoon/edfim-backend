<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use BelongsToTenants;

    public $timestamps = false;
    protected $hidden = ['tenant_id', 'id'];
}
