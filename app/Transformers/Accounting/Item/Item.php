<?php

namespace App\Transformers\Accounting\Item;

/**
 * @deprecated since 1.2.7 version. use Common\Item instead.
 */
class Item extends \App\Transformers\Accounting\Common\Item {}
