<?php

namespace App\Transformers\Accounting\Expense;

use App\Models\AccountingExpense\BillStatus as Model;
use League\Fractal\TransformerAbstract;

class BillStatus extends TransformerAbstract
{
    /**
     * @param Model $model
     * @return array
     */
    public function transform(Model $model)
    {
        return [
            'id' => $model->id,
            'company_id' => $model->company_id,
            'name' => $model->name,
            'code' => $model->code,
            'created_at' => $model->created_at->toIso8601String(),
            'updated_at' => $model->updated_at->toIso8601String(),
        ];
    }
}
