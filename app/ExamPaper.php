<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;

class ExamPaper extends Model
{
    use BelongsToTenants;
    protected $hidden = ['tenant_id'];
    protected $guarded = ['id', 'assessment_exam_id', 'tenant_id', 'deleted_at'];
    protected $dates = ['due_date', 'created_at', 'updated_at'];

    public function assessmentExam()
    {
        return $this->belongsTo('App\AssessmentExam');
    }

    public function student_score()
    {
        return $this->hasMany('App\ExamPaperScore');
    }

    public function parent()
    {
        return $this->belongsTo('App\ExamPaper');
    }

    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }
}
