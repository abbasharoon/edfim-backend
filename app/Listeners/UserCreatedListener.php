<?php

namespace App\Listeners;

use anlutro\LaravelSettings\Facade as Setting;
use App\ApiService;
use App\Events\UserCreated;
use App\Helpers\ErpNextHelper;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserCreatedListener
{
//    use InteractsWithQueue;
    /**
     * Create the event listener.
     *
     * @return void
     */
    private $erpNextHelper;

    public function __construct(ErpNextHelper $erpNextHelper)
    {
        $this->erpNextHelper = $erpNextHelper;
    }

    /**
     * Handle the event.
     *
     * @param UserCreated $user
     * @return void
     */
    public function handle(UserCreated $event)
    {
        $userRole = \Bouncer::is($event->user)->a('system-admin', 'finance-admin');
//        $userRole = $userRole ? $userRole : (\Bouncer::is($event->user)->a('parent', 'student'))
        Setting::setExtraColumns(['tenant_id' => $event->tenantId]);
        $this->erpNextHelper->createUser($event->user->email,
            $event->user->first_name,
            $event->user->last_name, $userRole);
        $this->erpNextHelper->addUserToCompany($event->user->email, setting('general.name'));
        $apiService = new ApiService();
        $apiService->name = 'erpNext';
        $apiService->user_id = $event->user->id;
        $apiService->service_id = $event->user->email;
        $apiService->save();
    }
}
