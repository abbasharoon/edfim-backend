<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;

class AssessmentExam extends Model
{
    use BelongsToTenants;
    protected $hidden = ['tenant_id'];


    public function papers()
    {
        return $this->hasMany('App\ExamPaper');
    }
}
