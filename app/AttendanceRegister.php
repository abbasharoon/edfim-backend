<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;

class AttendanceRegister extends Model
{

    use BelongsToTenants;
    protected $table = 'attendance_register';
    public $timestamps = false;
//    protected $dates = ['start_date', 'end_date'];

    function attendance()
    {
        return $this->hasMany('App\Attendance', 'attendance_register_id', 'id');
    }
}
