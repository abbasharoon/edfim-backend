<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subject extends Model
{
    use BelongsToTenants, softDeletes;

    /**
     * The attributes that should be mutated to dates
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'subjects';

    /**
     * The attributes that are mass-assignable
     *
     * @var [type]
     */
    protected $fillable = ['name', 'class_id'];
    protected $hidden = ['deleted_at', 'tenant_id'];

    public function subject_staff()
    {
        return $this->belongsToMany('App\User', 'subject_user', 'subject_id', 'user_id');
    }


    public function class()
    {
        return $this->hasOne('App\Classes', 'id', 'class_id');
    }

}
