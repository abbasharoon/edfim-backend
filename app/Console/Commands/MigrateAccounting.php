<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MigrateAccounting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:accounting {fresh?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate Accounting Database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->argument('fresh')) {
            if (env('environment') === 'production' || env('envrionment') === 'prod') {
                $this->error('App is in production, Fresh Migrations not allowed');
                return;
            }
            $this->call('migrate:fresh', [
                '--database' => 'accounting', '--path' => 'database/migrations/accounting'
            ]);
            $this->call('db:seed', [
                '--database' => 'accounting', '--class' => 'DoubleEntrySeeder'
            ]);
        } else {
            $this->call('migrate', [
                '--database' => 'accounting', '--path' => 'database/migrations/accounting'
            ]);
        }
    }
}
