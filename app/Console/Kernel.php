<?php

namespace App\Console;

use App\Jobs\ScheduleInvoices;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $schedule->call(function () {
            $carbon = Carbon::now();
            \DB::enableQueryLog();
            $quarterly = [$carbon->day . '-' . $carbon->month,
                $carbon->day . '-' . $carbon->addMonths(-3)->month,
                $carbon->day . '-' . $carbon->addMonths(-6)->month,
                $carbon->day . '-' . $carbon->addMonths(-9)->month];
            $triannual = [$carbon->day . '-' . $carbon->month,
                $carbon->day . '-' . $carbon->addMonths(-4)->month,
                $carbon->day . '-' . $carbon->addMonths(-8)->month];
            $biannual = [$carbon->day . '-' . $carbon->month,
                $carbon->day . '-' . $carbon->addMonths(-6)->month];
            $settings = Setting::where(function ($q) use ($carbon) {
                $q->where('key', 'fee.schedule.monthly');
                $q->where('value', $carbon->day);
            })->orwhere(function ($q) use ($quarterly) {
                $q->Where('key', 'fee.schedule.quarterly');
                $q->whereIn('value', $quarterly);
            })->orwhere(function ($q) use ($triannual) {
                $q->Where('key', 'fee.schedule.triannual');
                $q->whereIn('value', $triannual);
            })->orwhere(function ($q) use ($biannual) {
                $q->Where('key', 'fee.schedule.biannual');
                $q->whereIn('value', $biannual);
            })->orwhere(function ($q) use ($carbon) {
                $q->Where('key', 'fee.schedule.annual');
                $q->where('value', $carbon->day . '-' . $carbon->month);
            })->withoutGlobalScope('tenant_id')->get()->makeVisible('tenant_id');
            if ($settings) {
                $settings->each(function ($item, $key) {
                    if ($item['key']==='fee.schedule.monthly') {
                        ScheduleInvoices::dispatch($item['tenant_id'], 3);
                    } elseif ($item['key']==='fee.schedule.quarterly') {
                        ScheduleInvoices::dispatch($item['tenant_id'], 4);
                    } elseif ($item['key']==='fee.schedule.triannual') {
                        ScheduleInvoices::dispatch($item['tenant_id'], 5);
                    } elseif ($item['key']==='fee.schedule.biannual') {
                        ScheduleInvoices::dispatch($item['tenant_id'], 6);
                    } elseif ($item['key']==='fee.schedule.annual') {
                        ScheduleInvoices::dispatch($item['tenant_id'], 7);
                    }
                });
            }
            return $settings->toArray();
        })->monthly()->sendOutputTo(storage_path('logs/examplecommand.log'));;
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
