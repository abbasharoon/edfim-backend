<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInvoiceFee extends Model
{

    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'user_invoice_fee';
    protected $hidden = ['tenant_id'];

    public function targetFee()
    {
        return $this->belongsTo('App\Fee', 'target_fee_id', 'id');
    }

    public function fee()
    {
        return $this->belongsTo('App\Fee', 'fee_id', 'id');
    }
}
