<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BatchUserHistory extends Model
{
    protected $table = 'batch_user_histories';
}
