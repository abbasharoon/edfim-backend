<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassInvoice extends Model
{
    use BelongsToTenants, SoftDeletes;
    protected $hidden = ['tenant_id'];

    public function pivotFee()
    {
        return $this->belongsToMany(
            'App\Fee',
            'class_invoice_fee',
            'class_invoice_id',
            'fee_id');
    }


    public function fee()
    {
        return $this->hasMany('App\ClassInvoiceFee');
    }


    public function userInvoice()
    {
        return $this->hasMany('App\UserInvoice', 'class_invoice_id', 'id');
    }

    public function class()
    {
        return $this->belongsTo('App\Classes', 'class_id', 'id');
    }
}
