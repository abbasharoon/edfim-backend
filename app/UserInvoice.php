<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserInvoice extends Model
{

    use BelongsToTenants, SoftDeletes;
    protected $hidden = ['tenant_id'];
    protected $dates = ['due_date', 'created_at', 'updated_at', 'deleted_at'];

    public function pivotFee()
    {
        return $this->belongsToMany(
            'App\Fee',
            'user_invoice_fee',
            'user_invoice_id',
            'fee_id');
    }

    public function fee()
    {
        return $this->hasMany('App\UserInvoiceFee');
    }


    public function classInvoice()
    {
        return $this->belongsTo('App\ClassInvoice', 'class_invoice_id', 'id');
    }

    public function student()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function getAmountPaidAttribute($value)
    {
        return strpos($value, '.') !== false ? rtrim(rtrim($value, '0'), '.') : $value;
    }

    public function getTotalAmountAttribute($value)
    {
        return strpos($value, '.') !== false ? rtrim(rtrim($value, '0'), '.') : $value;
    }
}
