<?php

namespace App\Helpers;

class Reply
{
    /**
     * Return success response
     *
     * @param $messages
     *
     * @return array
     */
    public static function success($message, $data = [], $messageOptions = [])
    {
        return [
            "status" => true,
            "message" => trans($message, $messageOptions),
            "data" => $data
        ];
    }

    public static function error($message, $data = [], $messageOptions = [])
    {
        return [
            "status" => false,
            "message" => trans($message, $messageOptions),
            "data" => $data
        ];
    }

    public static function unAuthorized()
    {
        return response(['message' => 'This action is unauthorized'], 403);
    }

    /**
     * @param $actionStatus
     * @param $on
     * @param $action
     * @param $section
     * @param $messageName
     */
    public static function loggedResponse($actionStatus, $on, $action, $section, $messageName)
    {
        $message = $messageName . ucwords($action) . 'd';
        if ($actionStatus) {
            activity(config('app.tenant')->tenant_id)
                ->on($on)
                ->withProperties(['action' => $action, 'section' => $section])
                ->log(__('messages.log' . $message));
            return response()->json(Reply::success(__('messages.' . $message), $on));
        }
        return response()->json(Reply::error(__('messages.' . $message)));
    }

}