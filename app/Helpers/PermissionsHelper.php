<?php

namespace App\Helpers;

use App\Batch;
use App\BatchUserHistory;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PermissionsHelper
{

    protected $user;

    public function __construct()
    {
    }

    public function teacherClass($classId)
    {
        $user = User::whereId(Auth::id())->whereHas('classStaff', function ($q) use ($classId) {
            $q->where('class_user.class_id', $classId);
        });
        return $user->exists();
    }

    public function hasBatch($batchId)
    {
        if (!$batchId) return null;

        return DB::table('batches')
            ->join('class_user', 'batches.class_id', '=', 'class_user.class_id')
            ->where('class_user.user_id', Auth::id())
            ->where('batches.id', $batchId)
            ->exists();

    }

    public function hasSubject($subjectId)
    {
        return Auth::user()->whereHas('subjectStaff', function ($q) use ($subjectId) {
            $q->whereId($subjectId);
        })->exists();
    }

    public function studentHasBatch($batchId)
    {
        $exist = DB::table('users')
            ->leftJoin('batch_user_histories', 'batch_user_histories.user_id', '=', 'users.id')
            ->where(function ($q) use ($batchId) {
                $q->where('users.batch_id', $batchId);
                $q->orWhere('batch_user_histories.from_batch_id', $batchId);
                $q->orWhere('batch_user_histories.to_batch_id', $batchId);
            });
        if (Auth::user()->isA('student')) {
            $exist->where('users.id', Auth::id());
        } elseif (Auth::user()->isA('parent')) {
            $exist->where('users.parent_id', Auth::id());
        } else {
            return null;
        }
        return $exist->exists();
    }

    public function studentHasClass($classId = null)
    {
        if (Auth::user()->isA('student')) {
            $batch = Batch::whereId(Auth::user()->batch_id);
            if ($classId) {
                $batch = $batch->whereClassId($classId);
            }
            return [$batch->get()->pluck('class_id')];
        } elseif (Auth::user()->isA('parent')) {
            $users = User::whereParentId(Auth::id())->get();
            $batch = Batch::whereIn('id', $users->pluck('batch_id'));
            if ($classId) {
                $batch = $batch->whereClassId($classId);
            }
            return [$batch->get()->pluck('class_id')];
        }
        return false;

    }

    public function studentBatch($batchId = null)
    {

        if ($batchId && $this->studentHasBatch($batchId)) {
            return [$batchId];
        } else {
            if (Auth::user()->isA('student')) {
                $batchIds = [Auth::user()->batch_id];
                $history = BatchUserHistory::whereUserId(Auth::id())->get();

            } elseif (Auth::user()->isA('parent')) {
                $users = User::whereParentId(Auth::id())->get();
                $batchIds = $users->pluck('batch_id')->toArray();
                $history = BatchUserHistory::whereIn('user_id', $users->pluck('id'));
            }
            array_push($batchIds, $history->pluck('from_batch_id')->toArray());
            array_push($batchIds, $history->pluck('to_batch_id')->toArray());
            return collect($batchIds)->flatten();
        }
    }

    public function childrenIds($childId = null)
    {
        if ($childId) {
            if (User::find($childId)->where('parent_id', Auth::id())) {
                return [$childId];
            }
            return null;
        }
        return User::where('parent_id', Auth::id())->get()->pluck('id');
    }

    public function teacherBatch($batchId = null)
    {
        if ($batchId && $this->hasBatch($batchId)) {
            return [$batchId];
        } else {
            $batchIds = DB::table('batches')
                ->join('class_user', 'batches.class_id', '=', 'class_user.class_id')
                ->where('class_user.user_id', Auth::id())
                ->get()->pluck('id');
            return $batchIds;
        }
    }

    public function ownStudent($studentId)
    {

    }
}