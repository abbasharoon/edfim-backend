<?php

namespace App\Helpers;

class SMSHelper
{
    private $smsMessage;


    public function addLine($line)
    {
        $this->smsMessage .= $line . "\n";
        return $this;
    }

    public function addLineBreak()
    {
        $this->smsMessage .= "\n";
        return $this;
    }

    public function getMessage()
    {
        return $this->smsMessage;
    }

    public function smsCounts($text)
    {
        // Default 7-bit GSM char set
        $_7bit = "/^[@£\$¥èéùìòÇ\nØø\rÅåΔ_ΦΓΛΩΠΨΣΘΞÆæßÉ !\"#¤%&'\(\)\*\+,-\.\/0123456789:;<=>\?¡ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÑÜ§¿abcdefghijklmnopqrstuvwxyzäöñüà\f\^\{\}\\\[~\]|€]*$/";
        // 7-bit mask + 8-bit mask
        $_8bit = "/^[ÀÂâçÈÊêËëÎîÏïÔôŒœÙÛû«»₣„“”°@£\$¥èéùìòÇ\nØø\rÅåΔ_ΦΓΛΩΠΨΣΘΞÆæßÉ !\"#¤%&'\(\)\*\+,-\.\/0123456789:;<=>\?¡ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÑÜ§¿abcdefghijklmnopqrstuvwxyzäöñüà\f\^\{\}\\\[~\]|€]*$/";
        $is7bit = preg_match_all($_7bit, $text);
        $is8bit = preg_match_all($_8bit, $text);
        $textLen = mb_strlen($text);
        $selfOptions = [
            // latin as usual
            '_7bit' => 160,
            // french, german (specific symbols, not in utf-8)
            '_8bit' => 140,
            // cyrillic and other
            '_16bit' => 70,
        ];
        $limit = $is7bit ? $selfOptions['_7bit'] : ($is8bit ? $selfOptions['_8bit'] : $selfOptions['_16bit']);
        return [
            'totalSms' => ceil($textLen / $limit),
            'charsLeft' => $limit - ($textLen % $limit),
            'parts' => $this->sliceStr($text, $limit),
            'unicode' => $is7bit ? 0 : 1,
            'limit' => $limit,
        ];

    }

    public function sliceStr($str, $limit, $parts = [])
    {
        $end = substr($str, $limit);
        $begin = substr($str, 0, $limit);

        if (mb_strlen($begin) > 0) {
            array_push($parts, substr($str, 0, $limit));
        }
        if (mb_strlen($end) > 0) {
            $this->sliceStr($end, $limit, $parts);
        } else {
            return $parts;
        }
    }
}
