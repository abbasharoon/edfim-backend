<?php

namespace App\Helpers;

use App\Batch;
use App\Notifications\TestSaved;
use App\User;
use Illuminate\Support\Facades\Notification;

class TestHelper
{

    public static function notify($test, $scoreData = null)
    {
        $batch = Batch::findOrFail($test->batch_id);
        $batch->class;
        $score = null;

        if ($scoreData) {
            $scoreCollection = collect($scoreData);
            $score = [
                'scoreData' => $scoreCollection->keyBy('user_id'),
                'positions' => $scoreCollection->sortByDesc('score')->keyBy('score')->keys()->toArray(),
            ];
            $users = User::whereIn('id', $scoreCollection->pluck('user_id'))
                ->whereBatchId($test->batch_id)
                ->with('parent:id,first_name,middle_name,last_name,email,phone_no',
                    'parent.notificationPreference', 'notificationPreference')
                ->get(['id', 'first_name', 'middle_name', 'last_name', 'email', 'phone_no']);
        } else {
            $users = User::where('batch_id', $test->batch_id)
                ->with('parent:id,first_name,middle_name,last_name,email,phone_no',
                    'parent.notificationPreference', 'notificationPreference')
                ->get(['id', 'first_name', 'middle_name', 'last_name', 'email', 'phone_no']);
        }

        Notification::send($users, new TestSaved($test, $batch, $score));
        $users->filter(function ($value, $key) {
            return !is_null($value['parent']);
        })->each(function ($item) use ($test, $batch, $score) {
            if ($item['parents']) {
                Notification::send($item['parents'], new TestSaved($test, $batch, $score, $item));
            }
        });
    }
}