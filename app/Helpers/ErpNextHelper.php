<?php

namespace App\Helpers;


use App\Transformers\Accounting\Auth\User;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\RequestException;


class ErpNextHelper
{

    protected $apiEndPoints = [
        'api_secret' => '/api/method/frappe.core.doctype.user.user.generate_keys',
        'api_key' => '/api/method/frappe.desk.form.load.getdoc?doctype=User&',
        'savedocs' => '/api/resource/',
        'app_assets' => 'api/method/frappe.www.desk.get_desk_assets?',
    ];


    public function getApiToken()
    {
        $erpNext = auth()->user()->erpNext();
        if (is_null($erpNext) && !empty($erpNext->api_key)) {
            return $erpNext->api_key . ':' . $erpNext->api_secret;
        } else {
            $token = $this->generateApiToken($erpNext->service_username);
            if ($erpNext) {
                $erpNext->api_key = $token['apiKey'];
                $erpNext->api_secret = $token['apiSecret'];
                $erpNext->update();
            }
            return $token['apiKey'] . ':' . $token['apiSecret'];
        }
    }

    public function createCompany($name)
    {
        $abbr = '';
        foreach (preg_split("/\s+/", ucwords($name)) as $k => $v) {
            $abbr .= $v[0];
        }
        $data = [
            "name" => $name,
            "is_group" => 0,
            "country" => "Pakistan",
            "create_chart_of_accounts_based_on" => "Standard+Template",
            "allow_account_creation_against_child_company" => 0,
            "enable_perpetual_inventory" => 1,
            "company_name" => $name,
            "abbr" => $abbr,
            "domain" => "Education",
            "default_currency" => "PKR",
            "chart_of_accounts" => "Standard"
        ];
        return !empty($this->makeCall('Company', $data, 'post'));
    }

    public function createUser($email, $firstname, $lastname, $isAdmin=false)
    {
        $data = [
            "name" => $email,
            "enabled" => 1,
            "send_welcome_email" => 0,
            "user_type" => "System+User",
            "type" => "System User",
            "bypass_restrict_ip_check_if_2fa_enabled" => 0,
            "email" => $email,
            "first_name" => $firstname,
            "last_name" => $lastname,


        ];
        if($isAdmin){
            $data["roles"] = [
                [
                    "parent" => $email,
                    "parentfield" => "roles",
                    "parenttype" => "User",
                    "role" => "System Manager"
                ]
            ];
            $data['block_modules'] = [];
            $blockedModules = [
                'Users and Permissions',
                'Integrations',
                'Social',
                'CRM',
                'Customization',
                'Website',
                'Getting Started',
                'Education',
                'Help',
                'Marketplace',
                'Quality Management',
                'Projects',
                'Support',
                'Projects ',
            ];
            foreach ($blockedModules as $b) {
                array_push($data['block_modules'],
                    [
                        "parent" => $email,
                        "parentfield" => "block_modules",
                        "parenttype" => "User",
                        "module" => $b
                    ]);
            }

        }

        return $this->makeCall('User', $data, 'post');

    }

    public function addUserToCompany($userEmail, $companyName)
    {
        $data = [
            "name" => "New+User+Permission+3",
            "apply_to_all_doctypes" => 1,
//            "applicable_for" => null,
            "user" => $userEmail,
            "allow" => "Company",
            "for_value" => $companyName,
            "is_default" => 1
        ];
        $apiSecret = $this->makeCall('User Permission', $data, 'post');
    }

    public function getAssets()
    {
        return $this->makeCall('app_assets',['build_version'=>000]);
    }

    private function generateApiToken($username)
    {
        $username = 'test647851502@example.org';
        $apiSecret = $this->makeCall('api_secret', ['user' => $username],'post');
        $apiKey = $this->makeCall('api_key', ['name' => $username]);
        if (!empty($apiKey) && !empty($apiSecret)) {
            $apiKey = $apiKey->docs[0]->api_key;
            $apiSecret = $apiSecret->message->api_secret;
            return ['apiKey' => $apiKey, 'apiSecret' => $apiSecret];
        }
    }

    private function makeCall($docType, $data, $method = 'get')
    {
        $client = new Client();
        try {
            $url = 'http://edfimbooks-shared.edfim.com';
            if (array_key_exists($docType, $this->apiEndPoints)) {
                $url .= $this->apiEndPoints[$docType];
            } else {
                $url .= $this->apiEndPoints['savedocs'] . $docType;
            }
            $options = [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
//                    'Authorization' => 'token d65226fabbc3046:d2d9462096d9502'
                    'Authorization' => 'token 259f1a6347ce423:215b7b213df346e'
                ],
            ];
            switch ($method) {
                case 'get':
//                    $options['headers']['X-Frappe-Doctype'] = 'User';
                    $url .=   http_build_query($data);
                    $client = $client->get($url, $options);
                    break;
                case 'post':
                    $options['json'] = $data;
                    $client = $client->post($url, $options);
                    break;
            }


            return json_decode((string)$client->getBody());
        } catch (BadResponseException $e) {
            print_r((string)$e->getMessage());
            dd($e->getResponse()->getBody()->getContents());
            die();
        } catch (RequestException $e) {

        }
    }

}