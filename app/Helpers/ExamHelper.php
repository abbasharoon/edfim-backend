<?php

namespace App\Helpers;

use App\Batch;
use App\Notifications\ExamSaved;
use App\User;
use Illuminate\Support\Facades\Notification;

class ExamHelper
{
    public static function notify($exam)
    {
        if ($exam->status == 3) {
            $studentTotal = [];
            $exam->papers->each(function ($item) use (&$studentTotal) {
                foreach ($item->student_score as $score) {
                    if (isset($studentTotal[$score->user_id]['total'])) {
                        $studentTotal[$score->user_id]['total'] += $score->score;
                    } else {
                        $studentTotal[$score->user_id]['total'] = $score->score;
                        $studentTotal[$score->user_id]['user_id'] = $score->user_id;
                    }
                    $studentTotal[$score->user_id][$item->id] = $score->score;
                }
            });
            $numberFormatter = new \NumberFormatter('en_US', \NumberFormatter::ORDINAL);
            $totalScore = collect($studentTotal);
            $rankedScore = $totalScore->sortByDesc('total')->pluck('total')->toArray();
            $totalScore = $totalScore->map(function ($item) use ($rankedScore, $numberFormatter) {
                $item['position'] = $numberFormatter->format(array_search($item['total'], $rankedScore) + 1);
                return $item;
            });
            $papers = $exam->papers->groupBy('subject_id');
            $users = User::whereIn('id', $totalScore->pluck('user_id')->toArray())->with('parent:id,first_name,middle_name,last_name,email,phone_no',
                'parent.notificationPreference', 'notificationPreference')
                ->get(['id', 'first_name', 'middle_name', 'last_name', 'email', 'phone_no', 'parent_id']);
            Notification::send($users, new ExamSaved(Batch::find($exam->batch_id), $exam, $papers, $totalScore));
            $users->filter(function ($value, $key) {
                return !is_null($value['parent']);
            })->each(function ($item) use ($exam, $papers, $totalScore) {
                Notification::send($item['parent'], new ExamSaved(Batch::find($exam->batch_id), $exam, $papers, $totalScore, $item));
            });
        } else {
            $exam->due_date = $exam->papers[0]->due_date;
            $users = User::whereBatchId($exam->batch_id)->with('parent:id,first_name,middle_name,last_name,email,phone_no',
                'parent.notificationPreference', 'notificationPreference')
                ->get(['id', 'first_name', 'middle_name', 'last_name', 'email', 'phone_no', 'parent_id']);
            Notification::send($users, new ExamSaved(Batch::find($exam->batch_id), $exam));
            $batchData = Batch::find($exam->batch_id);
            $users->filter(function ($value, $key) {
                return !is_null($value['parent']);
            })->each(function ($item) use ($batchData, $exam) {
                Notification::send($item['parent'], new ExamSaved($batchData, $exam, null, null, $item));
            });
            unset($exam->due_date);
        }

    }
}