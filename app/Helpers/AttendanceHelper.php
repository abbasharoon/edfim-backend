<?php

namespace App\Helpers;

use App\AttendanceAbsent;
use App\Notifications\MarkedAbsent;
use App\Notifications\MarkedPresent;
use App\User;
use Illuminate\Support\Facades\Notification;

class AttendanceHelper
{
    public static function notify($attendance, $absentIds = null)
    {
        if (!$absentIds) {
            $absentees = AttendanceAbsent::whereAttendanceId($attendance->id)
                ->whereHas('reason',function ($query) {
                    $query->where('notify', 1);
                    $query->orWhere('notify', true);
                })->get();
            $absentIds = $absentees->pluck('user_id')->toArray();
        }
        if ($absentIds && !empty($absentIds) && count($absentIds) > 0) {
            $absentStudents = User::whereBatchId($attendance->batch_id)
                ->whereIn('id', $absentIds)
                ->with('parent:id,first_name,middle_name,last_name,email,phone_no',
                    'parent.notificationPreference', 'notificationPreference')
                ->get(['id', 'first_name', 'middle_name', 'last_name', 'email', 'phone_no', 'parent_id']);
//            Notification::send($absentStudents, new MarkedAbsent($attendance));
            $absentStudents->filter(function ($value, $key) use ($attendance) {
//                $value->notify(new MarkedAbsent($attendance));
                return !is_null($value['parent']);
            })->each(function ($item) use ($attendance) {
                if ($item['parent']) {
                    Notification::send($item['parent'], new MarkedAbsent($attendance, null, $item));
                }
            });
        }
        if (setting('notification.sms.present') == 1) {
            AttendanceHelper::notifyPresent($attendance);
        }
    }


    public static function notifyPresent($attendance, $presentStudents = null)
    {
        if (setting('notification.sms.present') == 1) {
            if (!$presentStudents) {
                $absentIds = AttendanceAbsent::whereAttendanceId($attendance->id)->get()->pluck('user_id');
                $presentStudents = User::whereBatchId($attendance->batch_id)
                    ->whereNotIn('id', $absentIds)
                    ->with('parent:id,first_name,middle_name,last_name,email,phone_no',
                        'parent.notificationPreference', 'notificationPreference')
                    ->get(['id', 'first_name', 'middle_name', 'batch_id', 'last_name', 'parent_id']);

            }
            $presentStudents->filter(function ($value, $key) {
                return !is_null($value['parent']);
            })->each(function ($item) use ($attendance) {
                if ($item['parent']) {
                    Notification::send($item['parent'], new MarkedPresent($attendance, $item));
                }
            });
        }
    }
}