<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;

class Helper
{

    /**
     * Store an image to a specified folder with unique identifier
     *
     * @param string $imageData Base 64 Image Data
     *
     * @return string|null Either returns file name or null
     */
    public static function store_image($imageData, $oldImageName = null, $name = null)
    {
        $encodedImage = str_replace('data:image/png;base64,', '', $imageData);
        $encodedImage = str_replace(' ', '+', $encodedImage);
        $img = base64_decode($encodedImage);
//        @todo Debug as null is assigned for stored files
        $extension = explode('/', (finfo_buffer(finfo_open(), $img, FILEINFO_MIME_TYPE)))[1];
        $fileName = uniqid() . '.' . $extension;
        if (isset($name)) {
            $fileName = $name;
        }
        if (isset($imageData) && Storage::put('public' . DIRECTORY_SEPARATOR . config('app.tenant')->hostname . DIRECTORY_SEPARATOR . $fileName, $img)) {
            if (isset($oldImageName)) {
                Storage::delete('public' . DIRECTORY_SEPARATOR . config('app.tenant')->hostname . DIRECTORY_SEPARATOR . $oldImageName);
            }
            return $fileName;
        } else return null;
    }

    public static function saveLogoBase64($data)
    {
        Storage::put('public' . DIRECTORY_SEPARATOR . config('app.tenant')->hostname . DIRECTORY_SEPARATOR . 'logo.base', $data);
    }
}