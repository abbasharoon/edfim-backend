<?php

namespace App\Helpers;

use App\Classes;
use App\ClassFee;
use App\ClassInvoice;
use App\ClassInvoiceFee;
use App\Fee;
use App\Notifications\InvoiceDue;
use App\Notifications\InvoiceUpdated;
use App\User;
use App\UserFee;
use App\UserInvoice;
use App\UserInvoiceFee;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Notification;

class InvoiceHelper
{

    public static function createClassInvoice($input)
    {
        if (!$input->schedule) {
            $input->schedule = 3;
        }

        $class = Classes::findOrFail($input->class_id);
        if ($input->exists('fee')) {
            $fee = Collection::make($input->fee)->map(function ($data) {
                $data['tenant_id'] = config('app.tenant')->tenant_id;
                $data['target_fee_id'] = array_key_exists('target_fee_id', $data) ? $data['target_fee_id'] : null;
                return $data;
            });
        } else {

            $fee = ClassFee::where('class_id', $input->class_id)
                ->whereHas('fee', function ($query) use ($input) {
                    $query->whereIn('schedule', [0, $input->schedule]);
                })->get(['fee_id', 'target_fee_id', 'tenant_id'])
                ->makeVisible('tenant_id');

        }

        if (count($fee->toArray()) === 0) {
            return false;
        }


//        Pluck all the ids of fees for querying details
        $feeIds = $fee->map(function ($data) {
            $returnArray = [];
            array_push($returnArray, $data['fee_id']);
            if (array_key_exists('target_fee_id', $data)) {
                array_push($returnArray, $data['target_fee_id']);
            }
            return $returnArray;
        })->flatten()->toArray();


//        dd($feeIds);
        $invoice = new ClassInvoice();
        $invoice->amount_paid = 0;
        $invoice->class_id = $input->class_id;
        $invoice->status = $input->status ? $input->status : 0;
        $invoice->schedule = $input->schedule;
        $invoice->schedule_duration = $input->schedule_duration;
        $invoice->due_date = $input->due_date;
        $invoice->save();

        $fee = Collection::make($fee)->map(function ($data) use ($invoice) {
            $data['class_invoice_id'] = $invoice->id;
            return $data;
        });
        ClassInvoiceFee::insert($fee->toArray());
//        $invoice->pivotFee()->attach($fee->toArray());

        $students = $class->load(['student:users.id,first_name,middle_name,last_name,email,phone_no',
            'student.parent',
            'student.parent.notificationPreference',
            'student.notificationPreference',
            'student.fee' => function ($q) use ($input) {
                if ($input['schedule']) {
                    $q->whereHas('fee', function ($sq) use ($input) {
                        $sq->whereIn('schedule', [0, $input['schedule']]);
                    });
                }
                $q->select(['user_id', 'fee_id', 'target_fee_id', 'tenant_id']);
            }]);
        if ($students->student) {
            $studentFeeIds = $students->student->map(function ($item) {

                return collect($item->fee)->map(function ($i) {
                    return [$i['fee_id'], $i['target_fee_id']];
                })->flatten()->toArray();
            })->flatten()->concat($feeIds)->unique()->filter()->toArray();
            $feeData = Fee::whereIn('id', $studentFeeIds)->get()->keyBy('id')->toArray();

            foreach ($students->student as $k => $v) {
                InvoiceHelper::createStudentInvoice($v,
                    $feeData,
                    $fee,
                    $invoice);
            }
        }
        return $invoice;
    }


    public static function createStudentInvoice($input, $feeData, $classFee = null, $classInvoice = null)
    {
        $fee = $input->fee;
        if ($classFee) {
            $fee = $fee->concat($classFee->map(function ($item) {
                unset($item['class_invoice_id']);
                return $item;
            }));
        }
        $fines = [];
        //@todo Test the fine functionality
//        @todo Refactor this shit
        $fee = collect($fee->toArray())->map(function ($data) use ($feeData, &$fines) {
            $data['tenant_id'] = config('app.tenant')->tenant_id;
            $data['target_fee_id'] = array_key_exists('target_fee_id', $data) ? $data['target_fee_id'] : null;
            if ($feeData[$data['fee_id']]['fee_category_id'] === setting()->get('fine.fee_category_id')) {
                array_push($fines, $data['fee_id']);
            }
            unset($data['user_id']);
            if (!isset($data['target_fee_id'])) {
                $data['target_fee_id'] = null;
            }
            return $data;
        });

        if (count($fines) > 0) {
            UserFee::whereIn('fee_id', $fines)->where('user_id', $input->user->id)->delete();
        }
        $invoice = new UserInvoice;
        $invoice->user_id = $input->id;
        $invoice->class_invoice_id = $classInvoice ? $classInvoice->id : null;
        $invoice->amount_paid = 0;
        $invoice->class_id = $classInvoice ? $classInvoice->class_id : $input->class_id;
        $invoice->status = $input->status ? $input->status : 0;
        $invoice->schedule = $classInvoice ? $classInvoice->schedule : $input->schedule;
        $invoice->schedule_duration = $classInvoice ? $classInvoice->schedule_duration : $input->schedule_duration;
        $invoice->due_date = $classInvoice ? $classInvoice->due_date : $input->due_date;
        $invoice->total_amount = 0;

        $invoiceData = InvoiceHelper::invoiceMaker($fee, $feeData);
        $invoice->total_amount = $invoiceData['total'];
        $invoice->save();

        $fee = Collection::make($fee)->map(function ($data) use ($invoice) {
            $data['user_invoice_id'] = $invoice->id;
            return $data;
        });

        UserInvoiceFee::insert($fee->toArray());
//        $invoice->pivotFee()->attach($fee->toArray());

        if ($invoice->status !== 0) {
            Notification::send($input, new InvoiceDue($invoiceData, $invoice));
            if ($input->parent) {
                Notification::send($input->parent, new InvoiceDue($invoiceData, $invoice, $input));
            }
        }
        return $invoice->load('fee');
    }


    public static function updateClassInvoice($input)
    {
        $invoice = ClassInvoice::findOrFail($input->invoice_id);
        $invoice->amount_paid = is_numeric($input->amount_paid) ? $input->amount_paid : $invoice->amount_paid;
        $invoice->status = ($input->status || $input->status == 0) ? $input->status : $invoice->status;
        $invoice->due_date = $input->due_date ? $input->due_date : $invoice->due_date;
        $invoice->schedule = $input->schedule ? $input->schedule : $invoice->schedule;
        $invoice->schedule_duration = $input->schedule_duration ? $input->schedule_duration : $invoice->schedule_duration;
        $invoice->save();

        if ($input->exists('fee')) {
            $fee = Collection::make($input->fee)->map(function ($data) use ($invoice) {
                $data['tenant_id'] = config('app.tenant')->tenant_id;
                $data['class_invoice_id'] = $invoice->id;
                $data['target_fee_id'] = array_key_exists('target_fee_id', $data) ? $data['target_fee_id'] : null;
                return $data;
            });
            $invoice->pivotFee()->detach();
//            $invoice->pivotFee()->attach($fee->keyBy('fee_id')->toArray());
            ClassInvoiceFee::insert($fee->toArray());
        }


        $studentInvoices = $invoice->load(['userInvoice' => function ($q) {
//            $q->where('status', 1);
        }, 'userInvoice.student',
            'userInvoice.student.parent',
            'userInvoice.student.notificationPreference',
            'userInvoice.student.parent.notificationPreference',
            'userInvoice.student.fee' => function ($q) use ($invoice) {
                if ($invoice['schedule']) {
                    $q->whereHas('fee', function ($sq) use ($invoice) {
                        $sq->whereIn('schedule', [0, $invoice['schedule']]);
                    });
                }
                $q->select(['user_id', 'fee_id', 'target_fee_id', 'tenant_id']);
            }]);
        foreach ($studentInvoices->userInvoice as $k => $v) {
            InvoiceHelper::updateStudentInvoice($input, $v->id, $v->student);
        }


        return $invoice;

    }

    public static function updateStudentInvoice($input, $invoiceId, $user = null)
    {
        $invoice = UserInvoice::findOrFail($invoiceId);
        $invoice->amount_paid = $input->amount_paid ? $input->amount_paid : $invoice->amount_paid;
        $invoiceStatus = $invoice->status;
        $invoice->status = (($input->status || $input->status == 0) && !$user) ? $input->status : $invoice->status;
        $invoice->due_date = $input->due_date ? $input->due_date : $invoice->due_date;
        $invoice->schedule = $input->schedule ? $input->schedule : $invoice->schedule;
        $invoice->schedule_duration = $input->schedule_duration ? $input->schedule_duration : $invoice->schedule_duration;


        if ($input->exists('fee')) {
            $fee = Collection::make($input->fee)->map(function ($data) use ($invoice) {
                $data['tenant_id'] = config('app.tenant')->tenant_id;
                $data['user_invoice_id'] = $invoice->id;
                $data['target_fee_id'] = array_key_exists('target_fee_id', $data) ? $data['target_fee_id'] : null;
                return $data;
            });
            $invoice->pivotFee()->detach();
//            $invoice->pivotFee()->attach($fee->keyBy('fee_id')->toArray());
            if ($user) {
                $fee->concat($user->fee);
            }
            UserInvoiceFee::insert($fee->toArray());


            $studentFeeIds = collect($fee)->map(function ($i) {
                return [$i['fee_id'], $i['target_fee_id']];
            })->flatten()->toArray();;
            $feeData = Fee::whereIn('id', $studentFeeIds)->get()->keyBy('id')->toArray();
            $invoiceData = InvoiceHelper::invoiceMaker($fee, $feeData);
            $invoice->total_amount = $invoiceData['total'];
        }

        $invoice->save();


        if ($invoice->status !== 0 && $invoice->status != $invoiceStatus) {
            if (!$user) {
                $user = User::with('parent', 'parent.notificationPreference', 'notificationPreference')
                    ->findOrFail($invoice->user_id);
            }
            Notification::send($user, new InvoiceUpdated($invoice, $input->status));
            if ($user->parent) {
                Notification::send($user->parent, new InvoiceUpdated($invoice, $input->status), $user);
            }
        }
        return $invoice;
    }

    public static function invoiceMaker($fees, $feeData)
    {
        $fees = collect($fees);
        $targetFees = $fees->groupBy('target_fee_id');
        $invoice = [];
        $total = 0;
        foreach ($fees as $k => $v) {
            if (!$v['target_fee_id']) {
                $fee = $feeData[$v['fee_id']];
                if ($fee['type'] == 1) {
//                    Create an invoice item
                    $invoiceItem = [];
                    $invoiceItem['id'] = $fee['id'];
                    $invoiceItem['name'] = $fee['name'];
//                    Set it's total amount to either positive or negative to be displayed
                    $invoiceItem['amount'] = $fee['amount'];
//                   Set this amount to just the number to be calculated for related fees
                    $itemTotal = $fee['amount'];
//                    Check if target fees exists for the current fee
                    if (!empty($targetFees[$fee['id']])) {
//                        If yes then loop over them
                        $invoiceItem['target_fee'] = [];
                        foreach ($targetFees[$fee['id']] as $t) {
                            $tf = $feeData[$t['fee_id']];
                            $targetItem = [];
                            $targetItem['id'] = $tf['id'];
                            $targetItem['name'] = $tf['name'];
                            $targetItem['amount'] = $tf['amount'];
                            $tIAmount = $tf['amount'];
                            if ($tf['type'] == 2) {
                                $tIAmount = (($fee['amount'] / 100) * $tIAmount['amount']);
                                $targetItem['amount'] .= '%';
                                $targetItem['percentage_amount'] = $tIAmount;
                            }
//                            if ($tf['addition'] == 0) {
//                                $itemTotal = $itemTotal - $tIAmount;
//                                $targetItem['amount'] = '-' . $targetItem['amount'];
//                            } else {
                            $itemTotal = $itemTotal + $tIAmount;
//                            }
                            array_push($invoiceItem['target_fee'], $targetItem);
                        }
                    }
//                    if ($fee['addition'] == 0) {
//                        $total = $total - $itemTotal;
//                        $invoiceItem['amount'] = '-' . $invoiceItem['amount'];
//                    } else {
                    $total = $total + $itemTotal;
//                    }
                    array_push($invoice, $invoiceItem);
                }
            }
        }
        $totalForPercentage = $total;
//        Apply percentages

        foreach ($fees as $k => $v) {
            if (!$v['target_fee_id']) {
                $fee = $feeData[$v['fee_id']];
                if ($fee['type'] == 2) {
//                    Create an invoice item
                    $invoiceItem = [];
                    $invoiceItem['id'] = $fee['id'];
                    $invoiceItem['name'] = $fee['name'];
//                    Set it's total amount to either positive or negative to be displayed
                    $invoiceItem['amount'] = $fee['amount'] . '%';
//                   Set this amount to just the number to be calculated for related fees
                    $itemTotal = ($fee['amount'] / 100) * $totalForPercentage;
                    $invoiceItem['percentage_amount'] = $itemTotal;
//                    Check if target fees exists for the current fee
                    if (!empty($targetFees[$fee['id']])) {
//                        If yes then loop over them
                        $invoiceItem['target_fee'] = [];
                        foreach ($targetFees[$fee['id']] as $t) {
                            $tf = $feeData[$t['fee_id']];
                            $targetItem = [];
                            $targetItem['id'] = $tf['id'];
                            $targetItem['name'] = $tf['name'];
                            $targetItem['amount'] = $tf['amount'];
                            $tIAmount = $tf['amount'];
                            if ($tf['type'] == 2) {
                                $tIAmount = (($fee['amount'] / 100) * $tIAmount['amount']);
                                $targetItem['amount'] .= '%';
                                $targetItem['percentage_amount'] = $tIAmount;
                            }
//                            if ($tf['addition'] == 0) {
//                                $itemTotal = $itemTotal - $tIAmount;
//                                $targetItem['amount'] = -$targetItem['amount'];
//                            } else {
                            $itemTotal = $itemTotal + $tIAmount;
//                            }
                            array_push($invoiceItem['target_fee'], $targetItem);
                        }
                    }
//                    if ($fee['addition'] == 0) {
//                        $total = $total - $itemTotal;
//                        $invoiceItem['amount'] = '-' . $invoiceItem['amount'];
//                    } else {
                    $total = $total + $itemTotal;
//                    }
                    array_push($invoice, $invoiceItem);
                }
            }
        }
        return ['invoices' => $invoice, 'total' => $total];
    }
}