<?php

namespace App\Helpers;

use App\Notifications\ClassChanged;
use App\User;
use Illuminate\Support\Facades\Notification;

class ClassHelper
{
    public static function notifyClassChange($batch)
    {
        $users = User::whereBatchId($batch->id)->with('parent:id,first_name,middle_name,last_name,email,phone_no',
            'parent.notificationPreference', 'notificationPreference')->get();
        Notification::send($users, new ClassChanged($batch));
        $users->filter(function ($value, $key) {
            return !is_null($value['parent']);
        })->each(function ($item) use ($batch) {
            $parent = $item['parent'];
            $parent['child'] = $item;
            Notification::send($parent, new ClassChanged($batch, $item));
        });

    }

    public function batchStudentCount()
    {

    }
}