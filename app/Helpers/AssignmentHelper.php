<?php

namespace App\Helpers;

use App\AssignmentScore;
use App\Batch;
use App\Notifications\AssignmentSaved;
use App\User;
use Illuminate\Support\Facades\Notification;

class AssignmentHelper
{
    public static function notify($assignment, $scoreData = null)
    {
        $scoreKeys = [];
        if ($scoreData) {
            $scoreData = collect($scoreData)->sortBy('score')->keyBy('user_id');
            $scoreKeys = $scoreData->sortByDesc('score')->pluck('score')->unique()->values()->toArray();
            $users = User::whereIn('id', $scoreData->pluck('user_id'));
        } elseif ($assignment->status === 2) {
            $scoreData = AssignmentScore::whereAssignmentId($assignment->id)->get();
            if (!$scoreData) {
                return;
            }
            $scoreData = $scoreData->sortBy('score')->keyBy('user_id');
            $scoreKeys = $scoreData->unique()->sortByDesc('score')->pluck('score')->unique()->values()->toArray();
            $users = User::whereIn('id', $scoreData->pluck('user_id'));
        } else {
            $users = User::whereBatchId($assignment->batch_id);
        }
        $batch = Batch::findOrFail($assignment->batch_id);
        $batch->class;
        $users = $users->with('parent:id,first_name,middle_name,last_name,email,phone_no',
            'parent.notificationPreference', 'notificationPreference')
            ->get(['id', 'first_name', 'middle_name', 'last_name', 'email', 'phone_no', 'parent_id']);
        Notification::send($users, new AssignmentSaved($assignment, $batch, $scoreData, $scoreKeys));
        $users->filter(function ($value, $key) {
            return !is_null($value['parent']);
        })->each(function ($item) use ($assignment, $batch, $scoreData, $scoreKeys) {
            if ($item['parent']) {
                Notification::send($item['parent'], new AssignmentSaved($assignment, $batch, $scoreData, $scoreKeys, $item));
            }
        });
    }

}