<?php

namespace App\Helpers;

use App\Channels\ZongSMSChannel;

class NotificationHelper
{
    public static function makeVia($notifiable, $smsType = null, $database = true, $email = true)
    {


        $via = ['database'];
        if (!$database) {
            $via = [];
        }


        if ($notifiable->notificationPreference->email
            && $notifiable->email
            && setting('email_count') > 0 && $email===true) {
            array_push($via, 'mail');
        }
        if ($notifiable->notificationPreference->sms
            && $notifiable->phone_no
            && setting('sms.count') > 0
            && (!$smsType || setting('notification.sms.' . $smsType) == 1)) {
            if (setting('sms.sender') == 'zong') {
                array_push($via, ZongSMSChannel::class);
            }
        }
        return $via;
    }

    public static function addressBy($notifiable, $verb = 'has', $own = null)
    {
        $verbs = [
            'is' => 'are',
            'has' => 'have',
            'was' => 'were',
        ];

        if ($notifiable->child) {
            if ($own) {
                return NotificationHelper::makeName($notifiable->child) . '\'s';
            }
            return NotificationHelper::makeName($notifiable->child) . ' ' . $verb;
        } else {
            if ($own) {
                return 'Your';
            }
            return 'You ' . $verbs[$verb];
        }
    }

    public static function makeFor($notifiable)
    {
        return $notifiable->child ? NotificationHelper::makeName($notifiable->child) : 'you';
    }

    public static function makeName($notifiable)
    {
        return $notifiable->first_name . ' '
            . $notifiable->middle_name . ' '
            . $notifiable->last_name;
    }
}