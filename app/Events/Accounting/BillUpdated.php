<?php

namespace App\Accounting\Events;

class BillUpdated
{
    public $bill;

    /**
     * Create a new event instance.
     *
     * @param $bill
     */
    public function __construct($bill)
    {
        $this->bill = $bill;
    }
}
