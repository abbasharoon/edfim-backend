<?php

namespace App\Accounting\Events;

class PaymentGatewayListing
{
    public $gateways;

    /**
     * Create a new event instance.
     *
     * @param $gateways
     */
    public function __construct($gateways)
    {
        $this->gateways = $gateways;
    }
}
