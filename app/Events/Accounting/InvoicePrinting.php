<?php

namespace App\Events\Accounting;

class InvoicePrinting
{
    public $invoice;

    /**
     * Create a new event instance.
     *
     * @param $invoice
     */
    public function __construct($invoice)
    {
        $this->invoice = $invoice;
    }
}
