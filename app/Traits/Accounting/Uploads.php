<?php

namespace App\Traits\Accounting;

use MediaUploader;

trait Uploads
{

    public function getUploadedFilePath($file, $folder = 'settings', $company_id = null)
    {
        $path = '';

        if (!$file || !$file->isValid()) {
            return $path;
        }

        if (!$company_id) {
            $company_id = session('company_id');
        }

        $file_name = $file->getClientOriginalName();

        // Upload file
        $file->storeAs($company_id . '/' . $folder, $file_name);

        // Prepare db path
        $path = $folder . '/' . $file_name;

        return $path;
    }

    public function getMedia($file, $tenant_id = null,$subDirectory=null)
    {
        $path = '';

        if (!$file || !$file->isValid()) {
            return $path;
        }


        if (!$tenant_id) {
            $tenant_id = config('app.tenant')->tenant_id;
        }

        $path = config('app.tenant')->hostname;
        if(is_string($file)){
            return MediaUploader::fromSource($file);
        }
        return MediaUploader::fromSource($file)->useHashForFilename()->toDirectory($path)->upload();
    }
}
