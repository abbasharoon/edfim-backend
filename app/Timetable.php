<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;

class Timetable extends Model
{
    use BelongsToTenants;

    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'timetables';
    protected $hidden = ['tenant_id'];

    /**
     * Return the associated class with the Schedule
     *
     * @return object
     */
    public function classTimetable()
    {
        return $this->belongsToMany('App\Classes',
            'class_timetable',
            'timetable_id',
            'class_id');
    }
}