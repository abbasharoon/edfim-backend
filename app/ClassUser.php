<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassUser extends Model
{
    public $timestamps = false;

    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'class_user';

    /**
     * The attributes that are mass-assignable
     *
     * @var [type]
     */
    protected $fillable = ['user_id'];

}
