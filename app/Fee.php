<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fee extends Model
{
    use BelongsToTenants, softDeletes;

    /**
     * The attributes that should be mutated to dates
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $hidden = ['tenant_id'];

    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'fees';

    public function category()
    {
        return $this->belongsTo('App\FeeCategory', 'fee_category_id');
    }

    public function classFee()
    {
        return $this->belongsToMany('App\Classes', 'class_fee', 'fee_id', 'class_id');
    }

    public function userFee()
    {
        return $this->belongsToMany('App\User', 'user_fee', 'fee_id', 'user_id');
    }

    public function class()
    {
        return $this->hasMany('App\ClassFee');
    }

    public function user()
    {
        return $this->hasMany('App\UserFee');
    }


    public function oldFee()
    {
        return $this->hasMany('App\Fee', 'old_fee_id', 'old_fee_id');
    }

    public function getAmountAttribute($value)
    {
        return strpos($value, '.') !== false ? floatval(rtrim(rtrim($value, '0'), '.')) : floatval($value);
    }

}
