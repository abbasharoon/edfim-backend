<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReputationBadge extends Model
{
    use BelongsToTenants, softDeletes;

    /**
     * The attributes that should be mutated to dates
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $hidden = ['tenant_id', 'deleted_at'];

    public function reputation()
    {
        return $this->hasMany('App\Reputation','badge_id','id');
    }
}
