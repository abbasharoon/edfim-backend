<?php

namespace App\Models\Accounting\DoubleEntry;

use App\Models\Accounting\AccountingModel;

class Type extends AccountingModel
{

    protected $table = 'account_types';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['key', 'name', 'type', 'tenant_id'];


    public function accounts()
    {
        return $this->hasMany('App\Models\Accounting\DoubleEntry\Account');
    }

    public function declass()
    {
        return $this->belongsTo('App\Models\Accounting\DoubleEntry\DEClass');
    }
}

//    "hipsterjazzbo/landlord": "^2.0",
