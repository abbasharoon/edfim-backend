<?php

namespace App\Models\Accounting\DoubleEntry;

use App\Models\Accounting\AccountingModel;

class AccountBank extends AccountingModel
{

    protected $table = 'account_bank';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['company_id', 'account_id', 'bank_id'];

    public function account()
    {
        return $this->belongsTo('App\Models\Accounting\DoubleEntry\Account', 'account_id', 'id');
    }

    public function bank()
    {
        return $this->belongsTo('App\Models\Accounting\Banking\Account', 'bank_id', 'id');
    }
}
