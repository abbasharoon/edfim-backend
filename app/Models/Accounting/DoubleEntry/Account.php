<?php

namespace App\Models\Accounting\DoubleEntry;

use App\Models\Accounting\AccountingModel;

class Account extends AccountingModel
{

    protected $table = 'accounts';

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['debit_total', 'credit_total'];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['tenant_id', 'type_id', 'code', 'name', 'description', 'parent', 'system', 'enabled'];

    public function type()
    {
        return $this->belongsTo('App\Models\Accounting\DoubleEntry\Type');
    }

    public function bank()
    {
        return $this->belongsTo('App\Models\Accounting\DoubleEntry\AccountBank', 'id', 'account_id');
    }

    public function tax()
    {
        return $this->belongsTo('App\Models\Accounting\DoubleEntry\AccountTax', 'id', 'account_id');
    }

    public function ledgers()
    {
        return $this->hasMany('App\Models\Accounting\DoubleEntry\Ledger');
    }

    /**
     * Scope code.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $code
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCode($query, $code)
    {
        return $query->where('code', $code);
    }

    /**
     * Get the name including rate.
     *
     * @return string
     */
    public function getDebitTotalAttribute()
    {
        return $this->ledgers()->sum('debit');
    }

    /**
     * Get the name including rate.
     *
     * @return string
     */
    public function getCreditTotalAttribute()
    {
        return $this->ledgers()->sum('credit');
    }
}
