<?php

namespace App\Models\Accounting\DoubleEntry;

use App\Models\Accounting\AccountingModel;

class DEClass extends AccountingModel
{

    protected $table = 'double_entry_classes';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new Company);
    }

    public function types()
    {
        return $this->hasMany('Modules\DoubleEntry\Models\Type', 'class_id');
    }

    public function accounts()
    {
        return $this->hasManyThrough('Modules\DoubleEntry\Models\Account', 'Modules\DoubleEntry\Models\Type', 'class_id', 'type_id');
    }
}
