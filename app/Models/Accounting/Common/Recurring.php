<?php

namespace App\Models\Accounting\Common;

use App\Models\Accounting\AccountingModel;
use App\Traits\Accounting\Recurring as RecurringTrait;

class Recurring extends AccountingModel
{
    use RecurringTrait;

    protected $table = 'recurring';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['tenant_id', 'recurable_id', 'recurable_type', 'frequency', 'interval', 'started_at', 'count'];


    /**
     * Get all of the owning recurable models.
     */
    public function recurable()
    {
        return $this->morphTo();
    }
}
