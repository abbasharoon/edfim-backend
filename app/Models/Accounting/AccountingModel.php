<?php

namespace App\Models\Accounting;

//use App\Scopes\Company;
use EloquentFilter\Filterable;
use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use Request;
use Route;

class AccountingModel extends Eloquent
{
    use Filterable, SoftDeletes, Sortable, BelongsToTenants;

    protected $dates = ['deleted_at'];
    protected $connection = 'accounting';

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

//        static::addGlobalScope(new Company);
    }

    /**
     * Global company relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('App\Models\Accounting\Common\Company');
    }

    /**
     * Define the filter provider globally.
     *
     * @return ModelFilter
     */
    public function modelFilter()
    {
        // Check if is api or web
        if (Request::is('api/*') && true == false) {
            $arr = array_reverse(explode('\\', explode('@', app()['api.router']->currentRouteAction())[0]));
            $folder = $arr[1];
            $file = $arr[0];
        } else {
            $routeParts = explode('api/', Route::current()->uri());
            if ($routeParts['1']) {
                list($folder, $file) = explode('/', $routeParts[1]);
            } else {
                list($folder, $file) = explode('/', $routeParts[2]);
            }

        }
        if (empty($folder) || empty($file)) {
            return $this->provideFilter();
        }

        $class = '\App\Filters\Accounting\\' . ucfirst($folder) . '\\' . ucfirst($file);

        return $this->provideFilter($class);
    }

    /**
     * Scope to only include company data.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $company_id
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCompanyId($query, $company_id)
    {
        return $query->where($this->table . '.company_id', '=', $company_id);
    }

    /**
     * Scope to get all rows filtered, sorted and paginated.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $sort
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCollect($query, $sort = 'name')
    {
        $request = request();
        if ($request->sort_direction) {
            $sort = [];
            $sort [$request->sort_column] = $request->sort_direction;
        }

        $input = $request->input();
        $limit = $request->get('page_size', 30);

        return $query->filter($input)->sortable($sort)->paginate($limit);
    }

    /**
     * Scope to only include active models.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeEnabled($query)
    {
        return $query->where('enabled', 1);
    }

    /**
     * Scope to only include passive models.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDisabled($query)
    {
        return $query->where('enabled', 0);
    }

    /**
     * Scope to only include reconciled models.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $value
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeReconciled($query, $value = 1)
    {
        return $query->where('reconciled', $value);
    }

    public function scopeAccount($query, $accounts)
    {
        if (empty($accounts)) {
            return;
        }

        return $query->whereIn('account_id', (array)$accounts);
    }

    public function scopeCustomer($query, $customers)
    {
        if (empty($customers)) {
            return;
        }

        return $query->whereIn('tenant_id', (array)$customers);
    }

    public function scopeVendor($query, $vendors)
    {
        if (empty($vendors)) {
            return;
        }

        return $query->whereIn('vendor_id', (array)$vendors);
    }
}
