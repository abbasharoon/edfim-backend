<?php

namespace App\Models\Accounting\Income;

use App\Models\Accounting\AccountingModel;

class InvoiceTax extends AccountingModel
{
    protected $fillable = ['tenant_id', 'tax_id', 'name', 'rate','taxable_amount','tax_amount','invoice_id'];
}
