<?php

namespace App\Models\Accounting\Income;

use App\Models\Accounting\AccountingModel;
use App\Traits\Accounting\Currencies;

class InvoiceItem extends AccountingModel
{

    use Currencies;

    protected $table = 'invoice_items';

    protected $appends = ['item_object', 'tax_object'];
    protected $hidden = ['item_object', 'tax_object'];
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['tenant_id', 'invoice_id', 'item_id', 'name', 'description', 'rate', 'quantity', 'amount', 'tax_id'];


    public function invoice()
    {
        return $this->belongsTo('App\Models\Accounting\Income\Invoice');
    }

    public function item()
    {
        return $this->belongsTo('App\Models\Accounting\Common\Item');
    }

    public function taxes()
    {
        return $this->hasMany('App\Models\Accounting\Income\InvoiceItemTax', 'invoice_item_id', 'id');
    }

    public function tax()
    {
        return $this->belongsTo('App\Models\Accounting\Setting\Tax', 'tax_id', 'id');
    }

    public function getItemObjectAttribute()
    {
        if ($this->item_id) {
            return $this->item;
        } else {
            return [
                'name' => $this->name,
            ];
        }
    }

    public function getTaxObjectAttribute()
    {
        return $this->tax;
    }

    /**
     * Convert price to double.
     *
     * @param  string $value
     *
     * @return void
     */
    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = (double)$value;
    }

    /**
     * Convert total to double.
     *
     * @param  string $value
     *
     * @return void
     */
    public function setTotalAttribute($value)
    {
        $this->attributes['total'] = (double)$value;
    }

    /**
     * Convert tax to double.
     *
     * @param  string $value
     *
     * @return void
     */
    public function setTaxAttribute($value)
    {
        $this->attributes['tax'] = (double)$value;
    }
}
