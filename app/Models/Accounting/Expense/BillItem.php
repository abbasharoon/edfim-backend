<?php

namespace App\Models\Accounting\Expense;

use App\Models\Accounting\AccountingModel;
use App\Traits\Accounting\Currencies;

//use App\Traits\Currencies;

class BillItem extends AccountingModel
{

    use Currencies;

    protected $table = 'bill_items';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['tenant_id', 'bill_id', 'item_id', 'name', 'sku', 'quantity', 'price', 'total', 'tax'];

    public function bill()
    {
        return $this->belongsTo('App\Models\Accounting\Expense\Bill');
    }

    public function item()
    {
        return $this->belongsTo('App\Models\Accounting\Common\Item');
    }

    public function taxes()
    {
        return $this->hasMany('App\Models\Accounting\Expense\BillItemTax', 'bill_item_id', 'id');
    }

    /**
     * Convert price to double.
     *
     * @param  string $value
     *
     * @return void
     */
    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = (double)$value;
    }

    /**
     * Convert total to double.
     *
     * @param  string $value
     *
     * @return void
     */
    public function setTotalAttribute($value)
    {
        $this->attributes['total'] = (double)$value;
    }

    /**
     * Convert tax to double.
     *
     * @param  string $value
     *
     * @return void
     */
    public function setTaxAttribute($value)
    {
        $this->attributes['tax'] = (double)$value;
    }
}
