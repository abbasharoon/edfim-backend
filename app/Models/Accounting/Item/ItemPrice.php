<?php

namespace App\Models\Accounting\Item;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;

class ItemPrice extends Model
{
    use BelongsToTenants;

    protected $connection = 'accounting';
    public $timestamps = false;


    protected $fillable = ['tenant_id', 'description', 'type', 'tax_inclusive', 'tax_id', 'account_id',
        'user_id', 'amount', 'item_id'];
    protected $table = 'item_prices';

}
