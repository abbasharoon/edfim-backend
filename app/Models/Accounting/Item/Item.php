<?php

namespace App\Models\Accounting\Item;

/**
 * @deprecated since 1.2.7 version. use Common\Item instead.
 */
class Item extends \App\Models\Accounting\Common\Item {}
