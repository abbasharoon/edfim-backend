<?php

namespace App\Models\Accounting\Setting;

use App\Models\Accounting\AccountingModel;

class Tax extends AccountingModel
{

    protected $table = 'taxes';

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['title'];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['tenant_id', 'name', 'sales_rate', 'sales_account', 'purchase_rate', 'purchase_account', 'type', 'enabled', 'description','tax_agency_id'];

    /**
     * Sortable columns.
     *
     * @var array
     */
    public $sortable = ['name', 'rate', 'enabled'];

    public function items()
    {
        return $this->hasMany('App\Models\Common\Item');
    }

    public function taxAgency()
    {
        return $this->belongsTo('App\Models\Accounting\Setting\TaxAgency');
    }

    public function bill_items()
    {
        return $this->hasMany('App\Models\Expense\BillItemTax');
    }

    public function invoice_items()
    {
        return $this->hasMany('App\Models\Income\InvoiceItemTax');
    }

    /**
     * Convert rate to double.
     *
     * @param  string $value
     *
     * @return void
     */
    public function setRateAttribute($value)
    {
        $this->attributes['rate'] = (double)$value;
    }

    /**
     * Get the name including rate.
     *
     * @return string
     */
    public function getTitleAttribute()
    {
        $title = $this->name . ' (';

        if (setting('general.percent_position', 'after') == 'after') {
            $title .= $this->rate . '%';
        } else {
            $title .= '%' . $this->rate;
        }

        $title .= ')';

        return $title;
    }
}
