<?php

namespace App\Models\Accounting\Setting;

use App\Http\Requests\Accounting\AccountingRequest;
use App\Models\Accounting\AccountingModel;

class Category extends AccountingModel
{
    protected $table = 'categories';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['tenant_id', 'name', 'type', 'color', 'enabled'];

    /**
     * Sortable columns.
     *
     * @var array
     */
    public $sortable = ['name', 'type', 'enabled'];

    public function bills()
    {
        return $this->hasMany('App\Models\Accounting\Expense\Bill');
    }

    public function invoices()
    {
        return $this->hasMany('App\Models\Accounting\Income\Invoice');
    }

    public function items()
    {
        return $this->hasMany('App\Models\Accounting\Common\Item');
    }

    public function payments()
    {
        return $this->hasMany('App\Models\Accounting\Expense\Payment');
    }

    public function revenues()
    {
        return $this->hasMany('App\Models\Accounting\Income\Revenue');
    }

    /**
     * Scope to only include categories of a given type.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeType($query, $type)
    {
        return $query->whereIn('type', (array) $type);
    }

    /**
     * Scope transfer category.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTransfer($query)
    {
        return $query->where('type', 'other')->pluck('id')->first();
    }
}
