<?php

namespace App\Models\Accounting\Setting;

use App\Models\Accounting\AccountingModel;

class TaxAgency extends AccountingModel
{

    protected $table = 'tax_agencies';


    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['tenant_id', 'name', 'registration_number', 'start_month', 'filing_frequency', 'reporting_method', 'enabled'];

    /**
     * Sortable columns.
     *
     * @var array
     */
    public $sortable = ['name', 'rate', 'enabled'];

    public function taxes()
    {
        return $this->hasMany('App\Models\Accounting\Setting\Tax');
    }

    public function accounts()
    {
        return $this->belongsToMany('App\App\Models\Accounting\DoubleEntry\Account');
//        return $this->belongsToMany('App\App\Models\Accounting\DoubleEntry\Account', 'tax_agency_account', 'account_id', 'tax_agency_id');
    }
}
