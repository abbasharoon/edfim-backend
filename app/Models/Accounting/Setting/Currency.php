<?php

namespace App\Models\Accounting\Setting;

use App\Models\Accounting\AccountingModel;

class Currency extends AccountingModel
{

    protected $table = 'currencies';
    protected $connection = 'accounting';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['tenant_id', 'name', 'code', 'rate', 'enabled', 'precision', 'symbol', 'symbol_first', 'decimal_mark', 'thousands_separator'];

    /**
     * Sortable columns.
     *
     * @var array
     */
    public $sortable = ['name', 'code', 'rate', 'enabled'];

    public function accounts()
    {
        return $this->hasMany('App\Models\Accounting\Banking\Account', 'currency_code', 'code');
    }

    public function customers()
    {
        return $this->hasMany('App\Models\Accounting\Income\Customer', 'currency_code', 'code');
    }

    public function invoices()
    {
        return $this->hasMany('App\Models\Accounting\Income\Invoice', 'currency_code', 'code');
    }

    public function invoice_payments()
    {
        return $this->hasMany('App\Models\Accounting\Income\InvoicePayment', 'currency_code', 'code');
    }

    public function revenues()
    {
        return $this->hasMany('App\Models\Accounting\Income\Revenue', 'currency_code', 'code');
    }

    public function bills()
    {
        return $this->hasMany('App\Models\Accounting\Expense\Bill', 'currency_code', 'code');
    }

    public function bill_payments()
    {
        return $this->hasMany('App\Models\Accounting\Expense\BillPayment', 'currency_code', 'code');
    }

    public function payments()
    {
        return $this->hasMany('App\Models\Accounting\Expense\Payment', 'currency_code', 'code');
    }

    /**
     * Convert rate to double.
     *
     * @param  string  $value
     * @return void
     */
    public function setRateAttribute($value)
    {
        $this->attributes['rate'] = (double) $value;
    }

    /**
     * Get the current precision.
     *
     * @return string
     */
    public function getPrecisionAttribute($value)
    {
        if (is_null($value)) {
            return config('money.' . $this->code . '.precision');
        }

        return $value;
    }

    /**
     * Get the current symbol.
     *
     * @return string
     */
    public function getSymbolAttribute($value)
    {
        if (is_null($value)) {
            return config('money.' . $this->code . '.symbol');
        }

        return $value;
    }

    /**
     * Get the current symbol_first.
     *
     * @return string
     */
    public function getSymbolFirstAttribute($value)
    {
        if (is_null($value)) {
            return config('money.' . $this->code . '.symbol_first');
        }

        return $value;
    }

    /**
     * Get the current decimal_mark.
     *
     * @return string
     */
    public function getDecimalMarkAttribute($value)
    {
        if (is_null($value)) {
            return config('money.' . $this->code . '.decimal_mark');
        }

        return $value;
    }

    /**
     * Get the current thousands_separator.
     *
     * @return string
     */
    public function getThousandsSeparatorAttribute($value)
    {
        if (is_null($value)) {
            return config('money.' . $this->code . '.thousands_separator');
        }

        return $value;
    }
}
