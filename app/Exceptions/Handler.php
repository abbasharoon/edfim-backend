<?php

namespace App\Exceptions;

use App\Helpers\Reply;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\PostTooLargeException;
use Plank\Mediable\Exceptions\MediaUploadException;
use Plank\Mediable\HandlesMediaUploadExceptions;

class Handler extends ExceptionHandler
{
    use HandlesMediaUploadExceptions;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param \Exception $exception
     *
     * @return void
     */
    public function report(Exception $exception)
    {
        if (app()->bound('sentry') && $this->shouldReport($exception)) {
            app('sentry')->captureException($exception);
        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception               $exception
     *
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof MediaUploadException || $exception instanceof PostTooLargeException) {
            return response($exception->getMessage(), $exception->getCode() !== 0 ? $exception->getCode() : 401);
        }
        if ($exception instanceof ModelNotFoundException) { // handle exception
            return response(['message' => 'Data not found'], 404);
        }

        if ($exception instanceof AuthorizationException) {
            if ($request->expectsJson()) {
                return Reply::unAuthorized();
            }
            // TODO: Redirect to error page instead
            return response()->json([$exception]);
        }

        return parent::render($request, $exception);
    }
}
