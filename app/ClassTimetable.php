<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassTimetable extends Model
{
    protected $table = 'class_timetable';
    protected $fillable = ['class_id'];
    public $timestamps = false;
}
