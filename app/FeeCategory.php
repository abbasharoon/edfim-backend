<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;

class FeeCategory extends Model
{

    use BelongsToTenants;
    protected $table = 'fee_categories';
    protected $hidden = ['tenant_id'];

    public function fee()
    {
        return $this->hasMany('App\Fee', 'fee_category_id');
    }
}
