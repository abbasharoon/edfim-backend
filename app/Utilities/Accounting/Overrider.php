<?php

namespace App\Utilities\Accounting;

use App\Models\Accounting\Setting\Currency;

class Overrider
{
    public static $tenant_id;

    public static function load($type)
    {
        // Overrides apply per company
        $tenant_id = config('app.tenant')->tenant_id;
        if (empty($tenant_id)) {
            return;
        }

        static::$tenant_id = $tenant_id;

        $method = 'load' . ucfirst($type);

        static::$method();
    }

    protected static function loadSettings()
    {
        // Set the active company settings
        setting()->setExtraColumns(['tenant_id' => static::$tenant_id]);
        setting()->load(true);

        // Timezone
        config(['app.timezone' => setting('general.timezone', 'UTC')]);
        date_default_timezone_set(config('app.timezone'));

        // Email
        $email_protocol = setting('general.email_protocol', 'mail');
        config(['mail.driver' => $email_protocol]);
        config(['mail.from.name' => setting('general.company_name')]);
        config(['mail.from.address' => setting('general.company_email')]);

        if ($email_protocol == 'sendmail') {
            config(['mail.sendmail' => setting('general.email_sendmail_path')]);
        } elseif ($email_protocol == 'smtp') {
            config(['mail.host' => setting('general.email_smtp_host')]);
            config(['mail.port' => setting('general.email_smtp_port')]);
            config(['mail.username' => setting('general.email_smtp_username')]);
            config(['mail.password' => setting('general.email_smtp_password')]);
            config(['mail.encryption' => setting('general.email_smtp_encryption')]);
        }

        // Session
        config(['session.lifetime' => setting('general.session_lifetime', '30')]);

        // Locale
        if (session('locale') == '') {
            //App::setLocale(setting('general.default_language'));
            //Session::put('locale', setting('general.default_language'));
            config(['app.locale' => setting('general.default_locale')]);
        }
    }

    protected static function loadCurrencies()
    {
        $currencies = Currency::all();

        foreach ($currencies as $currency) {
            if (!isset($currency->precision)) {
                continue;
            }

            config(['money.' . $currency->code . '.precision' => $currency->precision]);
            config(['money.' . $currency->code . '.symbol' => $currency->symbol]);
            config(['money.' . $currency->code . '.symbol_first' => $currency->symbol_first]);
            config(['money.' . $currency->code . '.decimal_mark' => $currency->decimal_mark]);
            config(['money.' . $currency->code . '.thousands_separator' => $currency->thousands_separator]);
        }

        // Set currencies with new settings
        \Akaunting\Money\Currency::setCurrencies(config('money'));
    }

}