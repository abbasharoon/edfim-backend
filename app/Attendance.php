<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    use BelongsToTenants;

    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'attendances';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $hidden = ['tenant_id'];
    protected $dates = [
        'created_at',
        'updated_at',
        'date'
    ];

    public function absent()
    {
        return $this->hasMany('App\AttendanceAbsent', 'attendance_id', 'id');
    }

    public function register()
    {
        return $this->belongsTo('App\AttendanceRegister', 'attendance_register_id', 'id');
    }
}
