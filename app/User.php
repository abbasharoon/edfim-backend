<?php

namespace App;

use App\Notifications\ResetPassword;
use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, BelongsToTenants, HasRolesAndAbilities, CanResetPassword;


    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'tenant_id', 'password', 'remember_token', 'updated_at',
    ];

    /**
     * Return the related user information from the users_data table
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user_data()
    {
        return $this->hasOne('App\UserData', 'user_id');
    }

    /**
     * Return the related user information from the users_data table
     *
     */
    public function parent()
    {
        return $this->belongsTo('App\User', 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany('App\User', 'parent_id', 'id');
    }

    public function fee()
    {
        return $this->hasMany('App\UserFee', 'user_id', 'id');
    }

    /**
     * Return the related user information from the users_data table
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function batch()
    {
        return $this->belongsTo('App\Batch', 'batch_id');
    }


    public function classStaff()
    {
        return $this->belongsToMany('App\Classes', 'class_user', 'user_id', 'class_id')
            ->withPivot('class_id', 'user_id', 'class_teacher');
    }


    public function subjectStaff()
    {
        return $this->belongsToMany('App\Subject', 'subject_user', 'user_id', 'subject_id');
    }


    /**
     * Return all the subjects of a teacher
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany of objects
     */
    public function subject()
    {
        return $this->hasMany('App\SubjectUser', 'user_id');
    }


    public function roles()
    {
        return $this->belongsToMany('App\Role', 'assigned_roles', 'entity_id', 'role_id');
    }

    public function notificationPreference()
    {
        return $this->hasOne('App\NotificationPreference', 'user_id');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    public function erpNext()
    {
        return $this->hasMany('App\ApiService')->where('service_id', 1)->first();
    }

    public function scopeWhereRole($query, $ability)
    {
        $query->where(function ($query) use ($ability) {
            // direct
            $query->whereHas('roles', function ($query) use ($ability) {
                $query->byName($ability);
            });
            // through roles
//            $query->orWhereHas('roles', function ($query) use ($ability) {
//                $query->whereHas('abilities', function ($query) use ($ability) {
//                    $query->byName($ability);
//                });
//            });
        });
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

}
