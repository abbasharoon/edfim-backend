<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Batch extends Model
{
    use BelongsToTenants, softDeletes;

    /**
     * The attributes that should be mutated to dates
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'batches';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'classes_id'];
    protected $hidden = ['tenant_id'];


    public function class()
    {
        return $this->hasOne('App\Classes', 'id', 'class_id');
    }

    public function classRelation()
    {
        return $this->hasOne('App\Classes', 'id', 'class_id');
    }

    public function student()
    {
        return $this->hasMany('App\User', 'batch_id', 'id');
    }


    public function batch_history()
    {
        return $this->belongsToMany('App\Classes',
            'batch_class_history',
            'batch_id',
            'to_class_id')->withPivot('from_class_id', 'created_at', 'updated_at');
    }

    public function batchRegisters()
    {
        return $this->hasMany('App\AttendanceRegister', 'batch_id', 'id');
    }


}
