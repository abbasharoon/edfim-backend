<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{


    public function tenantHostname()
    {
        return $this->hasMany('App\TenantHostname', 'tenant_id', 'id');
    }

    public function tenantSettings()
    {
        return $this->hasMany('App\Setting', 'tenant_id', 'id');
    }

    public function tenantUsers()
    {
        return $this->hasMany('App\User', 'tenant_id', 'id');
    }
}
