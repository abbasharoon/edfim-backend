<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;

class AssessmentTest extends Model
{
    use BelongsToTenants;
    protected $hidden = ['tenant_id'];
    protected $dates = ['created_at', 'deleted_at', 'updated_at', 'due_date'];

    public function student_score()
    {
        return $this->hasMany('App\AssessmentTestScore');
    }

    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }
}
