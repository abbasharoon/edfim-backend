<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;

class AttendanceAbsent extends Model
{
    use BelongsToTenants;
    public $timestamps = null;

    public function attendance()
    {
        return $this->belongsTo('App\Attendance', 'attendance_id', 'id');
    }

    public function reason() {
        return $this->belongsTo('App\AbsentReason', 'reason_id', 'id');
    }
}
