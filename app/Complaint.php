<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    use BelongsToTenants;
    protected $hidden = ['tenant_id'];



    public function complainee()
    {
        return $this->belongsToMany('App\User');
    }
    public function complainant()
    {
        return $this->belongsTo('App\User');
    }
}
