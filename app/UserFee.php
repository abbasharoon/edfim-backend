<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;

class UserFee extends Model
{
    use BelongsToTenants;
    protected $table = 'user_fee';
//    protected $hidden = ['tenant_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function fee()
    {
        return $this->belongsTo('App\Fee', 'fee_id', 'id');
    }

    public function targetFee()
    {
        return $this->belongsTo('App\Fee', 'target_fee_id', 'id');
    }
}
