<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;

class ClassFee extends Model
{
    use BelongsToTenants;

    protected $table = 'class_fee';
//    protected $hidden = ['tenant_id'];

    public function targetFee()
    {
        return $this->belongsTo('App\Fee', 'target_fee_id', 'id');
    }

    public function fee()
    {
        return $this->belongsTo('App\Fee', 'fee_id', 'id');
    }

    public function class()
    {
        return $this->belongsTo('App\Classes', 'class_id', 'id');
    }
}
