<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Classes extends Model
{
    use SoftDeletes, BelongsToTenants;

    /**
     * The attributes that should be mutated to dates
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'classes';

    /**
     * The attributes that are mass-assignable
     *
     * @var array
     */
    protected $fillable = ['name'];
    protected $hidden = [ 'tenant_id'];

    /**
     * A class has many subjects
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany of objects
     */
    public function subject()
    {
        return $this->hasMany('App\Subject', 'class_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function batch()
    {
        return $this->hasMany('App\Batch', 'class_id');
    }


    /**
     * Return all the schedules for a class
     *
     * @return array
     */
    public function time_table()
    {
        $this->hasMany('App\TimeTable', 'class_id');
    }

    public function class_staff()
    {
        return $this->belongsToMany('App\User', 'class_user', 'class_id', 'user_id');
    }

    public function fee()
    {
        return $this->hasMany('App\ClassFee', 'class_id', 'id');
    }

    public function student()
    {
        return $this->hasManyThrough(
            'App\User',
            'App\Batch',
            'class_id', // Foreign key on users table...
            'batch_id', // Foreign key on posts table...
            'id', // Local key on countries table...
            'id' // Local key on users table...
        );
    }

}