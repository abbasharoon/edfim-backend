<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class Assignment extends Model
{

    use BelongsToTenants, Mediable;
    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'assignments';
    protected $hidden = ['tenant_id'];

    protected $dates = [
        'created_at',
        'updated_at',
        'due_date'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function student_score()
    {
        return $this->hasMany('App\AssignmentScore');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function class()
    {
        return $this->belongsTo('App\Classes', 'class_id');
    }

    public function batch()
    {
        return $this->belongsTo('App\Batch', 'batch_id');
    }

    public function subject()
    {
        return $this->belongsTo('App\Subject', 'subject_id');
    }
}
