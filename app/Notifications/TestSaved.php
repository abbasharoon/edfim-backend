<?php

namespace App\Notifications;

use App\Helpers\NotificationHelper;
use App\Helpers\SMSHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class TestSaved extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $test;
    protected $batch;
    protected $score;
    private $config;
    private $child;

    public function __construct($test, $batch, $scoreData = null, $child = null)
    {
        $this->test = $test;
        $this->batch = $batch;
        $this->score = $scoreData;
        $this->child = $child;
        $this->config = config('app.tenant');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {

        $notifiable->child = $this->child;
        return NotificationHelper::makeVia($notifiable, 'test');
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $notifiable->child = $this->child;
        $mailMessage = new MailMessage();
        $studentId = $notifiable->child ? $notifiable->child->id : $notifiable->id;
        $mailMessage = $mailMessage->greeting('Hello ' . NotificationHelper::makeName($notifiable));
        if ($this->test->status == 1) {
            $mailMessage = $mailMessage->subject('New Test is Due')
                ->line('New Test ' . $this->test->name . ' has been announced for ' . $this->batch->class->name . ' which is 
            due to be taken on ' . $this->test->due_date->format('l jS \\of F Y'));
        } elseif ($this->test->status == 2) {
            $mailMessage = $mailMessage->subject('Test Status changed to "Taken"')
                ->line('Test ' . $this->test->name . ' for ' . $this->batch->class->name . ' which was 
            due on ' . $this->test->due_date->format('l jS \\of F Y') . ' is marked as Taken.');
        } elseif ($this->test->status == 3) {
            $numberFormatter = new \NumberFormatter('en_US', \NumberFormatter::ORDINAL);
            $mailMessage = $mailMessage->subject('Test Results Published')
                ->line('Results for test ' . $this->test->name . ' of ' . $this->batch->class->name . ' have been
            published')
                ->line(NotificationHelper::addressBy($notifiable) . ' scored ' . $this->score['scoreData'][$studentId]['score'] . ' out of ' .
                    $this->test->score . ', securing ' .
                    $numberFormatter->format(array_search($this->score['scoreData'][$studentId]['score'], $this->score['positions']) + 1)
                    . ' position in the Class.');
        } elseif ($this->test->status == 4) {
            $mailMessage = $mailMessage->subject('Test Status changed to "Cancelled"')
                ->line('Test ' . $this->test->name . ' for '
                    . $this->batch->class->name . ' has been canceled');
        }
        return $mailMessage->line('Click the Button below to view full details')
            ->action('Go to Test ',
                'https://' . $this->config['app.tenant']->hostname . '/assessment/test/' . $this->test->id);

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        $notifiable->child = $this->child;
        if ($notifiable->child) {
            return [
                'test_id' => $this->test->id,
                'status' => $this->test->status,
                'user_id' => $notifiable->child->id
            ];
        }
        return [
            'test_id' => $this->test->id,
            'status' => $this->test->status,
            'user_id' => $notifiable->id
        ];
    }

    public function toSms($notifiable)
    {
        $notifiable->child = $this->child;
        $message = new SMSHelper();
        $message->addLine('Dear' . NotificationHelper::makeName($notifiable));
        if ($this->test->status === 1) {
            $message->addLine('New Test is Due');
        } elseif ($this->test->status === 2) {
            $message->addLine('Test Taken');
        } elseif ($this->test->status === 3) {
            $message->addLine('Test Results are Published');
        } elseif ($this->test->status === 4) {
            $message->addLine('Test Cancelled');
        }

        if ($notifiable->child) {
            $message->addLine('Student ' . NotificationHelper::makeFor($notifiable) . '');
        }
        $message->addLine('Name: ' . $this->test->name)
            ->addLine('Subject: ' . $this->test->subject->name)
            ->addLine('Due Date: ' . $this->test->due_date->format('l jS \\of F Y'))
            ->addLine('Total Score: ' . $this->test->score);
        if ($this->test->status === 3) {
            $numberFormatter = new \NumberFormatter('en_US', \NumberFormatter::ORDINAL);
            $studentId = $notifiable->child ? $notifiable->child->id : $notifiable->id;
            $message->addLine('Obtained: ' . $this->score['scoreData'][$studentId]['score'])
                ->addLine('Position: ' . $numberFormatter->format(array_search($this->score['scoreData'][$studentId]['score'], $this->score['positions']) + 1));

        }
        return $message->getMessage();
    }
}
