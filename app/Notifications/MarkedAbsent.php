<?php

namespace App\Notifications;

use App\Helpers\NotificationHelper;
use App\Helpers\SMSHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class MarkedAbsent extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $attendance;
    protected $onlyDatabase;
    private $config;
    private $child;

    public function __construct($attendance, $onlyDatabase = null, $child = null)
    {
        $this->onlyDatabase = $onlyDatabase;
        $this->attendance = $attendance;
        $this->config = config('app.tenant');
        $this->child = $child;
    }

    /**
     * Get the notification's delivery channel`s.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        $notifiable->child = $this->child;
        if ($this->onlyDatabase) {
            return ['database'];
        }
        return NotificationHelper::makeVia($notifiable, 'absent');
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $notifiable->child = $this->child;
        return (new MailMessage)
            ->error()
            ->subject(NotificationHelper::addressBy($notifiable, 'was') . ' marked Absent on ' . $this->attendance->date->format('YYYY-MM-DD'))
            ->greeting('Hello ' . NotificationHelper::makeName($notifiable))
            ->line(NotificationHelper::addressBy($notifiable, 'was') . ' marked absent in attendance for ' .
                $this->attendance->date->format('l jS \\of F Y'))
            ->line('Click the following button to view attendance in detail')
            ->action('View Details', 'https://' . $this->config->hostname . '/student/' . ($notifiable->child ? $notifiable->child->id : $notifiable->id))
            ->line('Contact Administration in case you have any questions.');

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {

        $notifiable->child = $this->child;
        $data = [
            'batch_id' => $this->attendance->batch_id,
            'register_id' => $this->attendance->attendance_register_id,
            'user_id' => 0,
            'description' => '',
        ];
        if ($notifiable->child) {
            $data['user_id'] = $notifiable->child->id;
            $data['description'] = NotificationHelper::makeName($notifiable->child) . ' was marked absent on ' . $this->attendance->date->format('l jS \\of F Y');
        } else {
            $data['user_id'] = $notifiable->id;
            $data['description'] = 'You were marked absent on ' . $this->attendance->date->format('l jS \\of F Y');
        }
        return $data;
    }

    public function toSms($notifiable)
    {
        $notifiable->child = $this->child;
        $message = new SMSHelper();
        if ($notifiable->child || $notifiable['child']) {
            $message->addLine('Dear ' . NotificationHelper::makeName($notifiable) . ',')
                ->addLine('Your child ' . NotificationHelper::addressBy($notifiable, 'was') .
                    ' marked Absent in attendance for ' . $this->attendance->date->format('l jS \\of F Y'));
        } else {
            $message->addLine('Dear ' . NotificationHelper::makeName($notifiable) . ',')
                ->addLine("You have been marked Absent in attendance for " . $this->attendance->date->format('l jS \\of F Y'));
        }
        $message->addLineBreak();
        $message->addLine('Login to Edfim for details');

        return $message->getMessage();
    }
}
