<?php

namespace App\Notifications;

use App\Helpers\NotificationHelper;
use App\Helpers\SMSHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class MarkedPresent extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $attendance;
    private $config;
    private $child;

    public function __construct($attendance, $child = null)
    {
        $this->attendance = $attendance;
        $this->config = config('app.tenant');
        $this->child = $child;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        $notifiable->child = $this->child;
        return NotificationHelper::makeVia($notifiable, 'present', false);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $notifiable->child = $this->child;
        return (new MailMessage)
            ->subject(NotificationHelper::addressBy($notifiable, 'was') . 'was marked Present on ' . $this->attendance->date->format('l jS \\of F Y'))
            ->greeting('Hello ' . NotificationHelper::makeName($notifiable))
            ->line(NotificationHelper::addressBy($notifiable, 'was') . ' was marked Present in attendance for date ' .
                $this->attendance->date->format('l jS \\of F Y'))
            ->line('Click the following button to view attendance in detail')
            ->action('View Attendance', 'https://' . $this->config->hostname . '/student/' . ($notifiable->child ? $notifiable->child->id : $notifiable->id))
            ->line('Contact Administration if you have any question');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [

        ];
    }

    public function toSms($notifiable)
    {
        $notifiable->child = $this->child;
        $message = new SMSHelper();
        if ($notifiable->child) {
            $message->addLine('Dear ' . NotificationHelper::makeName($notifiable))
                ->addLine('Your child ' . NotificationHelper::addressBy($notifiable, 'was') .
                    ' marked Present in attendance for ' . $this->attendance->date->format('l jS \\of F Y'));
        } else {
            $message->addLine('Dear ' . NotificationHelper::makeName($notifiable))
                ->addLine("You have been marked Present in attendance for " . $this->attendance->date->format('l jS \\of F Y'));
        }
        $message->addLineBreak();
        $message->addLine('Login to Edfim for details');

        return $message->getMessage();
    }
}
