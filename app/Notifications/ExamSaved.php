<?php

namespace App\Notifications;

use App\Helpers\NotificationHelper;
use App\Helpers\SMSHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ExamSaved extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $scoreData;
    protected $papers;
    protected $exam;
    protected $batch;
    private $config;
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */


    protected $score;
    protected $child;

    public function __construct($batch, $exam = null, $papers = null, $scoreData = null, $child = null)
    {
        $this->batch = $batch;
        $this->score = $scoreData;
        $this->papers = $papers;
        $this->exam = $exam;
        $this->config = config('app.tenant');
        $this->child = $child;
    }

    public function via($notifiable)
    {
        $notifiable->child = $this->child;
        return NotificationHelper::makeVia($notifiable, 'exam');

    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $notifiable->child = $this->child;
        $mailMessage = new MailMessage();
        $studentId = $notifiable->child ? $notifiable->child->id : $notifiable->id;
        $mailMessage = $mailMessage->greeting('Hello ' . NotificationHelper::makeName($notifiable));
        if ($this->exam->status == 1) {
            $mailMessage = $mailMessage->subject('Exam Scheduled')
                ->line(NotificationHelper::addressBy($notifiable) . ' an Exam named' . $this->exam->name .
                    ' has been scheduled starting from ' . $this->exam->due_date->format('l jS \\of F Y') . '.');
        } elseif ($this->exam->status == 2) {
            $mailMessage = $mailMessage->subject('Exam status changed to "Taken"')
                ->line($this->exam->name . ' for ' . $this->batch->class->name . ' which was 
            due on ' . $this->exam->due_date->format('l jS \\of F Y') . ' is marked as Taken.');
        } elseif ($this->exam->status == 3) {
            $data = [
                'exam' => $this->exam,
                'papers' => $this->papers,
                'score' => $this->score[$notifiable->child ? $notifiable->child->id : $notifiable->id],
                'addressBy' => NotificationHelper::addressBy($notifiable),
                'name' => NotificationHelper::makeName($notifiable),
            ];
            return $mailMessage->subject('Exams results have been declared!')
                ->markdown('mail.exam.saved', ['data' => $data]);
        } elseif ($this->exam->status == 4) {
            $mailMessage = $mailMessage->subject('Exam Status changed to "Cancelled"')
                ->line($this->exam->name . ' for '
                    . $this->batch->class->name . ' has been canceled');
        }
        return $mailMessage->line('Click the Button below to view full details')
            ->action('Go to Exam ',
                'https://' . $this->config->hostname . '/assessment/test/' . $this->exam->id);

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        $notifiable->child = $this->child;
        if ($this->exam->status == 1) {
            $description = 'New Exam is due';
        } elseif ($this->exam->status === 2) {
            $description = 'Exam result published';
        } elseif ($this->exam->status === 3) {
            $description = 'Exam Cancelled';
        }
        if ($notifiable->chid) {
            $user_id = $notifiable->child->id;
            $description .= ' for ' . NotificationHelper::makeName($notifiable->child);
        } else {
            $user_id = $notifiable->id;
        }
        return [
            'exam_id' => $this->exam->id,
            'description' => $description,
            'status' => $this->exam->status,
            'user_id' => $user_id
        ];
    }


    public function toSms($notifiable)
    {
        $notifiable->child = $this->child;
        $message = new SMSHelper();
        $message->addLine('Dear' . NotificationHelper::makeName($notifiable));
        if ($this->exam->status == 1) {
            $message->addLine(NotificationHelper::addressBy($notifiable) . ' new Exam is Due:');
        } elseif ($this->exam->status === 2) {
            $message->addLine(NotificationHelper::addressBy($notifiable, '', true) . ' Exam result published:');
        } elseif ($this->exam->status === 3) {
            $message->addLine(NotificationHelper::addressBy($notifiable, '', true) . ' Exam Cancelled:');
        }
        $message->addLineBreak();
        $message->addLine('Name: ' . $this->exam->name);
        if ($this->exam->status === 2) {
            $message->addLine('Marks Obtained:' . $this->score[$notifiable->child ? $notifiable->child->id : $notifiable->id]['total']);
            $message->addLine('Class Position:' . $this->score[$notifiable->child ? $notifiable->child->id : $notifiable->id]['position']);
        } else {
//            $message->addLine('Start: ' . $this->exam->due_date->format('l jS \\of F Y'));
        }
        $message->addLineBreak();
        $message->addLine('Login to your Edfim account for details');
        return $message->getMessage();
    }
}
