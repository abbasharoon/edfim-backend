<?php

namespace App\Notifications;

use App\Helpers\NotificationHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AssignmentScoreUpdated extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $assignment;
    protected $score;
    protected $child;

    public function __construct($assignment, $score, $child = null)
    {
        $this->assignment = $assignment;
        $this->score = $score;
        $this->child = $child;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        $notifiable->child = $this->child;
        return NotificationHelper::makeVia($notifiable, 'test');
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $notifiable->child = $this->child;
        $message = new MailMessage();
        $message = $message->subject('Score for Assignment has been updated')
            ->greeting('Hello ' . NotificationHelper::makeName($notifiable) . '!');
        if ($this->assignment->score === 0) {
            $message = $message->line(NotificationHelper::addressBy($notifiable, '', true) . ' status for assignment named
            ' . $this->assignment->name . ' has been updated to ' . (($this->score->score == 1) ? 'Complete' : 'Incomplete'));
        } else {
            $message = $message->line(NotificationHelper::addressBy($notifiable, '', true) . ' score for assignment named
            ' . $this->assignment->name . ' has been updated to ' . $this->score->score);
        }
        $message->line('Click the following button to view full details.')
            ->action('Go to Detail', url('/'))
            ->line('Contact the administration if you have any question.')
            ->line('Regards');
        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        $notifiable->child = $this->child;
        return [
            'assignment' => $this->assignment->id,
            'score' => $this->score->score,
            'description' => NotificationHelper::addressBy($notifiable, '', true) . ' score for assignment was updated',
            'user_id' => ($notifiable->child) ? $notifiable->child->id : $notifiable->id
        ];
    }

    public function toSms($notifiable)
    {
        return [
//            TODO Implement sms
        ];
    }
}
