<?php

namespace App\Notifications;

use App\Helpers\NotificationHelper;
use App\Helpers\SMSHelper;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class InvoiceDue extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $invoiceData;
    protected $invoice;
    protected $child;

    public function __construct($invoiceData, $invoice, $child = null)
    {
        $this->invoiceData = $invoiceData;
        $this->invoice = $invoice;
        $this->child = $child;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        $notifiable->child = $this->child;
        return NotificationHelper::makeVia($notifiable, 'invoiceDue');
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $data['name'] = NotificationHelper::makeName($notifiable);
        $data['addressBy'] = NotificationHelper::addressBy($notifiable, 'has');
        $data['due_data'] = $this->invoice->due_date;
        $data['invoice_id'] = $this->invoice->id;
        $data['invoices'] = $this->invoiceData['invoices'];
        $data['total'] = $this->invoiceData['total'];
        return (new MailMessage)->markdown('mail.invoice.due', ['data' => $data]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        $notifiable->child = $this->child;
        return [
            'description' => 'New Invoice of ' . $this->invoiceData['total'] . 'is due on ' . $this->invoice->due_date->format('l jS \\of F Y'),
            'invoice_id' => $this->invoice->id,
            'total' => $this->invoiceData['total'],
            'user_id' => $this->invoice->user_id,
        ];
    }

    public function toSms($notifiable)
    {
        print_r(22);
        $notifiable->child = $this->child;
        return (new SMSHelper)
            ->addLine('Dear ' . NotificationHelper::makeName($notifiable) . ',')
            ->addLine('An Invoice is due for ' . NotificationHelper::makeFor($notifiable))
            ->addLine('Invoice #:' . $this->invoice->id)
            ->addLine('Schedule:' . ['Custom', 'Daily', 'Weekly', 'Monthly', 'Quarterly', '', 'BiAnnual', 'Annual'][$this->invoice->schedule])
            ->addLine('Due Amount:' . $this->invoiceData['total'])
            ->addLine('Due Date:' . $this->invoice->due_date->format('l jS \\of F Y'))
            ->addLineBreak()
            ->addLine('Kindly pay the invoice on time to avoid any inconvenience')->getMessage();
    }
}
