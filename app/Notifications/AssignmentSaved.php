<?php

namespace App\Notifications;

use App\Helpers\NotificationHelper;
use App\Helpers\SMSHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AssignmentSaved extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $assignment;
    protected $batch;
    protected $scoreData;
    protected $scoreKeys;
    protected $config;
    protected $child;

    public function __construct($assignment, $batch, $scoreData = null, $scoreKeys = null, $child = null)
    {
        $this->assignment = $assignment;
        $this->batch = $batch;
        $this->scoreData = $scoreData;
        $this->scoreKeys = $scoreKeys;
        $this->config = config('app.tenant');
        $this->child = $child;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        $notifiable->child = $this->child;
        return NotificationHelper::makeVia($notifiable, 'assignment');
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $notifiable->child = $this->child;
        $mailMessage = new MailMessage();
        $studentId = $notifiable->child ? $notifiable->child->id : $notifiable->id;
        $mailMessage = $mailMessage->greeting('Hi ' . NotificationHelper::makeName($notifiable));
        if ($this->assignment->status == 1) {
            $mailMessage = $mailMessage->subject('New Assignment is due')
                ->line('New assignment' . $this->assignment->name . ' has been announced for ' . $this->batch->class->name . ' which is 
            due to be submitted on ' . $this->assignment->due_date->format('l jS \\of F Y'));
        } elseif ($this->assignment->status == 2) {
            $numberFormatter = new \NumberFormatter('en_US', \NumberFormatter::ORDINAL);
            $mailMessage = $mailMessage->subject('Assignment Results Published')
                ->line('Results for assignment ' . $this->assignment->name . ' of ' . $this->batch->class->name . ' have been
            published');
            if ($this->assignment->score == 0) {
                $mailMessage = $mailMessage->line(NotificationHelper::addressBy($notifiable, '', true) .
                    ' assignment status has been marked as')
                    ->line(($this->scoreData[$studentId]['score'] == 1) ? 'Complete' : ' Incomplete');
            } else {
                $mailMessage = $mailMessage->line(NotificationHelper::addressBy($notifiable, 'has') . ' scored '
                    . $this->scoreData[$studentId]['score'] . ' out of ' .
                    $this->assignment->score . ', securing ' .
                    $numberFormatter->format(array_search($this->scoreData[$studentId]['score'],
                            $this->scoreKeys) + 1) . ' position in the class.');
            }
        } elseif ($this->assignment->status == 3) {
            $mailMessage = $mailMessage->subject('Assignment Status changed to "Cancelled"')
                ->line('Assignment ' . $this->assignment->name . ' for '
                    . $this->batch->class->name . ' has been canceled');
        }
        $mailMessage = $mailMessage->line('Click the Button below to view full details')
//            TODO Add url to results if results are published
            ->action('Go to Assignment',
                'https://' . $this->config->hostname . '/assignment/' . $this->assignment->id)
            ->line('Contact the administration in case you have any questions');
        return $mailMessage;

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        $notifiable->child = $this->child;
        if ($this->assignment->status == 1) {
            $description = 'New Assignment is due';
        } elseif ($this->assignment->status === 2) {
            $description = 'Assignment result published';
        } elseif ($this->assignment->status === 3) {
            $description = 'Assignment Cancelled';
        }
        if ($notifiable->chid) {
            $user_id = $notifiable->child->id;
            $description .= ' for ' . NotificationHelper::makeName($notifiable->child);
        } else {
            $user_id = $notifiable->id;
        }
        return [
            'assignment_id' => $this->assignment->id,
            'description' => $description,
            'status' => $this->assignment->status,
            'user_id' => $user_id
        ];
    }

    public function toSms($notifiable)
    {
        $notifiable->child = $this->child;
        $message = new SMSHelper();
        $message->addLine('Dear ' . NotificationHelper::makeName($notifiable) . ',');
        if ($this->assignment->status === 1) {
            $message->addLine('New Assignment is Due');
        } elseif ($this->assignment->status === 2) {
            $message->addLine('Assignment Results are Published');
        } elseif ($this->assignment->status === 3) {
            $message->addLine('Assignment Cancelled');
        }

        if ($notifiable->child) {
            $message->addLine('Student ' . NotificationHelper::makeFor($notifiable) . '');
        }
        $message->addLine('Name: ' . $this->assignment->name)
            ->addLine('Subject: ' . $this->assignment->subject->name)
            ->addLine('Due Date: ' . $this->assignment->due_date->format('l jS \\of F Y'));
        if ($this->assignment->status === 3) {
            $numberFormatter = new \NumberFormatter('en_US', \NumberFormatter::ORDINAL);
            $studentId = $notifiable->child ? $notifiable->child->id : $notifiable->id;
            if ($this->assignment->score == 0) {
                $message->addLine('Status: ' . (($this->scoreData[$studentId]['score'] == 1) ? 'Complete' : ' Incomplete'));
            } else {
                $message->addLine('Total Score ' . $this->assignment->score)
                    ->addLine('Obtained:' . $this->scoreData[$studentId]['score'])
                    ->addLine($numberFormatter->format(array_search($this->scoreData[$studentId]['score'],
                            $this->scoreKeys) + 1));
            }
        }
        return $message->getMessage();
    }
}
