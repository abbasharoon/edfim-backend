<?php

namespace App\Notifications;

use App\Channels\ZongSMSChannel;
use App\Helpers\SMSHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AccountCreated extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    private $password;
    private $type;
    private $config;
    private $setting = [];

    public function __construct($password, $type)
    {
        $this->password = $password;
        $this->type = $type;
        $this->setting['general.name'] = setting('general.name');
        $this->config = config('app.tenant');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        $via = [];
        if ($notifiable->email) {
            array_push($via, 'mail');
        }
        if ($notifiable->phone_no) {
            array_push($via, ZongSMSChannel::class);
        }
        return $via;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Welcome to Edfim!')
            ->greeting('Hello ' . $notifiable->first_name . ' ' . $notifiable->middle_name . ' ' . $notifiable->last_name)
            ->line('Your account has been created at ' . $this->setting['general.name'] . '\'s Online Management System. ')
            ->line('Your credentials are as follow:')
            ->line('Username: ' . $notifiable->username)
            ->line('Password: ' . $this->password)
            ->line('Click the following button to login')
            ->action('Login!', 'https://' . $this->config->hostname . '/login')
            ->line('If you have any question, please contact the administration');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toSms($notifiable)
    {
        return (new SMSHelper)
            ->addLine("$notifiable->first_name, Welcome to  Edfim!")
            ->addLineBreak()
            ->addLine("Your account has been created by " . $this->setting['general.name'] . " on Edfim, The Most Advance Student Management & Monitoring Software")
            ->addLine("Details are as follow:")
            ->addLineBreak()
            ->addLine("Username: $notifiable->username")
            ->addLine("Password: " . $this->password)
            ->addLine("     URL: https://" . $this->config->hostname)
            ->addLineBreak()
            ->addLine("Go to above url to access your account")->getMessage();
    }
}
