<?php

namespace App\Notifications\Accounting;

use App\Helpers\NotificationHelper;
use App\Helpers\SMSHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class InvoiceUpdated extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $invoice;
    protected $status = null;
    protected $child;

    public function __construct($invoice, $status = null, $child = null)
    {
        $this->invoice = $invoice;
        $this->status = $status;
        $this->child = $child;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        $notifiable->child = $this->child;
        return NotificationHelper::makeVia($notifiable, 'invoice');
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $notifiable->child = $this->child;
        $message = new MailMessage();
        $message = $message->subject('Invoice #' . $this->invoice->id . ' has been updated');
        $message = $message->greeting('Hi ' . NotificationHelper::makeName($notifiable));
        if ($this->status) {
            if ($this->invoice->status == 1) {
                $message = $message->line(NotificationHelper::addressBy($notifiable, '', true) . ' invoice #' . $this->invoice->id . '  status has been changed to DUE');
            } elseif ($this->invoice->status == 2) {
                $message = $message->line(NotificationHelper::addressBy($notifiable, '', true) . ' invoice #' . $this->invoice->id . '  has been marked as PAID');
            } elseif ($this->invoice->status == 3) {
                $message = $message->line(NotificationHelper::addressBy($notifiable, '', true) . ' invoice #' . $this->invoice->id . '  has been marked as Cancelled');
            }
        } else {
            $message = $message->line(NotificationHelper::addressBy($notifiable, '', true) . ' #' . $this->invoice->id . '  invoice has been updated.');
        }
        $message = $message->line('Click the following button to go to Invoice Details.')
            ->action('View Invoice', url('/'))
            ->line('If you have any question, please contact administration')
            ->line('Regards');
        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        $notifiable->child = $this->child;
        return [
            'description' => 'Invoice #' . $this->invoice->id . ' was' . ($this->invoice->status ? 'marked paid' : 'updated'),
            'invoice_id' => $this->invoice->id,
            'status' => $this->invoice->status,
            'user_id' => $this->invoice->user_id,
        ];
    }

    public function toSms($notifiable)
    {
        $notifiable->child = $this->child;
        if ($this->status) {
            if ($this->invoice->status == 1) {
                $message = NotificationHelper::addressBy($notifiable, '', true) . ' invoice #' . $this->invoice->id . '  status has been changed to DUE';
            } elseif ($this->invoice->status == 2) {
                $message = NotificationHelper::addressBy($notifiable, '', true) . ' invoice #' . $this->invoice->id . '  has been marked as PAID';
            } elseif ($this->invoice->status == 3) {
                $message = NotificationHelper::addressBy($notifiable, '', true) . ' invoice #' . $this->invoice->id . '  has been marked as Cancelled';
            }
        } else {
            $message = NotificationHelper::addressBy($notifiable, '', true) . ' #' . $this->invoice->id . '  invoice has been updated.';
        }
        return (new SMSHelper)
            ->addLine('Dear ' . NotificationHelper::makeName($notifiable) . ',')
            ->addLine($message)
            ->addLineBreak()->getMessage();
    }
}
