<?php

namespace App\Notifications;

use App\Channels\SmsChannel;
use App\Helpers\NotificationHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ResetPassword extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $token;
    private $config;

    public function __construct($token)
    {
        $this->token = $token;
        $this->config = config('app.tenant');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', SmsChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Reset Your Password')
            ->greeting('Hi, ' . NotificationHelper::makeName($notifiable))
            ->line('We have received a request to reset your account password on' .
                $this->config->hostname)
            ->line('Click the button below to set a new password')
            ->action('Reset Password', 'https://' .
                $this->config->hostname .
                '/reset-password/' . $this->token . '/' .
                encrypt($notifiable->email ? $notifiable->email : $notifiable->username))
            ->line('If you did not request a password reset, no further action is required.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toSms($notifiable)
    {
        return [
//            TODO Implement sms notificiton here
        ];
    }
}
