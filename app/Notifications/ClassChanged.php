<?php

namespace App\Notifications;

use App\Helpers\NotificationHelper;
use App\Helpers\SMSHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ClassChanged extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    protected $batch;
    private $setting = [];
    private $child;

    public function __construct($batch, $child = null)
    {
        $this->batch = $batch;
        $this->setting['general.name'] = setting('general.name');
        $this->child = $child;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        $notifiable->child = $this->child;
        return NotificationHelper::makeVia($notifiable, 'classChange');
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $notifiable->child = $this->child;
        return (new MailMessage)
            ->subject(NotificationHelper::addressBy($notifiable, '', true) . ' Class has been changed')
            ->line('Hi ' . NotificationHelper::makeName($notifiable))
            ->line(NotificationHelper::addressBy($notifiable, '', true) .
                ' class has been changed to ' . $this->batch->class->name)
            ->line('If you have any question or query, contact support.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        $notifiable->child = $this->child;
        return [
            'description' => NotificationHelper::addressBy($notifiable) . ' class was changed to' . $this->batch->class->name,
            'class_id' => $this->batch->class->id,
            'user_id' => ($notifiable->child) ? $notifiable->child->id : $notifiable->id,
        ];
    }

    public function toSms($notifiable)
    {
        $notifiable->child = $this->child;
        return (new SMSHelper)
            ->addLine('Dear ' . NotificationHelper::makeName($notifiable) . ',')
            ->addLine(NotificationHelper::addressBy($notifiable, '', true) . ' Class/Batch has been changed to ' . $this->batch->class->name)
            ->addLineBreak()
            ->addLine('Check your account for further Details')
            ->addLine($this->setting['general.name'])->getMessage();
    }
}
