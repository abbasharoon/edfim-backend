<?php

namespace App\Notifications;

use App\Helpers\NotificationHelper;
use App\Helpers\SMSHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ScoreUpdated extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $assessment;
    protected $score;
    protected $child;

    public function __construct($assessment, $score, $child = null)
    {
        $this->assessment = $assessment;
        $this->score = $score;
        $this->child = $child;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        $notifiable->child = $this->child;
        return NotificationHelper::makeVia($notifiable, 'test');
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $notifiable->child = $this->child;
        return (new MailMessage)
            ->subject('Score for Assessment has been updated')
            ->greeting(NotificationHelper::makeName($notifiable))
            ->line(NotificationHelper::addressBy($notifiable, '', true) . ' score for assessment named
            ' . $this->assessment->name . ' has been updated to' . $this->score)
            ->line('Click the following button to view full details.')
            ->action('Go to Detail', url('/'))
            ->line('Contact the administration if you have any question.')
            ->line('Regards');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        $notifiable->child = $this->child;
        return [
            'assessment_id' => $this->assessment->id,
            'score' => $this->score,
        ];
    }

    public function toSms($notifiable)
    {
        $notifiable->child = $this->child;
        return (new SMSHelper)
            ->addLine('Dear' . NotificationHelper::makeName($notifiable) . ',')
            ->addLine('Score for ' . NotificationHelper::addressBy($notifiable, '', true) .
                ' has been updated in' . $this->assessment->name)
            ->addLine('New score is ' . $this->score)->getMessage();
    }
}
