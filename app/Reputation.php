<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;

class Reputation extends Model
{
    use BelongsToTenants;
    protected $hidden = ['tenant_id', 'deleted_at'];

    public function badge()
    {
        return $this->belongsTo('App\ReputationBadge');
    }

}
