<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;

class AssignmentScore extends Model
{
    use BelongsToTenants;
    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $hidden = ['tenant_id'];
    public $timestamps = false;
    protected $table = 'assignment_score';
    protected $fillable = ['score'];

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;


    public function assignment()
    {
        return $this->belongsTo('App\Assignment');
    }

    public function student()
    {
        return $this->belongsTo('App\User', 'user_id', 'id')->withTrashed();
    }
}
