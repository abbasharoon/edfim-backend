<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class ParentUser implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $tenantId = Config('app.tenant')->tenant_id;
        return DB::table('roles')
            ->join('assigned_roles', 'roles.id', '=', 'assigned_roles.role_id')
            ->join('users', 'users.id', '=', 'assigned_roles.entity_id')
            ->where('users.id', $value)
            ->where('assigned_roles.entity_id', $value)
            ->where('roles.scope', $tenantId)
            ->where('assigned_roles.scope', $tenantId)
            ->where('users.tenant_id', $tenantId)
            ->where('roles.name', '=', 'parent')
            ->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Parent user does not exists';
    }
}
