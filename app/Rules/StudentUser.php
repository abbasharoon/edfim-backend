<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class StudentUser implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $batch_id;

    public function __construct($batch_id = null)
    {
        $this->batch_id = $batch_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $tenantId = Config('app.tenant')->tenant_id;
        $user = DB::table('roles')
            ->join('assigned_roles', 'roles.id', '=', 'assigned_roles.role_id')
            ->join('users', 'users.id', '=', 'assigned_roles.entity_id')
            ->where('users.id', $value)
            ->where('users.frozen', false)
            ->whereNull('users.deleted_at')
            ->where('assigned_roles.entity_id', $value)
            ->where('roles.scope', $tenantId)
            ->where('assigned_roles.scope', $tenantId)
            ->where('users.tenant_id', $tenantId)
            ->where('roles.name', '=', 'student');
        if ($this->batch_id) {
            $user->where('users.batch_id', $this->batch_id);
        }
        return $user->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Student doesn\'t exists';
    }
}
