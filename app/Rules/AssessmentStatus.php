<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class AssessmentStatus implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $currentStatus;

    public function __construct($currentStatus)
    {
        $this->currentStatus = $currentStatus;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
//        If current status is draft then it cannot be changed immediately to published or cancelled
        if ($this->currentStatus == 0 && $value > 2) {
            return false;
        }
//        If current status is not draft nor due then it cannot be changed back to draft
        if ($this->currentStatus > 1 && $value == 0) {
            return false;
        }
//        If current status is cancelled then it cannot be changed to published directly
        if ($this->currentStatus === 4 && ($value == 3 || $value == 2)) {
            return false;
        }
//        If current status is published then it cannot be changed to cancelled
        if ($this->currentStatus == 3 && $value == 4) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Cancelled status cannot be changed directly to published';

    }
}
