<?php

namespace App\Rules\Accounting;

use App\Models\Accounting\Setting\Currency as CurrencyModal;
use Illuminate\Contracts\Validation\Rule;

class Currency implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $value;

    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $status = false;

        if (!is_string($value) || (strlen($value) != 3)) {
            return $status;
        }
        $currencies = CurrencyModal::enabled()->pluck('code')->toArray();
        if (in_array($value, $currencies)) {
            $status = true;
        }
        $this->value = $value;

        return $status;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.custom.invalid_currency', ['attribute' => $this->value]);

    }
}
