<?php

namespace App\Rules;

use App\Timetable;
use Illuminate\Contracts\Validation\Rule;

class UniqueTimetable implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @param array        $class_id Ids fo classes for which timetable is added
     * @param string       $end_date End date is also checked
     * @param integer|null $id       id of the timetable row, if sat, it will ignore the current record
     *
     * @return void
     */
    protected $class_id;
    protected $end_date;
    protected $timetable_id;

    public function __construct($class_id, $end_date, $id = null)
    {
        $this->class_id = $class_id;
        $this->end_date = $end_date;
        $this->timetable_id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!$this->end_date) {
            // Add current start date as value of end value in case it's null
//            So that the rule doesn't check existing values for null dates
            $this->end_date = $value;
        }
//        Find the timetable between the dates
//        where id should not be either null (if creating)
//        or should not be equal to the id of the timetable being edited
//        And should not have any record related to the classes
        $timetableRows = Timetable::where(function ($query) use ($value) {
            $query->orWhereBetween('start_date', [$value, $this->end_date]);
            $query->orWhereBetween('end_date', [$value, $this->end_date]);
        })->whereHas('classTimetable', function ($query) {
            $query->whereIn('class_id', $this->class_id);
        });
        if ($this->class_id) {
            $timetableRows->whereNotIn('timetables.id', [$this->class_id]);
        }
        if ($timetableRows->count() > 0) {
            return false;
        } else return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
//        @todo: Use message and provide complete info about exisitng class and timetable
        return 'Timetable data already exists for one of the class';
    }
}
