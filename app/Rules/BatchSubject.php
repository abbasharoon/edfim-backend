<?php

namespace App\Rules;

use App\Batch;
use Illuminate\Contracts\Validation\Rule;

class BatchSubject implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */


    public function __construct($batch_id)
    {
        $this->batch_id = $batch_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return Batch::where('id', $this->batch_id)->whereHas('class.subject', function ($query) use ($value) {
            $query->where('id', $value);
        })->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Subject does not exist with the specified class';
    }
}
