<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;

class AssessmentTestScore extends Model
{
    use BelongsToTenants;
    public $timestamps = false;
    protected $hidden = ['tenant_id'];
    protected $fillable = ['user_id', 'score'];

    public function assessmentTest()
    {
        return $this->belongsTo('App\AssessmentTest');
    }

    public function student()
    {
        return $this->belongsTo('App\User','user_id')->withTrashed();
    }
}
