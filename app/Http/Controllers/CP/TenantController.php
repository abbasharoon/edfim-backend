<?php

namespace App\Http\Controllers\CP;

use App\Http\Controllers\Controller;
use App\Http\Requests\cp\StoreTenant;
use App\NotificationPreference;
use App\Tenant;
use App\TenantHostname;
use App\User;
use App\UserData;
use Bouncer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Novalis\Cloudflare\CloudflareFacade;

class TenantController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function checkLogin($request)
    {
        if (!$request->exists('pass') && !session('loggedIn')) {
            return redirect()->route('adminGetLogin');
        }
    }

    public function index()
    {
        $this->checkLogin(new Request());
        \Landlord::removeTenant('tenant_id');
        $tenants = Tenant::with('tenantHostname', 'tenantSettings')->withCount(['tenantUsers' => function ($q) {
            $q->whereHas('roles', function ($i) {
                $i->where('name', 'student');
            })->whereFrozen(false);
        }])->paginate(50);
        return view('cp.list-tenants', ['tenants' => $tenants]);
    }

    public function getLogin(Request $request)
    {
        return view('cp.login');

    }

    public function postLogin(Request $request)
    {
        if ($request->pass = 'dlskjfoOInNXCKslepsoifdoufklwhjensLSidje&(*d872iousd9*uoil3kOOD*9s8jah3nbskkO)SJ839s8*#)@khjsdkc') {
            session('loggedIn', true);
            return redirect()->route('admin');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->checkLogin(new Request());
        return view('cp.create-tenant');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTenant $request)
    {
        $this->checkLogin($request);
        CloudflareFacade::addRecord($request->institutionHostname, '172.104.179.60');


        $tenant = new Tenant();
        $tenant->name = $request->institutionName;
        $tenant->email = $request->adminEmail;
        $tenant->user_id = 1;
        $tenant->save();


        $tenantHostname = new TenantHostname();
        $tenantHostname->hostname = $request->institutionHostname . '.edfim.com';
        $tenantHostname->tenant_id = $tenant->id;
        $tenantHostname->status = 1;
        $tenantHostname->save();

        $admin = new User();
        $admin->first_name = $request->adminFirstName;
        $admin->last_name = $request->adminLastName;
        $admin->username = $request->adminUserName;
        $admin->phone_no = $request->adminPhone;
        $admin->email = $request->adminEmail;
        $admin->password = Hash::make($request->adminPassword);
        $admin->gender = $request->gender ? $request->gender : 1;
        $admin->tenant_id = $tenant->id;
        $admin->save();


        $tenant->user_id = $admin->id;
        $tenant->save();

        $notification = new NotificationPreference();
        $notification->sms = 1;
        $notification->web = 1;
        $notification->email = 1;
        $notification->app = 1;
        $notification->user_id = $admin->id;
        $notification->save();

        $userData = new UserData();
        $userData->user_id = $admin->id;
        $userData->save();


        Bouncer::scope()->to($tenant->id);
        Bouncer::allow('user-admin')->to([
            'manage-all-staff',
            'view-all-staff',
            'manage-all-student',
            'view-all-student',
            'manage-all-parent',
            'view-all-parent',
            'view-all-role',
            'change-all-student-batch',
        ]);
        Bouncer::allow('student-admin')->to([
            'manage-all-student',
            'view-all-student',
            'manage-all-parent',
            'view-all-parent',
            'change-all-student-batch']);
        Bouncer::allow('staff-admin')->to([
            'manage-all-staff',
            'view-all-staff',
            'view-all-role']);
        Bouncer::allow('class-admin')->to([
            'manage-all-class',
            'view-all-class',
            'manage-all-batch',
            'view-all-batch',
            'manage-all-subject',
            'view-all-subject',
            'change-all-batch-class']);
        Bouncer::allow('class-teacher')->to([
            'view-timetable',
            'view-class',
            'view-batch',
            'view-subject',
            'view-student',
            'view-attendance', 'manage-attendance',
            'view-assignment', 'manage-assignment',
            'view-test', 'manage-test',
            'view-exam', 'manage-exam',
            'view-paper', 'manage-paper',
            'view-reputation', 'manage-reputation',
            'view-reputation-badge',
            'view-fine',
            'manage-fine',
        ]);
        Bouncer::allow('teacher')->to([
            'view-timetable',
            'view-class',
            'view-batch',
            'view-subject',
            'view-student',
            'view-attendance',
            'view-assignment', 'manage-subject-assignment',
            'view-test', 'manage-subject-test',
            'view-paper', 'manage-subject-paper',
            'view-exam',
            'view-reputation', 'manage-reputation',
            'view-reputation-badge',
            'view-fine',
            'manage-fine',
        ]);
        Bouncer::allow('attendance-admin')->to([
            'manage-all-timetable',
            'view-all-timetable',
            'manage-all-attendance',
            'view-all-attendance',
            'view-all-class',
            'view-all-batch',
            'view-all-student',
        ]);
        Bouncer::allow('assignment-admin')->to(['manage-all-assignment',
            'view-all-timetable',
            'view-all-assignment',
            'view-all-class',
            'view-all-batch',
            'view-all-subject',
            'view-all-student',
        ]);
        Bouncer::allow('assessment-admin')->to([
            'view-all-timetable',
            'manage-all-test',
            'view-all-test',
            'manage-all-exam',
            'view-all-paper', 'add-all-paper',
            'view-all-exam',
            'view-class',
            'view-batch',
            'view-subject',
            'view-all-student',
        ]);
        Bouncer::allow('reputation-admin')->to([
            'manage-all-reputation-badge',
            'view-reputation-badge'
        ]);
        Bouncer::allow('finance-admin')->to([
            'manage-all-fee',
            'view-all-fee',
            'manage-all-class-invoice',
            'view-all-class-invoice',
            'manage-all-student-invoice',
            'view-all-student-invoice',
            'view-all-class',
            'view-all-batch',
            'view-all-student']);
        Bouncer::allow('student')->to([]);
        Bouncer::allow('parent')->to([]);
        Bouncer::allow('system-admin')->everything();
        Bouncer::assign('system-admin')->to($admin);


        $settingsData = [
            [
                'key' => 'general.country',
//                'value' => $request->institutionCountry, // Max 7 days
                'value' => 'PK', // Max 7 days
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'general.logo',
                'value' => 'none', // Max 7 days
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'general.name',
                'value' => $request->institutionName, // Max 7 days
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'general.address',
                'value' => $request->institutionAddress, // Max 7 days
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'general.phone',
                'value' => $request->institutionPhone, // Max 7 days
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'tenant.allowed_accounts',
                'value' => $request->institutionStudents, // Max 7 days
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'fee.schedule.weekly',
                'value' => '7', // Max 7 days
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'show_all_results',
//                0 hide results from other students, 1 show results
                'value' => 0,
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'finance.bank.name',
                'value' => '', //max 31 date
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'finance.bank.title',
                'value' => '', //max 31 date
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'finance.bank.number',
                'value' => '', //max 31 date
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'fee.currency',
                'value' => '', //max 31 date
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'fee.schedule.monthly',
                'value' => '05', //max 31 date
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'fee.schedule.quarterly',
                'value' => '30-03', // days up to 30 - months up to 3
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'fee.schedule.triannual',
                'value' => '30-04', // days up to 30 - months up to 4
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'fee.schedule.biannual',
                'value' => '30-06', // days up to 30 - months up to 6
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'fee.schedule.annual',
                'value' => '30-04', // days up to 30 - months up to 12
                'tenant_id' => $tenant->id,
            ],
            [
                'key' => 'schedule.due_days',
                'value' => 7, // days before schedule days to notify the fee. Max 10
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'fee.notify_before',
                'value' => 30, // days before schedule days to notify the fee. Max 10
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'fee.late_fine',
                'value' => 05, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'fine.fee_category_id',
                'value' => '', // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'fine.reputation_badge_id',
                'value' => 05, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'sms.count',
                'value' => ($request->institutionStudents * 10) + $request->additionalSms, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'sms.quota',
                'value' => ($request->institutionStudents * 10) + $request->additionalSms, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'sms.sender',
                'value' => 'zong', // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'email_count',
                'value' => ($request->institutionStudents * 30), // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'email.quota',
                'value' => ($request->institutionStudents * 30), // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'notification.include_name',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ],[
                'key' => 'notification.sms.absent',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'notification.sms.present',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'notification.sms.test',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'notification.sms.exam',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'notification.sms.assignment',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'notification.sms.classChange',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'notification.sms.noticeboard',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'module.users',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ],
            [
                'key' => 'module.classes',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ],
            [
                'key' => 'module.attendance',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'module.assignment',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'module.assessment',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'module.fee',
                'value' => 1, // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ],
            [
                'key' => 'user.student.fields',
                'value' => '', // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'user.parent.fields',
                'value' => '', // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'user.staff.fields',
                'value' => '', // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ], [
                'key' => 'bk.default.currency',
                'value' => 'PKR', // Fine applied to invoice per day in case of late fine.
                'tenant_id' => $tenant->id,
            ],
        ];
        DB::table('settings')->insert($settingsData);
        $absentReasons = [
            [
                'reason' => 'Illness',
                'tenant_id' => $tenant->id,
            ], [
                'reason' => 'Family Matter',
                'tenant_id' => $tenant->id,
            ], [
                'reason' => 'Transport Issue',
                'tenant_id' => $tenant->id,
            ], [
                'reason' => 'Inclement Weather',
                'tenant_id' => $tenant->id,
            ], [
                'reason' => 'Religious Event',
                'tenant_id' => $tenant->id,
            ]
        ];
        DB::table('absent_reasons')->insert($absentReasons);
        return response()->json(['wow']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}



