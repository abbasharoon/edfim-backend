<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Helper;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * @api            {get} /setting
     * @apiName        index Get Settings
     * @apiGroup       setting
     * @apiPermission  Only admin users
     * @apiSuccess {json} Returned Success Array
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *    {
     *    "status": true,
     *    "message": "successfully get settings data",
     *    "data": [
     * {
     * "id": 1,
     * "key": "foo",
     * "value": "update1",
     * "tenant_id": 1
     * },
     * {
     * "id": 2,
     * "key": "foo1",
     * "value": "update2",
     * "tenant_id": 1
     * }
     * ]
     *}
     *
     * @apiDescription Get list of setting
     */
    public function index()
    {
        return response()->json(Reply::success('', Setting::all()));
    }


    /**
     * @api            {PATCH} /setting/:id
     * @apiName        store Update setting data which given id
     * @apiGroup       setting
     * @apiParam {String} [value] Value of Setting item.
     *
     * @apiParamExample {json} Request-Example:
     *   {
     *       "token": "",
     *       "key":"value",
     *   }
     *
     * @apiPermission  Only admin Staff
     * @apiSuccess {json} Setting successfully created details
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Setting updated",
     *           "data": []
     *      }
     *
     * @apiDescription Setting Update
     */
//    @todo Implement resticitions on some type of settings
    public function store(Request $request)
    {
        $editableSettings = [
            'general.country',
            'general.logo',
            'general.name',
            'general.address',
            'general.phone',
            'fee.schedule.monthly',
            'fee.schedule.quarterly',
            'fee.schedule.triannual',
            'fee.schedule.biannual',
            'fee.schedule.annual',
            'finance.bank.name',
            'finance.bank.title',
            'finance.bank.number',
            'fee.currency',
            'fine.fee_category_id',
            'schedule.due_days',
            'notification.include_name',
            'notification.sms.absent',
            'notification.sms.present',
            'notification.sms.test',
            'notification.sms.exam',
            'notification.sms.assignment',
            'notification.sms.classChange',
            'user.student.fields',
            'user.parent.fields',
            'user.staff.fields',
            'academic.grades_status',
            'academic.grades',
        ];
        $sysAdminSettings = [
            'general.country',
            'general.logo',
            'general.name',
            'notification.include_name',
            'general.address',
            'general.phone',
        ];
        $financeSettings = [
            'fee.schedule.monthly',
            'fee.schedule.quarterly',
            'fee.schedule.triannual',
            'fee.schedule.biannual',
            'fee.schedule.annual',
            'finance.bank.name',
            'finance.bank.title',
            'finance.bank.number',
            'fee.currency',
            'fine.fee_category_id',
            'schedule.due_days',
        ];
        $userAdminSettings = [
            'user.student.fields',
            'user.parent.fields',
            'user.staff.fields',
        ];
        $academicAdminSettings = [
            'academic.grades_status',
            'academic.grades',
        ];
        $notificationKeys = [];
        if (($request->value || $request->value == 0) && array_search($request->key, $editableSettings)) {
            if (array_search($request->key, $sysAdminSettings) && auth()->user()->isNotA('system-admin')) {
                return Reply::unAuthorized();
            } elseif (array_search($request->keys, $financeSettings) && \Bouncer::cannot('manage-all-fee')) {
                return Reply::unAuthorized();
            } elseif (array_search($request->keys, $userAdminSettings) && \Bouncer::cannot('manage-all-staff')) {
                return Reply::unAuthorized();
            } elseif (($request->key == 'notification.sms.absent' || $request->key == 'notification.sms.present')
                && \Bouncer::cannot('manage-all-attendance')) {
                return Reply::unAuthorized();
            } elseif ($request->key == 'notification.sms.test'
                && \Bouncer::cannot('manage-all-test')) {
                return Reply::unAuthorized();
            } elseif ($request->key == 'notification.sms.exam'
                && \Bouncer::cannot('manage-all-exam')) {
                return Reply::unAuthorized();
            } elseif ($request->key == 'notification.sms.assignment'
                && \Bouncer::cannot('manage-all-assignment')) {
                return Reply::unAuthorized();
            } elseif ($request->key == 'notification.sms.classChange'
                && \Bouncer::cannot('manage-all-class')) {
                return Reply::unAuthorized();
            } elseif (array_search($request->key, $academicAdminSettings) && auth()->user()->cannot('manage-all-assignment') && auth()->user()->cannot('manage-all-test')) {
                return Reply::unAuthorized();
            }
            if ($request->key === "general.logo") {
                $image = Helper::store_image($request->value['picture_url'], null, 'logo.' . $request->value['fileext']);
                Helper::saveLogoBase64($request->value['picture_url']);
                if ($image) {
                    $request->value = $image;
                }
            }
            $setting = Setting::where('key', $request->key)->first();
            if (!$setting) {
                $setting = new Setting();
                $setting->key = $request->key;
            }
            $setting->value = $request->value;
        }
        if (isset($setting)) {
            return Reply::loggedResponse($setting->save(),
                $setting,
                'update',
                'Setting',
                'setting');
        } else {
            return Reply::unAuthorized();
        }

    }
}
