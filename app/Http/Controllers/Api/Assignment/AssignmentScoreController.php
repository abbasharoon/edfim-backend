<?php

namespace App\Http\Controllers\Api\Assignment;

use App\Assignment;
use App\AssignmentScore;
use App\Helpers\AssignmentHelper;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Assignment\StoreAssignmentScore;
use App\Http\Requests\Assignment\UpdateAssignmentScore;
use App\Notifications\AssignmentScoreUpdated;
use App\User;
use Illuminate\Support\Facades\Notification;

/**
 * @apiDefine Success
 * @apiSuccess {String} message Success Message
 * @apiSuccess {Boolean} status Returns True
 * @apiSuccess {Object[]} data Optionally data will be sent back
 *
 * @apiSuccessExample {json}Success-Response:
 *      HTTP/1.1 200 OK
 *      {
 *          "status":true,
 *          "message":"Assignment Created/Updated",
 *          "data":[
 *                      [
 *                      id: 1,
 *                      user_id: 2,
 *                      score: 10,
 *                      ],
 *                      [
 *                      id: 2,
 *                      user_id: 8,
 *                      score: 6,
 *                      ],
 *                      [
 *                      id: 8,
 *                      user_id: 456,
 *                      score: 2,
 *                      ],
 *            ]
 *      }
 *
 */

/**
 * @apiDefine Error
 * @apiError {String} message Error Message
 * @apiError {Boolean} status Returns False
 * @apiError {Object[]} data Optionally data will be sent back
 *
 */
class AssignmentScoreController extends Controller
{


    /**
     * @api       {POST} /assignment/score Store Score
     * @apiName   StoreAssignmentScore
     * @apiGroup  Assignments
     * @apiParam {Integer} assignment_id Id of assignment for which the score is being added
     * @apiParam {Boolean} [assignment_status]  True= publish results
     * @apiParam {Object[]}  students_score A nested array of students and their scores
     * @apiParam  {Integer}  students_score.user_id Id of student for whom score is added
     * @apiParam {Integer}  students_score.score Score/Assignment status of the student
     * @apiParamExample {json} Request-Example:
     *      {
     *           "assignment_id": 1,
     *            "assignment_status": 3,
     *          "students_score":[
     *                      [
     *                       user_id: 2,
     *                       score: 10,
     *                      ],
     *                      [
     *                          user_id: 8,
     *                          score: 6,
     *                      ],
     *                      [
     *                          user_id: 456,
     *                          score: 2,
     *                      ],
     *            ]
     *      }
     * @apiSuccess {String} message Success Message
     * @apiSuccess {Boolean} status Returns True
     * @apiUse    Error
     */
    public function store(StoreAssignmentScore $request)
    {

        $preparedData = [];
        $assignment = Assignment::find($request->assignment_id);
        if ($assignment->status === 2) {
            return response()->json(Reply::error(__('messages.assignmentScoreCancelled')));
        }
        if (!$request->students_score || count($request->students_score) === 0) {
            return response()->json(Reply::success(__('messages.assignmentScoreCreated')));
        }
        foreach ($request->students_score as $key => $value) {
            $preparedData[$key]['assignment_id'] = $request->assignment_id;
            $preparedData[$key]['user_id'] = $value['user_id'];
            $preparedData[$key]['score'] = $value['score'];
            $preparedData[$key]['tenant_id'] = config('app.tenant')->tenant_id;
        }
        if ($request->assignment_status) {
            $assignment->status = 3;
            $assignment->save();
            AssignmentHelper::notify($assignment, $request->students_score);
        }
        if (AssignmentScore::insert($preparedData)) {
            activity(config('app.tenant')->tenant_id)
                ->on($assignment)
                ->withProperties(['action' => 'create', 'section' => 'assignment_score'])
                ->log(__('messages.logAssignmentCreated'));

            return response()->json(Reply::success(__('messages.assignmentScoreCreated')));
        }
        return response()->json(Reply::error(__('messages.assignmentScoreNotCreated')));

    }

    /**
     * @api           {PUT|PATCH} /assignment/score/:assignment_score_id Update Score
     * @apiName       UpdateAssignmentScore
     * @apiGroup      Assignments
     * @apiPermission manage-assignment,manage-all-assignment,manage-subject-assignment
     * @apiParam  {Integer}  score Updated Score for the specified assignment score record
     * @apiSuccess {String} message Success Message
     * @apiSuccess {Boolean} status Returns True
     * @apiUse        Error
     */
    public function update(UpdateAssignmentScore $request, $id)
    {
        $assignmentScore = AssignmentScore::whereAssignmentId($id)->whereUserId($request->user_id)->first();
        $assignmentScore->score = $request->score;
        if ($assignmentScore->assignment->status == 2) {
            $user = User::findOrFail($assignmentScore->user_id);
            $user->notify(new AssignmentScoreUpdated($assignmentScore->assignment, $assignmentScore));
            if ($user->parent) {
                Notification::send($user->parent, new AssignmentScoreUpdated($assignmentScore->assignment, $assignmentScore, $user));

            }
        }
        if ($assignmentScore->save()) {
            activity(config('app.tenant')->tenant_id)
                ->on($assignmentScore)
                ->withProperties(['action' => 'update', 'section' => 'assignment_score'])
                ->log(__('messages.logAssignmentUpdated'));

            return response()->json(Reply::success(__('messages.assignmentScoreUpdated')));
        }
        return response()->json(Reply::error(__('messages.assignmentScoreNotUpdated')));
    }

}
