<?php

namespace App\Http\Controllers\Api\Assignment;

use App\Assignment;
use App\Helpers\AssignmentHelper;
use App\Helpers\PermissionsHelper;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Assignment\IndexAssignment;
use App\Http\Requests\Assignment\StoreAssignment;
use App\Http\Requests\Assignment\UpdateAssignment;
use Bouncer;
use Illuminate\Support\Facades\Auth;

/**
 * @apiDefine Params
 * @apiParam {String{..256}} name name/Name of Assignment
 * @apiParam {String{..1024}} detail detail of Assignment
 * @apiParam {Integer} batch_id Id of the batch for which the assignment is being added
 * @apiParam {Integer} subject_id Id of the subject for assignment
 * @apiParam {Date} due_date Date for submission of assignment. Format = YYYY-MM-DD
 * @apiParam {Integer=0,1}} status 0=Draft, 1=Due
 * @apiParam {Integer} score Score can be either boolean for done/not done and value submitted should be 0. Anything above zero will make assignment with proper score/marks
 */

/**
 * @apiDefine Success
 * @apiSuccess {String} message Success Message
 * @apiSuccess {Boolean} status Returns True
 * @apiSuccess {Object[]} data Optionally data will be sent back
 *
 * @apiSuccessExample {json}Success-Response:
 *      HTTP/1.1 200 OK
 *      {
 *          "status":true,
 *          "message":"Assignment Created/Updated",
 *          "data":[
 *                  id: 1,
 *                  name: "Maths Assignments"
 *                  detail: "Some detail"
 *                  batch_id: 1,
 *                  subject_id: 2,
 *                  status: 1,
 *                  score: 0,
 *                  due_date: 2019-02-01
 *                  ]
 *      }
 *
 */

/**
 * @apiDefine Error
 * @apiError {String} message Error Message
 * @apiError {Boolean} status Returns False
 * @apiError {Object[]} data Optionally data will be sent back
 *
 */
class AssignmentController extends Controller
{


    /**
     * @api           {GET} /assignment List of assignments
     * @apiName       ViewAssignments
     * @apiGroup      Assignments
     * @apiPermission view-all-assignmnet,view-assignmnet,(student/parent)
     * @apiParam {String{..1024}} [search_string] Any textual string to be matched with name or detail
     * @apiParam {Integer} [batch_id] Filter by batches id
     * @apiParam {Integer} [subject_id] Filter by subjects id
     * @apiParam {Integer=0,1,2,3}} [status] Filter by assignments status
     * @apiParam {Integer} [score] Search by score (can be 0 or >0)
     * @apiParam {Date} [due_date] Filter by the date on which assignment is due
     * @apiParam {Date} [created_at] Filter by the date assignment was created
     * @apiSuccess {String} message Success Message
     * @apiSuccess {Boolean} status Returns True
     * @apiSuccess {Object[]} data Requested data will be sent back if it exists
     *
     * @apiSuccessExample {json}Success-Response:
     *      HTTP/1.1 200 OK
     *      {
     *       "status":true,
     *       "message":"Assignment Created/Updated",
     *       "data":[
     *           "total": 500,
     *           "per_page": 50,
     *           "current_page": 1,
     *           "last_page": 10,
     *           "from": 1,
     *           "to": 50,
     *           "data":    [
     *                      id: 1,
     *                      name: "Maths Assignments"
     *                      detail: "Some detail"
     *                      batch_id: 1,
     *                      classes_id: 2,
     *                      subject_id: 2,
     *                      status: 1,
     *                      score: 0,
     *                      due_date: 2019-02-01
     *                  ],
     *                  [
     *                      id: 2,
     *                      name: "Bio Assignments"
     *                      detail: "Some detail"
     *                      batch_id: 3,
     *                      classes_id: 4,
     *                      subject_id: 1,
     *                      status: 3,
     *                      score: 100,
     *                      due_date: 2018-03-01
     *           ]
     *        ]
     *      }
     *
     * @apiUse        Error
     */


    public function index(IndexAssignment $request, PermissionsHelper $ph)
    {
        $assignment = Assignment::with('class', 'batch', 'subject');
        if (Bouncer::can('view-all-assignment')) {
            if ($request->filled('batch_id')) {
                $assignment->where('batch_id', $request->batch_id);
            }
        } elseif (Bouncer::can('view-assignment')) {
            $assignment->whereIn('batch_id', $ph->teacherBatch($request->batch_id));
        } elseif (Auth::user()->isA('student', 'parent')) {
            $assignment->whereIn('batch_id', $ph->studentBatch($request->batch_id));
            $assignment->where('status', '>', 0);
        } else {
            return Reply::unAuthorized();
        }
        if ($request->exists('name')) {
            $assignment->where('name', 'LIKE', '%' . $request->name . '%');
//            $assignment->orWhere('detail', 'LIKE', '%' . $request->name . '%');
        }
        if ($request->exists('due_date')) {
            $assignment->where('due_date', $request->due_date);
        }
        if ($request->exists('created_at')) {
            $assignment->where('created_at', $request->created_at);
        }
        if ($request->exists('status')) {
            $assignment->where('status', $request->status);
        }
        if ($request->exists('score')) {
            $assignment->where('score', $request->score);
        }
        if ($request->exists('subject_id')) {
            $assignment->where('subject_id', $request->subject_id);
        }
        if ($request->exists('class_id')) {
            $assignment->where('class_id', $request->class_id);
        }
        if (!$request->sort_column) {
            $request->sort_column = 'created_at';
            $request->sort_direction = 'desc';
        }
        if ($request->sort_direction === 'desc') {
            $assignment->orderByDesc($request->sort_column);
        } else {
            $assignment->orderBy($request->sort_column);
        }
        return response()->json($assignment->paginate($request->page_size));
    }

    /**
     * @api           {POST} /assignment Create Assignment
     * @apiName       CreateAssignment
     * @apiGroup      Assignments
     * @aiPermission  manage-all-assignment,manage-assignment,manage-subject-assignment
     * @apiUse        Params
     * @apiUse        Success
     * @apiUse        Error
     */
    public function store(StoreAssignment $request)
    {
        $assignment = new Assignment();
        $assignment->name = $request->name;
        $assignment->detail = $request->detail ? $request->detail : '. . .';
        $assignment->batch_id = $request->batch_id;
        $assignment->class_id = $request->class_id;
        $assignment->subject_id = $request->subject_id;
        $assignment->due_date = $request->due_date;
        $assignment->status = $request->status;
        $assignment->score = $request->score;
        $assignment->upload = $request->upload;
        $assignment->subject();
        if ($assignment->save()) {
            if ($assignment->status > 0) {
                AssignmentHelper::notify($assignment);
            }
            activity(config('app.tenant')->tenant_id)
                ->on($assignment)
                ->withProperties(['action' => 'create', 'section' => 'assignment'])
                ->log(__('messages.logAssignmentCreated'));

            return response()->json(Reply::success(__('messages.assignmentCreated'), $assignment));
        }

        return response()->json(Reply::error(__('messages.assignmentNotCreated')));


    }


    /**
     * @api           {GET} /assignment/:assignment_id Get Assignment Score
     * @apiName       ViewAssignmentScore
     * @aiPermission  view-all-assignment,view-assignment,(student/parent)
     * @apiGroup      Assignments
     *
     * @apiSuccess {Boolean} status Status of operation
     * @apiSuccess {String} message Success message if any
     * @apiSuccess {Object[]} data Object containing assignment data
     * @apiSuccess {Number} data.id Id of assignment in database
     * @apiSuccess {String} data.name name of assignment
     * @apiSuccess {String} data.detail detail/details of assignment
     * @apiSuccess {Number} data.batch_id Id of batch for which assignment was created
     * @apiSuccess {Number} data.subject_id Id of subject for whihc assignment was created in the batch
     * @apiSuccess {String} data.due_date Date on whihc assignment should be submitted in YYYY-MM-DD HH:MM:SS formate
     * @apiSuccess {Number} data.status Status of assignment, 0=daraf,1=due,2 published,3=taken
     * @apiSuccess {Number} data.score Score of assignment. If it's 0 then assignment is only complete/incomplete else students are given scores
     * @apiSuccess {Object[]} data.student_score An Object containing student score data if published for student/parents or if available for staff
     * @apiSuccess {Number} data.student_score.id Id of the score data in database
     * @apiSuccess {Number} data.student_score.assignment_id Id of the assignment
     * @apiSuccess {Number} data.student_score.user_id Id of the realted user
     * @apiSuccess {Numebr} data.student_score.score Score of the student
     * @apiSuccess {Object[]} data.student_score.student  Data of student for display
     * @apiSuccess {Number} data.student_score.student.id  id of student in database
     * @apiSuccess {String} data.student_score.student.first_name  First name of student
     * @apiSuccess {String} data.student_score.student.middle_name  Middle name of student
     * @apiSuccess {String} data.student_score.student.last_name  Last name of student
     * @apiSuccess {Object[]} data.student_score.student.user_data  User data contians miscellaneous student data
     * @apiSuccess {String} data.student_score.student.user_data.picture_url  Name of profile image
     * @apiSuccessExample {json}Success-Response:
     *      HTTP/1.1 200 OK
     *           {
     *               "status": true,
     *               "message": "",
     *               "data": {
     *                   "id": 1,
     *                   "name": "Homework",
     *                   "detail": "Corporis vel culpa recusandae totam. Ipsam omnis ducimus porro vitae. Possimus expedita laborum sunt ut eos veritatis voluptas. Nostrum harum quos optio atque totam alias. Aliquid dolores consequatur quia sit minima animi dolore. Delectus ut quo atque cupiditate. Veritatis repellendus est cumque et aliquam rerum. Facere blanditiis est dignissimos facilis laborum amet.",
     *                   "batch_id": 1,
     *                   "subject_id": 1,
     *                   "due_date": "2002-02-02 00:00:00",
     *                   "status": 1,
     *                   "score": 0,
     *                   "created_at": null,
     *                   "updated_at": null,
     *                   "student_score": [
     *                       {
     *                           "id": 2,
     *                           "assignment_id": 1,
     *                           "user_id": 14,
     *                           "score": 1,
     *                           "student": {
     *                               "id": 14,
     *                               "first_name": "Dallas",
     *                               "middle_name": "August",
     *                               "last_name": "Elliott",
     *                               "user_data": {
     *                                   "user_id": 14,
     *                                   "picture_url": "https://lorempixel.com/640/480/?36512"
     *                               }
     *                           }
     *                       },
     *                       {
     *                           "id": 3,
     *                           "assignment_id": 1,
     *                           "user_id": 15,
     *                           "score": 0,
     *                           "student": {
     *                           "id": 15,
     *                           "first_name": "Ernesto",
     *                           "middle_name": "Jesse",
     *                           "last_name": "Lue",
     *                           "user_data": {
     *                               "user_id": 15,
     *                               "picture_url": "https://lorempixel.com/640/480/?86113"
     *                       }
     *                       }
     *                       },
     *                   ]
     *               }
     *           }
     * @apiUse        Error
     */

//    @TODO Fetch extra data like score and files details only when asked for
    public function show($id, PermissionsHelper $ph)
    {
        $assignment = Assignment::with('student_score',
            'student_score.student:id,first_name,middle_name,last_name,roll_no,picture_url,gender')->findOrFail($id);
        $numberFormatter = new \NumberFormatter('en_US', \NumberFormatter::ORDINAL);

        if (Bouncer::can('view-all-assignment') ||
            (Bouncer::can('view-assignment') && $ph->teacherBatch($assignment->batch_id)) ||
            (Auth::user()->isA('student', 'parent') && $ph->studentBatch($assignment->batch_id) && $assignment->status > 0)) {
            if ($assignment->score > 0) {
                $score = collect($assignment->student_score)->sortByDesc('score')->keyBy('score')->keys()->toArray();
                $assignment->student_score = $assignment->student_score->map(function ($item) use ($score, $numberFormatter) {
                    $item['position'] = $numberFormatter->format(array_search($item['score'], $score) + 1);
                    return $item;
                });
            }
            if ($assignment->upload) {
                if (Auth::user()->isA('student')) {
                    $assignment->loadMedia(Auth::id());
                } elseif (Auth::user()->isA('parent')) {
                    $assignment->loadMedia($ph->childrenIds());
                } else {
                    $assignment->loadMedia();
                }
            }

            if (setting('show_all_results') == 0) {
                if (Auth::user()->isA('student')) {
                    $student_score = $assignment->student_score->where('user_id', Auth::id());
                    $assignment = collect($assignment)->put('student_score', $student_score);
                } elseif (Auth::user()->isA('parent')) {
                    $student_score = $assignment->student_score->whereIn('user_id', $ph->childrenIds());
                    $assignment = collect($assignment)->put('student_score', $student_score);
                }
            }

            return response()->json(Reply::success('', $assignment));
        }
        return Reply::unAuthorized();
    }


    /**
     * @api           {PUT|PATCH} /assessment/:id Update Assignment
     * @apiName       UpdateAssignment
     * @apiGroup      Assignments
     * @aiPermission  manage-all-assignment,manage-assignment,manage-subject-assignment
     * @apiParam {String{..256}} name name/Name of Assignment
     * @apiParam {String{..1024}} detail detail of Assignment
     * @apiParam {Date} due_date Date for submission of assignment. Format = Y-m-d
     * @apiParam {Integer=0,1}} status 0=Draft, 1=Due, 2=cancel, 3=published (if status is published, then it can't be canceled)
     * @apiParam {Integer} score Score can be either boolean for done/not done and value submitted should be 0. Anything above zero will make assignment with proper score/marks
     * @apiUse        Success
     * @apiUse        Error
     */
    public function update(UpdateAssignment $request, $id)
    {
        $assignment = Assignment::findOrFail($id);
        if ($request->fill_status) {
            $assignment->status = $request->status;
        } else {
            $assignment->name = $request->name;
            $assignment->detail = $request->detail;
            $assignment->due_date = $request->due_date;
            $assignment->upload = $request->upload;
            $assignment->score = $request->score;
        }
        if ($assignment->save()) {
            if ($assignment->status > 0 && $request->notify) {
                AssignmentHelper::notify($assignment);
            }
            activity(config('app.tenant')->tenant_id)
                ->on($assignment)
                ->withProperties(['action' => 'create', 'section' => 'assignment'])
                ->log(__('messages.logAssignmentUpdated'));

            return response()->json(Reply::success(__('messages.assignmentUpdated')));
        }

        return response()->json(Reply::error(__('messages.assignmentNotUpdated')));


    }


    /**
     * @api           {DELETE} /assignment/:id Delete Assignment
     * @apiName       DeleteAssignment
     * @apiGroup      Assignments
     * @aiPermission  manage-all-assignment,manage-assignment,manage-subject-assignment
     * @apiUse        Success
     * @apiUse        Error
     */
    public function destroy($id, PermissionsHelper $ph)
    {
        $assignment = Assignment::findOrFail($id);
        if (Bouncer::can('manage-all-assignment') ||
            (Bouncer::can('manage-assignment') && $ph->hasBatch($assignment->batch_id)) ||
            (Bouncer::can('manage-subject-assignment') && $ph->hasSubject($assignment->subject_id))) {
            if ($assignment->delete()) {
                activity(config('app.tenant')->tenant_id)
                    ->on($assignment)
                    ->withProperties(['action' => 'delete', 'section' => 'assignment'])
                    ->log(__('messages.logAssignmentDeleted'));

                return response()->json(Reply::success(__('messages.assignmentDeleted')));
            }

            return response()->json(Reply::error(__('messages.assignmentNotDeleted')));
        }
        return Reply::unAuthorized();
    }
}
