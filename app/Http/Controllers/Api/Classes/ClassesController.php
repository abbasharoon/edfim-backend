<?php

namespace App\Http\Controllers\Api\Classes;

use App\Batch;
use App\Classes;
use App\ClassUser;
use App\Helpers\PermissionsHelper;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Classes\StoreClass;
use App\Http\Requests\Classes\UpdateClass;
use App\Rules\StaffUser;
use Bouncer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ClassesController extends Controller
{
    /**
     * @api            {GET} /class Summary
     * @apiName        Get Classes as list
     * @apiGroup       Class
     * @apiSuccess {Object[]} data Object containing class data
     * @apiSuccess {Number} id  Id of class in database
     * @apiSuccess {String} name Name of class
     * @apiSuccess {batch_count} Number of batches in class
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *   "data" : [
     *       {
     *           "id": 6,
     *           "name": "Grade 1",
     *           "created_at": "2018-05-05 23:18:38",
     *           "updated_at": "2018-05-05 23:18:38",
     *           "batch_count": 3
     *           },
     *           {
     *           "id": 7,
     *           "name": "Grade 2",
     *           "created_at": "2018-05-05 23:18:38",
     *           "updated_at": "2018-05-05 23:18:38",
     *           "batch_count": 0
     *           },
     *           {
     *           "id": 8,
     *           "name": "Grade 3",
     *           "created_at": "2018-05-05 23:18:38",
     *           "updated_at": "2018-05-05 23:18:38",
     *           "batch_count": 0
     *           },
     *   ]
     *
     * @apiError {json} When data cannot be retrieved
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     * @apiDescription Get list of Class
     */
    public function index(Request $request, PermissionsHelper $ph)
    {

        if ($request->filled('withBatch')) {
            $classes = Classes::with('batch');
        } else {
            $classes = Classes::withCount('batch');
        }
        if ($request->filled('withTrashed')) {
            $classes = $classes->withTrashed();
        }
        if ($request->filled('withSubject')) {
            $classes = $classes->with('subject');
        }
        $classes = $classes->get();
        if (Bouncer::cannot('view-all-class') && Bouncer::can('view-class')
            && auth()->user()->isNotA('parent', 'student')) {
            $classes = array_values(collect($classes)->filter(function ($val) use ($ph) {
                return $ph->teacherClass($val['id']);
            })->toArray());
        }
        return response()->json(['data' => $classes]);
    }

    /**
     * @api            {POST} /class Create
     * @apiName        Create New Class
     * @apiGroup       Class
     *
     * @apiParam {String{3..64}} name class name.
     *
     * @apiParamExample {json} Request-Example:
     *   {
     *       "name":"test class name",
     *   }
     *
     * @apiPermission  manage-all-class
     * @apiSuccess {json} Class Created
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Classes created"
     *      }
     *
     * @apiError {json} Details of class cannot be saved
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription Classes Store
     */
    public function store(StoreClass $request)
    {
        $class = new Classes();
        $class->name = $request->name;

        return Reply::loggedResponse($class->save(),
            $class,
            'create',
            'classes',
            'classCreated');
    }


    /**
     * @api            {GET} /class/:id Get Single Class
     * @apiName        GetSingleClasDetails
     * @apiGroup       Class
     * @apiPermission  view-all-class,view-class (Students/parents)
     * @apiSuccess {json} Returned Success Array
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *   {
     *       "data": [
     *           {
     *               "id": 6,
     *               "name": "Grade 1",
     *               "created_at": "2018-04-09 01:14:41",
     *               "updated_at": "2018-04-09 01:14:41",
     *               "class_staff": [
     * {
     *                       "id": 1,
     *                       "first_name": "Abbas",
     *                       "middle_name": "",
     *                       "last_name": "Haroon",
     *                   },
     *                  ],
     *               "batch": [
     *                   {
     *                       "id": 1,
     *                       "name": "Batch 2012",
     *                       "class_id": 6,
     *                       "created_at": "2017-12-30 00:00:00",
     *                       "updated_at": "2017-12-30 00:00:00"
     *                   },
     *                   {
     *                       "id": 2,
     *                       "name": "Batch 2013",
     *                       "class_id": 6,
     *                       "created_at": "2017-12-30 00:00:00",
     *                       "updated_at": "2017-12-30 00:00:00"
     *                   },
     *                   {
     *                       "id": 3,
     *                       "name": "Batch 2014",
     *                       "class_id": 6,
     *                       "created_at": "2017-12-30 00:00:00",
     *                       "updated_at": "2017-12-30 00:00:00"
     *                   }
     *               ],
     *               "subject": [
     *               {
     *                   "id": 1,
     *                   "name": "Math",
     *                   "class_id": "6",
     *                   "created_at": "2017-12-30 00:00:00",
     *                   "updated_at": "2017-12-30 00:00:00",
     *                   "subject_user": []
     *               },
     *               {
     *                   "id": 2,
     *                   "name": "Physics",
     *                   "class_id": "6",
     *                   "created_at": "2017-12-30 00:00:00",
     *                   "updated_at": "2017-12-30 00:00:00",
     *                   "subject_user": []
     *               }
     *               ]
     *           }
     *       ]
     *   }
     *
     * @apiError {json} When data cannot be retrieved
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     * @apiDescription Get list of Class
     */
    public function show($id, PermissionsHelper $ph)
    {
        if (Bouncer::can('manage-all-class') ||
            (Bouncer::can('view-class') && $ph->teacherClass($id)) ||
            $ph->studentHasClass($id)) {
            $class = Classes::with('class_staff:user_id as id,first_name,middle_name,last_name,gender,picture_url,class_user.class_teacher',
                'class_staff.roles')
                ->with('batch')
                ->with('subject.subject_staff:id,first_name,middle_name,last_name,gender,picture_url')
                ->withTrashed()
                ->findOrFail($id);
            return response()->json(['data' => $class]);
        }
        return Reply::unAuthorized();
    }

    /**
     * @api             {PUT|PATCH} /class/:id Update
     * @apiName         Update class Name
     * @apiGroup        Class
     *
     * @apiParam {String} name class name.
     * @apiParam {String="put,patch"} [_method] If http method is not supported
     *
     * @apiParamExample {json} Request-Example:
     *   {
     *       "token": "",
     *       "name":"test subject",
     *   }
     *
     * @apiPermission   manage-all-class
     * @apiSuccess {json} Subject successfully created details
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Class updated",
     *           "data": []
     *      }
     *
     * @apiError {json} Details of Class couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription  Subject Update
     */
    public function update(UpdateClass $request, $id)
    {
        $class = Classes::withTrashed()->findOrFail($id);
        if ($request->filled('restore')) {
            $class->deleted_at = $request->deleted_at;
        }
        $class->name = $request->name;
        $class->save();
        activity(config('app.tenant')->tenant_id)
            ->performedOn($class)
            ->withProperties(['action' => 'update', 'section' => 'classes'])
            ->log(__('messages.logClassesUpdate'));
        return response()->json(Reply::success(__('messages.classUpdated')));
    }

    /**
     * @api             {DELETE} /class/:id Delete
     * @apiName         Delete class by id
     * @apiGroup        Class
     *
     * @apiParam {Number} class id
     *
     * @apiPermission   manage-all-class
     * @apiSuccess {json} Gives details of class which given by id
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Class deleted",
     *          "data": []
     *      }
     * @apiError {json} Details of class couldn't delete
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later"
     *     }
     * @apiDescription  Delete Class details by given class id
     */
    public function destroy($id, Request $request)
    {
        $class = Classes::findOrFail($id);
        if (Bouncer::cannot('manage-all-class')) {
            return Reply::unAuthorized();
        }

        $class->class_staff()->detach();
        if ($request->permanent) {
            $class->forceDelete();
            Batch::whereClassId($class->id)->update(['class_id' => null]);
        } else {
            $class->deleted_at = Carbon::now()->toDateTimeString();
            $class->save();
        }

        activity(config('app.tenant')->tenant_id)
            ->performedOn($class)
            ->withProperties(['action' => 'delete', 'section' => 'classes'])
            ->log(__('messages.logClassesDeleted'));

        return response()->json(Reply::success(__('messages.classDeleted')));
    }

    public function classUser(Request $request)
    {
        if (Bouncer::cannot('manage-all-classes')) {
            return Reply::unAuthorized();
        }
        $this->validate($request, [
            'class_id' => ['required', 'integer',
                Rule::exists('classes', 'id')
                    ->where('tenant_id', config('app.tenant')->tenant_id)
            ],
            'action' => 'in:attach,detach,classTeacher',
            'class_staff' => 'required_without:user_id|array',
            'class_staff .*.user_id' => ['required', 'integer', 'distinct', new StaffUser()],
            'user_id' => 'required_if:action,classTeacher,array',
            'user_id .*' => ['required', 'integer', 'distinct', new StaffUser()],
        ]);

        if ($request->filled('class_user_id')) {
            $classUser = ClassUser::findOrFail($request->class_user_id);
            $classUser->class_teacher = $request->class_teacher;
            $classUser->save();
            return Reply::success('Class Teacher Updated');
        } else {
            $class = Classes::findOrFail($request->class_id);
            if ($request->action == 'attach') {
                $class->class_staff()->attach($request->class_staff);
            } elseif ($request->action == 'detach') {
                $class->class_staff()->detach($request->class_staff);
            } elseif ($request->action === 'classTeacher') {
                ClassUser::whereIn('user_id', $request->user_id)
                    ->where('class_id', $request->class_id)
                    ->update(['class_teacher' => $request->class_teacher]);
            } else {
                $class->class_staff()->sync($request->class_staff);
            }
            return Reply::success('Staff updated for class');
        }
    }
}
