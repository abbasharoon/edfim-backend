<?php
/**
 * @file    BatchController.php
 *
 * BatchController Controller
 *
 * PHP Version 7
 *
 * @author  <Sandun Dissanayake> <java2012@gmail.com>
 *
 */

namespace App\Http\Controllers\Api\Classes;

use App\Batch;
use App\BatchClassHistory;
use App\Helpers\ClassHelper;
use App\Helpers\PermissionsHelper;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Batches\StoreBatch;
use App\Http\Requests\Batches\UpdateBatch;
use Bouncer;
use Illuminate\Http\Request;

class BatchController extends Controller
{


    /**
     * @apiIgnore      Doesn't server any purpose
     * @api            {GET} /batch
     * @apiName        Get Batches
     * @apiGroup       Batch
     * @apiSuccess {Object[]} data Object containing batches data
     * @apiSuccess {Number} data.id Id of batch in database
     * @apiSuccess {String} data.name Name of batch
     * @apiSuccess {Number} data.class_id Class id of batch associated with
     * @apiSuccess {String} data.created_at Date batch was created
     * @apiSuccess {String} data.updated_at Date batch was updated
     * @apiSuccess {Number} data.student_count Number of students in this batch
     * @apiSuccess {Object[]} data.class  Object containing associated class data
     * @apiSuccess {Number} data.class.id  Id of class in database
     * @apiSuccess {String} data.class.name  name of class in database
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *               "data": [
     *                       {
     *                           "id": 1,
     *                           "name": "Batch 2012",
     *                           "class_id": 6,
     *                           "created_at": "2017-12-30 00:00:00",
     *                           "updated_at": "2017-12-30 00:00:00",
     *                           "student_count": 10,
     *                           "class": {
     *                               "id": 6,
     *                               "name": "Grade 1",
     *                               "created_at": "2018-05-05 23:18:38",
     *                               "updated_at": "2018-05-05 23:18:38"
     *                           }
     *                       },
     *                       {
     *                           "id": 2,
     *                           "name": "Batch 2013",
     *                           "class_id": 6,
     *                           "created_at": "2017-12-30 00:00:00",
     *                           "updated_at": "2017-12-30 00:00:00",
     *                           "student_count": 0,
     *                           "class": {
     *                               "id": 6,
     *                               "name": "Grade 1",
     *                               "created_at": "2018-05-05 23:18:38",
     *                               "updated_at": "2018-05-05 23:18:38"
     *                       }
     *                  ]
     * @apiSuccess {Number} current_page Current Page number on pagination index
     * @apiSuccess {Number} from Start of number of received accounts
     * @apiSuccess {Number} to End of number of received accounts
     * @apiSuccess {Number} total Total Number of Accounts
     * @apiSuccess {Number} last_page Last page on pagination index
     * @apiSuccess {Number} per_page Number of records per page
     * @apiDescription Get list of batches
     */
    public function index(Request $request, PermissionsHelper $ph)
    {
        $batches = Batch::with('class')->withCount('student');
        if ($request->filled('class_id')) {
            $batches = $batches->where('class_id', $request->class_id);
        }
        if ($request->filled('withTrashed')) {
            $batches = $batches->withTrashed();
        }

        $batches = $batches->get();
        if (Bouncer::cannot('view-all-class') && Bouncer::can('view-class')
            && auth()->user()->isNotA('parent', 'student')) {
            $batches = $batches->filter(function ($val) use ($ph) {
                return $ph->teacherClass($val['class_id']);
            });
        }
        return Reply::success('', $batches);
    }

    /**
     * @api            {POST} /batch Create
     * @apiName        Create new Batch
     * @apiGroup       Batch
     *
     * @apiParam {String} name Batch name.
     * @apiParam {Number} class_id Associated Class id
     *
     * @apiParamExample {json} Request-Example:
     *   {
     *       "name":"test batches name",
     *       "class_id":1
     *   }
     *
     * @apiPermission  manage-all-batch
     * @apiSuccess {json} Batch successfully created details
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Batch has been successfully created"
     *      }
     *
     * @apiError {json} Details of Batch couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription Create New Batch in a class
     */
    public function store(StoreBatch $request)
    {
        $batch = new Batch();
        $batch->name = $request->name;
        $batch->class_id = $request->class_id;
        if ($batch->save()) {
            activity(config('app.tenant')->tenant_id)
                ->on($batch)
                ->withProperties(['action' => 'create', 'section' => 'batch'])
                ->log(__('messages.logBatchCreated'));

            return response()->json(Reply::success(__('messages.batchCreated'), $batch));
        }
        return response()->json(Reply::error(__('messages.batchError')));
    }

    /**
     *
     * @api            {GET} /batch/:id Get Single Batch
     * @apiName        Get Single Batch with details
     * @apiGroup       Batch
     *
     * @apiPermission  view-all-batch,view-batch,(student/parent)
     * @apiSuccess {json} Gives details of batch which given by id
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *        {
     *   "data": {
     *       "id": 1,
     *       "name": "Batch 12",
     *       "class_id": 8,
     *       "created_at": "2017-12-30 00:00:00",
     *       "updated_at": "2018-04-09 09:32:38",
     *       "class": {
     *           "id": 8,
     *           "name": "Grade 3"
     *           },
     *       "batch_history": [
     *               {
     *                   "id": 7,
     *                   "name": "Grade 2",
     *                   "created_at": "2018-04-09 09:23:04",
     *                   "updated_at": "2018-04-09 09:23:04",
     *                   "pivot": {
     *                   "batch_id": 1,
     *                       "to_class_id": 7,
     *                       "from_class_id": 6,
     *                       "created_at": "2018-04-09 09:32:34",
     *                       "updated_at": "2018-04-09 09:32:34"
     *                   }
     *               },
     *               {
     *                   "id": 8,
     *                   "name": "Grade 3",
     *                   "created_at": "2018-04-09 09:23:04",
     *                   "updated_at": "2018-04-09 09:23:04",
     *                   "pivot": {
     *                       "batch_id": 1,
     *                       "to_class_id": 8,
     *                       "from_class_id": 7,
     *                       "created_at": "2018-04-09 09:32:38",
     *                       "updated_at": "2018-04-09 09:32:38"
     *                   }
     *               }
     *           ],
     *       "student": [
     *               {
     *                   "id": 10,
     *                   "first_name": "Maddison",
     *                   "middle_name": "Eugene",
     *                   "last_name": "Juliet",
     *                   "batch_id": 1,
     *                   "user_data": {
     *                       "user_id": 10,
     *                       "picture_url": "https://lorempixel.com/640/480/?27422"
     *                   }
     *               },
     *               {
     *               "id": 11,
     *               "first_name": "Providenci",
     *               "middle_name": "Elsie",
     *               "last_name": "Chesley",
     *               "batch_id": 1,
     *               "user_data": {
     *                   "user_id": 11,
     *                   "picture_url": "https://lorempixel.com/640/480/?78652"
     *                   }
     *               }
     *           ]
     *       }
     *   }
     *
     * @apiDescription Gives batch details
     */
    public function show($id, PermissionsHelper $ph)
    {
        if (Bouncer::can('view-all-batch') ||
            (Bouncer::can('view-batch') && $ph->teacherBatch($id)) ||
            $ph->studentHasBatch($id)) {
            $batch = Batch::with('batch_history')
                ->with('batchRegisters')
                ->with('class.subject')
                ->with('class.class_staff');
            $batch = $batch->withCount('student');
            return response()->json(['data' => $batch->withTrashed()->find($id)]);
        } else {
            return Reply::unAuthorized();
        }
    }

    /**
     * @api            {PUT|PATCH} /batch/:id Update
     * @apiName        update Update batch data which given id
     * @apiGroup       Batch
     *
     * @apiParam {String} name Batch name.
     * @apiParam {Number} [class_id] Optional Class id if batch is promoted/demoted from one class to another
     *
     * @apiParamExample {json} Request-Example:
     *   {
     *       "token": "",
     *       "name":"test batches name",
     *       "class_id":1
     *   }
     *
     * @apiPermission  manage-all-batch
     * @apiSuccess {json} Batch successfully updated details
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Batch has been successfully updated"
     *      }
     *
     * @apiError {json} Details of Batch couldn't update
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later"
     *     }
     *
     * @apiDescription Batch update with group which give id
     */
    public function update(UpdateBatch $request, $id)
    {
        $batch = Batch::withTrashed()->findOrFail($id);

        $batch->name = $request->name;
        if ($request->filled('restore')) {
            $batch->deleted_at = $request->deleted_at;
        }
        // If batch is transferred to another class then create history log
        if ($request->filled('class_id') && $batch->class_id != $request->class_id
            && is_null($batch->deleted_at)) {
            $batchesClassesHistory = new BatchClassHistory();
            $batchesClassesHistory->batch_id = $id;
            $batchesClassesHistory->from_class_id = $batch->class_id;
            $batchesClassesHistory->to_class_id = $request->class_id;
            $batchesClassesHistory->tenant_id = config('app.tenant')->tenant_id;
            $batchesClassesHistory->save();
            ClassHelper::notifyClassChange($batch);
            $batch->class_id = $request->class_id;
        }
        if ($batch->save()) {

            activity(config('app.tenant')->tenant_id)
                ->performedOn($batch)
                ->withProperties(['action' => 'update', 'section' => 'batch'])
                ->log(__('messages.logBatchUpdated'));

            return response()->json(Reply::success(__('messages.batchUpdated')));
        }

        return response()->json(Reply::error(__('messages.batchError')));
    }

    /**
     * @api            {DELETE} /batch/:id Delete
     * @apiName        Delete a Batch
     * @apiGroup       Batch
     *
     * @apiParam {Number} batch id
     *
     * @apiPermission  manage-all-batch
     * @apiSuccess {json} Gives details of batch which given by id
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Batch has been successfully deleted"
     *      }
     * @apiError {json} Details of Batch couldn't delete
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later"
     *     }
     * @apiDescription Delete batch details by given batch id
     */
    public function destroy($id, Request $request)
    {
        if (Bouncer::can('manage-all-batch')) {
            $batch_data = Batch::findOrFail($id);
            if ($request->permanent) {
                $delete = $batch_data->forceDelete();
            } else {
                $delete = $batch_data->delete();
            }
            if ($delete) {
                return response()->json(Reply::success(__('messages.batchDeleted')));
            }
            return response()->json(Reply::error(__('messages.batchError')));
        }
        return Reply::unAuthorized();
    }
}
