<?php

namespace App\Http\Controllers\Api\Classes;

use App\Classes;
use App\Helpers\PermissionsHelper;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Subject\StoreSubject;
use App\Http\Requests\Subject\UpdateSubject;
use App\Subject;
use Bouncer;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    /**
     * @api            {GET} /subject Get Subjects List
     * @apiName        Retrieves subjects for a given class id
     * @apiGroup       Subject
     * @apiParam {Number} class_id Id of class for which subjects needs to be retrieved.
     *
     * @apiPermission  view-all-class,view-class,(student,parent)
     * @apiSuccess {json} Returned Success Array
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *    {
     *    "status": true,
     *    "message": "successfully get subjects data",
     *   {
     *       "data": [
     *           {
     *               "id": 1,
     *               "name": "Math",
     *               "class_id": "6",
     *               "created_at": "2017-12-30 00:00:00",
     *               "updated_at": "2017-12-30 00:00:00",
     *               "subject_staff": []
     *           },
     *           {
     *               "id": 2,
     *               "name": "Physics",
     *               "class_id": "6",
     *               "created_at": "2017-12-30 00:00:00",
     *               "updated_at": "2017-12-30 00:00:00",
     *               "subject_staff": []
     *           },
     *           {
     *               "id": 6,
     *               "name": "Urdu",
     *               "class_id": "6",
     *               "created_at": "2018-04-09 21:19:30",
     *               "updated_at": "2018-04-09 21:19:30",
     *               "subject_staff": [
     *                   {
     *                       "id": 2,
     *                       "first_name": "Abbas",
     *                       "middle_name": "",
     *                       "last_name": "Haroon",
     *                       "pivot": {
     *                           "subject_id": 6,
     *                           "user_id": 2
     *                       },
     *                       "user_data": {
     *                           "user_id": 2,
     *                           "picture_url": "https://lorempixel.com/640/480/?57131"
     *                       }
     *                   }
     *               ]
     *           }
     *       ]
     *   }
     *
     * @apiDescription Get list of Subject
     */
    public function index(Request $request, PermissionsHelper $ph)
    {
        if (Bouncer::can('view-all-class') ||
            (Bouncer::can('view-class') && $ph->teacherClass($request->class_id)) ||
            $ph->studentHasClass($request->class_id)) {
            $subjects = Subject::with('subject_staff:id,first_name,middle_name,last_name',
                'subject_staff.user_data:user_id,picture_url')
                ->where('class_id', $request->class_id)->get();
            return response()->json(['data' => $subjects]);
        }
        return Reply::unAuthorized();
    }

    /**
     * @api            {POST} /subject Create
     * @apiName        Create new subject
     * @apiGroup       Subject
     *
     * @apiParam {String} name subject name.
     * @apiParam {Number} class_id classes id.
     * @apiParam {Number[]} [user_id] Array of assigned user ids as teachers/managers of subjects
     *
     * @apiParamExample {json} Request-Example:
     *   {
     *       "name":"test subject",
     *       "user_id":[1,2],
     *       "class_id":1
     *   }
     *
     * @apiPermission  manage-all-subject
     * @apiSuccess {json} Fees successfully created details
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Subject created"
     *      }
     *
     * @apiError {json} Details of Subject couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription Subject Store
     */
    public function store(StoreSubject $request)
    {
        // add Subject
        $subject = new Subject;
        $subject->name = $request->name;
        $subject->class_id = $request->class_id;
        $subject->save();
        if ($request->user_id) {
            $subject->subject_staff()->sync($request->user_id);
            Classes::findOrFail($request->class_id)->class_staff()->syncWithoutDetaching($request->user_id);
        }
        activity(config('app.tenant')->tenant_id)
            ->on($subject)
            ->withProperties(['action' => 'create', 'section' => 'subject'])
            ->log(__('messages.logSubjectCreated'));
        return response()->json(Reply::success(__('messages.subjectCreated'), $subject));
    }

    /**
     * @api            {GET} /subject/:id Get Single Subject
     * @apiName        Get Single Subject with details
     * @apiGroup       Subject
     * @apiPermission  Only admin users
     * @apiSuccess {json} Returned Success Array
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *   {
     *       "data": {
     *           "id": 6,
     *           "name": "Urdu",
     *           "class_id": "6",
     *           "created_at": "2018-04-09 21:19:30",
     *           "updated_at": "2018-04-09 21:19:30",
     *           "subject_staff": [
     *               {
     *                   "id": 2,
     *                   "first_name": "Abbas",
     *                   "middle_name": "",
     *                   "last_name": "Haroon",
     *                   "pivot": {
     *                       "subject_id": 6,
     *                       "user_id": 2
     *                   },
     *                    "user_data": {
     *                       "user_id": 2,
     *                       "picture_url": "https://lorempixel.com/640/480/?57131"
     *                   }
     *               }
     *           ],
     *           "class": {
     *               "id": 6,
     *               "name": "Grade 1"
     *           }
     *       }
     *   }
     *
     * @apiDescription Get single fees
     */
    public function show($id)
    {

        $subject = Subject::with('subject_staff:id,first_name,middle_name,last_name',
            'subject_staff.user_data:user_id,picture_url')
            ->with('class:id,name')
            ->findOrFail($id);

//        return response()->json(['data' => $subject]);
    }

    /**
     * @api            {PATCH} /subject/:id
     * @apiName        store Update subject data which given id
     * @apiGroup       Subject
     *
     * @apiParam {String} name subject name.
     * @apiParam {Number[]} [user_id] User ids to assign to subject for management
     *
     * @apiParamExample {json} Request-Example:
     *   {
     *       "name":"test subject",
     *       "user_id":[1,2]
     *   }
     *
     * @apiPermission  manage-all-subject
     * @apiSuccess {json} Subject successfully created details
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Subject updated",
     *           "data": []
     *      }
     *
     * @apiError {json} Details of Subject couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription Subject Update
     */
    public function update(UpdateSubject $request, $id)
    {
        // add Subject
        $subject = Subject::findOrFail($id);
        $subject->name = $request->name;
        $subject->save();

        // add subjects users
        if (!empty($request->user_id)) {
            $subject->subject_staff()->sync($request->user_id);
            Classes::find($subject->class_id)->class_staff()->syncWithoutDetaching($request->user_id);
        }

        // add log
        activity(config('app.tenant')->tenant_id)
            ->performedOn($subject)
            ->withProperties(['action' => 'update', 'section' => 'subject'])
            ->log(__('messages.logSubjectUpdated'));


        return response()->json(Reply::success(__('messages.subjectUpdated')));
    }

    /**
     * @api            {DELETE} /subject/:id Delete
     * @apiName        Delete Subject
     * @apiGroup       Subject
     *
     * @apiPermission  manage-all-subject
     * @apiSuccess {json} Success message after deletion
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Subject deleted",
     *          "data": []
     *      }
     * @apiError {json} When subject cannot be deleted
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later"
     *     }
     * @apiDescription Delete Subject details by given subject id
     */
    public function destroy($id)
    {
        if (Bouncer::cannot('manage-all-subject')) {
            return Reply::unAuthorized();
        }
        $subject = Subject::findOrFail($id);
        $subject->subject_staff()->detach();
        $subject->delete();

        activity(config('app.tenant')->tenant_id)
            ->performedOn($subject)
            ->withProperties(['action' => 'delete', 'section' => 'subject'])
            ->log(__('messages.logSubjectDeleted'));

        return response()->json(Reply::success(__('messages.subjectDeleted')));
    }

}
