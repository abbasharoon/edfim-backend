<?php

namespace App\Http\Controllers\Api\Classes;

use App\Batch;
use App\BatchClassHistory;
use App\Helpers\ClassHelper;
use App\Helpers\PermissionsHelper;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Classes\StoreClassChange;
use Bouncer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChangeClassController extends Controller
{


    /**
     * @api            {GET} /change/batch Batch Class Change summary
     * @apiName        promoteDemoteBatch
     * @apiGroup       Change Class
     * @apiPermission  view-all-batch,view-batch,(student/parent)
     * @apiParam {Number} batch_id Id  of batch for which history is required
     * @apiSuccess {Object[]} data Object containing batch history data
     * @apiSuccess {Number} data.id  Id of history row in database
     * @apiSuccess {Number} data.from_class_id Id of class from which batch was moved
     * @apiSuccess {Number} data.to_class_id Id of class to which batch was moved
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *   "data" : [
     *       {
     *           "id": 6,
     *           "name": "Grade 1",
     *           "created_at": "2018-05-05 23:18:38",
     *           "updated_at": "2018-05-05 23:18:38",
     *           "batch_count": 3
     *           },
     *           {
     *           "id": 7,
     *           "name": "Grade 2",
     *           "created_at": "2018-05-05 23:18:38",
     *           "updated_at": "2018-05-05 23:18:38",
     *           "batch_count": 0
     *           },
     *           {
     *           "id": 8,
     *           "name": "Grade 3",
     *           "created_at": "2018-05-05 23:18:38",
     *           "updated_at": "2018-05-05 23:18:38",
     *           "batch_count": 0
     *           },
     *   ]
     *
     * @apiError {json} When data cannot be retrieved
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     * @apiDescription Get list of Class
     */
    public function index(Request $request, PermissionsHelper $ph)
    {
        if (Bouncer::can('view-all-batch') ||
            (Bouncer::can('view-batch') && $ph->teacherBatch($request->batch_id)) ||
            (Auth::user()->isA('student', 'parent') && $ph->studentBatch($request->batch_id))) {
            $histories = BatchClassHistory::where('batch_id', $request->batch_id);
            return response()->json(Reply::success('', $histories));
        }
        return Reply::unAuthorized();
    }


    /**
     * @api            {POST} /change/batch Change Batch Class
     * @apiName        ChangeBatchClass
     * @apiGroup       Change Class
     *
     * @apiParam {Number} batch_id Id of the batch whose class is changed
     * @apiParam {Number} to_class_id Id of class to which batch is moved
     * @apiParam {Number} batch_id Id of the batch whose class is changed
     *
     * @apiParamExample {json} Request-Example:
     *   {
     *          "data" : [
     *              "id":   23,
     *              "batch_id": 24,
     *              "from_class_id": 24,
     *              "to_class_id": 40,
     *          ]
     *   }
     *
     * @apiPermission  Only admin users
     * @apiSuccess {json} Class Created
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Classes created"
     *      }
     *
     * @apiError {json} Details of class cannot be saved
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription Classes Store
     */
    public function store(StoreClassChange $request)
    {
        $history = new BatchClassHistory();
        $batch = Batch::findOrFail($request->batch_id);
        $history->batch_id = $request->batch_id;
        $history->from_class_id = $batch->class_id;
        $history->to_class_id = $request->to_class_id;
        $history->tenant_id = config('app.tenant')->tenant_id;
        $save = $history->save();
        if ($save) {
            $batch->class_id = $request->to_class_id;
            $batch->save();
            ClassHelper::notifyClassChange($batch);
        }
        return Reply::loggedResponse($save, $history, 'create', 'BatchChange', 'batchChange');
    }


    /**
     * @api            {delete} /change/class/:id Delete batch Class Change
     * @apiName        DeleteBatchChange
     * @apiGroup       Change Class
     *
     * @apiParam {Number}  id If od the batch change history record to be delete. Only latest one can be delted.
     *
     * @apiPermission  Only admin users
     * @apiSuccess {json} Gives details of class which given by id
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Class deleted",
     *          "data": []
     *      }
     * @apiError {json} Details of class couldn't delete
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later"
     *     }
     * @apiDescription Delete Class details by given class id
     */

    public function destroy($id)
    {
        if (Bouncer::cannot('manage-all-batch')) {
            return Reply::unAuthorized();
        }
        $history = BatchClassHistory::findOrFail($id);
        $batch = Batch::findOrFail($history->batch_id);
        if ($history->to_class_id == $batch->class_id) {
            $delete = $history->delete();
            if ($delete) {
                $batch->class_id = $history->from_class_id;
                $batch->save();
                ClassHelper::notifyClassChange($batch);
            }
            return Reply::loggedResponse($delete, $history, 'delete', 'batchChange', 'batchChange');
        } else {
            return response()->json(Reply::error(__('messages.changeOnlyRecentHistory')));
        }
    }
}
