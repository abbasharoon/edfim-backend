<?php

namespace App\Http\Controllers\Api\Classes;

use App\Batch;
use App\BatchUserHistory;
use App\Helpers\PermissionsHelper;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Classes\StoreBatchChange;
use App\Notifications\ClassChanged;
use App\User;
use Bouncer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class ChangeBatchController extends Controller
{


    /**
     * @api            {GET} /change/student Student change summary
     * @apiName        promoteDemoteStudent
     * @apiGroup       Change Class
     * @apiPermission  view-all-student,view-student,(student/parent)
     * @apiParam {Number} user Id  of batch for which history is required
     * @apiSuccess {Object[]} data Object containing batch history data
     * @apiSuccess {Number} data.id  Id of history row in database
     * @apiSuccess {Number} data.from_batch_id Id of batch from which class was moved
     * @apiSuccess {Number} data.to_batch_id Id of class to which class was moved
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *   "data" : [
     *       {
     *           "id": 6,
     *           "name": "Grade 1",
     *           "created_at": "2018-05-05 23:18:38",
     *           "updated_at": "2018-05-05 23:18:38",
     *           "batch_count": 3
     *           },
     *           {
     *           "id": 7,
     *           "name": "Grade 2",
     *           "created_at": "2018-05-05 23:18:38",
     *           "updated_at": "2018-05-05 23:18:38",
     *           "batch_count": 0
     *           },
     *           {
     *           "id": 8,
     *           "name": "Grade 3",
     *           "created_at": "2018-05-05 23:18:38",
     *           "updated_at": "2018-05-05 23:18:38",
     *           "batch_count": 0
     *           },
     *   ]
     *
     * @apiError {json} When data cannot be retrieved
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     * @apiDescription Get list of Class
     */
    public function index(Request $request, PermissionsHelper $ph)
    {
        if (Bouncer::can('view-all-student') ||
            (Bouncer::can('view-student') && $ph->teacherBatch(User::find($request->user_id)->batch_id)) ||
            (Auth::user()->isA('student') && $request->user_id == Auth::id()) ||
            (Auth::user()->isA('parent')) && $ph->childrenIds($request->user_id)) {
            $histories = BatchUserHistory::where('user_id', $request->user_id);
            return response()->json(Reply::success('', $histories));
        }
        return Reply::unAuthorized();
    }


    /**
     * @api            {POST} /change/student Change Student Batch
     * @apiName        ChangeStudentBatch
     * @apiGroup       Change Class
     *
     * @apiParam {Number} user_id Id of the batch whose class is changed
     * @apiParam {Number} to_batch_id Id of class to which batch is moved
     *
     * @apiParamExample {json} Request-Example:
     *   {
     *          "data" : [
     *              "id":   23,
     *              "user_id": 24,
     *              "from_batch_id": 24,
     *              "to_batch_id": 40,
     *          ]
     *   }
     *
     * @apiPermission  manage-all-student
     * @apiSuccess {json} Class Created
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Classes created"
     *      }
     *
     * @apiError {json} Details of class cannot be saved
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription Classes Store
     */
    public function store(StoreBatchChange $request)
    {
        $user = User::findOrFail($request->user_id);
        $save = true;
        if ($user->count() > 0 && !is_null($user->batch_id) && !empty($user->batch_id)) {
            $history = new BatchUserHistory();
            $history->user_id = $request->user_id;
            $history->from_batch_id = $user->batch_id;
            $history->to_batch_id = $request->to_batch_id;
            $history->tenant_id = config('app.tenant')->tenant_id;
            $save = $history->save();
        }
        if ($save) {
            $user->batch_id = $request->to_batch_id;
            $user->save();
            $batch = Batch::findOrFail($request->to_batch_id);
            $user->notify(new ClassChanged($batch));
            Notification::send($user->parent, new ClassChanged($batch, $user));
        }
        return Reply::loggedResponse($save, $history, 'create', 'BatchChange', 'batchChange');
    }


    /**
     * @api            {delete} /change/student/:id Delete Student batch change
     * @apiName        DeleteBatchChangeStudent
     * @apiGroup       Change Class
     *
     * @apiParam {Number}  id If od the student batch change history record to be delete. Only latest one can be deleted.
     *
     * @apiPermission  manage-all-student
     * @apiSuccess {json} Gives details of class which given by id
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Class deleted",
     *          "data": []
     *      }
     * @apiError {json} Details of class couldn't delete
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later"
     *     }
     * @apiDescription Delete Class details by given class id
     */

    public function destroy($id)
    {
        if (Bouncer::cannot('manage-all-student')) {
            return Reply::unAuthorized();
        }
        $history = BatchUserHistory::findOrFail($id);
        $user = User::findOrFail($history->user_id);
        if ($history->to_batch_id == $user->batch_id) {
            $delete = $history->delete();
            if ($delete) {
                $user->batch_id = $history->from_batch_id;
                $user->save();

                $batch = Batch::findOrFail($history->from_batch_id);
                $user->notify(new ClassChanged($batch));
                Notification::send($user->parent, new ClassChanged($batch, $user));
            }
            return Reply::loggedResponse($delete, $history, 'delete', 'batchChange', 'batchChange');
        } else {
            return response()->json(Reply::error(__('messages.changeOnlyRecentHistory')));
        }
    }
}
