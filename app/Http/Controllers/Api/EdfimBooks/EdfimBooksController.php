<?php

namespace App\Http\Controllers\Api\EdfimBooks;

use App\Helpers\ErpNextHelper;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EdfimBooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        auth()->loginUsingId(1);
        $erpnext = new ErpNextHelper();
//        $erpnext->addUserToCompany('test123039232@example.org','tesffting705114079');
//        $erpnext->createUser('test'.rand().'@example.org','teset','dfdsf');

        //
        // url = this.serverUrl + '/api/method/frappe.templates.pages.desk.get_desk_assets';
        // this.http.get(this.serverUrl + '/api/method/frappe.core.doctype.user.user.generate_keys?user=administrator', {
        //   withCredentials: true,
        // }).subscribe((res) => {
        //   console.log(res);
        // });
        // this.http.get(this.serverUrl + '/api/method/frappe.desk.form.load.getdoc?doctype=User&name=Administrator&_=1597954756680', {
        //   withCredentials: true,
        // }).subscribe((res: any) => {
        //   console.log(res);
        //   console.log(res.docs[0]);
        // });
        return response()->json(['token' => $erpnext->getApiToken()]);
//        ->json([])->withCookie(cookie('sid', $cookies['sid'], Carbon::createFromTimeString($cookies['Expires'])->diffInMinutes(Carbon::now())), '/', '.edfim.com');
//        return response()->json($client);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
