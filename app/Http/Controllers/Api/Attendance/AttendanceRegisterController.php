<?php

namespace App\Http\Controllers\Api\Attendance;

use App\AttendanceRegister;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Attendance\StoreRegister;
use App\Http\Requests\Attendance\UpdateRegister;

class AttendanceRegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registers = AttendanceRegister::orderBy('class_id')->orderBy('batch_id')->get();
        return response()->json(['data' => $registers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRegister $request)
    {
        $register = new AttendanceRegister();
        $register->name = $request->name;
        $register->class_id = $request->class_id;
        $register->batch_id = $request->batch_id;
        $register->start_date = $request->start_date;
        $register->end_date = $request->end_date;
        return Reply::loggedResponse($register->save(), $register,
            'create',
            'attendance',
            'attendanceRegisterCreated');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRegister $request, $id)
    {
        $register = AttendanceRegister::findOrFail($id);
        $register->name = $request->name;
        $register->start_date = $request->start_date;
        $register->end_date = $request->end_date;
        return Reply::loggedResponse($register->save(), $register, 'update', 'attendance', 'attendanceRegisterUpdated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (\Bouncer::cannot('manage-all-attendance')) {
            return Reply::unAuthorized();
        }
        $register = AttendanceRegister::findOrFail($id);
        if ($register) {
            return Reply::loggedResponse($register->delete(), $register, 'delete',
                'attendance', 'attendanceRegisterDeleted');
        }
        //
    }
}
