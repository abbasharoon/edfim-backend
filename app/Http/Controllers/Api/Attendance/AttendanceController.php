<?php

namespace App\Http\Controllers\Api\Attendance;

use App\Attendance;
use App\AttendanceAbsent;
use App\AttendanceRegister;
use App\Helpers\AttendanceHelper;
use App\Helpers\PermissionsHelper;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Attendance\StoreAttendance;
use App\Http\Requests\Attendance\UpdateAttendance;
use Bouncer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AttendanceController extends Controller
{
    /**
     * @api            {GET} /attendance Attendance Summary
     * @apiName        Get attendance status summary for all or single class
     * @apiGroup       Attendance
     * @apiPermission  view-all-attendance,view-attendance,(student/parent)
     * @apiParam {String} date Date for which attendance is being required in YYYY-MM-DD format. Date alone will return all batches
     * @apiParam {Number} [batch_id] Providing the batch id to the query will retun attendance status for that batch for one month of the provided date.
     * @apiParam {Number} [user_id] Providing the user_id id to the query will return attendance absents for that user for one month of the provided date. Batch id is required when user_id is present
     * @apiPermission  Only admin users
     * @apiSuccess {Object[]} data Object containing attendance data
     * @apiSuccess {number} data.id Id of attendance in database
     * @apiSuccess {String} data.date Date for which attendance was added
     * @apiSuccess {Number} data.batch_id Batch id for which attendance was added
     * @apiSuccess {Number} data.class_id Class id of batch when attendance was taken
     * @apiSuccess {Number} data.status Status of attendance 0=not added/unfinished,1=added/closed
     * @apiSuccessExample {json} Multiple Batches attendance status returned:
     *     HTTP/1.1 200 OK
     * {
     *   "data": [
     *       {
     *           "id": 1,
     *           "date": "2018-09-30 00:00:00",
     *           "batch_id": 1,
     *           "status": 0,
     *           "created_at": "2018-04-15 20:37:51",
     *           "updated_at": "2018-04-15 20:52:43"
     *       },{
     *           "id": 2,
     *           "date": "2018-09-30 00:00:00",
     *           "batch_id": 2,
     *           "status": 1,
     *           "created_at": "2018-04-15 20:37:51",
     *           "updated_at": "2018-04-15 20:52:43"
     *       },{
     *           "id": 3,
     *           "date": "2018-09-30 00:00:00",
     *           "batch_id": 1,
     *           "status": 2,
     *           "created_at": "2018-04-15 20:37:51",
     *           "updated_at": "2018-04-15 20:52:43"
     *       }
     *     ]
     *   }
     * @apiSuccessExample {json} Single Batch with monthly attendance status:
     *     HTTP/1.1 200 OK
     * {
     *   "data": [
     *       {
     *           "id": 1,
     *           "date": "2018-09-30 00:00:00",
     *           "batch_id": 1,
     *           "status": 0,
     *           "created_at": "2018-04-15 20:37:51",
     *           "updated_at": "2018-04-15 20:52:43"
     *       },{
     *           "id": 2,
     *           "date": "2018-09-29 00:00:00",
     *           "batch_id": 1,
     *           "status": 1,
     *           "created_at": "2018-04-15 20:37:51",
     *           "updated_at": "2018-04-15 20:52:43"
     *       },{
     *           "id": 3,
     *           "date": "2018-09-02 00:00:00",
     *           "batch_id": 1,
     *           "status": 1,
     *           "created_at": "2018-04-15 20:37:51",
     *           "updated_at": "2018-04-15 20:52:43"
     *       }
     *     ]
     *   }
     *
     * @apiError {json} When data cannot be retrieved
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     * @apiDescription Get list of attendance status
     */
    public function index(Request $request, PermissionsHelper $ph)
    {
        $attendance = Attendance::query()->where('status', 1)->with('register');
        $registers = AttendanceRegister::query();
        if ($request->filled('batch_id')) {
            $date = Carbon::createFromFormat('Y-m-d', $request->date);
//            $registers = $registers->whereBetween('start_date',
//                [$date->firstOfQuarter()->toDateTimeString(), $date->lastOfQuarter()->toDateTimeString()])
//                ->orWhereBetween('end_date',
//                    [$date->firstOfQuarter()->toDateTimeString(), $date->lastOfQuarter()->toDateTimeString()]);
            $attendance = $attendance->whereBetween('date',
                [$date->firstOfQuarter()->toDateTimeString(), $date->lastOfQuarter()->toDateTimeString()]);
            $attendance->where('attendance_register_id', $request->attendance_register_id);
            if ($request->user_id && $request->user_id !== 'null') {
                $request->user_id = [$request->user_id];
            }
            if (Bouncer::can('manage-all-attendance')) {
                $attendance = $attendance->where('batch_id', $request->batch_id);
                $registers = $registers->where('batch_id', $request->batch_id);
            } elseif (Bouncer::can('manage-attendance')) {
                $attendance = $attendance->whereIn('batch_id', $ph->teacherBatch($request->batch_id));
                $registers = $registers->whereIn('batch_id', $ph->teacherBatch($request->batch_id));
            } elseif (Auth::user()->isA('student', 'parent')) {
                $attendance = $attendance->whereIn('batch_id', $ph->studentBatch($request->batch_id));
                $registers = $registers->whereIn('batch_id', $ph->studentBatch($request->batch_id));
                if (Auth::user()->isA('student')) {
                    $request->user_id = [Auth::id()];
                }
                if (Auth::user()->isA('parent')) {
                    $request->user_id = $ph->childrenIds($request->user_id);
                }
            }
            $attendance = $attendance->with(['absent' => function ($query) use ($request) {
                if (is_array($request->user_id)) {
                    $query->whereIn('user_id', $request->user_id);
                }
            }])->get();
        } else {
            $registers = $registers->where('start_date', '<=', $request->date)
                ->where(function ($q) use ($request) {
                    $q->where('end_date', '>=', $request->date);
                    $q->orWhere('end_date', null);
                });
            if (Bouncer::can('manage-all-attendance')) {
                $attendance = $attendance->where('date', $request->date)->get();
            } elseif (Bouncer::can('view-attendance')) {
                $attendance = $attendance->where('date', $request->date)->whereIn('batch_id', $ph->teacherBatch())->get();
                $registers = $registers->whereIn('batch_id', $ph->teacherBatch());
            }
        }
        $registers = $registers->get();
        return response()->json(compact('attendance', 'registers'));
    }


    /**
     * @api            {POST} /attendance Create Attendance
     * @apiName        Add attendance anchor for a date
     * @apiGroup       Attendance
     * @apiPermission  manage-all-attendance,manage-attendance
     * @apiParam {String} date Date for which attendance is being added in YYYY-MM-DD format
     * @apiParam {Number} batch_id Id of batch for which attendance is being added
     * @apiParam {Number} class_id Class id of batch
     *
     * @apiParamExample {json} Request-Example:
     *   {
     *          date:2018-9-14
     *          batch_id:1
     *   }
     *
     * @apiPermission  Only admin users
     * @apiSuccess {json} Attendance added
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *   {
     *       "status": true,
     *       "message": "Attendance created",
     *       "data": {
     *           "id": 8,
     *           "date": "2018-09-19 00:00:00",
     *           "batch_id": 1,
     *           "status": 0,
     *           "created_at": "2018-04-15 22:09:54",
     *           "updated_at": "2018-04-15 22:09:54"
     *       }
     *   }
     *
     * @apiError {json} Details of class cannot be saved
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription Classes Store
     */
    public function store(StoreAttendance $request)
    {
        $attendance = Attendance::where('date', $request->date)
            ->where('batch_id', $request->batch_id)
            ->where('attendance_register_id', $request->attendance_register_id)
            ->first();
        if (!$attendance) {
            $attendance = new Attendance;
            $attendance->date = $request->date;
            $attendance->class_id = $request->class_id;
            $attendance->batch_id = $request->batch_id;
            $attendance->attendance_register_id = $request->attendance_register_id;
            $attendance->status = 0;
            $attendance->save();
        }
        $register = $attendance->register;

        /** @var TYPE_NAME $attendance */
        $attendance->absent = $attendance->absent->keyBy('user_id');
        return response()->json(Reply::success(__('messages.attendanceCreated'), $attendance));
    }

    public function show($id)
    {
        $attendance = Attendance::with('absent')->findOrFail($id);
//        return response()->json(Reply::success(__('messages.attendanceCreated'), $attendance));
    }

    /**
     * @api            {PUT|PATCH} /attendance/:id Update Attendance
     * @apiName        UpdateAttendance
     * @apiGroup       Attendance
     * @apiPermission  manage-all-attendance,manage-attendance
     * @apiParam {String} date Date for which attendance is being updated in YYYY-MM-DD format. Same date should be sent if only status update is required
     * @apiParam {Number="0,1"} status Status of Attendance. 0=1 not done/1= Done
     *
     * @apiParamExample {json} Request-Example:
     *   {
     *          date:2018-9-14
     *          batch_id:1
     *   }
     *
     * @apiPermission  Only admin users
     * @apiSuccess {json} Attendance added
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *   {
     *       "status": true,
     *       "message": "Attendance Updated",
     *       "data": {
     *           "id": 8,
     *           "date": "2018-09-19 00:00:00",
     *           "batch_id": 1,
     *           "status": 0,
     *           "created_at": "2018-04-15 22:09:54",
     *           "updated_at": "2018-04-15 22:09:54"
     *       }
     *   }
     *
     * @apiError {json} Details of class cannot be saved
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription Classes Store
     */

    public function update(UpdateAttendance $request, $id)
    {
        $attendance = Attendance::findOrFail($id);
//        $attendance->date = $request->date;
        $attendance->status = $request->status;
        if ($request->status == 1) {
            AttendanceHelper::notify($attendance);
        }
        if ($attendance->save()) {
            activity(config('app.tenant')->tenant_id)
                ->performedOn($attendance)
                ->withProperties(['action' => 'update', 'section' => 'attendance'])
                ->log(__('messages.logAttendanceUpdated'));

            return response()->json(Reply::success(__('messages.attendanceUpdated'), $attendance));
        }
        return response()->json(Reply::error(__('messages.attendanceNotUpdated')));

    }

    public function destroy($id)
    {
        $attendance = Attendance::findOrFail($id);
        $attendance->delete();
        AttendanceAbsent::whereAttendanceId($id)->delete();
        activity(config('app.tenant')->tenant_id)
            ->performedOn($attendance)
            ->withProperties(['action' => 'delete', 'section' => 'attendance'])
            ->log(__('messages.logAttendanceDeleted'));
        return response()->json(Reply::success(__('messages.attendanceDeleted')));
    }

}
