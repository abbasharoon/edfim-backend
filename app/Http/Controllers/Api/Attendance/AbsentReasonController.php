<?php

namespace App\Http\Controllers\Api\Attendance;

use App\AbsentReason;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use Bouncer;
use Illuminate\Http\Request;

class AbsentReasonController extends Controller
{
    /**
     * @api            {GET} /attendance/reason Get Reasons List
     * @apiName        absentReasonsList
     * @apiGroup       Attendance
     * @apiSuccess {Object[]} data Object containing attendance data
     * @apiSuccess {Number} data.id Id of reason in database
     * @apiSuccess {String} data.reason Title of reason for absentee
     * @apiSuccessExample {json} Multiple Batches attendance status returned:
     *     HTTP/1.1 200 OK
     * {
     *   "data": [
     *       {
     *           "id": 1,
     *           "reason": "Ill",
     *       },{
     *           "id": 2,
     *           "reason": "Family Function",
     *       },{
     *           "id": 3,
     *           "reason": "Weather",
     *       }
     *     ]
     *   }
     * @apiError {json} When data cannot be retrieved
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     * @apiDescription Get list of reasons for student absentee
     */
    public function index()
    {
        if (Bouncer::can('view-all-attendance') ||
            Bouncer::can('view-attendance') ||
            auth()->user()->isA('student', 'parent')) {

            return response()->json(['data' => AbsentReason::withCount('absent')->get()]);
        }
        return Reply::unAuthorized();
    }


    /**
     * @api            {POST} /attendance/reason Create Reason
     * @apiName        CreateReason
     * @apiGroup       Attendance
     * @apiPermission  manage-all-attendance
     * @apiParam {String{..64}} reason Title of reason
     *
     * @apiParamExample {json} Request-Example:
     *   {
     *          reason:"Pneumonia"
     *   }
     *
     * @apiSuccess {json} Reason added
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *   {
     *       "status": true,
     *       "message": "Reason created",
     *       "data": {
     *       }
     *   }
     *
     * @apiError {json} Failed to save reason
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription Add new reason for absentee
     */
    public function store(Request $request)
    {
        if (Bouncer::cannot('manage-all-attendance')) {
            return Reply::unAuthorized();
        }

        $this->validate($request, [
            'reason' => 'required|string|max:64',
            'notify' =>  'boolean',
        ]);
        $reason = new AbsentReason();
        $reason->reason = $request->reason;
        $reason->notify = $request->notify;
        $reason->save();

        activity(config('app.tenant')->tenant_id)
            ->performedOn($reason)
            ->withProperties(['action' => 'create', 'section' => 'absentReason'])
            ->log(__('messages.logAbsentReasonCreated'));
        return response()->json(Reply::success(__('messages.absentReasonCreated')));


    }

    /**
     * @api            {PUT|PATCH} /attendance/reason/:id Update Reason
     * @apiName        UpdateReason
     * @apiGroup       Attendance
     *
     * @apiParam {String{..64}} reason Title of reason
     * @apiPermission  manage-all-attendance
     * @apiParamExample {json} Request-Example:
     *   {
     *          reason:"Pneumonia"
     *   }
     *
     * @apiSuccess {json} Reason Updated
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *   {
     *       "status": true,
     *       "message": "Reason Updated",
     *       "data": {
     *       }
     *   }
     *
     * @apiError {json} Failed to updated reason
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription Add new reason for absentee
     */
    public function update(Request $request, $id)
    {
        if (Bouncer::cannot('manage-all-attendance')) {
            return Reply::unAuthorized();
        }
        $this->validate($request, [
            'reason' => 'required|string|max:64',
            'notify'=>'boolean'
        ]);
        $reason = AbsentReason::findOrFail($id);
        $reason->reason = $request->reason;
        $reason->notify = $request->notify;
        $reason->save();

        activity(config('app.tenant')->tenant_id)
            ->performedOn($reason)
            ->withProperties(['action' => 'update', 'section' => 'absentReason'])
            ->log(__('messages.logAbsentReasonCreated'));
        return response()->json(Reply::success(__('messages.absentReasonUpdated')));
    }

    /**
     * @api            {DELETE} /attendance/reason Delete Reason
     * @apiName        Delete
     * @apiGroup       Attendance
     * @apiPermission  manage-all-attendnace
     * @apiSuccess {json} Reason Removed
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *   {
     *       "status": true,
     *       "message": "Reason Deleted",
     *       "data": {
     *       }
     *   }
     *
     * @apiError {json} Failed to delete reason
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription Add new reason for absentee
     */
    public function destroy($id)
    {
        if (Bouncer::cannot('manage-all-attendance')) {
            return Reply::unAuthorized();
        }
        $absentReason = AbsentReason::findOrFail($id);
        $absentReason->delete();
        activity(config('app.tenant')->tenant_id)
            ->performedOn($absentReason)
            ->withProperties(['action' => 'delete', 'section' => 'absentReason'])
            ->log(__('messages.logAbsentReasonCreated'));
        return response()->json(Reply::success(__('messages.absentReasonDeleted')));

    }
}
