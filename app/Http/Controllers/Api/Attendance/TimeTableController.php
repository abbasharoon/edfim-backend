<?php

namespace App\Http\Controllers\Api\Attendance;

use App\ClassTimetable;
use App\ClassUser;
use App\Helpers\PermissionsHelper;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTimeTable;
use App\Http\Requests\UpdateTimetable;
use App\Timetable;
use Bouncer;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * @apiDefine Params
 * @apiParam {String{10..60}} title Title for the Holiday/Timetable Event
 * @apiParam {String{..500}} [detail] Detail of the Holiday/Timetable Event
 * @apiParam {String} start_date Inclusive Start Date or the date of holiday/Timetable Event, Format: y-m-d
 * @apiParam {String} [end_date] End date for multiple day holiday/event, Format: y-m-d
 * @apiParam {Array} class_id An array of ids of classes for which the holiday is applicable
 */
/**
 * @apiDefine ParamExample
 * @apiParamExample {Object} Single Holiday Example:
 *            {
 *                title: "National Holiday",
 *                class_id: [1,2,4],
 *                start_date: "2017-11-22"
 *                detail: "Some Optional details about this holiday",
 *            }
 * @apiParamExample {Object} Multiple Holiday Example:
 *            {
 *                title: "Winter Vacations",
 *                class_id: [1,2,4],
 *                start_date: "2017-11-22"
 *                end_date: "2018-01-10"
 *                detail: "Some Optional details about this holiday",
 *            }
 */

/**
 * @apiDefine Success
 * @apiSuccess {String} message Success Message
 * @apiSuccess {Boolean} status Returns True
 * @apiSuccess {Object[]} data Optionally data will be sent back
 *
 * @apiSuccessExample {json}Success-Response:
 *      HTTP/1.1 200 OK
 *      {
 *          "status":true,
 *          "message":"Schedule has been successfully stored",
 *          "data":[]
 *      }
 *
 */

/**
 * @apiDefine Error
 * @apiError {String} message Error Message
 * @apiError {Boolean} status Returns False
 * @apiError {Object[]} data Optionally data will be sent back
 *
 */
class TimeTableController extends Controller
{
    /**
     * @api           {GET} /timetable Get Timetable data
     * @apiName       GetTimetable
     * @apiPermission view-a;;-timetable,view-timetable,(student/parent)
     * @apiGroup      Timetable
     * @apiParam {String} date Any date of an year for which Timetable records are required, Format Y-m-d
     * @apiSuccess {JSON} TimetableRecords A plain JSON Object of data is sent back
     * @apiSuccessExample {json} Success-Response:
     *      HTTP/1.1 200 OK
     *      [
     *          {
     *              id: 1,
     *              title: "Iqbal Holiday",
     *              detail: null,
     *              start_date: "2017-11-23 00:00:00",
     *              end_date: null,
     *              created_at: "2017-12-30 13:05:20",
     *              updated_at: "2017-12-30 13:05:20",
     *              class_timetable: [
     *                                   {
     *                                      id: 1,
     *                                      class_id: 1,
     *                                      timetable_id: 1
     *                                    },
     *                                    {
     *                                      id: 2,
     *                                      class_id: 2,
     *                                      timetable_id: 1
     *                                    }
     *                                  ]
     *          }
     *       ]
     * @apiUse        Error
     */
    public function index(Request $request, PermissionsHelper $ph)
    {

        $carbon = Carbon::createFromFormat('Y-m-d', $request->date);
        $timetableData = Timetable::where(function ($q) use ($carbon) {
            $q->whereBetween('start_date',
                [$carbon->startOfYear()->toDateTimeString(),
                    $carbon->endOfYear()->toDateTimeString()])
                ->orWhereBetween('end_date',
                    [$carbon->startOfYear()->toDateTimeString(),
                        $carbon->endOfYear()->toDateTimeString()]);
        })->with('classTimetable');
        if (Bouncer::can('view-all-timetable')) {

        } elseif (Bouncer::can('view-timetable')) {
            $timetableData->whereHas('classTimetable', function ($i) use ($ph) {
                $i->whereIn('class_id', ClassUser::whereUserId(auth()->id())->get()->pluck('class_id'));
            });
        } elseif (auth()->user()->isA('student', 'parent')) {
            $timetableData->whereHas('classTimetable', function ($i) use ($ph) {
                $i->whereIn('class_id', $ph->studentHasClass());
            });
        }
        return response()->json(Reply::success('', $timetableData->get()));
    }

    /**
     * @api           {post} /timetable Create
     * @apiName       StoreTimetable
     * @apiPermission manage-all-timetable
     * @apiPermission Only users with Permission (Permission name here)
     * @apiGroup      Timetable
     * @apiUse        Params
     * @apiUse        ParamExample
     * @apiUse        Success
     * @apiUse        Error
     */
    public function store(StoreTimeTable $request)
    {
        $timetable = new Timetable();
        $timetable->title = $request->title;
        $timetable->detail = $request->detail;
        $timetable->start_date = $request->start_date;
        $timetable->end_date = $request->end_date;
        if ($timetable->save()) {
            $classTimeTable = [];
            foreach ($request->class_id as $k => $v) {
                $classTimeTable[$k]['class_id'] = $v;
                $classTimeTable[$k]['timetable_id'] = $timetable->id;
                $classTimeTable[$k]['tenant_id'] = config('app.tenant')->tenant_id;
            }
            if (ClassTimetable::insert($classTimeTable)) {
                activity(config('app.tenant')->tenant_id)
                    ->on($timetable)
                    ->withProperties(['action' => 'create', 'section' => 'timetable'])
                    ->log(__('messages.logTimetableCreated'));
                return response()->json(Reply::success(__('messages.timeTableCreated'), $timetable));
            }

        }
        return response()->json(Reply::error(__('messages.timeTableNotCreated')));
    }


    /**
     * @api           {PUT | PATCH} /timetable/:id Update
     * @apiName       UpdateTimetable
     * @apiGroup      Timetable
     * @apiPermission manage-all-timetable
     * @apiUse        Params
     * @apiParamExample {Object} Single Holiday Example:
     *            {
     *                id: 1,
     *                title: "National Holiday",
     *                class_id: [1,2,4],
     *                start_date: "2017-11-22"
     *                detail: "Some Optional details about this holiday",
     *            }
     * @apiUse        Success
     * @apiUse        Error
     */
    public function update(UpdateTimetable $request, $id)
    {
        $timetable = Timetable::findOrFail($id);
        $timetable->title = $request->title;
        $timetable->start_date = date('Y-m-d H:i:s', strtotime($request->start_date));
        $timetable->end_date = (isset($request->end_date)) ? date('Y-m-d H:i:s', strtotime($request->end_date)) : null;
        $timetable->detail = $request->detail;


        if ($timetable->save()) {

            $classData = [];
            foreach ($request->class_id as $i) {
                $classData[] = [
                    'class_id' => $i,
                    'tenant_id' => config('app.tenant')->tenant_id,
                    'timetable_id' => $timetable->id,
                ];
            }
//            TODO debug that why sync isn't deleting old records
            $timetable->classTimetable()->detach();
            $timetable->classTimetable()->sync($classData);

            activity(config('app.tenant')->tenant_id)
                ->on($timetable)
                ->withProperties(['action' => 'update', 'section' => 'timetable'])
                ->log(__('messages.logTimetableUpdated'));
            return response()->json(Reply::success(__('messages.timeTableUpdated')));
        }

        return response()->json(Reply::error(__('messages.timeTableNotUpdated')));
    }

    /**
     * @api           {delete} /timetable/:id  Delete .
     * @apiName       DeleteTimeTable
     * @apiGroup      Timetable
     * @apiPermission manage-all-timetable
     * @apiParam {Integer} id Id of the timetable record to be deleted, passed via route path
     *
     * @apiUse        Success
     * @apiUse        Error
     */
    public function destroy(Timetable $timetable)
    {
        if (Bouncer::cannot('manage-all-timetable')) {
            return Reply::unAuthorized();
        }
        try {
            $timetable->delete();
            $timetable->classTimetable()->detach();
            activity(config('app.tenant')->tenant_id)
                ->performedOn($timetable)
                ->withProperties(['action' => 'delete', 'section' => 'timetable'])
                ->log(__('messages.logTimetableDeleted'));

            return response()->json(Reply::success(__('messages.timetableDeleted')));
        } catch (\Exception $e) {
            return response()->json(Reply::error(__('messages.timetableNotDeleted')));
        }
    }
}
