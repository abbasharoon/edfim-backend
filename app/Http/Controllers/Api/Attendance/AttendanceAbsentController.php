<?php

namespace App\Http\Controllers\Api\Attendance;

use App\Attendance;
use App\AttendanceAbsent;
use App\Helpers\PermissionsHelper;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Attendance\StoreAttendanceAbsent;
use App\Notifications\MarkedAbsent;
use App\Notifications\MarkedPresent;
use App\User;
use Bouncer;
use Illuminate\Http\Request;

class AttendanceAbsentController extends Controller
{


    /**
     * @api             {POST} /attendance/absent Add Absents
     * @apiName         Save attendance absent data
     * @apiGroup        Attendance
     * @apiPermission   manage-all-attendance,manage-attendance
     * @apiParam {Number} attendance_id Id of attendance for which absents are added. Attendance status must be 0.
     * @apiParam {Object[]} absent_students Array of objects containing ids of absent students with reasons.
     * @apiParam {Number} absent_students.user_id  Id of of absent user
     * @apiParam {Number} absent_students.reason_id  Id of reason for absentee.
     *
     * @apiParamExample {json} Request-Example:
     *   {
     *        "attendance_id":"1",
     *        "absent_students": [
     *                  {
     *                      "user_id": 1,
     *                      "reason_id: 4,
     *                  },{
     *                      "user_id": 2,
     *                      "reason_id: 3,
     *                  },{
     *                      "user_id": 4,
     *                      "reason_id: 3,
     *                  },{
     *                      "user_id": 33,
     *                      "reason_id: 1,
     *                  },
     *             ]
     *   }
     *
     * @apiPermission   Only admin users
     * @apiSuccess {json} Attendance successfully created details
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Attendance absent created"
     *      }
     *
     * @apiError {json} Details of Attendance couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription  Attendance Store
     */
    public function store(StoreAttendanceAbsent $request)
    {
        $attendance = Attendance::findOrFail($request->attendance_id);
        $attendanceAbsent = new AttendanceAbsent();
        $attendanceAbsent->attendance_id = $request->attendance_id;
        $attendanceAbsent->user_id = $request->user_id;
        $attendanceAbsent->reason_id = $request->reason_id;
        $attendanceAbsent->tenant_id = config('app.tenant')->tenant_id;
        $attendanceAbsent->save();
        if ($attendance->status === 1 && $attendanceAbsent->reason->notify) {
            $user = User::with('parent:id,first_name,middle_name,last_name,email,phone_no',
                'parent.notificationPreference', 'notificationPreference')->findOrFail($request->user_id);
            $user->notify(new MarkedAbsent($attendance, true));
            if ($user->parent) {
                \Notification::send($user->parent, new MarkedAbsent($attendance, null, $user));
            }
        }
        activity(config('app.tenant')->tenant_id)
            ->performedOn($attendance)
            ->withProperties(['action' => 'create', 'section' => 'attendanceAbsents'])
            ->log(__('messages.logAttendanceAbsentCreated'));

        return response()->json(Reply::success(__('messages.attendanceAbsentCreated'), $attendanceAbsent));
    }

//    Following Method is for bulk storing students

//    public function store(StoreAttendanceAbsent $request)
//    {
//        $absentStudents = [];
//        $studentIds = [];
//        $attendance = Attendance::findOrFail($request->attendance_id);
//        foreach ($request->absent_students as $key => $value) {
//            $absentStudents[$key]['attendance_id'] = $request->attendance_id;
//            $absentStudents[$key]['user_id'] = $request->absent_students[$key]['user_id'];
//            $absentStudents[$key]['reason_id'] = $request->absent_students[$key]['reason_id'];
//            $studentIds[$key] = $request->absent_students[$key]['user_id'];
//        }
//        AttendanceHelper::notify($attendance, $studentIds);
//        /** @noinspection PhpDynamicAsStaticMethodCallInspection */
//        AttendanceAbsent::insert($absentStudents);
//        // add log
//        activity(config('app.tenant')->tenant_id)
//            ->performedOn($attendance)
//            ->withProperties(['action' => 'create', 'section' => 'attendanceAbsents'])
//            ->log(__('messages.logAttendanceAbsentCreated'));
//
//        return response()->json(Reply::success(__('messages.attendanceAbsentCreated')));
//    }


    /**
     * @api            {DELETE} /attendance/absent/id Delete Absent
     * @apiName        delete attendance absent
     * @apiGroup       Attendance
     * @apiPermission  manage-all-attendance,manage-attendance
     * @apiPermission  manage-attendance,manage-all-attendance
     * @apiSuccess {json} Attendance successfully deleted
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Attendance absent deleted"
     *      }
     *
     * @apiError {json} Details of Attendance couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription Attendance Store
     */
    public function destroy(Request $request, PermissionsHelper $ph)
    {
        $query = AttendanceAbsent::whereUserId($request->user_id)
            ->whereAttendanceId($request->attendance_id);
        $attendanceAbsent = $query->first();
        if ($attendanceAbsent) {
            $attendance = $attendanceAbsent->attendance;
            if (Bouncer::can('manage-all-attendance') ||
                (Bouncer::can('manage-attendance') && $ph->hasBatch($attendance->batch_id))) {

                if ($attendanceAbsent->reason->notify) {
                    $user = User::with('parent:id,first_name,middle_name,last_name,email,phone_no',
                        'parent.notificationPreference')->findOrFail($attendanceAbsent->user_id);
                    if ($user->parent) {
                        \Notification::send($user->parent, new MarkedPresent($attendance, $user));
                    }
                }
                $query->delete();
                activity(config('app.tenant')->tenant_id)
                    ->performedOn($attendance)
                    ->withProperties(['action' => 'delete', 'section' => 'attendanceAbsents'])
                    ->log(__('messages.logAttendanceAbsentDeleted'));
                return response()->json(Reply::success(__('messages.attendanceAbsentDeleted')));

            }
            return Reply::unAuthorized();
        } else {
            return Reply::error('Attendance not found. Please try again');
        }
    }

}
