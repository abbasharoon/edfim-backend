<?php

namespace App\Http\Controllers\Api\Fee;

use App\Fee;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Fee\StoreFine;
use App\Reputation;
use App\UserFee;

class FineController extends Controller
{

    /**
     * @api            {GET} /fee/fine Get Fines List
     * @apiName        GetFeeFineData
     * @apiGroup       Fee
     *
     * @apiSuccess {Object[]} data Object containing required data
     * @apiSuccess {Number} data.id  Id of the fee in database
     * @apiSuccess {String} data.name Name of the fee for displaying
     * @apiSuccess {String} data.description  Details/Description of the fee if available
     * @apiSuccess {Number} data.amount Amount of the fee that's charged to the student
     * @apiSuccess {Number} data.schedule Schedule of the fee i.e 0=none,1=daily,2=weekly,3=monthly,4=triannual,5=quarterly,6=biannually,7=annually
     * @apiSuccess {Number} data.type Type denotes whether the fee is percentage or just number.1=number,2=percentage
     * @apiSuccess {Number} data.addition Addition specifies whether the fee is to be added or subtracted. 0=subtract,1=add
     * @apiSuccess {Number} data.fee_category_id Id of category to which fee is related.
     * @apiSuccessExample {Object} Success-Example:
     *                {
     *                   "data": [
     *                     {
     *                       "id": 1,
     *                       "name": "Class 1 Tuition Fee",
     *                       "description": "",
     *                       "amount": 400,
     *                       "schedule": 3,
     *                       "type": 1,
     *                       "addition": 0,
     *                       "fee_category_id": 1,
     *                       "deleted_at": null,
     *                       "created_at": null,
     *                       "updated_at": null
     *                   },
     *                   {
     *                       "id": 2,
     *                       "name": "Class 2 Tuition Fee",
     *                       "description": "",
     *                       "amount": 300,
     *                       "schedule": 3,
     * v                       "type": 1,
     *                       "addition": 0,
     *                       "fee_category_id": 1,
     *                       "deleted_at": null,
     *                       "created_at": null,
     *                       "updated_at": null
     *                   },
     *                   {
     *                       "id": 3,
     *                       "name": "Promotion Fee",
     *                       "description": "",
     *                       "amount": 200,
     *                       "schedule": 3,
     *                       "type": 1,
     *                       "addition": 0,
     *                       "fee_category_id": 2,
     *                       "deleted_at": null,
     *                       "created_at": null,
     *                       "updated_at": null
     *                   }
     *                       ],
     *                 }
     * @apiError {json} Details of Fees couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription Fees Store
     */
    public function index()
    {
        if (\Bouncer::can('view-fine')) {
            return response()->json(['data' => $fines = Fee::whereFeeCategoryId(setting('fine.fee_category_id'))->get()]);
        }
        return Reply::unAuthorized();
    }

    /**
     * @api            {POST} /fee/fine Create Fine
     * @apiName        storeFeeFineData
     * @apiGroup       Fee
     *
     * @apiParam {Number} user_id Id of user to whom fine is applied
     * @apiParam {Number} fee_id  Fee id from fine category
     * @apiPermission  Only admin users
     *
     * @apiSuccess {Boolean} status Status demonstrates the status of operation. True for success
     * @apiSuccess {String} message Success message to be displayed to end user if required
     * @apiSuccess {Object[]} data Object contining fee data
     * @apiSuccess {Number} data.id  Id of the User fee relation in database
     * @apiSuccess {String} data.fee_id Id of the applied fee
     * @apiSuccess {String} data.user_id  Id of the user to whom it's applied
     * @apiSuccessExample {json} Success-Response:
     *           {
     *               "status": true,
     *               "message": "messages.feeCreated",
     *               "data": {
     *                   "id": 6
     *                   "fee_id": 6
     *                   "user_id": 6
     *               }
     *           }
     * @apiError {json} Details of Fees couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription Fees Store
     */
    public function store(StoreFine $request)
    {
        $userFee = new UserFee();
        $userFee->user_id = $request->user_id;
        $userFee->fee_id = $request->fee_id;
        $save = $userFee->save();
        $fineBadgeId = setting()->get('fine.reputation_badge_id');
        if ($save && !empty($fineBadgeId)) {
            $badgeUser = new Reputation();
            $badgeUser->badge_id = $$fineBadgeId;
            $badgeUser->user_id = $request->user_id;
            $badgeUser->detail = $request->fee_id;
            $badgeUser->save();
        }
        return Reply::loggedResponse($save, $userFee, 'create', 'fine', 'fine');
    }

}
