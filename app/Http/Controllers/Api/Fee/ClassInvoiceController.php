<?php

namespace App\Http\Controllers\Api\Fee;

use App\ClassInvoice;
use App\Fee;
use App\Helpers\InvoiceHelper;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Fee\StoreClassInvoice;
use App\Http\Requests\Fee\UpdateClassInvoice;
use Bouncer;
use Illuminate\Http\Request;

class ClassInvoiceController extends Controller
{


    /**
     * @api            {GET} /fee/invoice/class Summary of class invoices
     * @apiName        GetClassInvoiceSummary
     * @apiGroup       Fee Invoice
     * @apiDescription A paginated list of class invoices
     * @apiSuccess {Object[]} data Object containing required data
     * @apiSuccess {Number} data.id  Id of the fee in database
     * @apiSuccess {Number} data.amount_paid Total amount paid for the invoice
     * @apiSuccess {Number} data.class_id  Id of the class for which invoice is created
     * @apiSuccess {Number} data.status Status of invoice, 0-draft,1=due,2=paid,3=canceled
     * @apiSuccess {String} data.due_date Date the invoice was due on
     * @apiSuccess {String} data.created_at Date on which invoice is created
     * @apiSuccess {String} data.updated_at Date on which invoice was updated
     * @apiSuccess {Number} current_page Current Page number on pagination index
     * @apiSuccess {Number} from Start of number of received fees numbers
     * @apiSuccess {Number} to End of number of received fees numbers
     * @apiSuccess {Number} total Total Number of Fees in database
     * @apiSuccess {Number} last_page Last page on pagination index
     * @apiSuccess {Number} per_page Number of records per page
     *
     * @apiParam {Number} class_id Id of class for which invoices are retrieved
     * @apiParam {Number} [page] Page number of results
     * @apiParam {String="created_at,updated_at,due_date,status,amount_paid"} [order_by] Order the result by given variable
     * @apiParam {String="desc,asc"} [order_type] Order results descending or ascending. Default ascending
     *
     * @apiSuccessExample {Object} Success-Example:
     *                {
     *                   "data": [
     *                             {
     *                               "id": 1,
     *                               "amount_paid": 0,
     *                               "class_id": 6,
     *                               "batch_id": null,
     *                               "status": 1,
     *                               "due_date": "2010-04-30 00:00:00",
     *                               "deleted_at": null,
     *                               "created_at": "2018-05-05 23:19:05",
     *                               "updated_at": "2018-05-05 23:19:05"
     *                             },
     *                             {
     *                               "id": 1,
     *                               "amount_paid": 0,
     *                               "class_id": 6,
     *                               "batch_id": null,
     *                               "status": 1,
     *                               "due_date": "2010-04-30 00:00:00",
     *                               "deleted_at": null,
     *                               "created_at": "2018-05-05 23:19:05",
     *                               "updated_at": "2018-05-05 23:19:05"
     *                            }
     *                       ],
     *                  "current_page": 1,
     *                  "last_page": 1,
     *                  "per_page": 50,
     *                  "from": 1,
     *                  "to": 1,
     *                  "total": 1
     *                   }
     *               }
     *
     * @apiPermission  view-all-class-invoice
     * @apiError {Boolean="false"}  status Status of the request. False in case of failure
     * @apiError {String}  message An error message to be displayed
     * @apiError {Object[]}  [errors] An optional object containing error causes/reasons
     *
     */
    public function index(Request $request)
    {
        if (Bouncer::can('view-all-class-invoice') ||
            Bouncer::can('view-all-student-invoice')) {
            $invoices = ClassInvoice::query();

            if ($request->class_id) {
                $invoices = $invoices->whereClassId($request->class_id);
            }


            if ($request->filled('invoice_id')) {
                $invoices = $invoices->where('id', $request->invoice_id);
            }

            if ($request->filled('due_date')) {
                $invoices = $invoices->where('due_date', $request->due_date);
            }
            if ($request->filled('status')) {
                $invoices = $invoices->where('status', $request->status);
            }

            if ($request->filled('schedule')) {
                $invoices = $invoices->where('schedule', $request->schedule);
            }

            if ($request->filled('amount_paid')) {
                $invoices = $invoices->where('amount_paid', $request->schedule);
            }


            if (!$request->sort_column) {
                $request->sort_column = 'created_at';
                $request->sort_direction = 'desc';
            }
            if ($request->sort_direction === 'desc') {
                $invoices->orderByDesc($request->sort_column);
            } else {
                $invoices->orderBy($request->sort_column);
            }


            $invoices = $invoices->paginate($request->page_size);
            return response()->json($invoices);
        }
        return Reply::unAuthorized();
    }

    /**
     * @api            {POST} /fee/invoice/class Create Class Invoice
     * @apiName        storeClassFeeInvoice
     * @apiGroup       Fee Invoice
     * @apiPermission  manage-all-class-invoice
     * @apiParam {Number} class_id Id of the class for which invoice needs to be created.
     * @apiParam {Number} [schedule] Schedule of associated fees to be included in the invoice
     * @apiParam {Object[]} [fee] An object containing the fees to be included in the invoice
     * @apiParam {Number} fee.fee_id Id of the fee
     * @apiParam {Number} [target_fee_id] Id of the fee on which the fee is applied, it must be already associated with class
     *
     * @apiSuccess {Boolean} status Status demonstrates the status of operation. True for success
     * @apiSuccess {String} message Success message to be displayed to end user if required*
     * @apiSuccess {Object[]} data Object containing fee data
     * @apiSuccess {Number} data.id  Id of the fee in database
     * @apiSuccess {Number} data.amount_paid Total amount paid for the invoice
     * @apiSuccess {Number} data.class_id  Id of the class for which invoice is created
     * @apiSuccess {Number} data.status Status of invoice, 0-draft,1=due,2=paid,3=canceled
     * @apiSuccess {String} data.due_date Date the invoice was due on
     * @apiSuccess {String} data.created_at Date on which invoice is created
     * @apiSuccess {String} data.updated_at Date on which invoice was updated
     * @apiSuccessExample {json} Success-Response:
     *           {
     *               "status": true,
     *               "message": "messages.feeCreated",
     *               "data": {
     *                    "id": 1,
     *                    "amount_paid": 0,
     *                    "class_id": 6,
     *                    "status": 1,
     *                    "due_date": "2010-04-30 00:00:00",
     *                    "deleted_at": null,
     *                    "created_at": "2018-05-05 23:19:05",
     *                    "updated_at": "2018-05-05 23:19:05"
     *                    }
     *           }
     * @apiError {json} Details of Fees couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription Fees Store
     */
    public function store(StoreClassInvoice $request)
    {
        if (\Bouncer::cannot('manage-all-class-invoice')) {
            return Reply::unAuthorized();
        }
        $invoice = InvoiceHelper::createClassInvoice($request);
        return Reply::loggedResponse($invoice,
            $invoice,
            'create',
            'invoice',
            'classInvoice');
    }

    /**
     * @api            {GET} /fee/invoice/class/:id Get Single Class Invoice
     * @apiName        GetSingleClassInvoice
     * @apiGroup       Fee Invoice
     * @apiDescription Invoice with details of the fees and students it was applied to
     * @apiSuccess {Object[]} data Object containing required data
     * @apiSuccess {Number} data.id  Id of the fee in database
     * @apiSuccess {Number} data.amount_paid Total amount paid for the invoice
     * @apiSuccess {Number} data.class_id  Id of the class for which invoice is created
     * @apiSuccess {Number} data.status Status of invoice, 0-draft,1=due,2=paid,3=canceled
     * @apiSuccess {String} data.due_date Date the invoice was due on
     * @apiSuccess {String} data.created_at Date on which invoice is created
     * @apiSuccess {String} data.updated_at Date on which invoice was updated
     * @apiSuccess {Object[]} data.fee Object containing fee data related to the invoice
     * @apiSuccess {Object[]} data.fee.fee  Sub object having fee name and details
     * @apiSuccess {Number} data.fee.fee.id  Id of the fee in database
     * @apiSuccess {String} data.fee.fee.name Name of the fee for displaying
     * @apiSuccess {String} data.fee.fee.description  Details/Description of the fee if available
     * @apiSuccess {Number} data.fee.fee.amount Amount of the fee that's charged to the student
     * @apiSuccess {Number} data.fee.fee.schedule Schedule of the fee i.e 0=none,1=daily,2=weekly,3=monthly,4=triannual,5=quarterly,6=biannually,7=annually
     * @apiSuccess {Number} data.fee.fee.type Type denotes whether the fee is percentage or just number.1=number,2=percentage
     * @apiSuccess {Number} data.fee.fee.addition Addition specifies whether the fee is to be added or subtracted. 0=subtract,1=add
     * @apiSuccess {Number} data.fee.fee.fee_category_id Id of category to which fee is related.
     * @apiSuccess {Object[]} data.fee.target_fee Sub object having fee name and detail of target fee, can be null
     * @apiSuccess {Number} data.fee.target_fee.id  Id of the fee in database
     * @apiSuccess {String} data.fee.target_fee.name Name of the fee for displaying
     * @apiSuccess {String} data.fee.target_fee.description  Details/Description of the fee if available
     * @apiSuccess {Number} data.fee.target_fee.amount Amount of the fee that's charged to the student
     * @apiSuccess {Number} data.fee.target_fee.schedule Schedule of the fee i.e 0=none,1=daily,2=weekly,3=monthly,4=triannual,5=quarterly,6=biannually,7=annually
     * @apiSuccess {Number} data.fee.target_fee.type Type denotes whether the fee is percentage or just number.1=number,2=percentage
     * @apiSuccess {Number} data.fee.target_fee.addition Addition specifies whether the fee is to be added or subtracted. 0=subtract,1=add
     * @apiSuccess {Number} data.fee.target_fee.fee_category_id Id of category to which fee is related.
     *
     * @apiSuccessExample {Object} Success-Example:
     *           {
     *               "status": true,
     *               "message": "",
     *               "data": {
     *                   "id": 1,
     *                   "amount_paid": 0,
     *                   "class_id": 6,
     *                   "batch_id": null,
     *                   "status": 1,
     *                   "deleted_at": null,
     *                   "created_at": "2018-05-05 23:19:05",
     *                   "updated_at": "2018-05-05 23:19:05",
     *                   "fee": [
     *                       {
     *                           "id": 4,
     *                           "class_invoice_id": 1,
     *                           "fee_id": 1,
     *                           "target_fee_id": null,
     *                           "created_at": null,
     *                           "updated_at": null,
     *                           "fee": {
     *                               "id": 1,
     *                               "name": "Class 1 Tuition Fee",
     *                               "description": "",
     *                               "amount": 400,
     *                               "schedule": 3,
     *                               "type": 1,
     *                               "addition": 0,
     *                               "fee_category_id": 1,
     *                               "deleted_at": null,
     *                               "created_at": null,
     *                               "updated_at": null
     *                           },
     *                           "target_fee": {
     *                              "id": 2,
     *                               "name": "Class 2 Tuition Fee",
     *                               "description": "",
     *                               "amount": 300,
     *                               "schedule": 3,
     *                               "type": 1,
     *                               "addition": 0,
     *                               "fee_category_id": 1,
     *                               "deleted_at": null,
     *                               "created_at": null,
     *                               "updated_at": null
     *                           }
     *                       },
     *                       {
     *                           "id": 9,
     *                           "class_invoice_id": 1,
     *                           "fee_id": 3,
     *                           "target_fee_id": null,
     *                           "created_at": null,
     *                           "updated_at": null,
     *                           "fee": {
     *                               "id": 3,
     *                               "name": "Promotion Fee",
     *                               "description": "",
     *                               "amount": 200,
     *                               "schedule": 3,
     *                               "type": 1,
     *                               "addition": 0,
     *                               "fee_category_id": 2,
     *                               "deleted_at": null,
     *                               "created_at": null,
     *                               "updated_at": null
     *                           },
     *                           "target_fee": null
     *                       }
     *                   ],
     *                   "user_invoice": [
     *                       {
     *                           "id": 1,
     *                           "user_id": 13,
     *                           "class_invoice_id": 1,
     *                           "amount_paid": 0,
     *                           "status": 1,
     *                           "due_date": null,
     *                           "deleted_at": null,
     *                           "created_at": "2018-05-05 23:19:05",
     *                           "updated_at": "2018-05-05 23:19:05"
     *                       },
     *                       {
     *                           "id": 6,
     *                           "user_id": 18,
     *                           "class_invoice_id": 1,
     *                           "amount_paid": 0,
     *                           "status": 1,
     *                           "due_date": null,
     *                           "deleted_at": null,
     *                           "created_at": "2018-05-05 23:19:05",
     *                           "updated_at": "2018-05-05 23:19:05"
     *                       },
     *                       {
     *                           "id": 7,
     *                           "user_id": 19,
     *                           "class_invoice_id": 1,
     *                           "amount_paid": 0,
     *                           "status": 1,
     *                           "due_date": null,
     *                           "deleted_at": null,
     *                           "created_at": "2018-05-05 23:19:05",
     *                           "updated_at": "2018-05-05 23:19:05"
     *                       },
     *                   ]
     *               }
     *           }
     * @apiPermission  view-all-class-invoice
     * @apiError {Boolean="false"}  status Status of the request. False in case of failure
     * @apiError {String}  message An error message to be displayed
     * @apiError {Object[]}  [errors] An optional object containing error causes/reasons
     *
     */
    public function show($id)
    {
        if (Bouncer::cannot('view-all-class-invoice')) {
            return Reply::unAuthorized();
        }
        $invoice = ClassInvoice::with('fee', 'fee.fee', 'fee.targetFee', 'userInvoice', 'userInvoice.student', 'class')->findOrFail($id);

        $feeIds = $invoice->fee->pluck('fee_id')
            ->concat($invoice->fee->pluck('target_fee_id'))->filter()->unique();
        $feeData = Fee::whereIn('id', $feeIds)->get()->keyBy('id')->toArray();
        $invoiceData = InvoiceHelper::invoiceMaker($invoice->fee, $feeData);
        $invoice['invoices'] = $invoiceData['invoices'];
        $invoice['total'] = $invoiceData['total'];
        return response()->json(Reply::success('', $invoice));

    }

    /**
     * @api            {PUT|PATCH} /fee/invoice/class/:id Update Class Invoice
     * @apiName        updateClassInvoice
     * @apiGroup       Fee Invoice
     * @apiPermission  manage-all-class-invoice
     * @apiParam {Number="1,2,3"}} [status] Status of Invoice
     * @apiParam {Number} [amount_paid] Amount paid by the user
     * @apiParam {String} [due_date] Due date of invoice
     * @apiParam {Number} invoice_Id Id of invoice which needs to be updated.
     * @apiParam {Object[]} [add_fee] An object containing the fees to be added to invoice
     * @apiParam {Number} add_fee.fee_id Id of the fee
     * @apiParam {Number} [add_fee.target_fee_id] Id of the fee on which the fee is applied
     * @apiParam {Object[]} [remove_fee] An object containing the fees to be removed from invoice
     * @apiParam {Number} remove_fee.fee_id Id of the fee
     *
     * @apiSuccess {Boolean} status Status demonstrates the status of operation. True for success
     * @apiSuccess {String} message Success message to be displayed to end user if required*
     * @apiSuccess {Object[]} data Object containing fee data
     * @apiSuccess {Number} data.id  Id of the fee in database
     * @apiSuccess {Number} data.amount_paid Total amount paid for the invoice
     * @apiSuccess {Number} data.class_id  Id of the class for which invoice is created
     * @apiSuccess {Number} data.status Status of invoice, 0-draft,1=due,2=paid,3=canceled
     * @apiSuccess {String} data.due_date Date the invoice was due on
     * @apiSuccess {String} data.created_at Date on which invoice is created
     * @apiSuccess {String} data.updated_at Date on which invoice was updated
     * @apiSuccessExample {json} Success-Response:
     *           {
     *               "status": true,
     *               "message": "messages.feeCreated",
     *               "data": {
     *                    "id": 1,
     *                    "amount_paid": 0,
     *                    "class_id": 6,
     *                    "status": 1,
     *                    "due_date": "2010-04-30 00:00:00",
     *                    "deleted_at": null,
     *                    "created_at": "2018-05-05 23:19:05",
     *                    "updated_at": "2018-05-05 23:19:05"
     *                    }
     *           }
     * @apiError {json} Details of Fees couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription Fees Store
     */
    public function update(UpdateClassInvoice $request)
    {
        $invoice = InvoiceHelper::updateClassInvoice($request);
        $invoice = ClassInvoice::findOrFail($invoice->id);
        return Reply::loggedResponse($invoice, $invoice, 'update', 'invoice', 'classInvoice');
    }


}
