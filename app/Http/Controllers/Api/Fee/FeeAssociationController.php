<?php

namespace App\Http\Controllers\Api\Fee;

use App\ClassFee;
use App\Fee;
use App\Helpers\InvoiceHelper;
use App\Helpers\PermissionsHelper;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Fee\StoreFeeAssociation;
use App\User;
use App\UserFee;
use Bouncer;
use Illuminate\Http\Request;

class FeeAssociationController extends Controller
{


    /**
     * @api            {GET} /fee/associate/ Get Association Details
     * @apiName        GetAssociationDetails
     * @apiGroup       Fee
     * @apiDescription A list of fees for classes or users
     * @apiSuccess {Object[]} data Object containing required data
     * @apiSuccess {Object[]} data.fee  Object containing details of fee if any
     * @apiSuccess {Number} data.fee.id  Id of fee
     * @apiSuccess {String} data.fee.name  Name of fee
     * @apiSuccess {String} data.fee.amount  Name of fee
     * @apiSuccess {Object[]} data.fee.target_fee  An array of target fees applicable to the original fee
     * @apiSuccess {Number} data.fee.target_fee.id  Id of the target fee in database
     * @apiSuccess {Number} data.fee.target_fee.name  Name of the target fee in database
     * @apiSuccess {String} data.fee.target_fee.amount  Amount of fee applicable
     * @apiSuccess {String} data.total  Total amount applicable
     *
     *
     * @apiParam {Number} [class_id] Id of class for which fee details are required
     * @apiParam {Number} [user_id] Id of user for which fee details are required
     * @apiParam {Number} [schedule="0,1,2,3,4,5,6,7"] Schedule type for which fees are required. Default to 3
     *
     * @apiSuccessExample {Object} Class Association data:
     *            {
     *               "status": true,
     *               "message": "",
     *               "data": {
     *                   "fee": [
     *                       {
     *                           "id": 4,
     *                           "name": "Fine",
     *                           "amount": 100
     *                       },
     *                       {
     *                           "id": 2,
     *                           "name": "Class 2 Tuition Fee",
     *                           "amount": 300,
     *                           "target": [
     *                               {
     *                                   "id": 1,
     *                                   "name": "Class 1 Tuition Fee",
     *                                   "amount": 400
     *                               },
     *                               {
     *                                   "id": 1,
     *                                   "name": "Class 1 Tuition Fee",
     *                                   "amount": 400
     *                               }
     *                           ]
     *                       },
     *                       {
     *                           "id": 1,
     *                           "name": "Class 1 Tuition Fee",
     *                           "amount": 400
     *                     }
     *                   ],
     *                   "total": 1600
     *               }
     *           }
     * @apiPermission  manage-all-class-invoice,manage-all-student-invoice,(student/parent)
     * @apiError {Boolean="false"}  status Status of the request. False in case of failure
     * @apiError {String}  message An error message to be displayed
     * @apiError {Object[]}  [errors] An optional object containing error causes/reasons
     *
     */
    public function index(Request $request, PermissionsHelper $ph)
    {
        if (Bouncer::can('view-all-class-invoice') ||
            Bouncer::can('view-all-student-invoice') ||
            (auth()->user()->isA('student') &&
                ($request->user_id == auth()->id() ||
                    $ph->studentHasClass($request->class_id))) ||
            (auth()->user()->isA('parent') &&
                ($ph->childrenIds($request->user_id) ||
                    $ph->studentHasClass($request->user_id)))) {

            if ($request->filled('schedule') && $request->schedule < 8) {
                $schedule = $request->schedule;
            } else {
                $schedule = 3;
            }

            if ($request->filled('class_id')) {
                $fees = ClassFee::whereClassId($request->class_id)
                    ->whereHas('fee', function ($q) use ($schedule) {
                        $q->whereIn('schedule', [0, $schedule]);
                    })->get();
            } elseif ($request->filled('user_id')) {
                $classFees = ClassFee::whereClassId(User::findOrFail($request->user_id)->batch->class->id)
                    ->whereHas('fee', function ($q) use ($schedule) {
                        $q->whereIn('schedule', [0, $schedule]);
                    })->get();
                $userFees = UserFee::whereUserId($request->user_id)
                    ->whereHas('fee', function ($q) use ($schedule) {
                        $q->whereIn('schedule', [0, $schedule]);
                    })->get();

                $fees = $classFees->concat($userFees);
            }
            $feeIds = $fees->pluck('fee_id')
                ->concat($fees->pluck('target_fee_id'))->filter()->unique();;
            $feeData = Fee::whereIn('id', $feeIds)->get()->keyBy('id')->toArray();
            $data = InvoiceHelper::invoiceMaker($fees, $feeData);
            return response()->json(Reply::success('', ['fee' => $data['invoices'], 'total' => $data['total']]));
        }
        return Reply::unAuthorized();
    }

    /**
     * @api            {GET} /fee/associate/:id Get Association List
     * @apiName        GetAssociatinList
     * @apiGroup       Fee
     * @apiDescription A list of classes OR Users using the fee
     * @apiSuccess {Object[]} data Object containing required data
     * @apiSuccess (Class) {Number} data.class_id  Id of the associated class
     * @apiSuccess (Class) {Number} data.target_fee_id  Id of the fee in database
     * @apiSuccess (Class) {Object[]} data.class  Object containing class details
     * @apiSuccess (Class) {Number} data.class.id  Id of class
     * @apiSuccess (Class) {String} data.class.name  Name of class
     * @apiSuccess (User) {Number} data.user_id  Id of the user if queries by users
     * @apiSuccess (User) {Object[]} data.user  Object containing user details
     * @apiSuccess (User) {Number} data.class.id  Id of class
     * @apiSuccess (User) {String} data.class.first_name  First Name of student/user
     * @apiSuccess (User) {String} data.class.middle_name  Middle Name of user
     * @apiSuccess (User) {String} data.class.last_name  Last Name of user
     * @apiSuccess (User) {Object[]} data.target_fee  Object containing details of target fee if any
     * @apiSuccess (User) {Number} data.target_fee.id  Id of target fee
     * @apiSuccess (User) {String} data.target_fee.name  Name of target fee
     * @apiSuccess {Number} current_page Current Page number on pagination index
     * @apiSuccess {Number} from Start of number of received data list
     * @apiSuccess {Number} to End of number of received data list
     * @apiSuccess {Number} total Total Number of data in database
     * @apiSuccess {Number} last_page Last page on pagination index
     * @apiSuccess {Number} per_page Number of records per page
     *
     *
     * @apiParam {String="class,user"} data Specify whether to retrieve fee associations for user or classes
     * @apiParam {Number} [page] Page number of results
     *
     * @apiSuccessExample {Object} Class Association data:
     *                {
     *                   "data": [
     *                               {
     *                                   "class_id": 6,
     *                                   "target_fee_id": 2,
     *                                   "class": {
     *                                       "id": 6,
     *                                       "name": "Grade 1",
     *                                       "created_at": "2018-05-05 23:18:38",
     *                                       "updated_at": "2018-05-05 23:18:38"
     *                                   },
     *                                       "target_fee": {
     *                                       "id": 2,
     *                                       "name": "Class 2 Tuition Fee",
     *                                   }
     *                               },
     *                               {
     *                                   "fee_id": 1,
     *                                   "target_fee_id": null,
     *                                   "class": {
     *                                       "id": 6,
     *                                       "name": "Grade 1",
     *                                       "created_at": "2018-05-05 23:18:38",
     *                                       "updated_at": "2018-05-05 23:18:38"
     *                                   },
     *                                   "target_fee": null
     *                               },
     *                   ],
     *                  "current_page": 1,
     *                  "last_page": 1,
     *                  "per_page": 50,
     *                  "from": 1,
     *                  "to": 1,
     *                  "total": 1
     *                   }
     *               }
     *
     *
     * @apiSuccessExample {Object} Student Association data:
     *                {
     *                   "data": [
     *                       {
     *                           "user_id": 1,
     *                           "target_fee_id": 2,
     *                           "user": {
     *                               "id": 14,
     *                               "first_name": "Abby",
     *                               "middle_name": "Charity",
     *                               "last_name": "Roberta",
     *                           },
     *                           "target_fee": {
     *                               "id": 2,
     *                               "name": "Class 2 Tuition Fee",
     *                           }
     *                       },
     *                       {
     *                           "user_id": 1,
     *                           "target_fee_id": null,
     *                           "user": {
     *                               "id": 16,
     *                               "first_name": "Asa",
     *                               "middle_name": "Gina",
     *                               "last_name": "Rhoda",
     *                           },
     *                           "target_fee": null
     *                       }
     *                   ],
     *                  "current_page": 1,
     *                  "last_page": 1,
     *                  "per_page": 50,
     *                  "from": 1,
     *                  "to": 1,
     *                  "total": 1
     *                   }
     *               }
     *
     * @apiPermission  manage-all-class-invoice
     * @apiError {Boolean="false"}  status Status of the request. False in case of failure
     * @apiError {String}  message An error message to be displayed
     * @apiError {Object[]}  [errors] An optional object containing error causes/reasons
     *
     */
    public function show($id, Request $request)
    {
        if (Bouncer::cannot('manage-all-class-invoice')) {
            return Reply::unAuthorized();
        }
        $data = [];
        if ($request->association_for == 'class') {
            $data = ClassFee::with('class', 'targetFee:id,name')
                ->select(['id', 'class_id', 'target_fee_id'])
                ->whereFeeId($id);
            if ($request->name) {
                $data = $data->whereHas('class', function ($q) use ($request) {
                    $q->where('name', 'like', '%' . $request->name . '%');
                });
            }
        } elseif ($request->association_for == 'user') {
            $data = UserFee::with('user:id,first_name,middle_name,last_name,picture_url,gender', 'targetFee:id,name')
                ->select(['id', 'user_id', 'target_fee_id'])
                ->whereFeeId($id);
            if ($request->name) {
                $data = $data->whereHas('user', function ($q) use ($request) {
                    $q->where('first_name', 'like', '%' . $request->name . '%');
                    $q->orWhere('middle_name', 'like', '%' . $request->name . '%');
                    $q->orWhere('last_name', 'like', '%' . $request->name . '%');
                });
            }
        }
        if (!$request->sort_column) {
            $request->sort_column = 'created_at';
            $request->sort_direction = 'desc';
        }
        if ($request->sort_direction === 'desc') {
            $data->orderByDesc($request->sort_column);
        } else {
            $data->orderBy($request->sort_column);
        }
        return response()->json($data->paginate($request->page_size));
    }

    /**
     * @api            {PUT|PATCH} /fee/associate Associate fee
     * @apiName        associateFee
     * @apiGroup       Fee
     *
     * @apiParam {Number} fee_id Id of the fee which needs to be updated.
     * @apiParam {Object[]} [class] An object containing classes for fee
     * @apiParam {Object[]} [class.add_fee] Object for adding fees
     * @apiParam {Number} class.add_fee.class_id Id of the class to be to which the fee needs to be added
     * @apiParam {Number} [class.add_fee.target_fee_id] Id of the fee on which the fee is applied
     * @apiParam {Object[]} [class.remove_fee] Object for removing fees
     * @apiParam {Number} class.remove_fee.class_id Id of the class from which fees need to be removed
     * @apiParam {Object[]} [student] An object containing classes for fee
     * @apiParam {Object[]} [student.add_fee] Object for adding fees
     * @apiParam {Number} student.add_fee.class_id Id of the class to be to which the fee needs to be added
     * @apiParam {Number} [student.add_fee.target_fee_id] Id of the fee on which the fee is applied
     * @apiParam {Object[]} [student.remove_fee] Object for removing fees
     * @apiParam {Number} student.remove_fee.class_id Id of the class from which fees need to be removed
     *
     * @apiSuccess {Boolean} status Status demonstrates the status of operation. True for success
     * @apiSuccess {String} message Success message to be displayed to end user if required
     *           {
     *               "status": true,
     *               "message": "messages",
     *               "data": {
     *                    }
     *           }
     * @apiError {json} Details of Fees couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription Fees Store
     */
    public function update(StoreFeeAssociation $request, $id)
    {
//        @TODO Make sure tenants don't get crossed here
        $fee = Fee::findOrFail($id);
        if ($request->association_for == 'class') {
            if ($request->filled('add_fee') && $request->add_fee['class_id']) {
                $classFee = new ClassFee();
                $classFee->fee_id = $id;
                $classFee->class_id = $request->add_fee['class_id'];
                $classFee->target_fee_id = $request->add_fee['target_fee_id'];
                return Reply::loggedResponse($classFee->save(), $classFee, 'update', 'fee', 'fee');
            }
            if ($request->filled('remove')) {
                ClassFee::destroy($request->remove);
            }
        }

        if ($request->association_for == 'user') {
            if ($request->filled('add_fee') && $request->add_fee['user_id']) {
                $userFee = new UserFee();
                $userFee->fee_id = $id;
                $userFee->user_id = $request->add_fee['user_id'];
                $userFee->target_fee_id = $request->add_fee['target_fee_id'];
                return Reply::loggedResponse($userFee->save(), $userFee, 'update', 'fee', 'fee');
            }
            if ($request->filled('remove')) {
                UserFee::destroy($request->remove);
            }
        }
        return Reply::loggedResponse($fee, $fee, 'update', 'fee', 'fee');
    }


}
