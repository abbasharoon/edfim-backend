<?php

namespace App\Http\Controllers\Api\Fee;

use App\Fee;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Fee\StoreFee;
use App\Http\Requests\Fee\UpdateFee;
use Bouncer;
use Illuminate\Http\Request;

class FeeController extends Controller
{
    /**
     * @api            {GET} /fee Get Summary of Fees
     * @apiName        GetFeeSummary
     * @apiGroup       Fee
     * @apiDescription A list of Fees for displaying to user in various occasions
     * @apiSuccess {Object[]} data Object containing required data
     * @apiSuccess {Number} data.id  Id of the fee in database
     * @apiSuccess {String} data.name Name of the fee for displaying
     * @apiSuccess {String} data.detail  Details/Detail of the fee if available
     * @apiSuccess {Number} data.amount Amount of the fee that's charged to the student
     * @apiSuccess {Number} data.schedule Schedule of the fee i.e 0=none,1=daily,2=weekly,3=monthly,4=triannual,5=quarterly,6=biannually,7=annually
     * @apiSuccess {Number} data.type Type denotes whether the fee is percentage or just number.1=number,2=percentage
     * @apiSuccess {Number} data.addition Addition specifies whether the fee is to be added or subtracted. 0=subtract,1=add
     * @apiSuccess {Number} data.fee_category_id Id of category to which fee is related.
     * @apiSuccess {Number} current_page Current Page number on pagination index
     * @apiSuccess {Number} from Start of number of received fees numbers
     * @apiSuccess {Number} to End of number of received fees numbers
     * @apiSuccess {Number} total Total Number of Fees in database
     * @apiSuccess {Number} last_page Last page on pagination index
     * @apiSuccess {Number} per_page Number of records per page
     *
     * @apiParam {String} [search_string] Search fee by name or detail
     * @apiParam {Number} [schedule="0,1,2,3,4,5,6,7"] Filter Fees by schedule type
     * @apiParam {Number} [type] Filter Fee by whole number or percentage
     * @apiParam {Number} [addition] Filter by either addison or subtraction
     * @apiParam {Number} [fee_category_id] Filter by category
     * @apiParam {Number} [user_id] Only fees applied to the provided user id
     * @apiParam {Number} [class_id] Only fees applied to the provided class_id
     * @apiParam {Number} [page] Page number of results
     * @apiParam {String="name,schedule,type,addition,fee_category_id"} [order_by] Order the result by given variable
     * @apiParam {String="desc,asc"} [sort_direction] Order results descending or ascending. Default ascending
     *
     * @apiSuccessExample {Object} Success-Example:
     *                {
     *                   "data": [
     *                     {
     *                       "id": 1,
     *                       "name": "Class 1 Tuition Fee",
     *                       "detail": "",
     *                       "amount": 400,
     *                       "schedule": 3,
     *                       "type": 1,
     *                       "addition": 0,
     *                       "fee_category_id": 1,
     *                       "deleted_at": null,
     *                       "created_at": null,
     *                       "updated_at": null
     *                   },
     *                   {
     *                       "id": 2,
     *                       "name": "Class 2 Tuition Fee",
     *                       "detail": "",
     *                       "amount": 300,
     *                       "schedule": 3,
     * v                       "type": 1,
     *                       "addition": 0,
     *                       "fee_category_id": 1,
     *                       "deleted_at": null,
     *                       "created_at": null,
     *                       "updated_at": null
     *                   },
     *                   {
     *                       "id": 3,
     *                       "name": "Promotion Fee",
     *                       "detail": "",
     *                       "amount": 200,
     *                       "schedule": 3,
     *                       "type": 1,
     *                       "addition": 0,
     *                       "fee_category_id": 2,
     *                       "deleted_at": null,
     *                       "created_at": null,
     *                       "updated_at": null
     *                   }
     *                       ],
     *                  "current_page": 1,
     *                  "last_page": 1,
     *                  "per_page": 50,
     *                  "from": 1,
     *                  "to": 1,
     *                  "total": 1
     *                   }
     *               }
     *
     * @apiPermission  manage-all-fee
     * @apiError {Boolean="false"}  status Status of the request. False in case of failure
     * @apiError {String}  message An error message to be displayed
     * @apiError {Object[]}  [errors] An optional object containing error causes/reasons
     *
     */
    public function index(Request $request)
    {
        if (Bouncer::cannot('manage-all-fee')) {
            return Reply::unAuthorized();
        }
        if ($request->autoComplete) {
            $fees = Fee::where('name', 'like', '%' . $request->name . '%')->limit(50)->get();
            return response()->json($fees);
        }
        $fees = Fee::where('instant', false)->with('category')->withCount('classFee')->withCount('userFee');
        if ($request->filled('name')) {
            $fees = $fees->where(function ($query) use ($request) {
                $query->where('name', 'like', '%' . $request->name . '%');
//                $query->orWhere('detail', 'like', '%' . $request->name . '%');
            });
        }
        if ($request->filled('schedule')) {
            $fees = $fees->where('schedule', $request->schedule);
        }
        if ($request->filled('type')) {
            $fees = $fees->where('type', $request->type);
        }
        if ($request->filled('addition')) {
            $fees = $fees->where('addition', $request->addition);
        }
        if ($request->filled('fee_category_id')) {
            $fees = $fees->where('fee_category_id', $request->fee_category_id);
        }

        if ($request->filled('user_id')) {
            $fees = $fees->whereHas('user', function ($query) use ($request) {
                $query->where('user_id', $request->user_id);
            });
        }

        if ($request->filled('class_id')) {
            $fees = $fees->whereHas('class', function ($query) use ($request) {
                $query->where('class_id', $request->class_id);
            });
        }

        if (!$request->sort_column) {
            $request->sort_column = 'created_at';
            $request->sort_direction = 'desc';
        }
        if ($request->sort_direction === 'desc') {
            $fees->orderByDesc($request->sort_column);
        } else {
            $fees->orderBy($request->sort_column);
        }
        $fees = $fees->paginate($request->page_size);
        return response()->json($fees);
    }


    /**
     * @api            {POST} /fee Create Fee
     * @apiName        storeFeeData
     * @apiGroup       Fee
     *
     * @apiParam {String{..256}} name Fee name.
     * @apiParam {String{..1024}} [detail] Detail of Fee.
     * @apiParam {Number} amount Amount to be charged to student
     * @apiParam {Number="0=none,1=daily,2=weekly,3=monthly,4=triannual,5=quarterly,6=biannually,7=annually6="} schedule type
     * @apiParam {Number="1=number,2=percentage"} type Type of fee. 1= number while 2 denotes a percentage
     * @apiParam {Number="0=subtraction,1=addition"} addition Whether to add or subtract the fee from other fees.
     * @apiParam {Number} fees_category_id Category of the fee
     * @apiPermission  manage-all-fee
     *
     * @apiSuccess {Boolean} status Status demonstrates the status of operation. True for success
     * @apiSuccess {String} message Success message to be displayed to end user if required
     * @apiSuccess {Object[]} data Object contining fee data
     * @apiSuccess {Number} data.id  Id of the fee in database
     * @apiSuccess {String} data.name Name of the fee for displaying
     * @apiSuccess {String} data.detail  Details/Detail of the fee if available
     * @apiSuccess {Number} data.amount Amount of the fee that's charged to the student
     * @apiSuccess {Number} data.schedule Schedule of the fee i.e 0=none,1=daily,2=weekly,3=monthly,4=triannual,5=quarterly,6=biannually,7=annually
     * @apiSuccess {Number} data.type Type denotes whether the fee is percentage or just number.1=number,2=percentage
     * @apiSuccess {Number} data.addition Addition specifies whether the fee is to be added or subtracted. 0=subtract,1=add
     * @apiSuccess {Number} data.fee_category_id Id of category to which fee is related.
     * @apiSuccessExample {json} Success-Response:
     *           {
     *               "status": true,
     *               "message": "messages.feeCreated",
     *               "data": {
     *                   "name": "Tution Fee",
     *                   "detail": "none given",
     *                   "amount": "500",
     *                   "schedule": "1",
     *                   "type": "1",
     *                   "addition": "1",
     *                   "fee_category_id": "1",
     *                   "updated_at": "2018-05-06 05:15:27",
     *                   "created_at": "2018-05-06 05:15:27",
     *                   "id": 6
     *               }
     *           }
     * @apiError {json} Details of Fees couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription Fees Store
     */
    public function store(StoreFee $request)
    {
        // add fess
        $fee = new Fee;
        $fee->name = $request->name;
        $fee->detail = $request->detail;
        $fee->amount = $request->amount;
        $fee->schedule = $request->schedule;
        $fee->type = $request->type;
        $fee->instant = $request->instant;
        $fee->fee_category_id = $request->fee_category_id;

        return Reply::loggedResponse($fee->save(), $fee, 'create', 'fee', 'fee');
    }

    /**
     * @api            {GET} /fee/:id Get Single Fee
     * @apiName        GetSingleFee
     * @apiGroup       Fee
     * @apiDescription A single fee with complete details on where it's used
     * @apiSuccess {Object[]} data Object containing required data
     * @apiSuccess {Number} data.id  Id of the fee in database
     * @apiSuccess {String} data.name Name of the fee for displaying
     * @apiSuccess {String} data.detail  Details/Detail of the fee if available
     * @apiSuccess {Number} data.amount Amount of the fee that's charged to the student
     * @apiSuccess {Number} data.schedule Schedule of the fee i.e 0=none,1=daily,2=weekly,3=monthly,4=triannual,5=quarterly,6=biannually,7=annually
     * @apiSuccess {Number} data.type Type denotes whether the fee is percentage or just number.1=number,2=percentage
     * @apiSuccess {Number} data.addition Addition specifies whether the fee is to be added or subtracted. 0=subtract,1=add
     * @apiSuccess {Number} data.fee_category_id Id of category to which fee is related.
     *
     *
     * @apiSuccessExample {Object} Success-Example:
     *                {
     *                   "data": [
     *                     {
     *                       "id": 1,
     *                       "name": "Class 1 Tuition Fee",
     *                       "detail": "",
     *                       "amount": 400,
     *                       "schedule": 3,
     *                       "type": 1,
     *                       "addition": 0,
     *                       "fee_category_id": 1,
     *                       "deleted_at": null,
     *                       "created_at": null,
     *                       "updated_at": null
     *                   },
     *                       ],
     *               }
     *
     * @apiPermission  manage-all-fee
     * @apiError {Boolean="false"}  status Status of the request. False in case of failure
     * @apiError {String}  message An error message to be displayed
     * @apiError {Object[]}  [errors] An optional object containing error causes/reasons
     *
     */
    public function show($id)
    {
        $fee = Fee::with('category')
            ->with(['oldFee' => function ($query) {
                $query->withTrashed();
                $query->orderByDesc('id');
            }])
            ->findOrFail($id);
        if ($fee->old_fee_id) {
            $primaryFee = Fee::withTrashed()->findOrFail($fee->old_fee_id);
            if ($primaryFee) {
                $fee->oldFee->push($primaryFee);
            }
        }
        return response()->json(Reply::success('successfully get fees data', $fee));
    }


    /**
     * @api            {PUT|PATCH} /fee/:id Update Fee
     * @apiName        store Save fees data
     * @apiGroup       Fee
     *
     * @apiParam {String{..256}} name Fee name.
     * @apiParam {String{..1024}} [detail] Detail of Fee.
     * @apiParam {Number} amount Amount to be charged to student
     * @apiParam {Number="0=none,1=daily,2=weekly,3=monthly,4=triannual,5=quarterly,6=biannually,7=annually6="} schedule type
     * @apiParam {Number="1=number,2=percentage"} type Type of fee. 1= number while 2 denotes a percentage
     * @apiParam {Number="0=subtraction,1=addition"} addition Whether to add or subtract the fee from other fees.
     * @apiParam {Number} fees_category_id Category of the fee
     * @apiPermission  manage-all-fee
     *
     * @apiSuccess {Boolean} status Status demonstrates the status of operation. True for success
     * @apiSuccess {String} message Success message to be displayed to end user if required*
     * @apiSuccess {Object[]} data Object confining fee data
     * @apiSuccess {Number} data.id  !!ATTENTION!! Updating fee will send back a new id for the fee
     * @apiSuccess {String} data.name Name of the fee for displaying
     * @apiSuccess {String} data.detail  Details/Detail of the fee if available
     * @apiSuccess {Number} data.amount Amount of the fee that's charged to the student
     * @apiSuccess {Number} data.schedule Schedule of the fee i.e 0=none,1=daily,2=weekly,3=monthly,4=triannual,5=quarterly,6=biannually,7=annually
     * @apiSuccess {Number} data.type Type denotes whether the fee is percentage or just number.1=number,2=percentage
     * @apiSuccess {Number} data.addition Addition specifies whether the fee is to be added or subtracted. 0=subtract,1=add
     * @apiSuccess {Number} data.fee_category_id Id of category to which fee is related.
     * @apiSuccessExample {json} Success-Response:
     *           {
     *               "status": true,
     *               "message": "messages.feeCreated",
     *               "data": {
     *                   "name": "Tution Fee",
     *                   "detail": "none given",
     *                   "amount": "500",
     *                   "schedule": "1",
     *                   "type": "1",
     *                   "addition": "1",
     *                   "fee_category_id": "1",
     *                   "updated_at": "2018-05-06 05:15:27",
     *                   "created_at": "2018-05-06 05:15:27",
     *                   "id": 6
     *               }
     *           }
     * @apiError {json} Details of Fees couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription Fees Store
     */
    public function update(UpdateFee $request, $id)
    {
        // delete fees
        $oldFee = Fee::findOrFail($id);
        // add fees
        if (($request->amount == $oldFee->amount && $request->type == $oldFee->type) || $oldFee->instant) {
            $fee = $oldFee;
        } else {
            $fee = new Fee;
            $fee->old_fee_id = $oldFee->old_fee_id ? $oldFee->old_fee_id : $oldFee->id;
        }
        $fee->name = $request->name;
        $fee->detail = $request->detail;
        $fee->amount = $request->amount;
        $fee->schedule = $request->schedule;
        $fee->type = $request->type;
//        $fee->addition = $request->addition;
        $fee->fee_category_id = $request->fee_category_id;
        $save = $fee->save();

        // add classes fees
        // add classes fee
        if ($save && !(($request->amount == $oldFee->amount && $request->type == $oldFee->type) || $oldFee->instant)) {

            $classFee = $oldFee->class()->get(['class_id', 'target_fee_id', 'tenant_id'])->toArray();
            $userFee = $oldFee->user()->get(['user_id', 'target_fee_id', 'tenant_id'])->toArray();
            $fee->classFee()->attach($classFee);
            $fee->userFee()->attach($userFee);
            $oldFee->classFee()->detach();
            $oldFee->userFee()->detach();
            $oldFee->deleted_at = $fee->created_at;
            $oldFee->save();
        }
        return Reply::loggedResponse($save,
            $fee,
            'update',
            'fee',
            'fee');
    }

    /**
     * @api            {DELETE} /fee/:id Delete Fee
     * @apiName        DeleteFee
     * @apiGroup       Fee
     *
     * @apiPermission  manage-all-fee
     * @apiSuccess {json} Gives details of Fees which given by id
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Fees deleted",
     *          "data": []
     *      }
     * @apiError {json} Details of Fees couldn't delete
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later"
     *     }
     * @apiDescription Delete fees details by given fees id
     */
    public function destroy($id)
    {
        if (Bouncer::cannot('manage-all-fee')) {
            return Reply::unAuthorized();
        }
        $fee = Fee::findOrFail($id);
        $fee->classFee()->detach();
        $fee->userFee()->detach();

        return Reply::loggedResponse($fee->delete(),
            $fee,
            'update',
            'fee',
            'fee');
    }

}
