<?php

namespace App\Http\Controllers\Api\Fee;

use App\FeeCategory;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FeeCategoryController extends Controller
{


    /**
     * @api            {GET} /fee/category Fee Categories Summary
     * @apiName        GetFeeCategory
     * @apiGroup       Fee
     * @apiDescription A list of categories of fee. Categories are used for internal management of fees by School staff
     * @apiSuccess {Object[]} data Object containing arrays of categories
     * @apiSuccess {Number} data.id Id of category in database
     * @apiSuccess {String} data.name Name of category
     *
     * @apiParamExample {json} Request-Example:
     *       {
     *       "status": true,
     *       "message": "",
     *       "data": [
     *               {
     *                   "id": 1,
     *                   "name": "Tuition Fee",
     *                   "created_at": null,
     *                   "updated_at": null
     *               },
     *               {
     *                   "id": 2,
     *                   "name": "Promotion",
     *                   "created_at": null,
     *                   "updated_at": null
     *               },
     *               {
     *                   "id": 3,
     *                   "name": "Exam Fee",
     *                   "created_at": null,
     *                   "updated_at": null
     *               },
     *               {
     *                   "id": 4,
     *                   "name": "Fine",
     *                   "created_at": null,
     *                   "updated_at": null
     *               }
     *           ]
     *       }
     *
     * @apiError {Boolean="false"}  status Status of the request. False in case of failure
     * @apiError {String}  message An error message to be displayed
     * @apiError {Object[]}  [errors] An optional object containing error causes/reasons
     *
     */
    public function index()
    {
        $feeCategory = FeeCategory::withCount('fee')->get();
        return response()->json(Reply::success('', $feeCategory));
    }


    /**
     * @api            {POST} /fee/category Create Fee Category
     * @apiName        CreateFeeCategory
     * @apiGroup       Fee
     * @apiDescription Delete Fee category.
     * @apiSuccess {Boolean} status Status demonstrates the status of operation. True for success
     * @apiSuccess {String} message Success message to be displayed to end user
     * @apiSuccess {Object[]} data Object containing newly created category
     * @apiSuccess {Number} data.id Id of category in database
     * @apiSuccess {String} data.name Name of category
     *
     * @apiParamExample {json} Request-Example:
     *       {
     *       "status": true,
     *       "message": "",
     *       "data": [
     *               {
     *                   "id": 1,
     *                   "name": "Tuition Fee",
     *                   "created_at": null,
     *                   "updated_at": null
     *               }
     *           ]
     *       }
     *
     * @apiPermission  Only admin users
     * @apiError {Boolean="false"}  status Status of the request. False in case of failure
     * @apiError {String}  message An error message to be displayed
     * @apiError {Object[]}  [errors] An optional object containing error causes/reasons
     *
     */
    public function store(Request $request)
    {
        if (\Bouncer::cannot('manage-all-fee')) {
            return Reply::unAuthorized();
        }
        $this->validate($request, [
            'name' => 'required|max:256|string',
        ]);
        $feeCategory = new FeeCategory();
        $feeCategory->name = $request->name;
        return Reply::loggedResponse($feeCategory->save(),
            $feeCategory,
            'create',
            'fee',
            'feeCategory');
    }


    /**
     * @api            {GET} /fee/category/:id Get Single Category
     * @apiName        GetSingleCategory
     * @apiGroup       Fee
     * @apiDescription Get single category with all the associated fees with it. .
     * @apiSuccess {Boolean} status Status demonstrates the status of operation. True for success
     * @apiSuccess {String} message Success message to be displayed to end user if required
     * @apiSuccess {Object[]} data Object containing newly created category
     * @apiSuccess {Number} data.id Id of category in database
     * @apiSuccess {String} data.name Name of category
     * @apiSuccess {Object[]} data.fee An Object containing all the fees in the categories
     * @apiSuccess {Number} data.fee.id  Id of the fee in database
     * @apiSuccess {String} data.fee.name Name of the fee for displaying
     * @apiSuccess {String} data.fee.description  Details/Description of the fee if available
     * @apiSuccess {Number} data.fee.amount Amount of the fee that's charged to the student
     * @apiSuccess {Number} data.fee.schedule Schedule of the fee i.e 0=none,1=daily,2=weekly,3=monthly,4=triannual,5=quarterly,6=biannually,7=annually6=
     * @apiSuccess {Number} data.fee.type Type denotes whether the fee is percentage or just number.1=number,2=percentage
     * @apiSuccess {Number} data.fee.addition Addition specifies whether the fee is to be added or subtracted. 0=subtract,1=add
     *
     * @apiParamExample {json} Request-Example:
     *           {
     *               "status": true,
     *               "message": "",
     *               "data": {
     *                  "id": 1,
     *                  "name": "Tuition Fee",
     *                  "created_at": null,
     *                  "updated_at": null,
     *                  "fee": [
     *                       {
     *                           "id": 1,
     *                           "name": "Class 1 Tuition Fee",
     *                           "description": "",
     *                           "amount": 400,
     *                           "schedule": 3,
     *                           "type": 1,
     *                           "addition": 0,
     *                           "fee_category_id": 1,
     *                           "deleted_at": null,
     *                           "created_at": null,
     *                           "updated_at": null
     *                       },
     *                       {
     *                           "id": 2,
     *                           "name": "Class 2 Tuition Fee",
     *                           "description": "",
     *                           "amount": 300,
     *                           "schedule": 3,
     *                           "type": 1,
     *                           "addition": 0,
     *                           "fee_category_id": 1,
     *                           "deleted_at": null,
     *                           "created_at": null,
     *                           "updated_at": null
     *                       }
     *                    ]
     *               }
     *           }
     * @apiPermission  view-all-fee,view-fee
     * @apiError {Boolean="false"}  status Status of the request. False in case of failure
     * @apiError {String}  message An error message to be displayed
     * @apiError {Object[]}  [errors] An optional object containing error causes/reasons
     *
     */
    public function show($id)
    {
        if (\Bouncer::cannot('view-all-fee') && \Bouncer::cannot('view-fee')) {
            return Reply::unAuthorized();
        }
        $feeCategory = FeeCategory::with('fee')->findOrFail($id);
        return response()->json(Reply::success('', $feeCategory));
    }


    /**
     * @api            {PUT|PATCH} /fee/category/:id Update Fee Category
     * @apiName        UpdateFeeCategory
     * @apiGroup       Fee
     * @apiDescription Update Fee category name.
     * @apiSuccess {Boolean} status Status demonstrates the status of operation. True for success
     * @apiSuccess {String} message Success message to be displayed to end user
     * @apiSuccess {Object[]} data Object containing newly created category
     * @apiSuccess {Number} data.id Id of category in database
     * @apiSuccess {String} data.name Name of category
     *
     * @apiParamExample {json} Request-Example:
     *       {
     *       "status": true,
     *       "message": "",
     *       "data": [
     *               {
     *                   "id": 1,
     *                   "name": "Tuition Fee",
     *                   "created_at": null,
     *                   "updated_at": null
     *               }
     *           ]
     *       }
     *
     * @apiPermission  manage-all-fee
     * @apiError {Boolean="false"}  status Status of the request. False in case of failure
     * @apiError {String}  message An error message to be displayed
     * @apiError {Object[]}  [errors] An optional object containing error causes/reasons
     *
     */
    public function update(Request $request, $id)
    {
        if (\Bouncer::cannot('manage-all-fee')) {
            return Reply::unAuthorized();
        }
        $this->validate($request, [
            'name' => 'required|max:256|string',
        ]);
        $feeCategory = FeeCategory::findOrFail($id);
        $feeCategory->name = $request->name;
        return Reply::loggedResponse($feeCategory->save(),
            $feeCategory,
            'update',
            'fee',
            'feeCategory');
    }

    /**
     * @api            {DELETE} /fee/category/:id Delete Fee Category
     * @apiName        DeleteFeeCategory
     * @apiGroup       Fee
     * @apiDescription Create Fee Category by submitting category name.
     * @apiSuccess {Boolean} status Status demonstrates the status of operation. True for success
     * @apiSuccess {String} message Success message to be displayed to end user
     * @apiSuccess {Object[]} data Object containing newly created category
     * @apiSuccess {Number} data.id Id of category in database
     * @apiSuccess {String} data.name Name of category
     *
     * @apiParamExample {json} Request-Example:
     *       {
     *       "status": true,
     *       "message": "Message Here",
     *       "data": [
     *               {
     *                   "id": 1,
     *                   "name": "Tuition Fee",
     *                   "created_at": null,
     *                   "updated_at": null
     *               }
     *           ]
     *       }
     *
     * @apiPermission  manage-all-fee
     * @apiError {Boolean="false"}  status Status of the request. False in case of failure
     * @apiError {String}  message An error message to be displayed
     * @apiError {Object[]}  [errors] An optional object containing error causes/reasons
     *
     */
    public function destroy($id)
    {
        if (\Bouncer::cannot('manage-all-fee')) {
            return Reply::unAuthorized();
        }
        $feeCategory = FeeCategory::findOrFail($id);

            return Reply::loggedResponse($feeCategory->delete(),
                $feeCategory,
                'delete',
                'fee',
                'feeCategory');
    }
}
