<?php

namespace App\Http\Controllers\API;

use App\Assignment;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Plank\Mediable\Exceptions\MediaUploadException;
use Plank\Mediable\HandlesMediaUploadExceptions;

class MediaManager extends Controller
{
    use HandlesMediaUploadExceptions;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->type === 'assignment') {
            $forModel = $this->forAssignment($request);
            $subDirectory = 'assignment';
        }

        if ($forModel) {
            try {
                $path = config('app.tenant')->hostname . '/' . $subDirectory;
                $media = \MediaUploader::fromSource($request->file('file'))
                    ->toDirectory($path)
                    ->upload();
                $forModel->attachMedia($media, [config('app.tenant')->mediaTag, Auth::id()]);
                return response()->json(Reply::success(__(''), $media));
            } catch (MediaUploadException $e) {
                return response($e->getMessage(), $e->getCode() ? $e->getCode() : 401);
            }
        }
        return response('Max Files already submitted', 401);
    }


    private function forAssignment($request)
    {
        $assignment = Assignment::findOrFail($request->id);
        return $assignment;
        if (Auth::user()->isA('student') &&
            $assignment->batch_id === Auth::user()->batch_id
            && 100 > $assignment->getMediaMatchAll([config('app.tenant')->mediaTag, Auth::id()])->count()) {
            return $assignment;
        }

        return false;

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mediableCheck = DB::table('mediables')->where('media_id', $id)->where('tag', Auth::id())->exists();
        if ($mediableCheck) {
            Media::findOrFail($id)->delete();
            return response(Reply::success(_('')));
        }
        return response(Reply::error(__('File doesn\'t exists')), 404);

    }
}
