<?php

namespace App\Http\Controllers\Api\Accounting\DoubleEntry;

use App\Helpers\Reply;
use App\Http\Controllers\Api\Accounting\AccountingController;
use App\Http\Requests\Accounting\DoubleEntry\Account as Request;
use App\Models\Accounting\Banking\Account as CoreAccount;
use App\Models\Accounting\DoubleEntry\Account;
use App\Models\Accounting\DoubleEntry\AccountBank;

class ChartOfAccounts extends AccountingController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response()->json(Reply::success('', Account::with('type')->get()));
    }

    /**
     * Show the form for viewing the specified resource.
     *
     * @return Response
     */
    public function show()
    {
        return redirect()->route('chart-of-accounts.index');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $account = Account::create($request->all());

        if ($account->type_id == '6' && 2 === 1) {
            $bank = CoreAccount::create([
                'company_id' => session('company_id'),
                'name' => $account->name,
                'number' => $account->code,
                'currency_code' => setting('general.default_currency'),
                'opening_balance' => 0,
                'enabled' => $account->enabled,
                'bank_name' => 'chart-of-accounts',
            ]);

            AccountBank::create([
                'company_id' => session('company_id'),
                'account_id' => $account->id,
                'bank_id' => $bank->id,
            ]);
        }

//        $message = trans('messages.success.added', ['type' => trans_choice('general.accounts', 1)]);


        return Reply::loggedResponse($account, $account, 'create', 'bookkeeping.accounting', 'accounting');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Account $chart_of_account
     * @param  Request $request
     *
     * @return Response
     */
    public function update(Account $chart_of_account, Request $request)
    {


        $action = $chart_of_account->update($request->all());
        return Reply::loggedResponse($action, $chart_of_account, 'update', 'bookkeeping.accounting', 'accounting');
//        if ($chart_of_account->type_id == '6' && 1==2) {
//            $core_account = $chart_of_account->bank->bank;
//
//            $core_account->update([
//                'company_id' => session('company_id'),
//                'name' => $chart_of_account->name,
//                'number' => $core_account->number,
//                'currency_code' => $core_account->currency_code,
//                'opening_balance' => $core_account->opening_balance,
//                'enabled' => $chart_of_account->enabled,
//            ]);
//        }


//            $message = trans('messages.success.updated', ['type' => trans_choice('general.accounts', 1)]);


//            $message = trans('messages.warning.disabled', ['name' => $chart_of_account->name, 'text' => implode(', ', $relationships)]);


    }

    /**
     * Enable the specified resource.
     *
     * @param  Account $chart_of_account
     *
     * @return Response
     */
    public function enable(Account $chart_of_account)
    {
        $chart_of_account->enabled = 1;
        $chart_of_account->save();

        $message = trans('messages.success.enabled', ['type' => trans_choice('general.accounts', 1)]);

        flash($message)->success();

        return redirect()->route('chart-of-accounts.index');
    }

    /**
     * Disable the specified resource.
     *
     * @param  Account $chart_of_account
     *
     * @return Response
     */
    public function disable(Account $chart_of_account)
    {
        $chart_of_account->enabled = 0;
        $chart_of_account->save();

        $message = trans('messages.success.disabled', ['type' => trans_choice('general.accounts', 1)]);

        flash($message)->success();

        return redirect()->route('chart-of-accounts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Account $chart_of_account
     *
     * @return Response
     */
    public function destroy(Account $chart_of_account)
    {
        /*$relationships = $this->countRelationships($account, [
            'items' => 'items',
            'invoice_items' => 'invoices',
            'bill_items' => 'bills',
        ]);*/

        if (empty($relationships) && !$chart_of_account->system) {
            $chart_of_account->delete();

            $message = trans('messages.success.deleted', ['type' => trans_choice('general.accounts', 1)]);

            flash($message)->success();
        } else {
            $message = trans('messages.warning.deleted', ['name' => $chart_of_account->name, 'text' => implode(', ', $relationships)]);

            flash($message)->warning();
        }

        return redirect()->route('chart-of-accounts.index');
    }
}


