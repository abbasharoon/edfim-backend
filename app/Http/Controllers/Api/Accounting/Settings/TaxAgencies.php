<?php

namespace App\Http\Controllers\Api\Accounting\Settings;

use App\Helpers\Reply;
use App\Http\Controllers\Api\Accounting\AccountingController;
use App\Models\Accounting\DoubleEntry\Account;
use App\Models\Accounting\DoubleEntry\Type;
use App\Models\Accounting\Setting\TaxAgency;
use Illuminate\Http\Request;

class TaxAgencies extends AccountingController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        return response()->json(Reply::success('', TaxAgency::with('taxes')->get()));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $salestaxpayable = Type::where('key', 'salestaxpayable')->first();
        $taxExpense = Type::where('key', 'globaltaxexpense')->first();
        $taxSuspense = Type::where('key', 'globaltaxsuspense')->first();
        $salestaxpayableAccount = new Account;
        $salestaxpayableAccount->name = $request->name . ' Payable';
        $salestaxpayableAccount->type_id = $salestaxpayable->id;
        $salestaxpayableAccount->save();

        $taxExpenseAccount = new Account();
        $taxExpenseAccount->name = $request->name . ' Expense';
        $taxExpenseAccount->type_id = $taxExpense->id;
        $taxExpenseAccount->save();

        $taxSuspenseAccount = new Account();
        $taxSuspenseAccount->name = $request->name . ' Suspense';
        $taxSuspenseAccount->type_id = $taxSuspense->id;
        $taxSuspenseAccount->save();

        $action = TaxAgency::create($request->all());
//        $action->accounts()->attach([
//            [
//                'account_id' => $salestaxpayableAccount->id,
//                'tenant_id' => config('app.tenant')->tenant_id
//            ],
//            [
//                'account_id' => $taxExpense->id,
//                'tenant_id' => config('app.tenant')->tenant_id
//            ],
//            [
//                'account_id' => $taxSuspense->id,
//                'tenant_id' => config('app.tenant')->tenant_id
//            ]]);
        return Reply::loggedResponse($action, $action, 'create', 'accounting.taxes', 'taxAgency');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
