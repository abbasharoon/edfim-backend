<?php

namespace App\Http\Controllers\Api\Accounting\Settings;

use App\Helpers\Reply;
use App\Http\Controllers\Api\Accounting\AccountingController;
use App\Http\Requests\Accounting\Setting\Tax as Request;
use App\Models\Accounting\Setting\Tax;

class Taxes extends AccountingController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $taxes = Tax::with('taxAgency')->get();

        $types = [
//            'normal' => trans('taxes.normal'),
//            'inclusive' => trans('taxes.inclusive'),
//            'compound' => trans('taxes.compound'),
        ];

        return response()->json(Reply::success('', $taxes));

    }

    /**
     * Show the form for viewing the specified resource.
     *
     * @return Response
     */
    public function show()
    {
        return redirect('settings/taxes');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $action = Tax::create($request->all());
        return Reply::loggedResponse($action, $action, 'create', 'accounting.taxes', 'taxes');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Tax $tax
     *
     * @return Response
     */
    public function edit(Tax $tax)
    {
        $types = [
            'normal' => trans('taxes.normal'),
            'inclusive' => trans('taxes.inclusive'),
            'compound' => trans('taxes.compound'),
        ];

        return view('settings.taxes.edit', compact('tax', 'types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Tax     $tax
     * @param  Request $request
     *
     * @return Response
     */
    public function update(Tax $tax, Request $request)
    {
        $relationships = $this->countRelationships($tax, [
            'items' => 'items',
            'invoice_items' => 'invoices',
            'bill_items' => 'bills',
        ]);

        if (empty($relationships) || $request['enabled']) {
            $tax->update($request->all());

            $message = trans('messages.success.updated', ['type' => trans_choice('general.tax_rates', 1)]);

            flash($message)->success();

            return redirect('settings/taxes');
        } else {
            $message = trans('messages.warning.disabled', ['name' => $tax->name, 'text' => implode(', ', $relationships)]);

            flash($message)->warning();

            return redirect('settings/taxes/' . $tax->id . '/edit');
        }
    }

    /**
     * Enable the specified resource.
     *
     * @param  Tax $tax
     *
     * @return Response
     */
    public function enable(Tax $tax)
    {
        $tax->enabled = 1;
        $tax->save();

        $message = trans('messages.success.enabled', ['type' => trans_choice('general.tax_rates', 1)]);

        flash($message)->success();

        return redirect()->route('taxes.index');
    }

    /**
     * Disable the specified resource.
     *
     * @param  Tax $tax
     *
     * @return Response
     */
    public function disable(Tax $tax)
    {
        $relationships = $this->countRelationships($tax, [
            'items' => 'items',
            'invoice_items' => 'invoices',
            'bill_items' => 'bills',
        ]);

        if (empty($relationships)) {
            $tax->enabled = 0;
            $tax->save();

            $message = trans('messages.success.disabled', ['type' => trans_choice('general.tax_rates', 1)]);

            flash($message)->success();
        } else {
            $message = trans('messages.warning.disabled', ['name' => $tax->name, 'text' => implode(', ', $relationships)]);

            flash($message)->warning();
        }

        return redirect()->route('taxes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Tax $tax
     *
     * @return Response
     */
    public function destroy(Tax $tax)
    {
        $relationships = $this->countRelationships($tax, [
            'items' => 'items',
            'invoice_items' => 'invoices',
            'bill_items' => 'bills',
        ]);

        if (empty($relationships)) {
            $tax->delete();

            $message = trans('messages.success.deleted', ['type' => trans_choice('general.taxes', 1)]);

            flash($message)->success();
        } else {
            $message = trans('messages.warning.deleted', ['name' => $tax->name, 'text' => implode(', ', $relationships)]);

            flash($message)->warning();
        }

        return redirect('settings/taxes');
    }
}
