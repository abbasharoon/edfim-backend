<?php

namespace App\Http\Controllers\Modals;

use App\Http\Controllers\Controller;
use App\Http\Requests\Accounting\Income\Customer as Request;
use App\User;
use App\Models\Accounting\Income\Customer;
use App\Models\Accounting\Income\Invoice;
use App\Models\Accounting\Income\Revenue;
use App\Models\Accounting\Setting\Currency;
use App\Utilities\Accounting\Import;
use App\Utilities\Accounting\ImportFile;
use Illuminate\Http\Request as FRequest;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class Customers extends Controller
{
    /**
     * Instantiate a new controller instance.
     */
    public function __construct()
    {
        // Add CRUD permission check
        $this->middleware('permission:create-incomes-customers')->only(['create', 'store', 'duplicate', 'import']);
        $this->middleware('permission:read-incomes-customers')->only(['index', 'show', 'edit', 'export']);
        $this->middleware('permission:update-incomes-customers')->only(['update', 'enable', 'disable']);
        $this->middleware('permission:delete-incomes-customers')->only('destroy');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $currencies = Currency::enabled()->pluck('name', 'code');

        $html = view('modals.customers.create', compact('currencies'))->render();

        return response()->json([
            'success' => true,
            'error' => false,
            'message' => 'null',
            'html' => $html,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $customer = Customer::create($request->all());

        $message = trans('messages.success.added', ['type' => trans_choice('general.customers', 1)]);

        return response()->json([
            'success' => true,
            'error' => false,
            'data' => $customer,
            'message' => $message,
            'html' => 'null',
        ]);
    }
}
