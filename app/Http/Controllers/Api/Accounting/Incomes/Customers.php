<?php

namespace App\Http\Controllers\Api\Accounting\Incomes;

use App\Helpers\Reply;
use App\Http\Controllers\Api\Accounting\AccountingController;
use App\Http\Requests\Accounting\Income\Customer as Request;
use App\Models\Accounting\Income\Customer;
use App\Models\Accounting\Income\Invoice;
use App\Models\Accounting\Income\Revenue;
use App\Models\Accounting\Setting\Currency;
use App\Utilities\Accounting\Import;
use App\Utilities\Accounting\ImportFile;
use Illuminate\Http\Request as FRequest;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class Customers extends AccountingController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (\request('autoComplete')) {
            $customers = ['data' => collect(Customer::filter(request()->input())->get())->toArray()];
        } else {
            $customers = Customer::collect();
        }
        return response()->json($customers);
    }

    /**
     * Show the form for viewing the specified resource.
     *
     * @param  Customer $customer
     *
     * @return Response
     */
    public function show(Customer $customer)
    {
        $amounts = [
            'paid' => 0,
            'open' => 0,
            'overdue' => 0,
        ];

        $counts = [
            'invoices' => 0,
            'revenues' => 0,
        ];

        // Handle invoices
        $invoices = Invoice::with(['status', 'payments'])->where('customer_id', $customer->id)->get();

        $counts['invoices'] = $invoices->count();

        $invoice_payments = [];

        $today = Date::today()->toDateString();

        foreach ($invoices as $item) {
            $payments = 0;

            foreach ($item->payments as $payment) {
                $payment->category = $item->category;

                $invoice_payments[] = $payment;

                $amount = $payment->getConvertedAmount();

                $amounts['paid'] += $amount;

                $payments += $amount;
            }

            if ($item->invoice_status_code == 'paid') {
                continue;
            }

            // Check if it's open or overdue invoice
            if ($item->due_at > $today) {
                $amounts['open'] += $item->getConvertedAmount() - $payments;
            } else {
                $amounts['overdue'] += $item->getConvertedAmount() - $payments;
            }
        }

        // Handle revenues
        $revenues = Revenue::with(['account', 'category'])->where('customer_id', $customer->id)->get();

        $counts['revenues'] = $revenues->count();

        // Prepare data
        $items = collect($revenues)->each(function ($item) use (&$amounts) {
            $amounts['paid'] += $item->getConvertedAmount();
        });

        $limit = request('limit', setting('general.list_limit', '25'));
        $transactions = $this->paginate($items->merge($invoice_payments)->sortByDesc('paid_at'), $limit);
        $invoices = $this->paginate($invoices->sortByDesc('paid_at'), $limit);
        $revenues = $this->paginate($revenues->sortByDesc('paid_at'), $limit);

        return view('incomes.customers.show', compact('customer', 'counts', 'amounts', 'transactions', 'invoices', 'revenues'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $action = Customer::create($request->all());
        return Reply::loggedResponse($action, $action, 'create', 'accounting.customer', 'customer');
    }

    /**
     * Duplicate the specified resource.
     *
     * @param  Customer $customer
     *
     * @return Response
     */
    public function duplicate(Customer $customer)
    {
        $clone = $customer->duplicate();

        $message = trans('messages.success.duplicated', ['type' => trans_choice('general.customers', 1)]);

        flash($message)->success();

        return redirect('incomes/customers/' . $clone->id . '/edit');
    }

    /**
     * Import the specified resource.
     *
     * @param  ImportFile $import
     *
     * @return Response
     */
    public function import(ImportFile $import)
    {
        if (!Import::createFromFile($import, 'Income\Customer')) {
            return redirect('common/import/incomes/customers');
        }

        $message = trans('messages.success.imported', ['type' => trans_choice('general.customers', 2)]);

        flash($message)->success();

        return redirect('incomes/customers');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  Customer $customer
     * @param  Request  $request
     *
     * @return Response
     */
    public function update(Customer $customer, Request $request)
    {

        $action = $customer->update($request->all());
        return Reply::loggedResponse($action, $customer, 'update', 'accounting.customer', 'customer');

    }

    /**
     * Enable the specified resource.
     *
     * @param  Customer $customer
     *
     * @return Response
     */
    public function enable(Customer $customer)
    {
        $customer->enabled = 1;
        return Reply::loggedResponse($customer->save(), $customer, 'update', 'accounting.customer', 'customer');
    }

    /**
     * Disable the specified resource.
     *
     * @param  Customer $customer
     *
     * @return Response
     */
    public function disable(Customer $customer)
    {
        $customer->enabled = 0;
        return Reply::loggedResponse($customer->save(), $customer, 'update', 'accounting.customer', 'customer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Customer $customer
     *
     * @return \App\Http\Controllers\Api\Accounting\Incomes\Response|array
     */
    public function destroy(Customer $customer)
    {
        $relationships = $this->countRelationships($customer, [
            'invoices' => 'invoices',
            'revenues' => 'revenues',
        ]);

        if (empty($relationships)) {

            return Reply::loggedResponse($customer->delete(), $customer, 'delete', 'accounting.customer', 'customer');

        }
        return response()->json(Reply::error('messages.warning.deleted', ['name' => $customer->name, 'text' => implode(', ', $relationships)]));

    }

    /**
     * Export the specified resource.
     *
     * @return Response
     */
    public function export()
    {
        \Excel::create('customers', function ($excel) {
            $excel->sheet('customers', function ($sheet) {
                $sheet->fromModel(Customer::filter(request()->input())->get()->makeHidden([
                    'id', 'company_id', 'created_at', 'updated_at', 'deleted_at'
                ]));
            });
        })->download('xlsx');
    }

    public function currency()
    {
        $customer_id = (int)request('customer_id');

        if (empty($customer_id)) {
            return response()->json([]);
        }

        $customer = Customer::find($customer_id);

        if (empty($customer)) {
            return response()->json([]);
        }

        $currency_code = setting('general.default_currency');

        if (isset($customer->currency_code)) {
            $currencies = Currency::enabled()->pluck('name', 'code')->toArray();

            if (array_key_exists($customer->currency_code, $currencies)) {
                $currency_code = $customer->currency_code;
            }
        }

        // Get currency object
        $currency = Currency::where('code', $currency_code)->first();

        $customer->currency_name = $currency->name;
        $customer->currency_code = $currency_code;
        $customer->currency_rate = $currency->rate;

        $customer->thousands_separator = $currency->thousands_separator;
        $customer->decimal_mark = $currency->decimal_mark;
        $customer->precision = (int)$currency->precision;
        $customer->symbol_first = $currency->symbol_first;
        $customer->symbol = $currency->symbol;

        return response()->json($customer);
    }

    public function customer(Request $request)
    {
        $customer = Customer::create($request->all());

        return response()->json($customer);
    }

    public function field(FRequest $request)
    {
        $html = '';

        if ($request['fields']) {
            foreach ($request['fields'] as $field) {
                switch ($field) {
                    case 'password':
                        $html .= \Form::passwordGroup('password', trans('auth.password.current'), 'key', [], null, 'col-md-6 password');
                        break;
                    case 'password_confirmation':
                        $html .= \Form::passwordGroup('password_confirmation', trans('auth.password.current_confirm'), 'key', [], null, 'col-md-6 password');
                        break;
                }
            }
        }

        $json = [
            'html' => $html
        ];

        return response()->json($json);
    }

    /**
     * Generate a pagination collection.
     *
     * @param array|Collection $items
     * @param int              $perPage
     * @param int              $page
     * @param array            $options
     *
     * @return LengthAwarePaginator
     */
    public function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
