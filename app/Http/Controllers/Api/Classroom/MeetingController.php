<?php


namespace App\Http\Controllers\Api\Classroom;

use App\Classroom;
use App\Helpers\Reply;
use App\Http\Requests\Classroom\GetMeetingRequest;
use App\Http\Requests\classroom\JoinMeetingRequest;
use App\Http\Requests\Classroom\SaveMeeting;
use App\Meeting;
use BigBlueButton\BigBlueButton;
use BigBlueButton\Parameters\CreateMeetingParameters;
use BigBlueButton\Parameters\GetMeetingInfoParameters;
use BigBlueButton\Parameters\JoinMeetingParameters;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class MeetingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(GetMeetingRequest $request)
    {

        $meetings = Meeting::where('classroom_id', $request->classroom_id);
        $meetings->orderByDesc('start_time');
        $data = $meetings->paginate($request->page_size);
        foreach ($data as $k => $v) {
            if ($v['status'] == 1) {
                $bbb = new BigBlueButton();
//                $meetingParams = new GetMeetingInfoParameters();
//                $meetingParams->setPassword();
//                $meetingParams->setMeetingId($meetings->meeting_id);
//                    $bbb->getMeetingInfo();
            }
        }
        return response()->json($data);


    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveMeeting $request)
    {
        $meeting = new Meeting();
        $meeting->meeting_id = Str::uuid();
        $meeting->name = $request->name;
        $meeting->creator_id = auth()->id();
        $meeting->classroom_id = $request->classroom_id;
        $meeting->start_time = $request->start_time;
        $meeting->status = 0;
        $meeting->recorded = $request->recorded;
        $meeting->attendee_password = Str::uuid();
        $meeting->presenter_password = Str::uuid();
        $meeting->welcome_message = $request->welcome_message;
        $meetingStatus = $meeting->save();
        if ($meetingStatus) {
            Reply::loggedResponse($meetingStatus, $meeting, 'create', 'classroom', 'meeting');
            if ($request->exists('status')) {
                $timeRemaining = Carbon::createFromFormat('Y-m-d H:i:s', $request->start_time)->diffInSeconds(Carbon::now());
                $returnData = $meeting->toArray();
                if ($timeRemaining < 121) {
                    $newMeeting = Meeting::findOrFail($meeting->id)->makeVisible(['meeting_id', 'attendee_password', 'presenter_password']);
                    $startMeeting = $this->startMeeting($newMeeting);
                    $join = $this->joinMeetingApiCall($newMeeting->meeting_id, auth()->user(), $newMeeting->presenter_password);
                    $newMeeting->status = 1;
                    $newMeeting->save();
                    $returnData = $newMeeting->toArray();
                    $returnData['join'] = $join;
                }
                return response()->json(Reply::success('', $returnData));
            }
        }
        return response()->json(Reply::error(''));
    }

    private function startMeeting($meeting)
    {
//        $meeting = Meeting::findOrFail($id)->makeVisible(['meeting_id', 'attendee_password', 'presenter_password']);
        $bbb = new BigBlueButton();
        $meetingId = $meeting->meeting_id;
        $meetingParams = new CreateMeetingParameters($meeting->meeting_id, $meeting->name);
        $meetingParams->setAttendeePassword($meeting->attendee_password);
        $meetingParams->setModeratorPassword($meeting->presenter_password);
        $meetingParams->setLogo('http://localhost:4200/assets/logo-white.png');
        $meetingParams->setMaxParticipants(30);
        $meetingParams->setWelcomeMessage($meeting->welcome_message);
        $meetingParams->setDuration($meeting->duration);
        $response = $bbb->createMeeting($meetingParams);
        if ($response->getReturnCode() == 'SUCCESS') {
            return $response;
        } else {
            return false;
        }

    }

    public function postJoinMeeting(JoinMeetingRequest $request)
    {
        $newMeeting = Meeting::findOrFail($request->meeting_id)->makeVisible(['meeting_id', 'attendee_password', 'presenter_password']);
        $joinUrl = $this->joinMeetingApiCall($newMeeting->meeting_id, auth()->user(), $newMeeting->presenter_password);
//        echo $joinUrl;
        return response()->json(Reply::success('', $joinUrl));

    }

    private function joinMeetingApiCall($meetingId, $user, $password = false)
    {
        $bbb = new BigBlueButton();
        $joinMeetingParams = new JoinMeetingParameters($meetingId, $user->first_name . ' ' . $user->last_name, $password);
        $joinMeetingParams->setRedirect(true);
        $joinMeetingParams->setUserId($user->id);
        $joinMeetingParams->setRedirect(false);
        $joinMeetingParams->setJoinViaHtml5(true);
//        $joinMeetingParams->addUserData('displayBrandingArea', false);
        $joinMeetingParams->addUserData('userdata-bbb_display_branding_area', false);
        $joinMeetingParams->addUserData('userdata-bbb_show_participants_on_login', false);
        $joinMeetingParams->addUserData('showParticipantsOnLogin', false);
//        $joinMeetingParams->addUserData('customStyle', ':root {
//        --color-link-hover: #b56d00 !important;
//          --list-item-bg-hover: #f1ebe2;
//          --loader-bg: #4e342e !important;
//             --list-item-bg: #f5f2ef;
//             --poll-blue: #f90;
//             --color-danger: #dc3545;
//             --color-gray-dark : rgb(78 52 46);
//             --color-warning: #ffc107;
//             --color-success: #28a745;
//
//         --systemMessage-background-color: #fbf9f7;
//    --systemMessage-border-color: #d8d2c9;
//               --btn-primary-color: #4e342e;
//              --poll-stats-border-color: #e2ded8;
//              --color-off-white: #f9f7f4;
//              --color-primary: #ff9900 !important;
//                --color-blue-light: #ffad14;
//                --color-blue-lighter: #ffad14b3;
//                --color-blue-lightest: #ffad1463;
//            }
//            .overlay--Z1jBtUT {
//    background-color: rgb(78 52 46 / 0.75);
//}
//            .overlay--1aTlbi {
//                background-color:#fff !important;
//             }
//             .scrollableList--Z2s6Her {
//                background-color: transparent !important
//              }
//               body {
//                    background-color:#4e342e !important;
//               }
//               .audioBtn--1H6rCK span:first-child {
//                        background-color: #fbf5ec !important;
//                        border: 5px solid #fff1df; !important
//                }
//                .notificationsBar--ZrXHnR
//                {
//                    height: 50px !important;
//                    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
//                    box-sizing: border-box !important;
//                    position: relative !important;
//                    z-index: 9 !important;
//                }
//               .audioBtn--1H6rCK:hover span:first-child, .audioBtn--1H6rCK:focus span:first-child {
//                   background-color: #fdf9f3 !important;
//                }');
//        $joinMeetingParams->setAvatarURL('https://' . config('app.tenant')->hostname . '/api/storage/' . config('app.tenant')->hostname . '/' . $user->picture_url);

        $joinMeeting = $bbb->joinMeeting($joinMeetingParams);
//        echo $joinMeeting->getUrl() . '&authToken=' . $joinMeeting->getAuthToken() . '&userId=' . $joinMeeting->getUserId();
        return ['url' => $joinMeeting->getUrl() . '&authToken=' . $joinMeeting->getAuthToken() . '&userId=' . $joinMeeting->getUserId() . '&meetingId=' . $joinMeeting->getMeetingId(),
            'ponger' => $bbb->getJSessionId()];

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        //
    }
}
