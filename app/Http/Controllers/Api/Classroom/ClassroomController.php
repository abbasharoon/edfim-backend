<?php

namespace App\Http\Controllers\Api\Classroom;

use App\Classroom;
use App\Helpers\Reply;
use App\Http\Requests\Classroom\ParticipantRequest;
use App\Http\Requests\Classroom\SaveClassroom;
use App\Subject;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClassroomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Bouncer::can('manage-all-classroom')) {
            $classrooms = Classroom::with(['participant.roles', 'meeting' => function ($q) {
                $q->where('status', 1);
            }])->withCount('meeting');
            $classrooms = $classrooms->get();
        } else {
//             TODO: Optimize this shit in single query
            $moderatedClasses = Classroom::whereHas('moderator', function ($q) {
                $q->where('classroom_user.user_id', auth()->id());
            })->get()->pluck('id')->toArray();

            $classrooms = Classroom::whereHas('participant', function ($q) {
                $q->where('classroom_user.user_id', auth()->id());
            })->with(['participant.roles', 'participant' => function ($q) use ($moderatedClasses) {
                if (count($moderatedClasses) === 0) {
                    $q->where('classroom_user.user_id', auth()->id());
                    $q->where('classroom_user.role', 1);
                }
                $q->whereIn('classroom_user.classroom_id', $moderatedClasses);
            }])->withCount('meeting')->get();
        }
        return response()->json($classrooms);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $classroom = new Classroom();
        $classroom->name = $request->name;
        $classroom->class_id = $request->class_id;
        $classroom->batch_id = $request->batch_id;
        $classroom->subject_id = $request->subject_id;
        $save = $classroom->save();
        if (!is_null($classroom->class_id)) {

            if (!is_null($classroom->batch_id)) {
                $users = User::where('batch_id', $classroom->batch_id)->get()->pluck('id')->toArray();
            } else {
                $users = User::whereHas('batch.class', function ($q) use ($classroom) {
                    $q->where('classes.id', $classroom->class_id);
                })->get()->pluck('id')->toArray();
            }
            if (!is_null($classroom->subject_id)) {
                User::whereHas('subjectstaff', function ($q) use ($classroom) {
                    $q->where('subject_user.subject_id', $classroom->subject_id);
                })->get()->pluck('id')->each(function ($i) use ($users) {
                    $users[$i] = ['role' => 1];
                });
            }
            $classroom->participant()->attach($users);
        }
        $classroom->load('participant.roles');

        return Reply::loggedResponse($save, $classroom, 'create', 'classroom', 'classroom');

    }

    public function updateParticipants(ParticipantRequest $request)
    {
        $classroom = Classroom::findOrFail($request->classroom_id);
        if ($classroom->participant()->sync($request->participants)) {
            return response()->json(Reply::success(''));
        }
        return response()->json(Reply::error(''));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
