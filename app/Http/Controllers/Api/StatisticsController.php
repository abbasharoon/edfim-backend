<?php

namespace App\Http\Controllers\Api;

use App\AssessmentTest;
use App\Assignment;
use App\Attendance;
use App\ExamPaper;
use App\Helpers\PermissionsHelper;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use Bouncer;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StatisticsController extends Controller
{


    public function batchActivity(Request $request, PermissionsHelper $ph)
    {
        if (Bouncer::can('view-all-batch') ||
            (Bouncer::can('view-batch') && $ph->teacherBatch($request->batch_id)) ||
            $ph->studentHasBatch($request->batch_id)) {
            $date = Carbon::createFromFormat('Y-m-d', $request->date);
            $startOfMonth = $date->startOfMonth()->toDateTimeString();
            $endOfMonth = $date->endOfMonth()->toDateTimeString();
            $assignmentsCount = Assignment::select('due_date', \DB::raw('count(*) as total'))
                ->where('batch_id', $request->batch_id)
                ->groupBy('due_date')
                ->whereIn('status', [1, 2])
                ->whereBetween('due_date',
                    [$startOfMonth, $endOfMonth])
                ->pluck('total', 'due_date')->all();
            $testsCount = AssessmentTest::select('due_date', \DB::raw('count(*) as total'))
                ->where('batch_id', $request->batch_id)
                ->whereIn('status', [1, 2, 3])
                ->groupBy('due_date')
                ->whereBetween('due_date',
                    [$startOfMonth, $endOfMonth])
                ->pluck('total', 'due_date')->all();
            $examsCount = ExamPaper::select('due_date', \DB::raw('count(*) as total'))
                ->whereHas('assessmentExam', function ($query) use ($request) {
                    $query->where('batch_id', $request->batch_id);
                    $query->whereIn('status', [1, 2, 3]);
                })
                ->groupBy('due_date')
                ->whereBetween('due_date',
                    [$startOfMonth, $endOfMonth])
                ->pluck('total', 'due_date')->all();
            return response()->json(Reply::success('', compact('assignmentsCount',
                'testsCount', 'examsCount', 'startOfMonth', 'endOfMonth')));
        }
    }

    public function attendanceAggregate(Request $request, PermissionsHelper $ph)
    {
        if ($request->batch_id) {
            if (Bouncer::can('view-all-batch') ||
                (Bouncer::can('view-batch') && $ph->teacherBatch($request->batch_id)) ||
                $ph->studentHasBatch($request->batch_id)) {
                $startDate = Carbon::createFromFormat('Y-m-d', $request->start_date)->toDateTimeString();
                $endDate = Carbon::createFromFormat('Y-m-d', $request->end_date)->toDateTimeString();
                $totalAttendances = Attendance::whereBatchId($request->batch_id)
                    ->where('status', 1)
                    ->where('attendance_register_id', $request->attendance_register_id)
                    ->whereBetween('date', [$startDate, $endDate])
                    ->withCount(['absent' => function ($q) {
                        if (auth()->user()->isA('student')) {
                            $q->where('user_id', auth()->id);
                        }
                    }])
                    ->get(['id', 'date']);
                $absenteesCount = Attendance::whereBatchId($request->batch_id)
                    ->where('status', 1)->whereBetween('date', [$startDate, $endDate])->get();
            }
        }
    }
}
