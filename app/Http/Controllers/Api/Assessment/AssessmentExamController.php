<?php

namespace App\Http\Controllers\Api\Assessment;

use App\AssessmentExam;
use App\Helpers\ExamHelper;
use App\Helpers\PermissionsHelper;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Assessment\StoreAssessmentExam;
use App\Http\Requests\Assessment\UpdateAssessmentExam;
use App\User;
use Bouncer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * @apiDefine Params
 * @apiParam {String{..256}} name Title/Name of Assessment
 * @apiParam {String{..1024}} [detail] Detail of assessment
 * @apiParam {Integer} batch_id Id of the batch for which the assessment is being added
 * @apiParam {Integer} class_id Id of the class to which the batch belongs
 * @apiParam {Integer="0=Non,1=Daily,2=Weekly,3=Monthly,4=Quarterly,5=Triannual,6=biannual,7=Yearly}} schedule Schedule type for if test is repeatable
 * @apiParam {Integer=0,1}} status 0=Draft, 1=Due
 */


/**
 * @apiDefine Success
 * @apiSuccess {String} message Success Message
 * @apiSuccess {Boolean} status Returns True
 * @apiSuccess {Object[]} data Optionally data will be sent back
 *
 * @apiSuccessExample {json}Success-Response:
 *      HTTP/1.1 200 OK
 *      {
 *          "id": 4
 *          "status": true,
 *           "message": "Assessment Successfully Created",
 *           "data": {
 *              "name": "Math Homework",
 *              "detail": "Some exam denials",
 *              "batch_id": "1",
 *              "status": 0,
 *              "schedule": "0",
 *              "class_id": "1",
 *              "updated_at": "2018-04-30 06:00:12",
 *              "created_at": "2018-04-30 06:00:12",
 *           }
 *      }
 *
 */

/**
 * @apiDefine Error
 * @apiError {String} message Error Message
 * @apiError {Boolean} status Returns False
 * @apiError {Object[]} data Optionally data will be sent back
 *
 */
class AssessmentExamController extends Controller
{


    /**
     * @api           {GET} /assessment/exam List Exams
     * @apiName       ViewAssessmentExams
     * @apiGroup      Assessments
     * @apiPermission view-all-exam,view-exam,(student/parent)
     * @apiParam {String{..1024}} [search_string] Any textual string to be matched with name or detail of exam or papers
     * @apiParam {Integer} [batch_id] Filter by batches id
     * @apiParam {Integer} [class_id] Filter by class id
     * @apiParam {Integer=0,1,2,3}} [status] Filter by assessment status
     * @apiParam {Integer=0,1,2,3,4,5,6,7}} [status] Filter by assessment schedule
     * @apiParam {Date} [due_date] Filter by the date on which assessment is due
     * @apiParam {Date} [created_at] Filter by the date assessment was created
     *
     * @apiSuccess {String} message Success Message
     * @apiSuccess {Boolean} status Returns True
     * @apiSuccess {Object[]} data Requested data will be sent back if it exists
     * @apiSuccess {Number} current_page Current Page number on pagination index
     * @apiSuccess {Number} from Start of number of received accounts
     * @apiSuccess {Number} to End of number of received accounts
     * @apiSuccess {Number} total Total Number of Accounts
     * @apiSuccess {Number} last_page Last page on pagination index
     * @apiSuccess {Number} per_page Number of records per page
     *
     * @apiSuccessExample {json}Success-Response:
     *      HTTP/1.1 200 OK
     *      {
     *       "status":true,
     *       "message":"Assignment Created/Updated",
     *       "data":[
     *           "total": 500,
     *           "per_page": 50,
     *           "current_page": 1,
     *           "last_page": 10,
     *           "from": 1,
     *           "to": 50,
     *           "data":    [
     *                       {
     *                           "id": 1,
     *                           "name": "Biology Monthly Test",
     *                           "description": "Some test description",
     *                           "type": "Verbal",
     *                           "schedule": 7,
     *                           "status": 0,
     *                           "score": 0,
     *                           "due_date": "2018-12-08",
     *                           "subject_id": 1,
     *                           "batch_id": 1,
     *                           "deleted_at": null,
     *                           "created_at": "2018-04-27 08:13:39",
     *                           "updated_at": "2018-04-27 08:22:34"
     *                      },
     *                       {
     *                           "id": 2,
     *                           "name": "Math Test",
     *                           "description": "Some test description",
     *                           "type": "Verbal",
     *                           "schedule": 0,
     *                           "status": 0,
     *                           "score": 0,
     *                           "due_date": "2018-12-08",
     *                           "subject_id": 1,
     *                           "batch_id": 1,
     *                           "deleted_at": null,
     *                           "created_at": "2018-04-27 08:15:57",
     *                           "updated_at": "2018-04-27 08:15:57"
     *                      }
     *           ]
     *        ]
     *      }
     *
     * @apiUse        Error
     */
    public function index(Request $request, PermissionsHelper $ph)
    {

        $assessment = AssessmentExam::query();

        if (Bouncer::can('view-all-exam')) {
            if ($request->filled('batch_id')) {
                $assessment->where('batch_id', $request->batch_id);
            }
        } elseif (Bouncer::can('view-exam')) {
            $assessment->whereIn('batch_id', $ph->teacherBatch($request->batch_id));
        } elseif (Auth::user()->isA('student', 'parent')) {
            $assessment->whereIn('batch_id', $ph->studentBatch($request->batch_id));
            $assessment->where('status', '!=', 0);
        } else {
            return Reply::unAuthorized();
        }
        if ($request->filled('name')) {
            $assessment->where('name', 'LIKE', '%' . $request->name . '%');
//            $assessment->orWhere('detail', 'LIKE', '%' . $request->name . '%');
//            $assessment->orWhereHas('papers', function ($query) use ($request) {
//                $query->where('title', 'LIKE', '%' . $request->title . '%');
//                $query->orWhere('detail', 'LIKE', '%' . $request->title . '%');
//            });
        }
        if ($request->filled('due_date')) {
            $assessment->WhereHas('papers', function ($query) use ($request) {
                $query->where('due_date', $request->due_date);
            });
        }
        if ($request->filled('created_at')) {
            $assessment->where('created_at', $request->created_at);
        }
        if ($request->filled('status')) {
            $assessment->where('status', $request->status);
        }
        if ($request->filled('schedule')) {
            $assessment->where('schedule', $request->status);
        }
        if ($request->filled('class_id')) {
            $assessment->where('class_id', $request->class_id);
        }


        if (!$request->sort_column) {
            $request->sort_column = 'created_at';
            $request->sort_direction = 'desc';
        }
        if ($request->sort_direction === 'desc') {
            $assessment->orderByDesc($request->sort_column);
        } else {
            $assessment->orderBy($request->sort_column);
        }

        return response()->json($assessment->paginate($request->page_size));
    }


    /**
     * @api           {POST} /assessment/exam Create Exam
     * @apiName       CreateExam
     * @apiPermission manage-all-exam,manage-exam
     * @apiGroup      Assessments
     * @apiUse        Params
     * @apiUse        Success
     * @apiUse        Error
     */
    public function store(StoreAssessmentExam $request)
    {
        $exam = new AssessmentExam();
        $exam->name = $request->name;
        $exam->detail = $request->detail;
        $exam->batch_id = $request->batch_id;
        $exam->status = 0;
        $exam->schedule = $request->schedule;
        $exam->class_id = $request->class_id;
        return Reply::loggedResponse($exam->save(),
            $exam,
            'create',
            'assessment',
            'assessmentExam');
    }


    /**
     * @api           {GET} /assignment/exam/:id Get Exam
     * @apiName       ViewAssessmentExam
     * @apiPermission view-exam,view-all-exam,(student/parent)
     * @apiGroup      Assessments
     * @apiSuccess {Boolean} status Status of request
     * @apiSuccess {String} message Success message
     * @apiSuccess {Object[]} data Object holding complete exam data
     * @apiSuccess {Number} data.id Id of exam in database
     * @apiSuccess {String} data.name Name of Exam
     * @apiSuccess {String} data.detail Detail of exam
     * @apiSuccess {Number} data.schedule Schedule type of exam 1=daily,2=weekly,3=monthly,4=quarterly,5=triannual,6=biannually,7=annually
     * @apiSuccess {Number} data.status  Status of Exam. 0=draft, 1 =due, 2=taken, 3 =published, 4=cancelled
     * @apiSuccess {Number} data.batch_id Id of batch for which exam was created
     * @apiSuccess {Number} data.class_id of class of the batch
     * @apiSuccess {Object[]} data.papers Object contianing all the related papers
     * @apiSuccess {Number} data.papers.id  Id of paper in database
     * @apiSuccess {String} data.papers.name Name of Paper
     * @apiSuccess {String} data.papers.detail Details of paper
     * @apiSuccess {String} data.papers.type  Type of paper
     * @apiSuccess {Number} data.papers.status Paper Status 0=draft,1=due,2=take,3=published,4=cancelled
     * @apiSuccess {Number} data.papers.score  Score of paper
     * @apiSuccess {String} data.papers.due_date Date string in YYYY-MM-DD format
     * @apiSuccess {Number} data.papers.subject_id  Related Subject id
     * @apiSuccess {Number} data.papers.assessment_exam_id Related assessment id
     * @apiSuccess {Object[]} data.papers.subject Object contianing subject detail of paper
     * @apiSuccess {Number} data.papers.subject.id Id of subject in database
     * @apiSuccess {Number} data.papers.subject.name Name of subject in database
     * @apiSuccess {Object[]} data.student_score An object containing all the students scores along with student details. It's may or may not be available
     * @apiSuccess {Number} data.student_score.id Id of student in database
     * @apiSuccess {Number} data.student_score.total Total score of student in all papers
     * @apiSuccess {String} data.student_score.position Position of student in batch/class
     * @apiSuccess {Number} data.student_score.paper_id  An variable number of score  for papers "KEYED BY THE PAPER ID" e.g 1:25,2:56 where 1 and 2 are paper ids
     * @apiSuccess {Object[]} data.student_score.student  Data of student for display
     * @apiSuccess {Number} data.student_score.student.id  id of student in database
     * @apiSuccess {String} data.student_score.student.first_name  First name of student
     * @apiSuccess {String} data.student_score.student.middle_name  Middle name of student
     * @apiSuccess {String} data.student_score.student.last_name  Last name of student
     * @apiSuccess {Object[]} data.student_score.student.user_data  User data contians miscellaneous student data
     * @apiSuccess {String} data.student_score.student.user_data.picture_url  Name of profile image
     * @apiSuccessExample {json}Success-Response:
     *      HTTP/1.1 200 OK
     *
     *     {
     *           "status": true,
     *           "message": "",
     *           "data": {
     *               "id": 1,
     *               "name": "Math Homework",
     *               "detail": "This is a must to do homeowrk",
     *               "schedule": 0,
     *               "status": 0,
     *               "batch_id": 1,
     *               "class_id": 1,
     *               "student_score": {
     *                   "13": {
     *                       "1": 61,
     *                       "2": 1,
     *                       "total": 62,
     *                       "id": 13,
     *                       "position": "5th",
     *                       "student": {
     *                           "id": 13,
     *                           "first_name": "Llewellyn",
     *                           "middle_name": "Sallie",
     *                           "last_name": "Jocelyn",
     *                           "user_data": {
     *                               "user_id": 13,
     *                               "picture_url": "https://lorempixel.com/640/480/?99025"
     *                           }
     *                       }
     *                   },
     *               },
     *               "papers": [
     *               {
     *                   "id": 1,
     *                   "name": "Math Homework",
     *                   "detail": "This is a must to do homeowrk",
     *                   "type": "Verbal",
     *                   "status": 0,
     *                   "score": 200,
     *                   "due_date": "2018-12-08 00:00:00",
     *                   "subject_id": 5,
     *                   "assessment_exam_id": 1,
     *                   "deleted_at": null,
     *                   "created_at": "2018-05-30 13:08:41",
     *                   "updated_at": "2018-05-30 13:08:41",
     *                   "subject": {
     *                       "id": 5,
     *                       "name": "Urdu",
     *                       "class_id": "1",
     *                       "created_at": "2017-12-30 00:00:00",
     *                       "updated_at": "2017-12-30 00:00:00"
     *                       }
     *                   },
     *               ]
     *           }
     *       }
     * @apiUse        Error
     */
    public function show($id, PermissionsHelper $ph)
    {

        $exam = AssessmentExam::with('papers',
            'papers.student_score')->findOrFail($id);
        if (Bouncer::can('view-all-exam') ||
            (Bouncer::can('view-exam') && $ph->hasBatch($exam->batch_id)) ||
            ($ph->studentHasBatch($exam->batch_id) && $exam->status > 0)) {
            $childrenIds = null;
            if (auth()->user()->isA('parent')) {
                $childrenIds = $ph->childrenIds();
            }
            $studentTotal = [];
            $exam->papers->transform(function ($item) use (&$studentTotal, $childrenIds, $exam) {
                foreach ($item->student_score as $score) {
                    if (isset($studentTotal[$score->user_id]['total'])) {
                        $studentTotal[$score->user_id]['total'] += $score->score;
                    } else {
                        $studentTotal[$score->user_id]['total'] = $score->score;
                        $studentTotal[$score->user_id]['id'] = $score->user_id;
                    }
                    $studentTotal[$score->user_id][$item->id] = $score->score;
                }
                unset($item['student_score']);
                return $item;
            });
            $numberFormatter = new \NumberFormatter('en_US', \NumberFormatter::ORDINAL);
            $totalScore = collect($studentTotal);
            $rankedScore = $totalScore->sortByDesc('total')->keyBy('total')->keys()->toArray();
            $totalScore = $totalScore->map(function ($item) use ($rankedScore, $numberFormatter) {
                $item['position'] = $numberFormatter->format(array_search($item['total'], $rankedScore) + 1);
                return $item;
            });


            if (setting('show_all_results') == 0 &&
                auth()->user()->isA('student', 'parent')) {
                if (auth()->user()->isA('student')) {
                    $totalScore = $totalScore->where('id', auth()->id());
                } elseif (auth()->user()->isA('parent')) {
                    $totalScore = $totalScore->whereIn('id', $childrenIds);
                }
            }
            $users = User::whereIn('id', $totalScore->pluck('id'))
                ->withTrashed()->get(['id', 'first_name', 'middle_name', 'last_name', 'roll_no', 'picture_url', 'gender'])->keyBy('id');
            $totalScore->transform(function ($i) use ($users) {
                $i['student'] = $users[$i['id']];
                return $i;
            });

            if ((auth()->user()->isA('student', 'parent') && $exam->status > 2) ||
                auth()->user()->isNotA('student', 'parent')) {
                $exam['student_score'] = $totalScore->values();
            }

            return response()->json(Reply::success('', $exam));
        }
        return Reply::unAuthorized();
    }


    /**
     * @api           {PUT|PATCH} /assessment/exam/:id Update Exam
     * @apiName       UpdateAssessmentExam
     * @apiGroup      Assessments
     * @apiPermission manage-all-exam,manage-exam
     * @apiParam {String{..256}} name Title/Name of Assessments
     * @apiParam {String{..1024}} Description Description of Assessment
     * @apiParam {Integer="0=draft,1=due,2=published,3=canceled"} status Assessment new status
     * @apiParam {Integer="0,1,2,3,4,5,6,7"} schedule Schedule type of assessment
     * @apiUse        Success
     * @apiUse        Error
     */
    public function update(UpdateAssessmentExam $request, $id)
    {
        $exam = AssessmentExam::findOrFail($id);
        if ($request->fill_status) {
            $exam->status = $request->status;
            if ($request->status > 0 && $request->notify) {
                $exam->load(['papers' => function ($q) {
                    $q->min('due_date');
                }]);
                if ($request->status == 3) {
                    $exam->load('papers', 'papers.subject', 'papers.student_score');
                }
                ExamHelper::notify($exam);
            }
        } else {
            $exam->name = $request->name;
            $exam->detail = $request->detail;
            $exam->schedule = $request->schedule;
        }
        return Reply::loggedResponse($exam->save(),
            $exam,
            'update',
            'assessment',
            'assessmentExam');
    }


    /**
     * @api           {DELETE} /assessment/exam/:id Delete Exam
     * @apiName       DeleteAssessmentExam
     * @apiPermission manage-exam,manage-all-exam
     * @apiGroup      Assessments
     * @apiUse        Success
     * @apiUse        Error
     */
    public function destroy($id, PermissionsHelper $ph)
    {
        $exam = AssessmentExam::findOrFail($id);
        if (Bouncer::can('manage-all-exam') ||
            (Bouncer::can('manage-exam') && $ph->hasBatch($exam->batch_id))) {
            return Reply::loggedResponse($exam->delete(),
                $exam,
                'delete',
                'assessment',
                'assessmentExam');
        }
        return Reply::unAuthorized();
    }
}
