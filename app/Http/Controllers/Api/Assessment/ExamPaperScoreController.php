<?php

namespace App\Http\Controllers\Api\Assessment;

use App\ExamPaper;
use App\ExamPaperScore;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Assessment\StoreExamPaperScore;
use App\Http\Requests\Assessment\UpdateExamPaperScore;


/**
 * @apiDefine Success
 * @apiSuccess {String} message Success Message
 * @apiSuccess {Boolean} status Returns True
 * @apiSuccess {Object[]} data Optionally data will be sent back
 *
 */

/**
 * @apiDefine Error
 * @apiError {String} message Error Message
 * @apiError {Boolean} status Returns False
 * @apiError {Object[]} data Optionally data will be sent back
 *
 */
class ExamPaperScoreController extends Controller
{

    /**
     * @api           {POST} /assessment/test/score Store Exam Score
     * @apiName       StoreAssessmentExamScore
     * @apiGroup      Assessments
     * @apiPermission manage-all-paper,manage-paper,manage-subject-paper
     * @apiParam {Integer} assessment_id Id of assessment for which the score is being added, Assessment must not have status=3=cancelled
     * @apiParam {Boolean} [assessment_status]  True= publish results
     * @apiParam {Object[]}  students_score A nested array of students and their scores
     * @apiParam  {Integer}  students_score.user_id Id of student for whom score is added
     * @apiParam  {Integer}  students_score.score Score status of the student
     * @apiParamExample {json} Request-Example:
     *      {
     *           "status": true,
     *           "message": "",
     *           "data": {
     *               "id": 1,
     *               "name": "XYZ Test",
     *               "description": "Some assessment details",
     *               "type": "Verbal",
     *               "schedule": 7,
     *               "status": 0,
     *               "score": 200,
     *               "due_date": "2018-12-08",
     *               "subject_id": 1,
     *               "batch_id": 1,
     *               "deleted_at": null,
     *               "created_at": "2018-04-29 11:08:22",
     *               "updated_at": "2018-04-29 11:10:55",
     *               "student_score": [
     *                   {
     *                       "id": 1,
     *                       "assessment_test_id": 1,
     *                       "user_id": 13,
     *                       "score": 200
     *                   },
     *                   {
     *                       "id": 2,
     *                       "assessment_test_id": 1,
     *                       "user_id": 14,
     *                       "score": 1
     *                   },
     *                   {
     *                       "id": 3,
     *                       "assessment_test_id": 1,
     *                       "user_id": 15,
     *                       "score": 0
     *                   },
     *                   {
     *                       "id": 4,
     *                       "assessment_test_id": 1,
     *                       "user_id": 16,
     *                       "score": 1
     *                   },
     *                   {
     *                       "id": 5,
     *                       "assessment_test_id": 1,
     *                       "user_id": 17,
     *                       "score": 0
     *                   },
     *                   {
     *                       "id": 6,
     *                       "assessment_test_id": 1,
     *                       "user_id": 18,
     *                       "score": 1
     *                   }
     *               ]
     *           }
     *      }
     * @apiSuccess {String} message Success Message
     * @apiSuccess {Boolean} status Returns True
     * @apiUse        Error
     */
    public function store(StoreExamPaperScore $request)
    {
        $assessment = ExamPaper::findOrFail($request->assessment_id);
        if ($request->filled('status')) {
            $assessment->status = 2;
            $assessment->save();
        }
        $assessment->student_score()->createMany($request->students_score);
        return Reply::loggedResponse($assessment->save(),
            $assessment, 'create',
            'assessment',
            'assessmentPaperScore');
    }

    /**
     * @api           {PUT|PATCH} /assessment/test/score/:id Update Exam Score
     * @apiName       UpdateAssessmentExanScore
     * @apiGroup      Assessments
     * @apiPermission manage-all-paper,manage-paper,manage-subject-paper
     * @apiParam  {Integer}  score Updated Score for the specified assessment score record
     * @apiSuccess {String} message Success Message
     * @apiSuccess {Boolean} status Returns True
     * @apiUse        Error
     */
    public function update(UpdateExamPaperScore $request, $id)
    {
        $assessmentScore = ExamPaperScore::findOrFail($id);
        $assessmentScore = ExamPaperScore::whereExamPaperId($id)->whereUserId($request->user_id)->first();
        $assessmentScore->score = $request->score;
        return Reply::loggedResponse($assessmentScore->save(),
            $assessmentScore, 'update',
            'assessment',
            'assessmentPaperScore');
    }
}
