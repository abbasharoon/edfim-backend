<?php

namespace App\Http\Controllers\Api\Assessment;

use App\AssessmentTest;
use App\Helpers\PermissionsHelper;
use App\Helpers\Reply;
use App\Helpers\TestHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Assessment\StoreAssessmentTest;
use App\Http\Requests\Assessment\UpdateAssessmentTest;
use Bouncer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


/**
 * @apiDefine Params
 * @apiParam {String{..256}} name Title/Name of Assessment
 * @apiParam {String{..1024}} Detail Detail of assessment
 * @apiParam {String} [type] Assessment type as user defined string e.g Verbal, Written etc
 * @apiParam {Integer} batch_id Id of the batch for which the assessment is being added
 * @apiParam {Integer} subject_id Id of the subject for assessment
 * @apiParam {Integer="0=Non,1=Daily,2=Weekly,3=Monthly,4=Quarterly,5=Triannual,6=biannual,7=Yearly}} schedule Schedule type for if test is repeatable
 * @apiParam {Date} due_date Date on which assessment will be taken. Format = YYYY-MM-DD
 * @apiParam {Integer=0,1}} status 0=Draft, 1=Due
 * @apiParam {Integer} score Total score of assessment
 */


/**
 * @apiDefine Success
 * @apiSuccess {String} message Success Message
 * @apiSuccess {Boolean} status Returns True
 * @apiSuccess {Object[]} data Optionally data will be sent back
 *
 * @apiSuccessExample {json}Success-Response:
 *      HTTP/1.1 200 OK
 *      {
 *          "status":true,
 *          "message":"Assessment Created/Updated",
 *          "data":[
 *                  id: 1,
 *                  title: "Maths Assessment"
 *                  detail: "Some detail"
 *                  batch_id: 1,
 *                  subject_id: 2,
 *                  schedule: 0,
 *                  type: Written,
 *                  status: 1,
 *                  score: 100,
 *                  due_date: 2019-02-01
 *                  ]
 *      }
 *
 */

/**
 * @apiDefine Error
 * @apiError {String} message Error Message
 * @apiError {Boolean} status Returns False
 * @apiError {Object[]} data Optionally data will be sent back
 *
 */
class AssessmentTestController extends Controller
{
    /**
     * @api           {GET} /assessment/test List Test
     * @apiName       ViewAssessmentTests
     * @apiGroup      Assessments
     * @apiPermission view-all-test,view-test,(student/parent)
     * @apiParam {String{..1024}} [search_string] Any textual string to be matched with title or detail
     * @apiParam {Integer} [batch_id] Filter by batches id
     * @apiParam {Integer} [subject_id] Filter by subjects id
     * @apiParam {string} [type] Filter by assessment type
     * @apiParam {Integer=0,1,2,3}} [status] Filter by assessment status
     * @apiParam {Integer=0,1,2,3,4,5,6,7}} [status] Filter by assessment schedule
     * @apiParam {Integer} [score] Search by score
     * @apiParam {Date} [due_date] Filter by the date on which assessment is due
     * @apiParam {Date} [created_at] Filter by the date assessment was created
     * @apiSuccess {String} message Success Message
     * @apiSuccess {Boolean} status Returns True
     * @apiSuccess {Object[]} data Requested data will be sent back if it exists
     * @apiSuccess {Number} current_page Current Page number on pagination index
     * @apiSuccess {Number} from Start of number of received accounts
     * @apiSuccess {Number} to End of number of received accounts
     * @apiSuccess {Number} total Total Number of Accounts
     * @apiSuccess {Number} last_page Last page on pagination index
     * @apiSuccess {Number} per_page Number of records per page
     *
     * @apiSuccessExample {json}Success-Response:
     *      HTTP/1.1 200 OK
     *      {
     *       "status":true,
     *       "message":"Assignment Created/Updated",
     *       "data":[
     *           "total": 500,
     *           "per_page": 50,
     *           "current_page": 1,
     *           "last_page": 10,
     *           "from": 1,
     *           "to": 50,
     *           "data":    [
     *                       {
     *                           "id": 1,
     *                           "name": "Biology Monthly Test",
     *                           "detail": "Some test detail",
     *                           "type": "Verbal",
     *                           "schedule": 7,
     *                           "status": 0,
     *                           "score": 0,
     *                           "due_date": "2018-12-08",
     *                           "subject_id": 1,
     *                           "batch_id": 1,
     *                           "deleted_at": null,
     *                           "created_at": "2018-04-27 08:13:39",
     *                           "updated_at": "2018-04-27 08:22:34"
     *                      },
     *                       {
     *                           "id": 2,
     *                           "name": "Math Test",
     *                           "detail": "Some test detail",
     *                           "type": "Verbal",
     *                           "schedule": 0,
     *                           "status": 0,
     *                           "score": 0,
     *                           "due_date": "2018-12-08",
     *                           "subject_id": 1,
     *                           "batch_id": 1,
     *                           "deleted_at": null,
     *                           "created_at": "2018-04-27 08:15:57",
     *                           "updated_at": "2018-04-27 08:15:57"
     *                      }
     *           ]
     *        ]
     *      }
     *
     * @apiUse        Error
     */
    public function index(Request $request, PermissionsHelper $ph)
    {
        $assessment = AssessmentTest::query();
        if (Bouncer::can('view-all-test')) {
            if ($request->filled('batch_id')) {
                $assessment->where('batch_id', $request->batch_id);
            }
            if ($request->exists('status')) {
                $assessment->where('status', $request->status);
            }
        } elseif (Bouncer::can('view-test')) {
            $assessment->whereIn('batch_id', $ph->teacherBatch($request->batch_id));
            if ($request->exists('status')) {
                $assessment->where('status', $request->status);
            }
        } elseif (Auth::user()->isA('student', 'parent')) {
            $assessment->whereIn('batch_id', $ph->studentBatch($request->batch_id));
            if ($request->exists('status') && $request->status != 0) {
                $assessment->where('status', '!=', $request->status);
            } else {
                $assessment->where('status', '!=', 0);
            }
        } else {
            return Reply::unAuthorized();
        }


        if ($request->exists('name')) {
            $assessment->where('name', 'LIKE', '%' . $request->name . '%');
        }
        if ($request->exists('due_date')) {
            $assessment->where('due_date', $request->due_date);
        }
        if ($request->exists('type')) {
            $assessment->where('type', $request->type);
        }
        if ($request->exists('created_at')) {
            $assessment->where('created_at', $request->created_at);
        }

        if ($request->exists('schedule')) {
            $assessment->where('schedule', $request->schedule);
        }

        if ($request->exists('status')) {
            $assessment->where('schedule', $request->status);
        }

        if ($request->exists('score')) {
            $assessment->where('score', $request->score);
        }
        if ($request->exists('subject_id')) {
            $assessment->where('subject_id', $request->subject_id);
        }


        if (!$request->sort_column) {
            $request->sort_column = 'created_at';
            $request->sort_direction = 'desc';
        }
        if ($request->sort_direction === 'desc') {
            $assessment->orderByDesc($request->sort_column);
        } else {
            $assessment->orderBy($request->sort_column);
        }
        return response()->json($assessment->paginate($request->page_size));
    }


    /**
     * @api           {POST} /assessment/test Create Test
     * @apiPermission manage-all-test,manage-test,manage-subject-test
     * @apiName       CreateTest
     * @apiGroup      Assessments
     * @apiUse        Params
     * @apiUse        Success
     * @apiUse        Error
     */
    public function store(StoreAssessmentTest $request)
    {
        $assessment = new AssessmentTest;
        $assessment->name = $request->name;
        $assessment->detail = $request->detail;
        $assessment->status = $request->status;
        $assessment->due_date = $request->due_date;
        $assessment->score = $request->score;
        $assessment->schedule = $request->schedule;
        $assessment->subject_id = $request->subject_id;
        $assessment->class_id = $request->class_id;
        $assessment->batch_id = $request->batch_id;
        $assessment->type = $request->type;
        if ($assessment->save()) {
            if ($request->status > 0) {
                TestHelper::notify($assessment);
            }
            activity(config('app.tenant')->tenant_id)
                ->on($assessment)
                ->withProperties(['action' => 'create', 'section' => 'assessment'])
                ->log(__('messages.logAssessmentTestCreated'));

            return response()->json(Reply::success(__('messages.assessmentTestCreated'), $assessment));
        }

        return response()->json(Reply::error(__('messages.assessmentTestNotCreated')));


    }


    /**
     * @api           {GET} /assignment/test/:id Get Test with Score
     * @apiName       ViewAssessmentTestScore
     * @apiPermission view-all-test,view-test,(student/parent)
     * @apiGroup      Assessments
     * @apiUse        Success
     * @apiUse        Error
     */
    public function show($id, PermissionsHelper $ph)
    {
        $assessment = AssessmentTest::with('student_score',
            'student_score.student:id,first_name,middle_name,last_name,gender,picture_url')->findOrFail($id);
//        dd($ph->studentHasBatch($assessment->batch_id));
        if (Bouncer::can('view-all-test') ||
            (Bouncer::can('view-test') && $ph->hasBatch($assessment->batch_id)) ||
            ($ph->studentHasBatch($assessment->batch_id) && $assessment->status != 0)) {
            if (auth()->user()->isA('student', 'parent') && $assessment->status != 3) {
                $assessment = collect($assessment)->forget('student_score');
            } else {
                $numberFormatter = new \NumberFormatter('en_US', \NumberFormatter::ORDINAL);
                $score = collect($assessment->student_score)->sortByDesc('score')->keyBy('score')->keys()->toArray();
                $assessment->student_score = $assessment->student_score->map(function ($item) use ($score, $numberFormatter) {
                    $item['position'] = $numberFormatter->format(array_search($item['score'], $score) + 1);
                    return $item;
                });
                if (setting('show_all_results') == 0) {
                    if (Auth::user()->isA('student')) {
                        $student_score = $assessment->student_score->where('user_id', Auth::id());
                        $assessment = collect($assessment)->put('student_score', $student_score);
                    } elseif (Auth::user()->isA('parent')) {
                        $student_score = $assessment->student_score->whereIn('user_id', $ph->childrenIds());
                        $assessment = collect($assessment)->put('student_score', $student_score);
                    }
                }
            }
            return response()->json(Reply::success('', $assessment));
        }
        return Reply::unAuthorized();
    }

    /**
     * @api           {PUT|PATCH} /assessment/test/:id Update Test
     * @apiName       UpdateAssessmentTest
     * @apiGroup      Assessments
     * @apiPermission manage-all-test,manage-test,manage-subject-test
     * @apiParam {String{..256}} name Title/Name of Assessments
     * @apiParam {String{..1024}} Detail Detail of Assessment
     * @apiParam {Integer="0=draft,1=due,2=published,3=canceled"} status Assessment new status
     * @apiParam {Date} due_date Date for submission of assignment. Format = YYYY-MM-DD
     * @apiParam {Integer} score Score of assessment
     * @apiParam {Integer="0,1,2,3,4,5,6,7"} schedule Schedule type of assessment
     * @apiParam {String} type Assessment type defined by user e.g verbal, oral etc
     * @apiUse        Success
     * @apiUse        Error
     */
    public function update(UpdateAssessmentTest $request, $id)
    {
        $assessment = AssessmentTest::findOrFail($id);
        if ($request->fill_status) {
            $assessment->status = $request->status;
            if ($request->status > 0 && $request->notify) {
                $studentScore = null;
                if ($request->status == 3) {
                    $studentScore = $assessment->student_score;
                }
                $subjects = $assessment->subject;
                TestHelper::notify($assessment, $studentScore);
            }
        } else {
            $assessment->name = $request->name;
            $assessment->detail = $request->detail;
            $assessment->status = $request->status;
            $assessment->due_date = $request->due_date;
            $assessment->score = $request->score;
            $assessment->schedule = $request->schedule;
            $assessment->type = $request->type;
        }
        if ($assessment->save()) {
            activity(config('app.tenant')->tenant_id)
                ->on($assessment)
                ->withProperties(['action' => 'create', 'section' => 'assessment'])
                ->log(__('messages.logAssessmentTestUpdated'));

            return response()->json(Reply::success(__('messages.assessmentTestUpdated'), $assessment));
        }

        return response()->json(Reply::error(__('messages.assessmentTestNotUpdated')));
    }

    /**
     * @api           {DELETE} /assessment/test/:id Delete Test
     * @apiName       DeleteAssessmentTests
     * @apiPermission manage-all-test,manage-test,manage-subject-test
     * @apiGroup      Assessments
     * @apiUse        Success
     * @apiUse        Error
     */
    public function destroy($id, PermissionsHelper $ph)
    {
        $assessment = AssessmentTest::findOrFail($id);
        if (Bouncer::can('manage-all-test') ||
            (Bouncer::can('manage-test') && $ph->hasBatch($assessment->batch_id)) ||
            Bouncer::can('manage-subject-test') && $ph->hasSubject($assessment->subject_id)) {
            if ($assessment->delete()) {
                activity(config('app.tenant')->tenant_id)
                    ->on($assessment)
                    ->withProperties(['action' => 'create', 'section' => 'assessment'])
                    ->log(__('messages.logAssessmentTestDeleted'));

                return response()->json(Reply::success(__('messages.assessmentTestDeleted')));
            }
            return response()->json(Reply::error(__('messages.assessmentTestNotDeleted')));
        }
        return Reply::unAuthorized();
    }
}
