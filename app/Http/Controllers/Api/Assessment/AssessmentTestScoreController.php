<?php

namespace App\Http\Controllers\Api\Assessment;

use App\AssessmentTest;
use App\AssessmentTestScore;
use App\Helpers\Reply;
use App\Helpers\TestHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Assessment\StoreTestScore;
use App\Http\Requests\Assessment\UpdateTestScore;
use App\Notifications\ScoreUpdated;
use App\User;
use Illuminate\Support\Facades\Notification;


/**
 * @apiDefine Success
 * @apiSuccess {String} message Success Message
 * @apiSuccess {Boolean} status Returns True
 * @apiSuccess {Object[]} data Optionally data will be sent back
 *
 */

/**
 * @apiDefine Error
 * @apiError {String} message Error Message
 * @apiError {Boolean} status Returns False
 * @apiError {Object[]} data Optionally data will be sent back
 *
 */
class AssessmentTestScoreController extends Controller
{

    /**
     * @api           {POST} /assessment/test/score Store Test Score
     * @apiName       StoreAssessmentTestScore
     * @apiGroup      Assessments
     * @apiPermission manage-all-test,manage-test,manage-subject-test
     * @apiParam {Integer} assessment_id Id of assessment for which the score is being added, Assessment must not have status=3=cancelled
     * @apiParam {Boolean} [assessment_status]  True= publish results
     * @apiParam {Object[]}  students_score A nested array of students and their scores
     * @apiParam  {Integer}  students_score.user_id Id of student for whom score is added
     * @apiParam  {Integer}  students_score.score Score status of the student
     * @apiParamExample {json} Request-Example:
     *      {
     *           "status": true,
     *           "message": "",
     *           "data": {
     *               "id": 1,
     *               "name": "XYZ Test",
     *               "description": "Some assessment details",
     *               "type": "Verbal",
     *               "schedule": 7,
     *               "status": 0,
     *               "score": 200,
     *               "due_date": "2018-12-08",
     *               "subject_id": 1,
     *               "batch_id": 1,
     *               "deleted_at": null,
     *               "created_at": "2018-04-29 11:08:22",
     *               "updated_at": "2018-04-29 11:10:55",
     *               "student_score": [
     *                   {
     *                       "id": 1,
     *                       "assessment_test_id": 1,
     *                       "user_id": 13,
     *                       "score": 200
     *                   },
     *                   {
     *                       "id": 2,
     *                       "assessment_test_id": 1,
     *                       "user_id": 14,
     *                       "score": 1
     *                   },
     *                   {
     *                       "id": 3,
     *                       "assessment_test_id": 1,
     *                       "user_id": 15,
     *                       "score": 0
     *                   },
     *                   {
     *                       "id": 4,
     *                       "assessment_test_id": 1,
     *                       "user_id": 16,
     *                       "score": 1
     *                   },
     *                   {
     *                       "id": 5,
     *                       "assessment_test_id": 1,
     *                       "user_id": 17,
     *                       "score": 0
     *                   },
     *                   {
     *                       "id": 6,
     *                       "assessment_test_id": 1,
     *                       "user_id": 18,
     *                       "score": 1
     *                   }
     *               ]
     *           }
     *      }
     * @apiSuccess {String} message Success Message
     * @apiSuccess {Boolean} status Returns True
     * @apiUse        Error
     */
    public function store(StoreTestScore $request)
    {
        $assessment = AssessmentTest::findOrFail($request->assessment_id);
        $assessment->student_score()->createMany($request->students_score);
        if ($request->filled('status')) {
            $assessment->status = 3;
            $assessment->save();
            TestHelper::notify($assessment, $request->students_score);
        }
        activity(config('app.tenant')->tenant_id)
            ->on($assessment)
            ->withProperties(['action' => 'update', 'section' => 'assessment'])
            ->log(__('messages.logAssessmentTestUpdated'));

        return response()->json(Reply::success(__('messages.assessmentTestUpdated'), $assessment));

    }

    /**
     * @api           {PUT|PATCH} /assessment/test/score/:id Update Test Score
     * @apiName       UpdateAssessmentTestScore
     * @apiGroup      Assessments
     * @apiPermission manage-all-test,manage-test,manage-subject-test
     * @apiParam  {Integer}  score Updated Score for the specified assessment score record
     * @apiSuccess {String} message Success Message
     * @apiSuccess {Boolean} status Returns True
     * @apiUse        Error
     */
    public function update(UpdateTestScore $request, $id)
    {
        $assessmentTestScore = AssessmentTestScore::whereAssessmentTestId($id)->whereUserId($request->user_id)->first();
        $assessmentTestScore->score = $request->score;
        if ($assessmentTestScore->assessmentTest->status > 2) {
            $user = User::findOrFail($assessmentTestScore->user_id);
            if ($user->parent) {
                Notification::send($user->parent, new ScoreUpdated($assessmentTestScore->assessmentTest, $assessmentTestScore->score, $user));
            }
            $user->notify(new ScoreUpdated($assessmentTestScore->assessmentTest, $assessmentTestScore->score));
        }
        if ($assessmentTestScore->save()) {
            activity(config('app.tenant')->tenant_id)
                ->on($assessmentTestScore)
                ->withProperties(['action' => 'update', 'section' => 'assessment'])
                ->log(__('messages.logAssessmentTestUpdated'));

            return response()->json(Reply::success(__('messages.assessmentTestUpdated')));
        }
        return response()->json(Reply::error(__('messages.assessmentTestUpdated')));
    }

}
