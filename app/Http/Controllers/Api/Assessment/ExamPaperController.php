<?php

namespace App\Http\Controllers\Api\Assessment;

use App\AssessmentExam;
use App\ExamPaper;
use App\Helpers\PermissionsHelper;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Assessment\StoreExamPaper;
use App\Http\Requests\Assessment\UpdateExamPaper;
use Bouncer;
use Illuminate\Http\Request;

/**
 * @apiDefine Params
 * @apiParam {String{..256}} name Title/Name of Assessment
 * @apiParam {String{..1024}} [detail] Detail of assessment
 * @apiParam {Integer} subject_id Id of the subject for which paper is being added
 * @apiParam {Integer} score Score of the  paper
 * @apiParam {Integer} status status of the  paper
 * @apiParam {Integer} [type] Paper type i.e verbal, written etc
 * @apiParam {Integer} due_date Date on which paper will be taken
 */


/**
 * @apiDefine Success
 * @apiSuccess {String} message Success Message
 * @apiSuccess {Boolean} status Returns True
 * @apiSuccess {Object[]} data Optionally data will be sent back
 *
 * @apiSuccessExample {json}Success-Response:
 *      HTTP/1.1 200 OK
 *      {
 *           "status": true,
 *           "message": "Paper Successfully Created",
 *           "data": {
 *              "name": "Math Test",
 *              "detail": "Optional details here",
 *              "score": "100",
 *              "status": "2",
 *              "type": "Verbal",
 *              "due_date": "2018-12-8",
 *              "subject_id": "1",
 *              "assessment_exam_id": "1",
 *              "updated_at": "2018-04-30 10:03:55",
 *              "created_at": "2018-04-30 10:03:55",
 *              "id": 2
 *           }
 *       }
 *
 */

/**
 * @apiDefine Error
 * @apiError {String} message Error Message
 * @apiError {Boolean} status Returns False
 * @apiError {Object[]} data Optionally data will be sent back
 *
 */
class ExamPaperController extends Controller
{

    /**
     * @api           {GET} /assessment/exam/paper Get Papers
     * @apiName       ViewAssessmentExamsPapers
     * @apiGroup      Assessments
     * @apiPermission view-all-exam,view-exam
     * @apiParam {Integer} assessment_exam_id Id of the assessment exam whose papers are required
     * @apiParam {Integer} subject_id specified subject id within assessment exam
     *
     * @apiSuccess {String} message Success Message
     * @apiSuccess {Boolean} status Returns True
     * @apiSuccess {Object[]} data Requested data will be sent back if it exists
     * @apiSuccess {Number} data.id  Id of paper in database
     * @apiSuccess {String} data.name Name of Paper
     * @apiSuccess {String} data.detail Details of paper
     * @apiSuccess {String} data.type  Type of paper
     * @apiSuccess {Number} data.status Paper Status 0=draft,1=due,2=take,3=published,4=cancelled
     * @apiSuccess {Number} data.score  Score of paper
     * @apiSuccess {String} data.due_date Date string in YYYY-MM-DD format
     * @apiSuccess {Number} data.subject_id  Related Subject id
     * @apiSuccess {Number} data.assessment_exam_id Related assessment id
     * @apiSuccess {String} data.created_at Date of creation in YYYY-MM-DD format
     * @apiSuccess {String} data.updated_at Date of update in YYYY-MM-DD format
     * @apiSuccess {Object[]} data.student_score Object containing student score or empty if doesn't exist
     * @apiSuccess {Number} data.student_score.user_id Id of student in database
     * @apiSuccess {Number} data.student_score.score Marks scored by the student for this paper
     *
     * @apiSuccessExample {json}Success-Response:
     *      HTTP/1.1 200 OK
     *      {
     *           "status": true,
     *           "message": "",
     *           "data": [
     *               {
     *                   "id": 1,
     *                   "name": "Math Paper",
     *                   "detail": "Optional Details",
     *                   "type": "Written",
     *                   "status": 0,
     *                   "score": 0,
     *                   "due_date": "2018-12-08",
     *                   "subject_id": 1,
     *                   "assessment_exam_id": 1,
     *                   "created_at": "2018-04-30 10:02:47",
     *                   "updated_at": "2018-04-30 10:02:47",
     *                   "student_score": []
     *               },
     *               {
     *                    "id": 2,
     *                   "name": "Math Paper",
     *                   "detail": "Optional Details",
     *                   "type": "Verbal",
     *                   "status": 0,
     *                   "score": 0,
     *                   "due_date": "2018-12-08",
     *                   "subject_id": 1,
     *                   "assessment_exam_id": 1,
     *                   "parent_id": null,
     *                   "deleted_at": null,
     *                   "created_at": "2018-04-30 10:03:55",
     *                   "updated_at": "2018-04-30 10:03:55",
     *                   "student_score": []
     *               }
     *           ]
     *      }
     *
     * @apiUse        Error
     */
    public function index(Request $request, PermissionsHelper $ph)
    {
        $papers = ExamPaper::whereAssessmentExamId($request->assessment_exam_id);
        $assessment = AssessmentExam::findOrFail($request->assessment_exam_id);
        if (Bouncer::can('view-all-paper') ||
            (Bouncer::can('view-paper') && $ph->teacherBatch($assessment->batch_id))) {
            $papers = $papers->whereSubjectId($request->subject_id)
                ->with('student_score')->get();
            return response()->json(Reply::success('', $papers));
        } else {
            return Reply::unAuthorized();
        }
    }

    public function show($id, PermissionsHelper $ph)
    {
        $paper = ExamPaper::with('student_score')->findOrFail($id);
        $assessment = $paper->assessmentExam;
        if (Bouncer::can('view-all-paper') ||
            (Bouncer::can('view-paper') && $ph->teacherBatch($assessment->batch_id))) {
            return response()->json(Reply::success('', $paper));
        } else {
            return Reply::unAuthorized();
        }
    }


    /**
     * @api           {POST} /assessment/exam/paper Create Paper
     * @apiPermission manage-all-paper,manage-paper,manage-subject-paper
     * @apiName       CreateExamPaper
     * @apiGroup      Assessments
     * @apiUse        Params
     * @apiUse        Success
     * @apiUse        Error
     */
    public function store(StoreExamPaper $request)
    {
        $paper = new ExamPaper();
        $paper->name = $request->name;
        $paper->detail = $request->detail;
        $paper->score = $request->score;
        $paper->status = 0;
        $paper->type = $request->type;
        $paper->due_date = $request->due_date;
        $paper->subject_id = $request->subject_id;
        $paper->assessment_exam_id = $request->assessment_exam_id;
        return Reply::loggedResponse($paper->save(),
            $paper,
            'create',
            'assessment',
            'assessmentPaper');
    }


    /**
     * @api           {PUT|PATCH} /assessment/exam/score/:id Update Paper
     * @apiName       UpdateAssessmentExamPaper
     * @apiGroup      Assessments
     * @apiPermission manage-all-paper,manage-paper,manage-subject-paper
     * @apiParam {String{..256}} name Title/Name of Assessment
     * @apiParam {String{..1024}} [detail] Detail of assessment
     * @apiParam {Integer} score Score of the  paper
     * @apiParam {Integer} status status of the  paper
     * @apiParam {Integer} [type] Paper type i.e verbal, written etc
     * @apiParam {Integer} due_date Date on which paper will be taken
     * @apiUse        Success
     * @apiUse        Error
     */
    public function update(UpdateExamPaper $request, $id)
    {
        $paper = ExamPaper::findOrFail($id);
        $paper->name = $request->name;
        $paper->detail = $request->detail;
        $paper->score = $request->score;
        $paper->due_date = $request->due_date;
//        $paper->status = $request->status;
        $paper->type = $request->type;
        return Reply::loggedResponse($paper->save(),
            $paper,
            'update',
            'assessment',
            'assessmentPaper');
    }


    /**
     * @api           {DELETE} /assessment/exam/score/:id Delete Paper
     * @apiName       DeleteAssessmentExamPaper
     * @apiPermission manage-all-paper,manage-paper,manage-subject-paper
     * @apiGroup      Assessments
     * @apiUse        Success
     * @apiUse        Error
     */
    public function destroy($id, PermissionsHelper $ph)
    {
        $paper = ExamPaper::findOrFail($id);

        if (Bouncer::can('manage-all-paper') ||
            (Bouncer::can('manage-paper') && $ph->teacherClass($paper->assessmentExam->class_id)) ||
            (Bouncer::can('manage-subject-paper') && $ph->hasSubject($paper->subject_id))) {
            return Reply::loggedResponse($paper->delete(),
                $paper,
                'delete',
                'assessment',
                'assessmentPaper');
        }
        return Reply::unAuthorized();
    }
}
