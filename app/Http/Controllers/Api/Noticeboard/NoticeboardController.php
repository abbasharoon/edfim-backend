<?php

namespace App\Http\Controllers\Api\Noticeboard;

use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Noticeboard\StoreNotification;
use App\Notifications\NoticeboardNotification;
use Illuminate\Support\Facades\Notification;
use App\User;
use Illuminate\Http\Request;

class NoticeboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNotification $request)
    {
        $users = User::query();
        if ($request->filled('target_batches')) {
            if ($request->filled('target_users')) {
                if (array_search('staff', $request->target_users) !== false) {
                    $users = $users->orWhere(function ($query) use ($request) {
                        $query->whereHas('classStaff.batch', function ($q) use ($request) {
                            $q->whereIn('id', $request->target_batches);
                        });
                    });
                }
                if (array_search('student', $request->target_users) !== false) {
                    $users = User::orWhereIn('batch_id', $request->target_batches);
                }
                if (array_search('parent', $request->target_users) !== false) {
                    $users = User::orWhere(function ($query) use ($request) {
                        $query->whereHas('children', function ($q) use ($request) {
                            $q->whereIn('batch_id', $request->target_batches);
                        });
                    });
                }
            }
        }

        if ($request->filled('staff')) {
            $users = $users->orWhereIn('id', $request->staff);
        }
        if ($request->filled('parents')) {
            $users = $users->orWhereIn('id', $request->parents);
        }
        if ($request->filled('students')) {
            $users = $users->orWhereIn('id', $request->students);
        }

        Notification::send($users->with('notificationPreference')->get(), new NoticeboardNotification($request->message));

        activity(config('app.tenant')->tenant_id)
            ->withProperties(['action' => 'NotificationCreated', 'section' => 'Noticeboard'])
            ->log(__('messages.log' . 'noticeboardNotified'));
        return response()->json(Reply::success(__('messages.noticeboardNotified')));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
