<?php
/**
 * @file    ParentController.php
 *
 * ParentController Controller
 *
 * PHP Version 7
 *
 * @author  <Mainul Hasan> <hasanmbstu13@gmail.com>
 *
 */

namespace App\Http\Controllers\Api\User;

use App\Helpers\Helper;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Parent\StoreParent;
use App\Http\Requests\Parent\UpdateParent;
use App\NotificationPreference;
use App\Notifications\AccountCreated;
use App\User;
use App\UserData;
use Bouncer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ParentController extends Controller
{

    /**
     * @api            {POST} /parent Summary
     * @apiName        ParentAccountSummary
     * @apiGroup       Parent
     * @apiDescription Summary of parent account as list
     * @apiPermission  view-all-parent,view-parent
     *
     *
     * @apiParam {String} [name] Name is searched in first,middle,last and username
     * @apiParam {String} [email] Filter by Email of user
     * @apiParam {String} [phone_no] Filter by phone number
     * @apiParam {Number="1,2,3"} [gender] Filter by gender of user. 1= Male, 2 Female, 3= Other
     * @apiParam {String} [dob] Filter by date of birth
     * @apiParam {String} [address]Filter by address.*
     *
     *
     * @apiSuccess {Object[]} data List of Student Profiles
     * @apiSuccess {Number} data.id Id of user in database
     * @apiSuccess {String} data.first_name First name of user
     * @apiSuccess {String} data.middle_name Middle name of user
     * @apiSuccess {String} data.last_name Last name of user
     * @apiSuccess {String} data.username Username of user for login purpose
     * @apiSuccess {String} data.email Email of user
     * @apiSuccess {String} data.phone_no Phone Number of user
     * @apiSuccess {Number} data.children_count Count of children user have
     * @apiSuccess {Object} data.user_data Extra Data related to user
     * @apiSuccess {Number} data.user_data.address Address of user
     * @apiSuccess {String} data.user_data.picture_url Name of User profile Picture
     * @apiSuccess {Number} current_page Current Page number on pagination index
     * @apiSuccess {Number} from Start of number of received accounts
     * @apiSuccess {Number} to End of number of received accounts
     * @apiSuccess {Number} total Total Number of Accounts
     * @apiSuccess {Number} last_page Last page on pagination index
     * @apiSuccess {Number} per_page Number of records per page
     *
     * @apiSuccessExample {json} Success-Response:
     *  {
     *       "data": [
     *          "id": 6,
     *           "first_name": "Rozella",
     *           "middle_name": "Edgardo",
     *           "last_name": "Pinkie",
     *           "username": "kaleb.yundt",
     *           "email": "leola.paucek@example.org",
     *           "phone_no": "850-601-9155",
     *           "children_count": 2,
     *           "user_data": {
     *               "user_id": 6,
     *               "address": "7947 Schuster Pine Apt. 379\nAlfonsoton, NM 06487-2486",
     *               "picture_url": "https://lorempixel.com/640/480/?63854"
     *               }
     *           },
     *          {
     *           "id": 7,
     *           "first_name": "Kristian",
     *           "middle_name": "Mario",
     *           "last_name": "Leora",
     *           "username": "loyal51",
     *           "email": "earnestine.lehner@example.com",
     *           "phone_no": "759-703-6529",
     *           "children_count": 1,
     *           "user_data": {
     *               "user_id": 7,
     *               "address": "47598 Houston View\nLake Zacheryport, MS 14994-4742",
     *               "picture_url": "https://lorempixel.com/640/480/?91443"
     *               }
     *          },
     *       ],
     *       "current_page": 1,
     *       "last_page": 1,
     *       "per_page": 50,
     *       "from": 1,
     *       "to": 1,
     *       "total": 1
     *   }
     *
     * @apiError {json} Details of Parent account couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *     }
     *
     */
    public function index(Request $request)
    {
        if (Bouncer::cannot('view-all-parent') || Bouncer::cannot('view-parent')) {
            return Reply::unAuthorized();
        }
        $users = User::query();
        if ($request->filled('name')) {
            $users = $users->where(function ($query) use ($request) {
                $query->where('first_name', 'like', '%' . $request->name . '%');
                $query->orWhere('middle_name', 'like', '%' . $request->name . '%');
                $query->orWhere('last_name', 'like', '%' . $request->name . '%');
                $query->orWhere('username', 'like', '%' . $request->name . '%');
            });
        }
        if ($request->filled('email')) {
            $users = $users->where('email', 'like', '%' . $request->email . '%');
        }
        if ($request->filled('phone_no')) {
            $users = $users->where('phone_no', 'like', '%' . $request->phone_no . '%');
        }


        $users = $users->whereHas('user_data', function ($query) use ($request) {
            if ($request->filled('gender')) {
                $query->where('gender', $request->gender);
            }
            if ($request->filled('dob')) {
                $query->where('dob', $request->dob);
            }
            if ($request->filled('address')) {
                $query->where('address', 'like', '%' . $request->address . '%');
            }
        });

        $users = $users->whereHas('roles', function ($query) {
            $query->where('name', 'parent');
        });


        if (!$request->sort_column) {
            $request->sort_column = 'created_at';
            $request->sort_direction = 'desc';
        }
        if ($request->sort_direction === 'desc') {
            $users->orderByDesc($request->sort_column);
        } else {
            $users->orderBy($request->sort_column);
        }

        $users = $users->select(['id', 'first_name', 'middle_name', 'last_name', 'picture_url', 'username', 'email', 'phone_no'])
            ->withCount('children')
            ->paginate($request->page_size);
        return response()->json($users);

    }


    /**
     * @api            {POST} /parent Create
     * @apiName        Create parent Accounts
     * @apiGroup       Parent
     * @apiPermission  manage-all-parent
     *
     * @apiParam {String[]{..64}} first_name First name of user.
     * @apiParam {String[]{..64}} [middle_name] User Middle name.
     * @apiParam {String[]{..64}} last_name User Last name.
     * @apiParam {String[]} [username] Username is optional if email or phone number is filled
     * @apiParam {String[]} [email] Email of user. Optional if either username or phone number is filled
     * @apiParam {String[]} [phone_no] Phone number of student. Optional if either username or email is filled
     * @apiParam {String[]} [nic] National identity card/security number of user
     * @apiParam {String[]} [password] Password for user else default will beused
     * @apiParam {Number[]="1,2,3"} gender User Gender. 1= Male, 2 Female, 3= Other
     * @apiParam {String[]} [picture_data] Base64 encoded image data as profile photo
     * @apiParam {String[]} dob Date of birth of user
     * @apiParam {String[]} [address] User's postal address
     *
     * @apiParamExample {json} Request-Example:
     *   {
     *       "first_name":  ["Ali","Jim","Xin"]
     *       "middle_name": ["Jamel","James",""]
     *       "last_name":   ["Khan","Jordan","Chow"]
     *       "username":    ["alijameel5","",""]
     *       "email":       ["email1@test.com","email2@test.com",""],
     *       "phone_no":    ["","","+87 564 321 0"],
     *       "nic":   ["83kso83ns5","ksk38sk2","lsl3is3104"],
     *       "password":    ["passw123","pass@1234",""],
     *       "picture_data":["base64...","base64...",""],
     *       "gender":     ["1","2","2"],
     *       "dob":         ["2005-4-22","2001-03-04","2018-27-03"],
     *       "address":     ["xyz street","abc city",""],
     *   }
     *
     * @apiSuccess {json} Student successfully created details
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Students have been successfully created"
     *      }
     *
     * @apiError {json} Details of Students couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *       "errors" : [
     *          email.1: {
     *                 message: Email.1 already exists
     *      ]
     *     }
     *
     * @apiDescription Students Store
     */


    public function store(StoreParent $request)
    {
        try {
            foreach ($request['first_name'] as $key => $value) {
                $parent = new User;
                $parent->first_name = $value;
                $parent->middle_name = $request['middle_name'][$key];
                $parent->last_name = $request['last_name'][$key];
                $parent->gender = $request['gender'][$key];
                $parent->picture_url = Helper::store_image($request['picture_data'][$key]);
                $parent->username = (!empty($request['username'][$key])) ? $request['username'][$key] : $request['phone_no'][$key];
                $parent->email = $request['email'][$key];
                $parent->phone_no = $request['phone_no'][$key];
                $parent->password = (!empty($request['password'][$key])) ? Hash::make($request['password'][$key]) : Hash::make('a1b1c1d1');
                $parent->nic = $request['nic'][$key];
                if ($parent->save()) {
                    $parent->assign('parent');
                    $user_data = new UserData;
                    $user_data->dob = $request['dob'][$key];
                    $user_data->address = $request['address'][$key];
                    $user_data->extra_fields = $request->extra_fields[$key];
                    $parent->user_data()->save($user_data);

                    $np = new NotificationPreference();
                    $np->email = true;
                    $np->sms = true;
                    $np->web = true;
                    $np->app = true;
                    $parent->notificationPreference()->save($np);

                    $parent->notify(new AccountCreated((!empty($request['password'][$key])) ? $request['password'][$key] : 'a1b1c1d1', 'staff'));
                    activity(config('app.tenant')->tenant_id)
                        ->performedOn($parent)
                        ->withProperties(['action' => 'create', 'section' => 'parent', 'message' => count($request['first_name']) . ' new account created'])
                        ->log(__('messages.logParentCreated'));
                }
            }

            return response()->json(Reply::success(__('messages.bulkParentCreated')));

        } catch (\Exception $e) {
            return response()->json(Reply::error(__('messages.bulkParentNotCreated')));
        }

    }

    /**
     * @api            {GET} /parent/:id Get Single User
     * @apiName        Get Single User
     * @apiGroup       Parent
     * @apiPermission  view-all-parent,view-parent
     *
     * @apiSuccess {Object[]} data List of Student Profiles
     * @apiSuccess {Number} data.id Id of user in database
     * @apiSuccess {String} data.first_name First name of user
     * @apiSuccess {String} data.middle_name Middle name of user
     * @apiSuccess {String} data.last_name Last name of user
     * @apiSuccess {String} data.username Username of user for login purpose
     * @apiSuccess {String} data.email Email of user
     * @apiSuccess {String} data.phone_no Phone Number of user
     * @apiSuccess {String} data.nic National Identity Card/Security Number of user
     * @apiSuccess {Object} data.user_data Extra Data related to user
     * @apiSuccess {Number} data.user_data.gender Gender of User. 1=Male,2=Female,3=Other
     * @apiSuccess {String} data.user_data.dob Date of birth of user
     * @apiSuccess {String} data.user_data.address Address of user
     * @apiSuccess {String} data.user_data.picture_url Name of User profile Picture
     * @apiSuccess {Object[]} data.children Object containing details about user's children
     * @apiSuccess {String} data.children.id Id of user's child in database
     * @apiSuccess {String} data.children.first_name First name of user's child
     * @apiSuccess {String} data.children.middle_name Middle name of users's child
     * @apiSuccess {String} data.children.last_name Last name of user's child
     * @apiSuccess {String} data.children.user_data.picture_url Profile Picture of child
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     *   "data": [
     *       {
     *           "id": 3,
     *           "first_name": "Myra",
     *           "middle_name": "Jenifer",
     *           "last_name": "Kaia",
     *           "username": "roslyn52",
     *           "email": "noemy28@example.com",
     *           "phone_no": "228-309-5236 x20645",
     *           "parent_id": null,
     *           "nic": "113770871v",
     *           "user_data": {
     *               "user_id": 3,
     *               "gender": 1,
     *               "picture_url": "imagex.com",
     *               "dob": "2018-04-07",
     *               "address": "33175 Madilyn Mountains\nKovacekberg, CO 53319-6775"
     *           },
     *           "children": [
     *               {
     *                   "id": 15,
     *                   "first_name": "Abbas unknown",
     *                   "middle_name": null,
     *                   "last_name": "Haroon",
     *                   "parent_id": 3
     *                   "user_data": {
     *                      "user_id": 3,
     *                      "picture_url": "picure.png"
     *                 }
     *               }
     *           ]
     *       }
     *   ]
     *  }
     *
     * @apiError {json} Details of Student couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "Failed to reterive user. Please try again",
     *
     *     }
     */
    public function show($id)
    {

        $user = User::with('user_data:user_id,dob,address,extra_fields')
            ->with('children:id,first_name,picture_url,gender,middle_name,last_name,parent_id')
            ->where('id', $id)
            ->whereHas('roles', function ($query) {
                $query->where('name', 'parent');
            })->first(['id', 'first_name', 'middle_name', 'gender', 'picture_url', 'last_name', 'username', 'email', 'phone_no', 'parent_id', 'nic']);
        if (Bouncer::can('view-all-parent')
            || Bouncer::can('view-parent')
            || auth()->id() == $user->id
            || auth()->user()->parent_id == $user->id) {
            return response()->json(Reply::success('', $user));
        }
        return Reply::unAuthorized();
    }

    /**
     * @api            {PUT|PATCH} /parent/:id
     * @apiName        store Update parent data which given id
     * @apiGroup       Parent
     *
     * @apiParam {array} picture_data Parent Picture data.
     * @apiParam {url} profile_url Parent Profile url.
     * @apiParam {string} first_name Parent First Name.
     * @apiParam {string} last_name Parent Last Name.
     * @apiParam {string} middle_name Parent Middle Name.
     * @apiParam {string} [username] Parent Username.
     * @apiParam {string/number} password Parent Password.
     * @apiParam {string} email Parent Email.
     * @apiParam {number} phone_no Parent Phone no.
     * @apiParam {number} gender Parent Gender.
     * @apiParam {date} [dob] Parent dob.
     * @apiParam {string} [address] Parent Address.
     * @apiParam {string} [_method="put,patch"] Required if http put/patch isn't available
     *
     * @apiParamExample {json} Request-Example:
     *   {
     *       "token": "",
     *       "first_name":"test1Parent",
     *       "last_name":"test1Parent",
     *       "middle_name":"test1Parent",
     *       "email":"test1Parent@gmail.com",
     *       "phone_no":"1234567890",
     *       "password":"123456",
     *       "gender":1,
     *       "dob":"YYYY-MM-DD",
     *       "address":"test address",
     *   }
     *
     * @apiPermission  manage-all-parent
     * @apiSuccess {json} Parent successfully update details
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Parent has been successfully updated",
     *           "data": []
     *      }
     *
     * @apiError {json} Details of Fees couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription Parent Update
     */
    public function update(UpdateParent $request, $id)
    {
        $parent = User::findOrFail($id);
        if ($parent->isNotA('parent')) {
            return response()->json(Reply::error(__('messages.notParentUser')));
        }
        $parent->first_name = $request->first_name;
        $parent->middle_name = $request->middle_name;
        $parent->last_name = $request->last_name;
        $parent->username = $request->username;
        $parent->email = $request->email;
        $parent->phone_no = $request->phone_no;
        $parent->nic = $request->nic;
        $parent->gender = $request->gender;
        $parent->picture_url = Helper::store_image($request->picture_url, $parent->picture_url);
        $parent->user_data->dob = $request->dob;
        $parent->user_data->address = $request->address;

        if ($parent->push()) {
            activity(config('app.tenant')->tenant_id)
                ->on($parent)
                ->withProperties(['action' => 'update', 'section' => 'parent', 'resource' => $parent->toArray()])
                ->log(__('messages.logParentUpdated'));
            return response()->json(Reply::success(__('messages.parentUpdated')));
        }
        return response()->json(Reply::error(__('messages.parentNotUpdated')));
    }

    /**
     * @api            {delete} /parent/:id Delete
     * @apiName        Delete Parent
     * @apiGroup       Parent
     *
     * @apiParam {Number} Parent id
     *
     * @apiPermission  manage-all-parent
     * @apiSuccess {json} Gives details of Student which given by id
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Parent has been successfully deleted",
     *          "data": []
     *      }
     * @apiError {json} Details of Parent couldn't delete
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later"
     *     }
     * @apiDescription Delete parent details by given student id
     */
    public function destroy($id)
    {
        if (Bouncer::cannot('manage-all-parent')) {
            return Reply::unAuthorized();
        }
        $parent = User::findOrFail($id);
        if ($parent->isNotA('parent')) {
            return response()->json(Reply::error(__('messages.notParentUser')));
        }
        $parent->email = $parent->email . '-' . rand(10000, 10000000);
        $parent->phone_no = $parent->phone . '-' . rand(10000, 10000000);
        $parent->save();
        $parent->delete();

        activity(config('app.tenant')->tenant_id)
            ->performedOn($parent)
            ->withProperties(['action' => 'delete', 'section' => 'parent', 'message' => 'Account of' . $parent->first_name . ' ' .
                $parent->last_name . ' was deleted'])
            ->log(__('messages.logParentDeleted'));

        return response()->json(Reply::success(__('messages.parentDeleted')));
    }
}
