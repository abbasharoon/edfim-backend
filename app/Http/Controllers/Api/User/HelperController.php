<?php

namespace App\Http\Controllers\Api\User;

use App\BatchUserHistory;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\NotificationPreference;
use App\User;
use Bouncer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class HelperController extends Controller
{


    /**
     * @api            {POST} /user/edit Edit own Profile
     * @apiName        EditUserHelperMethods
     * @apiGroup       User Helpers
     * @apiParam {String} [email] New email Address
     * @apiParam {String} [phone]_no New phone_no
     * @apiParam {String} [username] New username
     * @apiParam {String} [old_password] Old password to be provided if password is changed
     * @apiParam {String} [new_password] New Password
     * @apiParam {String} [confirm_password] Confirmed password matchng the new password
     * @apiParam {String="512"} [qualification] User Qualification
     * @apiParam {String="512"} [biography] Biography of user
     * @apiParam {Boolean="true,false"} [email_notification] Set preference for email notification
     * @apiParam {Boolean="true,false"} [sms_notification] Set preference for email notification
     * @apiParam {Boolean="true,false"} [web_notification] Set preference for email notification
     * @apiParam {Boolean="true,false"} [app_notification] Set preference for email notification
     * @apiDescription Check if an email or phone number or username already exists in database
     */
    public function getNotificationSettings(Request $request)
    {
        if ($request->user_id) {
            if (!\Bouncer::can('manage-all-student')
                && !\Bouncer::can('manage-all-parent')
                && !\Bouncer::can('manage-all-staff')) {
                return Reply::unAuthorized();
            }
            $userId = $request->user_id;
        } else {
            $userId = Auth::id();
        };

        return Reply::success('', NotificationPreference::whereUserId($userId)->first());
    }


    /**
     * @api            {POST} /user/edit Edit own Profile
     * @apiName        EditUserHelperMethods
     * @apiGroup       User Helpers
     * @apiParam {String} [email] New email Address
     * @apiParam {String} [phone]_no New phone_no
     * @apiParam {String} [username] New username
     * @apiParam {String} [old_password] Old password to be provided if password is changed
     * @apiParam {String} [new_password] New Password
     * @apiParam {String} [confirm_password] Confirmed password matchng the new password
     * @apiParam {String="512"} [qualification] User Qualification
     * @apiParam {String="512"} [biography] Biography of user
     * @apiParam {Boolean="true,false"} [email_notification] Set preference for email notification
     * @apiParam {Boolean="true,false"} [sms_notification] Set preference for email notification
     * @apiParam {Boolean="true,false"} [web_notification] Set preference for email notification
     * @apiParam {Boolean="true,false"} [app_notification] Set preference for email notification
     * @apiDescription Check if an email or phone number or username already exists in database
     */
    public function editOwn(Request $request)
    {
        if ($request->user_id) {
            $user = User::findOrFail($request->user_id);
            if (!(($user->isA('parent') || $user->isA('student')) && \Bouncer::can('manage-all-student')) &&
                !(($user->isNotA('student') && $user->isNotA('parent')) && \Bouncer::can('manage-all-staff'))) {
                return Reply::unAuthorized();
            }
        } else {
            $user = User::find(auth()->id())->withTrashed();
        }
        if ($request->filled('email')) {
            $user->email = $request->email;
        }
        if ($request->filled('phone_no')) {
            $user->email = $request->phone_no;
        }
        if ($request->filled('username')) {
            $user->email = $request->username;
        }
        if ($request->filled('new_password')) {
            if ($request->filled('user_id') || Hash::check($request->old_password, $user->password)) {
                $user->password = Hash::make($request->new_password);
            } else {
                return Reply::error('Old password is incorrect');
            }
        }
        if ($request->filled('qualification')) {
            $user->user_data->qualification = $request->qualificaiton;
        }
        if ($request->filled('biography')) {
            $user->user_data->biography = $request->biography;
        }
        if ($request->filled('email_notification')) {
            $user->notificationPreference->email = $request->email_notification;
        }
        if ($request->filled('sms_notification')) {
            $user->notificationPreference->sms = $request->sms_notification;
        }
        if ($request->filled('web_notification')) {
            $user->notificationPreference->web = $request->web_notification;
        }
        if ($request->filled('app_notification')) {
            $user->notificationPreference->app = $request->app_notification;
        }
        if ($user->push()) {
            return Reply::success('', $user);
        }
    }


    /**
     * @api            {POST} /user/status Change account status
     * @apiName        EditUserHelperMSatus
     * @apiGroup       User Helpers
     * @apiParam {String="achieve,restore"} action Specify whether to restore accounts or achieve them
     * @apiParam {Number} [batch_id] Deactivate/Activate All student in a batch
     * @apiParam {Number[]} [user_id] An array of user account ids other than students to be activated/deactivated
     * @apiParam {Number} [student_id] Array of ids of students to be achieved/resorted
     * @apiDescription Change account status of users
     */
    public function deactivateUsers(Request $request)
    {
        $this->validate($request, [
            'batch_id' => ['integer',
                Rule::exists('batches', 'id')->where(function ($q) {
                    $q->whereTenantId(config('app.tenant')->tenant_id);
                })],
            'student_id' => 'array',
            'student_id.*' => ['integer', 'exists:users,id'],
        ]);
        $user = User::query();
        if ($request->filled('batch_id') && Bouncer::can('manage-all-student')) {
            $users = $user->whereBatchId($request->batch_id);
        } elseif ($request->filled('student_id') && Bouncer::can('manage-all-student')) {
            $users = $user->whereIn('id', $request->student_id)->whereHas('roles', function ($i) {
                $i->where('name', 'student');
            })->withTrashed();
        } else {
            return Reply::unAuthorized();
        }

        $userCount = $users->count();
        if ($request->action == "restore") {
            $totalStudents = User::whereHas('roles', function ($i) {
                $i->where('name', 'student');
            })->whereFrozen(false)->count();
            if (($userCount + $totalStudents) > setting('tenant.allowed_accounts')) {
                return response(['message' => 'You are exceeding your allowed quota of student accounts. Increase it now from the settings section']);
            }
            $users->update(['frozen' => false, 'deleted_at' => null]);
            return Reply::success('Accounts Successfully Restored.');
        } else {
            foreach ($users->get() as $user) {
                $history = new BatchUserHistory();
                $history->user_id = $user->id;
                $history->from_batch_id = $user->batch_id;
                $history->to_batch_id = null;
                $history->tenant_id = config('app.tenant')->tenant_id;
                $history->save();
            }
            $users->update(['frozen' => true, 'batch_id' => null]);
            return Reply::success('Selected accounts have been frozen');
        }
    }


    /**
     * @api            {GET} /user/exist Check Existence
     * @apiName        UserHelperMethods
     * @apiGroup       User Helpers
     * @apiPermission  manage-all-staff,manage-all-student,manage-all-parent
     * @apiSuccess {Boolean="true,false"} exists Returns true if already exists, false if doesn't exists
     * @apiDescription Check if an email or phone number or username already exists in database
     * @apiParam {String} [email] Check email existence
     * @apiParam {String} [phone_no] Check phone number existence
     * @apiParam {String} [username] Check username existence
     */
    public function checkUnique(Request $request)
    {
        if (Bouncer::can('manage-all-staff') ||
            Bouncer::can('manage-all-student') ||
            Bouncer::can('manage-all-parent')) {
            $user = User::query();
            $hasQuery = false;
            if ($request->filled('email')) {
                $hasQuery = true;
                $user = $user->where('email', $request->email)->whereNotNull('email');
            }
            if ($request->filled('phone_no')) {
                $hasQuery = true;
                $formattedNumber = str_replace(' ', '', $request->phone_no);
                $user = $user->where(function ($q) use ($request, $formattedNumber) {
                    $q->where('phone_no', $request->phone_no);
                    $q->orWhere('phone_no', $formattedNumber);
                })->whereNotNull('phone_no');
            }
            if ($request->filled('username')) {
                $hasQuery = true;

                $user = $user->where('username', $request->username)->whereNotNull('username');
            }
            if ($request->filled('user_id')) {
                $user = $user->where('id', '!=', $request->user_id);
            }
            if ($hasQuery) {
                return response()->json(['exists' => $user->withTrashed()->exists()]);
            }
            return response()->json(['exists' => false]);
        }
        return Reply::unAuthorized();
    }
}
