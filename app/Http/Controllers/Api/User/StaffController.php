<?php

namespace App\Http\Controllers\Api\User;

use App\Events\UserCreated;
use App\Helpers\Helper;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Staff\StoreStaff;
use App\Http\Requests\Staff\UpdateStaff;
use App\NotificationPreference;
use App\Notifications\AccountCreated;
use App\User;
use App\UserData;
use Bouncer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class StaffController extends Controller
{

    /**
     * @api            {POST} /staff Create
     * @apiName        CreateStaff
     * @apiGroup       Staff
     * @apiDescription Bulk Create Staff Accounts
     * @apiPermission  manage-all-staff
     * @apiParam {String[]} first_name Staff User First Name.
     * @apiParam {String[]} [middle_name] Staff User Middle Name.
     * @apiParam {String[]} last_name Staff User Last Name.
     * @apiParam {String[]} email Email of Staff User.
     * @apiParam {String[]} [username] Username for user login.
     * @apiParam {String[]} [password] Password for user account
     * @apiParam {String[]} phone_no Phone number of staff user.
     * @apiParam {String[]} [nic] National Identification Card Number/Security Number etc.
     * @apiParam {Number[]="1,2,3"} gender National Identification Card Number/Security Number etc.
     * @apiParam {String[]} [picture_data] Base64 encoded images string for profile picture.
     * @apiParam {String[]} [dob] Base64 encoded images string for profile picture. Format: YYYY-MM-DD
     * @apiParam {String[]{..500}} [qualification] Comma Separated list of Staff Qualifications
     * @apiParam {String[]{..500}} [bio_data] Bio Data of User
     * @apiParam {String[]{..64}} [job_title] Job Title of staff in the institution
     * @apiParam {Array[]} role_name Nested array with each array having roles name for each user
     *
     * @apiParamExample {json} Request-Example:
     *   {
     *       "first_name":  ["Harshad","Abbas","Valters"],
     *       "middle_name": ["","Haroon",""],
     *       "last_name":   ["test1Parent","test1Parent"],
     *       "email":       ["mail1@gmail.com","mail2@gmail.com","mail3@gmail.com"],
     *       "username":    ["username1",null,"username2"],
     *       "password":    ["123456","123456","123456"],
     *       "phone_no":    ["+92 336 9415618","+91 112 54234","+1 456 78843"],
     *       "nic":         ["14576 567383 74858 3","652-16-5043","ASD837B8394"],
     *       "gender":      [1,1,1],
     *       "picture_data":["base64..","","base64"],
     *       "dob":         ["1992-02-30","1991-03-28","1990-03-12"]
     *       "qualification":["M.A\,Bsc\,Phd","","BA"]
     *       "bio_data":["something etc","something else","else else"]
     *       "job_title":["Superintended","Controller",""]
     *       "role_name":[["staff-admin","teacher"],["finance-admin"],["Admin"]]
     *   }
     * @apiSuccess {Boolean} status Status of create operation
     * @apiSuccess {string} message Success Message
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Users have been successfully created"
     *      }
     *
     * @apiError {json} Json Object containing details about error. See example
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *        "errors": {
     *           "role_name.1.1": [
     *              "The selected role_name.1.1 is invalid."
     *           ]
     *       }
     *
     */

    public function store(StoreStaff $request)
    {

        try {
            foreach ($request['first_name'] as $key => $value) {
                $user = new User;
                $user->first_name = $value;
                $user->middle_name = $request['middle_name'][$key];
                $user->last_name = $request['last_name'][$key];
                $user->username = (!empty($request['username'][$key])) ? $request['username'][$key] : $request['phone_no'][$key];
                $user->gender = $request['gender'][$key];
                $user->picture_url = Helper::store_image($request['picture_data'][$key]);
                $user->nic = $request['nic'][$key];
                $user->email = $request['email'][$key];
                $user->password = (!empty($request['password'][$key])) ? Hash::make($request['password'][$key]) : Hash::make('a1b1c1d1');
                $user->phone_no = $request['phone_no'][$key];
                if ($user->save()) {
                    $user->assign($request['role'][$key]);
                    $user_data = new UserData;
                    $user_data->dob = $request['dob'][$key];
                    $user_data->qualification = $request['qualification'][$key];
                    $user_data->bio_data = $request['bio_data'][$key];
                    $user_data->job_title = $request['job_title'][$key];
                    $user_data->address = $request['address'][$key];
                    $user_data->extra_fields = $request->extra_fields[$key];
                    $user->user_data()->save($user_data);
                    $np = new NotificationPreference();
                    $np->email = true;
                    $np->sms = true;
                    $np->web = true;
                    $np->app = true;
                    $user->notificationPreference()->save($np);
                    event(new UserCreated($user,config('app.tenant')->tenant_id));
                    $user->notify(new AccountCreated((!empty($request['password'][$key])) ? $request['password'][$key] : 'a1b1c1d1', 'staff'));
                    activity(config('app.tenant')->tenant_id)
                        ->performedOn($user)
                        ->withProperties(['action' => 'create', 'section' => 'staff', 'message' => count($request->first_name) . ' new account added'])
                        ->log(__('messages.logStaffCreated'));
                }
            }

            return response()->json(Reply::success(__('messages.staffCreated')));
        } catch (\Exception $e) {
            dd($e);
            return response()->json(Reply::success(__('messages.staffNotCreated')));
        }

    }

    /**
     * @api            {GET} /staff Get List
     * @apiName        Get Staff Summary
     * @apiGroup       Staff
     * @apiPermission  view-all-staff
     * @apiSuccess {Object[]} data List of Staff Profiles
     * @apiSuccess {Number} data.id Id of user in database
     * @apiSuccess {String} data.first_name First name of user
     * @apiSuccess {String} data.middle_name Middle name of user
     * @apiSuccess {String} data.last_name Last name of user
     * @apiSuccess {String} data.username Username of user for login purpose
     * @apiSuccess {String} data.email Email of user
     * @apiSuccess {String} data.phone_number Phone Number of user
     * @apiSuccess {Object} data.user_data Extra Data related to user
     * @apiSuccess {Number} data.user_data.gender Gender of User. 1=Male,2=Female,3=Other
     * @apiSuccess {String} data.user_data.picture_url Name of User profile Picture
     * @apiSuccess {Object} data.roles List of roles assigned to user
     * @apiSuccess {Number} data.roles.id Id of role in database
     * @apiSuccess {String} data.roles.name Name of role for internal user
     * @apiSuccess {String} data.roles.title Name of role to be shown to user
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *                   [
     *                       {
     *                           "id": 2,
     *                           "first_name": "Abbas",
     *                           "middle_name": "",
     *                           "last_name": "Haroon",
     *                           "username": null,
     *                           "email": "test@example.org",
     *                           "phone_no": null,
     *                           "user_data": {
     *                                   "user_id": 2,
     *                                   "gender": 1,
     *                                   "picture_url": "exampleimage.png"
     *                               },
     *                           "roles": [
     *                                   {
     *                                       "id": 4,
     *                                       "name": "staff",
     *                                       "title": "Staff",
     *                                       "pivot": {
     *                                          "user_id": 2,
     *                                           "role_name": 4
     *                                       }
     *                                   }
     *                           ]
     *                       },
     *                       {
     *                           "id": 13,
     *                           "first_name": "Abbas",
     *                           "middle_name": null,
     *                           "last_name": "Haroon",
     *                           "username": "abbasharoon",
     *                           "email": "admin@mobspider.tk",
     *                           "phone_no": "0938311541",
     *                           "user_data": {
     *                                   "user_id": 13,
     *                                   "gender": 1,
     *                                   "picture_url": "exampleimage2.png"
     *                               },
     *                           "roles": [
     *                                   {
     *                                       "id": 4,
     *                                       "name": "staff",
     *                                       "title": "Staff",
     *                                       "pivot": {
     *                                           "user_id": 13,
     *                                           "role_name": 4
     *                                       }
     *                                   }
     *                           ]
     *                       }
     *                   ]
     * @apiDescription Get list of staff users as summary
     */
    public function index(Request $request)
    {
        if ($request->exists('allUsers') && Bouncer::can('view-all-users')) {
            $users = User::with('roles')->where('first_name', 'like', '%' . $request->name . '%')
                ->orwhere('middle_name', 'like', '%' . $request->name . '%')
                ->orwhere('last_name', 'like', '%' . $request->name . '%')->limit(30)->get();
            return response()->json($users);
        }
        if (Bouncer::cannot('view-all-staff')) {
            return Reply::unAuthorized();
        }
        if ($request->filled('list')) {
            $users = User::whereHas('roles', function ($query) {
                $query->where('name', '!=', 'parent');
                $query->where('name', '!=', 'student');
            })->get(['id', 'first_name', 'middle_name', 'last_name']);

        } else {
            $users = User::with('roles:id,name,title')
                ->whereHas('roles', function ($query) {
                    $query->where('name', '!=', 'parent');
                    $query->where('name', '!=', 'student');
                })->get(['id', 'first_name', 'middle_name', 'last_name', 'username', 'email', 'gender', 'picture_url', 'phone_no']);
        }

        return response()->json(['data' => $users]);
    }

    /**
     * @api             {GET} /staff/:id Get Single
     * @apiName         ViewStaff
     * @apiGroup        Staff
     * @apiDescription  Get Account Detail for single Staff Account
     * @apiDescription  Get single Staff
     * @apiSuccess {json} Returned Success Array
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *          {
     *              "id": 2,
     *              "first_name": "Abbas",
     *              "middle_name": "",
     *              "last_name": "Haroon",
     *              "username": null,
     *              "nic": "860443880v",
     *              "email": "test@example.org",
     *              "phone_no": null,
     *              "user_data": {
     *                  "user_id": 2,
     *                  "gender": 1,
     *                  "picture_url": "image.png",
     *                  "dob": "2018-04-04",
     *                  "qualification": "Commodi accusantium ut corporis id. Ut .",
     *                  "bio_data": "Aut soluta maiores omnis quis. Est sed aut .",
     *                  "job_title": "Police Detective",
     *                  "address": "44325 Hayes Meadows Suite 868\nHilllmouth, ID 81930",
     *                  "created_at": "2018-04-04 20:54:47",
     *                  "updated_at": "2018-04-04 20:54:47"
     *                  },
     *              "roles": [
     *                  {
     *                      "id": 4,
     *                      "name": "staff",
     *                      "title": "Staff",
     *                      "pivot": {
     *                          "user_id": 2,
     *                          "role_name": 4
     *                      }
     *                  }
     *              ]
     *       }
     * @apiSuccess {String} first_name Staff User First Name.
     * @apiSuccess {String} middle_name Staff User Middle Name.
     * @apiSuccess {String} last_name Staff User Last Name.
     * @apiSuccess {String} email Email of Staff User.
     * @apiSuccess {String} username Username for user login.
     * @apiSuccess {String} password Password for user account
     * @apiSuccess {String} nic National Identity Card/
     * @apiSuccess {String} phone_no Phone number of staff user.
     * @apiSuccess {Object} user_data Nested array with each array having roles ids for each user
     * @apiSuccess {Number} user_data.gender National Identification Card Number/Security Number etc.
     * @apiSuccess {String} user_data.picture_url Name of profile image file.
     * @apiSuccess {String} user_data.dob Date of birth of Staff user in mysql date time format
     * @apiSuccess {String} user_data.qualification Comma Separated list of Staff Qualifications
     * @apiSuccess {String} user_data.bio_data Bio Data of User
     * @apiSuccess {String} user_data.job_title Job Title of staff in the institution
     * @apiSuccess {String} user_data.address Postal Address of Staff User
     * @apiSuccess {String} user_data.created_at Date of user account creation
     * @apiSuccess {String} user_data.updated_at Date of user account deletion
     * @apiSuccess {Object} roles Nested array with each array having roles ids for each user
     * @apiSuccess {Array} roles.id Id of role in database
     * @apiSuccess {Array} roles.name Name of role for internal use
     * @apiSuccess {Array} roles.title Name of role to be displayed on front end
     *
     */
    public function show($id)
    {
        $user = User::with('user_data:user_id,dob,extra_fields,' .
            'qualification,bio_data,job_title,address,created_at,updated_at', 'roles:id,name,title')
            ->where('id', $id)
            ->whereDoesntHave('roles', function ($query) {
                $query->where('name', 'parent');
                $query->where('name', 'student');
            })
            ->first(['id', 'first_name', 'middle_name', 'last_name', 'username', 'email', 'gender', 'picture_url', 'nic', 'phone_no']);

        return response()->json(Reply::success('', $user));
    }

    /**
     * @api            {PUT|PATCH} /staff/:id Update
     * @apiName        store Update staff data which given id
     * @apiGroup       Staff
     * @apiPermission  manage-all-staff
     * @apiParam {String} first_name Staff User First Name.
     * @apiParam {String} [middle_name] Staff User Middle Name.
     * @apiParam {String} last_name Staff User Last Name.
     * @apiParam {String} email Email of Staff User.
     * @apiParam {String} username Username for user login.
     * @apiParam {String} phone_no Phone number of staff user.
     * @apiParam {String} [nic] National Identification Card Number/Security Number etc.
     * @apiParam {Number} gender National Identification Card Number/Security Number etc.
     * @apiParam {String} [picture_data] Base64 encoded images string for profile picture.
     * @apiParam {String} [dob] Base64 encoded images string for profile picture.
     * @apiParam {String{..500}} [qualification] Comma Separated list of Staff Qualifications
     * @apiParam {String{..500}} [bio_data] Bio Data of User
     * @apiParam {String{..64}} [job_title] Job Title of staff in the institution
     * @apiParam {String[]} role_name Array of role names for user
     *
     * @apiParamExample {json} Request-Example:
     *           [
     *                  "first_name": "Abbas",
     *                  "middle_name": "",
     *                  "last_name": "Haroon",
     *                  "username": null,
     *                  "email": "test@example.org",
     *                  "nic": "860443880v",
     *                  "phone_no": null,,
     *                  "gender": 1,
     *                  "picture_data": "base65...",
     *                  "dob": "2018-04-04",
     *                  "qualification": "Commodi accusantium ut corporis id. Ut .",
     *                  "bio_data": "Aut soluta maiores omnis quis. Est sed aut .",
     *                  "job_title": "Police Detective",
     *                  "address": "44325 Hayes Meadows Suite 868\nHilllmouth, ID 81930",
     *                  },
     *                  "role_name": [
     *                      "staff",
     *                      "teacher"
     *                  ]
     *          ]
     * @apiSuccess {json} Staff successfully created details
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "User has benn successfully updated",
     *           "data": []
     *      }
     *
     * @apiError {json} Details of Staff couldn't save
     * @apiErrorExample {
     * json} Error - Response:
     *     HTTP / 1.1 404 Not Found
     *     {
     *
     * "status": false,
     *       "message" : "There has a error. Please try again later",
     *        "errors": {
     *
     * "role_name": [
     *              "The selected role_name is invalid."
     *           ]
     *       }
     *
     *     }
     *
     * @apiDescription Staff Update
     */
    public function update($id, UpdateStaff $request)
    {
        $user = User::findOrFail($id);
        if ($user->isA('student', 'parent')) {
            return response()->json(Reply::error(__('messages.notParentUser')));
        } elseif ($user->isA('system-admin') && auth()->user()->isNotA('system-admin')) {
            return Reply::unAuthorized();
        }
//        @todo Change the following code to sync
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->middle_name = $request->middle_name;
        $user->gender = $request->gender;
        $user->picture_url = $request->filled('picture_data') ? Helper::store_image($request->picture_data, $user->picture_url) : $user->picture_url;
        $user->email = $request->email;
        $user->username = (!empty($request->username)) ? $request->username : $request->phone_no;
        $user->phone_no = $request->phone_no;
        $user->nic = $request->nic;
//        $user_data = UserData::where('user_id', $id)->firstOrFail();

        $user->user_data->dob = $request->dob;
        $user->user_data->qualification = $request->qualification;
        $user->user_data->bio_data = $request->bio_data;
        $user->user_data->job_title = $request->job_title;
        $user->user_data->address = $request->address;

//        Todo: Use sync if possible
//        Bouncer::sync(User::find($id))->roles(['system-admin', 'staff-admin']);
        $user->roles()->detach();
        $user->assign($request->role_id);
        if ($user->push()) {
            activity(config('app.tenant')->tenant_id)
                ->performedOn($user)
                ->withProperties(['action' => 'update', 'section' => 'staff', 'resource' => $user->toArray()])
                ->log(__('messages.logStaffUpdated'));

            return response()->json(Reply::success(__('messages.staffUpdated')));
        }
        return response()->json(Reply::error(__('messages.staffNotUpdated')));
    }

    /**
     * @api            {delete} /staff/:id Delete
     * @apiName        destroy
     * @apiGroup       Staff
     * @apiPermission  manage-all-staff
     * @apiSuccess {json} Gives details of Staff which given by id
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "User has been successfully deleted",
     *          "data": []
     *      }
     * @apiError {json} Json Object containing details of error
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later"
     *     }
     * @apiDescription Delete Staff Delete by given staff id
     */
    public function destroy($id)
    {
        if (Bouncer::cannot('manage-all-staff')) {
            return Reply::unAuthorized();
        }
        $user = User::findOrFail($id);
        if ($user->isAll('parent', 'student')) {
            return response()->json(Reply::error(__('messages.notParentUser')));
        }
        if ($user->isA('system-admin') &&
            (auth()->user()->isNotA('system-admin') || auth()->id() === $user->id)) {
            return Reply::unAuthorized();
        }
        $user->email = $user->email . '-' . rand(10000, 10000000);
        $user->phone_no = $user->phone . '-' . rand(10000, 10000000);
        $user->save();
        $user->delete();
        activity(config('app.tenant')->tenant_id)
            ->on($user)
            ->withProperties(['action' => 'delete', 'section' => 'staff', 'message' => 'Account of' . $user->first_name . ' ' . ' was deleted'])
            ->log(__('messages.logStaffDeleted'));

        return response()->json(Reply::success(__('messages.staffDeleted')));

    }
}
