<?php
/**
 * @file    RoleController.php
 *
 * RoleController Controller
 *
 * PHP Version 7
 *
 * @author  <Sandun Dissanayake> <java2012@gmail.com>
 *
 */

namespace App\Http\Controllers\Api\User;

use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{


    /**
     * @api            {GET} /role Get all roles with permissions
     * @apiName        index Get roles
     * @apiGroup       Role
     * @apiPermission  Only admin users
     * @apiSuccess {Object[]} data An object containing arrays of permissions
     * @apiSuccess {Number} data.id Id of role in database
     * @apiSuccess {String} data.name Name of role for internal use for checking role existence for a user
     * @apiSuccess {String} data.title Name of role to be displayed to user wherever required
     * @apiSuccess {Object[]} data.permissions Object of arrays containing details of permissions associated with a role
     * @apiSuccess {Number} data.permissions.id Id of role in database
     * @apiSuccess {String} data.permissions.name Name of role for internal use for checking role existence for a user
     * @apiSuccess {String} data.permissions.display_name Name of role to be displayed to user wherever required
     * @apiSuccess {String} data.permissions.description Description of role to be shown to user wherever required
     * @apiSuccessExample {json} Success response without permissions:
     *     HTTP/1.1 200 OK
     *       {
     *              "status": true,
     *              "message": "",
     *               "data": [
     *                   {
     *                       "id": 4,
     *                       "name": "staff",
     *                       "display_name": "Staff",
     *                       "description": "Staff member"
     *                   }
     *               ]
     *         ]
     * @apiSuccessExample {json} Success response with permissions:
     *     HTTP/1.1 200 OK
     *       {
     *              "status": true,
     *              "message": "",
     *               "data": [
     *                   {
     *                       "id": 4,
     *                       "name": "staff",
     *                       "display_name": "Staff",
     *                       "description": "Staff member"
     *                       "permissions": [
     *                              ]
     *                   }
     *               ]
     *         ]
     *
     *
     * @apiDescription Get list of role
     */
    public function index(Request $request)
    {
        if ($request->exists('withPermissions')) {
            $roles = Role::whereNotIn('name', ['parent', 'student'])
                ->where('scope', config('app.tenant')->tenant_id)
                ->with('permissions')->get();
        } else {
            $roles = Role::whereNotIn('name', ['parent', 'student'])
                ->where('scope', config('app.tenant')->tenant_id)
                ->get();
        }
        return response()->json(Reply::success('', $roles));
    }
}
