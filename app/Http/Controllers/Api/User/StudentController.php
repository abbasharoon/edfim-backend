<?php
/**
 * @file         StudentController.php
 *
 * StudentController Controller
 *
 * PHP Version 7
 *
 * @author       <Sandun Dissanayake> <java2012@gmail.com>
 * @modified_by  <Mainul Hasan> <hasanmbstu13@gmail.com>
 *
 *
 */

namespace App\Http\Controllers\Api\User;

use App\Helpers\Helper;
use App\Helpers\PermissionsHelper;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Student\StoreStudent;
use App\Http\Requests\Student\UpdateStudent;
use App\NotificationPreference;
use App\Notifications\AccountCreated;
use App\User;
use App\UserData;
use Bouncer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class StudentController extends Controller
{


    /**
     * @api            {GET} /student Summary
     * @apiName        GetStudentsSummary
     * @apiGroup       Student
     * @apiDescription Get list of students
     * @apiPermission  view-all-student,view-student
     *
     *
     * @apiParam {String} [name] Name is searched in first,middle,last and username
     * @apiParam {String} [email] Filter by Email of user
     * @apiParam {String} [phone_no] Filter by phone number
     * @apiParam {Number} batch_id  Filter by assigned batch id
     * @apiParam {Number} parent_id   Filter by parent/guardian of user
     * @apiParam {Number="1,2,3"} gender Filter by gender of user. 1= Male, 2 Female, 3= Other
     * @apiParam {String} dob Filter by date of birth
     * @apiParam {String} roll_no Filter by roll number of user.*
     * @apiParam {Boolean} withFrozen Load Frozen Account
     * @apiParam {Booleean} withTrashed Load Archieved Accounts
     *
     *
     * @apiSuccess {Object[]} data List of Student Profiles
     * @apiSuccess {Number} data.id Id of user in database
     * @apiSuccess {String} data.first_name First name of user
     * @apiSuccess {String} data.middle_name Middle name of user
     * @apiSuccess {String} data.last_name Last name of user
     * @apiSuccess {String} data.username Username of user for login purpose
     * @apiSuccess {String} data.email Email of user
     * @apiSuccess {String} data.phone_no Phone Number of user
     * @apiSuccess {String} data.batch_id Assigned Batch for the user
     * @apiSuccess {String} data.roll_no Roll Number of student
     * @apiSuccess {Object} data.user_data Extra Data related to user
     * @apiSuccess {String} data.user_data.picture_url Name of User profile Picture
     * @apiSuccess {Object[]} data.batch  Object containing batch data of student
     * @apiSuccess {Number} data.batch.id Batch id in database
     * @apiSuccess {String} data.batch.name  Batch name
     * @apiSuccess {Object[]} data.batch.class  Batch Class details
     * @apiSuccess {Number} data.batch.class.id  Id of class in database
     * @apiSuccess {String} data.batch.class.name  Name of class
     * @apiSuccess {Object[]} data.parent Details of parents
     * @apiSuccess {Number} data.parent.id Id of parent in database
     * @apiSuccess {String} data.parent.first_name Parent's first name
     * @apiSuccess {String} data.parent.middle_name Parent's middle name
     * @apiSuccess {String} data.parent.last_name Parent's last name
     * @apiSuccess {Number} current_page Current Page number on pagination index
     * @apiSuccess {Number} from Start of number of received accounts
     * @apiSuccess {Number} to End of number of received accounts
     * @apiSuccess {Number} total Total Number of Accounts
     * @apiSuccess {Number} last_page Last page on pagination index
     * @apiSuccess {Number} per_page Number of records per page
     *
     * @apiSuccessExample {json} Success-Response:
     *  {
     *       "data": [
     *                   {
     *                       "id": 11,
     *                       "roll_no": null,
     *                       "first_name": "Claude",
     *                       "middle_name": "June",
     *                       "last_name": "Katelyn",
     *                       "username": "vandervort.megane",
     *                       "email": "fvolkman@example.org",
     *                       "phone_no": "(759) 864-9208 x21424",
     *                       "batch_id": 1,
     *                       "parent_id": 10,
     *                       "user_data": {
     *                           "user_id": 11,
     *                           "picture_url": "image.png"
     *                       },
     *                       "batch": {
     *                           "id": 1,
     *                           "name": "Batch 2012",
     *                           "class_id": 1,
     *                           "class": {
     *                               "id": 1,
     *                               "name": "Grade 1",
     *                           }
     *                       },
     *                       "parent": {
     *                           "id": 10,
     *                           "first_name": "Abbas",
     *                           "middle_name": "",
     *                           "last_name": "Haroon"
     *                       }
     *                   },
     *       ],
     *       "current_page": 1,
     *       "last_page": 1,
     *       "per_page": 50,
     *       "from": 1,
     *       "to": 1,
     *       "total": 1
     *   }
     *
     * @apiError {json} Details of Students couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *     }
     *
     */
    public function index(Request $request, PermissionsHelper $ph)
    {
        $users = User::query();
        $pagination = 50;

        $users = $users->whereHas('roles', function ($query) {
            $query->where('name', 'student');
        });

        if ($request->filled('name')) {
            $users = $users->where(function ($query) use ($request) {
                $query->where('first_name', 'like', '%' . $request->name . '%');
                $query->orWhere('middle_name', 'like', '%' . $request->name . '%');
                $query->orWhere('last_name', 'like', '%' . $request->name . '%');
                $query->orWhere('username', 'like', '%' . $request->name . '%');
            });
            if (Bouncer::can('view-all-student') && $request->autoComplete) {
                return response()->json(['data' => $users->with('parent:id,first_name,middle_name,last_name,gender,picture_url')->limit(500)->get()]);
            }
        }
        if ($request->filled('email')) {
            $users = $users->where('email', 'like', '%' . $request->email . '%');
        }
        if ($request->filled('phone_no')) {
            $users = $users->where('phone_no', 'like', '%' . $request->phone_no . '%');
        }
        if ($request->filled('batch_id')) {
            $users = $users->where('batch_id', $request->batch_id);
            $request->page_size = 500;
        }
        if ($request->filled('parent_id')) {
            $users = $users->where('parent_id', $request->parent_id);
        }


        $users = $users->whereHas('user_data', function ($query) use ($request) {
            if ($request->filled('gender')) {
                $query->where('gender', $request->gender);
            }
            if ($request->filled('dob')) {
                $query->where('dob', $request->dob);
            }
            if ($request->filled('roll_no')) {
                $query->where('roll_no', $request->roll_no);
            }
        });


        if (!$request->filled('withFrozen')) {
            $users = $users->where('frozen', 0);
        }
        if ($request->filled('withTrashed')) {
            $users = $users->withTrashed();
        }


        if (!$request->sort_column || $request->sort_column == 'undefined') {
            $request->sort_column = 'created_at';
            $request->sort_direction = 'desc';
        }

        if ($request->sort_column === 'roll_no') {

            $users->orderByRaw('LENGTH(roll_no)', $request->sort_direction);
        }

        if ($request->sort_direction === 'desc') {
            $users->orderByDesc($request->sort_column);
        } else {
            $users->orderBy($request->sort_column);
        }

        if (Bouncer::can('view-all-student')) {
            $users = $users->with('batch.class',
                'parent:id,first_name,middle_name,last_name');
            $users = $users->select(['id', 'roll_no', 'frozen', 'deleted_at', 'first_name', 'middle_name', 'last_name', 'username', 'email', 'phone_no', 'picture_url', 'gender', 'batch_id', 'parent_id'])
                ->paginate($request->page_size);
            return response()->json($users);
        } elseif ((Bouncer::can('view-student'))) {
            $users = $users->with('batch.class',
                'parent:id,first_name,middle_name,last_name')
                ->whereIn('batch_id', $ph->teacherBatch($request->batch_id));
            $users = $users->select(['id', 'roll_no', 'first_name', 'middle_name', 'last_name', 'picture_url', 'gender', 'batch_id'])
                ->paginate($request->page_size);
            return response()->json($users);
        }
        return Reply::unAuthorized();
    }

    /**
     * @api            {POST} /student Create
     * @apiName        Create Student Accounts
     * @apiGroup       Student
     * @apiPermission  manage-all-student
     *
     * @apiParam {String[]{..64}} first_name First name of student.
     * @apiParam {String[]{..64}} [middle_name] Student Middle name.
     * @apiParam {String[]{..64}} last_name Student Last name.
     * @apiParam {String[]} [username] Username is optional if email or phone number is filled
     * @apiParam {String[]} [email] Email of user. Optional if either username or phone number is filled
     * @apiParam {String[]} [phone_no] Phone number of student. Optional if either username or email is filled
     * @apiParam {Number[]} parent_id Id of Parent's account to associate with the student
     * @apiParam {Number[]} batch_id Id of the batch assigned to student
     * @apiParam {String[]} [password] Password for account. Missing password will be filled with default 123456
     * @apiParam {Number[]="1,2,3"} gender Student Gender. 1= Male, 2 Female, 3= Other
     * @apiParam {String[]} [picture_data] Base64 encoded image data as profile photo
     * @apiParam {String[]} roll_no roll number of student in class/batch
     * @apiParam {String[]} dob Date of birth of student
     * @apiParam {String[]} [address] Student's postal address
     *
     * @apiParamExample {json} Request-Example:
     *   {
     *       "first_name":  ["Ali","Jim","Xin"]
     *       "middle_name": ["Jamel","James",""]
     *       "last_name":   ["Khan","Jordan","Chow"]
     *       "username":    ["alijameel5","",""]
     *       "email":       ["email1@test.com","email2@test.com",""],
     *       "phone_no":    ["","","+87 564 321 0"],
     *       "parent_id":   ["5","2","104"],
     *       "batch_id":  ["4","9","3"],
     *       "password":    ["passw123","pass@1234",""],
     *       "gender":    ["1","1","2"],
     *       "picture_data":["base64...","base64...",""],
     *       "roll_no":     ["3","52","20"],
     *       "dob":         ["2005-4-22","2001-03-04","2018-27-03"],
     *       "address":     ["xyz street","abc city",""],
     *   }
     *
     * @apiSuccess {json} Student successfully created details
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Students have been successfully created"
     *      }
     *
     * @apiError {json} Details of Students couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *       "errors" : [
     *          email.1: {
     *                 message: Email.1 already exists
     *      ]
     *     }
     *
     * @apiDescription Students Store
     */
    public function store(StoreStudent $request)
    {
        $totalStudents = User::whereHas('roles', function ($i) {
            $i->where('name', 'student');
        })->whereFrozen(false)->count();
        if ((count($request->first_name) + $totalStudents) > setting('tenant.allowed_accounts')) {
            return Reply::error('You are exceeding your allowed quota of student accounts. Increase it now from the settings section');
        }
        foreach ($request->first_name as $key => $value) {
            $student = new User;

            $student->first_name = $request->first_name[$key];
            $student->middle_name = $request->middle_name[$key];
            $student->last_name = $request->last_name[$key];
            $student->gender = $request->gender[$key];
            $student->picture_url = Helper::store_image($request->picture_data[$key]);
            $student->username = $request->username[$key];
            $student->email = $request->email[$key];
            $student->phone_no = $request->phone_no[$key];
            $student->roll_no = $request->roll_no[$key];
            $student->batch_id = $request->batch_id[$key];
            $student->parent_id = $request->parent_id[$key];
            $student->password = (!empty($request->password[$key])) ? Hash::make($request->password[$key]) : Hash::make('a1b1c1d1');
            if (!empty($request->doa[$key])) {
                $student->created_at = $request->doa[$key];
            }

            if ($student->save()) {
                $student->assign('student');
                $user_data = new UserData;
                $user_data->dob = $request->dob[$key];
                $user_data->address = $request->address[$key];
                $user_data->extra_fields = $request->extra_fields[$key];
                $student->user_data()->save($user_data);
                $np = new NotificationPreference();
                $np->email = true;
                $np->sms = true;
                $np->web = true;
                $np->app = true;
                $student->notificationPreference()->save($np);
                $student->notify(new AccountCreated((!empty($request['password'][$key])) ? $request['password'][$key] : 'a1b1c1d1', 'staff'));
                activity(config('app.tenant')->tenant_id)
                    ->performedOn($student)
                    ->withProperties(['action' => 'create', 'section' => 'student'])
                    ->log(__('messages.logStudentCreated'));
            }
        }
        return response()->json(Reply::success(__('messages.bulkStudentCreated')));

    }

    /**
     * @api            {GET} /student/:id Get single user
     * @apiName        Get Single User
     * @apiGroup       Student
     * @apiPermission  view-all-student,view-student
     *
     * @apiSuccess {Object[]} data List of Student Profiles
     * @apiSuccess {Number} data.id Id of user in database
     * @apiSuccess {String} data.first_name First name of user
     * @apiSuccess {String} data.middle_name Middle name of user
     * @apiSuccess {String} data.last_name Last name of user
     * @apiSuccess {String} data.username Username of user for login purpose
     * @apiSuccess {String} data.email Email of user
     * @apiSuccess {String} data.phone_no Phone Number of user
     * @apiSuccess {String} data.roll_no roll number of user
     * @apiSuccess {String} data.batch_id Assigned Batch for the user
     * @apiSuccess {String} data.parent_id Associated parent id of user
     * @apiSuccess {Object} data.user_data Extra Data related to user
     * @apiSuccess {Number} data.user_data.gender Gender of User. 1=Male,2=Female,3=Other
     * @apiSuccess {String} data.user_data.dob Date of birth of user
     * @apiSuccess {String} data.user_data.address Address of user
     * @apiSuccess {String} data.user_data.picture_url Name of User profile Picture
     * @apiSuccess {Object[]} data.parent Object containing details about user parent
     * @apiSuccess {String} data.parent.id Id of user's parent in database
     * @apiSuccess {String} data.parent.first_name First name of user's parent
     * @apiSuccess {String} data.parent.middle_name Middle name of users's parent
     * @apiSuccess {String} data.parent.last_name Last name of user's parent
     * @apiSuccess {Object[]} data.batch Object containing details about user batch/class
     * @apiSuccess {String} data.batch.id Id of batch in database
     * @apiSuccess {String} data.batch.name Name of batch
     * @apiSuccess {Object[]} data.batch.class Object containing class detials
     * @apiSuccess {String} data.class.id Id of batch in database
     * @apiSuccess {String} data.class.name Name of batch
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     *   "data": [
     *       {
     *           "id": 13,
     *           "first_name": "George",
     *           "middle_name": "Lou",
     *           "last_name": "Carolyn",
     *           "username": "olson.mireille",
     *           "email": "america.goodwin@example.net",
     *           "phone_no": "331.773.0831 x552",
     *           "roll_no": null,
     *           "parent_id": 5,
     *           "batch_id": 1,
     *           "user_data": {
     *               "user_id": 13,
     *               "gender": 2,
     *               "picture_url": "https://lorempixel.com/640/480/?60083",
     *               "dob": "2018-05-24",
     *               "address": "287 Gilda Points\nCarleyborough, NV 24154"
     *           },
     *           "parent": {
     *               "id": 5,
     *               "first_name": "Abbas",
     *               "middle_name": "",
     *               "last_name": "Haroon"
     *           },
     *           "batch": {
     *               "id": 1,
     *               "name": "Batch 2012",
     *               "class": {
     *                   "id": 1,
     *                   "name": "Grade 1",
     *               }
     *           }
     *       }
     *   ]
     *}
     *
     * @apiError {json} Details of Student couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "Failed to reterive user. Please try again",
     *
     *     }
     *
     */
    public function show($id, PermissionsHelper $ph)
    {

        $user = User::with('user_data:user_id,dob,address,extra_fields')
            ->with('parent:id,first_name,middle_name,last_name', 'batch.class', 'batch.batchRegisters')
            ->whereHas('roles', function ($query) {
                $query->where('name', 'student');
            })->find($id);
        if (Bouncer::can('view-all-student') ||
            (Bouncer::can('view-student') && $ph->teacherBatch($user->batch_id)) ||
            $user->id === auth()->id() ||
            $user->parent_id == auth()->id()) {
            return response()->json(['data' => $user]);
        }
        return Reply::unAuthorized();
    }

    /**
     * @api            {PUT|PATCH} /student/:id Update
     * @apiName        Update Student Account
     * @apiGroup       Student
     *
     * @apiParam {string} picture_data Student Picture data.
     * @apiParam {url} profile_url Student Profile url.
     * @apiParam {string} first_name Student First Name.
     * @apiParam {string} last_name Student Last Name.
     * @apiParam {string} middle_name Student Middle Name.
     * @apiParam {string} [username] Student Username.
     * @apiParam {string/number} password Student Password.
     * @apiParam {string} email Student Email.
     * @apiParam {number} phone_no Student Phone no.
     * @apiParam {number} gender Student Gender.
     * @apiParam {date} [dob] Student dob.
     * @apiParam {number} parent_id Student Parent id.
     * @apiParam {string} address Student Address.
     *
     * @apiParamExample {json} Request-Example:
     *   {
     *       "token": "",
     *       "first_name":"test1Student",
     *       "last_name":"test1Student",
     *       "middle_name":"test1Student",
     *       "username":"test1Student",
     *       "password":"123456",
     *       "email":"test1Student@gmail.com",
     *       "phone_no":"1234567890",
     *       "gender":1,
     *       "dob":"YYYY-MM-DD",
     *       "parent_id":1,
     *       "address":"test address",
     *   }
     *
     * @apiPermission  manage-all-student
     * @apiSuccess {json} Student successfully update details
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Student has been successfully updated",
     *           "data": []
     *      }
     *
     * @apiError {json} Details of Student couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription Student Update
     */
    public function update(UpdateStudent $request, $id)
    {
        $student = User::findOrFail($id);
        if ($student->isNotA('student')) {
            return response()->json(Reply::error(__('messages.notStudentUser')));
        }
        $student->first_name = $request->first_name;
        $student->last_name = $request->last_name;
        $student->middle_name = $request->middle_name;
        $student->gender = $request->gender;
        $student->picture_url = Helper::store_image($request->picture_data, $student->picture_url);
        $student->username = $request->username;
        $student->email = $request->email;
        $student->phone_no = $request->phone_no;
        $student->batch_id = $request->batch_id;
        $student->parent_id = $request->parent_id;
        $student->roll_no = $request->roll_no;
        $student->user_data->dob = $request->dob;
        $student->user_data->address = $request->address;
        $student->user_data->extra_fields = $request->extra_fields;
        if (!empty($request->doa)) {
            $student->created_at = $request->doa;
        }

        if ($student->push()) {
            activity(config('app.tenant')->tenant_id)
                ->on($student)
                ->withProperties(['action' => 'update', 'section' => 'student'])
                ->log(__('messages.logStudentUpdated'));

            return response()->json(Reply::success(__('messages.studentUpdated')));
        }

        return response()->json(Reply::error(__('messages.studentNotUpdated')));
    }

    /**
     * @api            {delete} /student/:id
     * @apiName        destroy
     * @apiGroup       Student
     *
     * @apiParam {Number} student id
     *
     * @apiPermission  manage-all-student
     * @apiSuccess {json} Gives details of Student which given by id
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Student has been successfully deleted",
     *          "data": []
     *      }
     * @apiError {json} Details of Parent couldn't delete
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later"
     *     }
     * @apiDescription Delete Student details by given student id
     */
    public function destroy($id, Request $request)
    {
        if (Bouncer::cannot('manage-all-student')) {
            return Reply::unAuthorized();
        }
        $student = User::findOrFail($id);
        if ($student->isNotA('student')) {
            return response()->json(Reply::error(__('messages.notStudentUser')));
        } else {
            if ($request->permanent && $request->permanent != 'false') {
                $student->forceDelete();
            }
            $student->delete();

            activity(config('app.tenant')->tenant_id)
                ->on($student)
                ->withProperties(['action' => 'delete', 'section' => 'student'])
                ->log(__('messages.logStudentDeleted'));

            return response()->json(Reply::success(__('messages.studentDeleted')));
        }
    }
}



