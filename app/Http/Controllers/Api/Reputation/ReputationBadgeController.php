<?php

namespace App\Http\Controllers\Api\Reputation;

use App\Helpers\Helper;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Reputation\StoreBadge;
use App\Http\Requests\Reputation\UpdateBadge;
use App\ReputationBadge;

class ReputationBadgeController extends Controller
{
    /**
     * @api            {GET} /reputation/badge Get Badges
     * @apiName        GetReputationBadges
     * @apiGroup       Reputation
     * @apiSuccess {json} Returned Success Array
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *           {
     *               "status": true,
     *               "message": "",
     *               "data": [
     *                   {
     *                       "id": 1,
     *                       "name": "Watcher",
     *                       "description": "Random Details",
     *                       "image": null,
     *                       "color": "#ffffff",
     *                       "score": 8,
     *                       "created_at": "2018-04-30 21:44:01",
     *                       "updated_at": "2018-04-30 21:44:01"
     *                   },
     *                   {
     *                       "id": 2,
     *                       "name": "School Star",
     *                       "description": "Random Details",
     *                       "image": null,
     *                       "color": "#ffffff",
     *                       "score": 0,
     *                       "created_at": "2018-04-30 21:44:28",
     *                       "updated_at": "2018-04-30 21:44:28"
     *                   },
     *                   {
     *                       "id": 3,
     *                       "name": "Fight Penalty",
     *                       "description": "some description",
     *                       "image": null,
     *                       "color": "#ffffff",
     *                       "score": 0,
     *                       "created_at": "2018-04-30 21:58:59",
     *                       "updated_at": "2018-04-30 21:58:59"
     *                   },
     *                   {
     *                       "id": 4,
     *                       "name": "xyz name",
     *                       "description": "some description",
     *                       "image": null,
     *                       "color": "#ffffff",
     *                       "score": 10,
     *                       "created_at": "2018-04-30 21:59:25",
     *                       "updated_at": "2018-04-30 21:59:25"
     *                   }
     *               ]
     *           }
     *
     * @apiDescription Get list of Badge
     */
    public function index()
    {
        return response()->json(Reply::success('', ReputationBadge::withCount('reputation')->get()));
    }


    /**
     * @api            {POST} /reputation/badge Create Badge
     * @apiName        CreateReputationBadge
     * @apiGroup       Reputation
     * @apiParam {String{..64}} name Badge Name.
     * @apiParam {String{..512}} description Badge Description.
     * @apiParam {Integer{0..10}} score Badge Score.
     * @apiParam {String} [image] A base64 encoded image data
     * @apiParam {string{..6}} [color] HEX color code.
     *
     * @apiParamExample {json} Request-Example:
     *   {
     *       "name":"Name",
     *       "description":"Description",
     *       "score":3
     *   }
     *
     * @apiPermission  manage-all-reputation-badge
     * @apiSuccess {json} Badge successfully created details
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *           {
     *               "status": true,
     *               "message": "Badge Created Successfully",
     *               "data": {
     *                   "name": "Watcher",
     *                   "description": "Bullying reporter and watcher of illegal activities in school",
     *                   "score": "10",
     *                   "image": null,
     *                   "color": "#ffffff",
     *                   "updated_at": "2018-04-30 21:59:25",
     *                   "created_at": "2018-04-30 21:59:25",
     *                   "id": 4
     *               }
     *           }
     *
     * @apiError {json} Details of Fees couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There was am error. Please try again later",
     *
     *     }
     *
     * @apiDescription Badge Store
     */
    public function store(StoreBadge $request)
    {
        $badge = new ReputationBadge;
        $badge->name = $request->name;
        $badge->description = $request->description;
        $badge->score = $request->score;
        $badge->icon = $request->icon;
        $badge->bg_color = $request->bg_color;
        $badge->icon_color = $request->icon_color;

        return Reply::loggedResponse($badge->save(),
            $badge,
            'create',
            'reputation',
            'reputationBadge');

    }


    /**
     * @api            {PUT|PATCH} /reputation/badge/:id Update Badge
     * @apiName        UpdateReputationBadge
     * @apiGroup       Reputation
     * @apiParam {String{..64}} name Badge Name.
     * @apiParam {String{..512}} description Badge Description.
     * @apiParam {Integer{0..10}} score Badge Score.
     * @apiParam {String} [image] A base64 encoded image data
     * @apiParam {string{..6}} [color] HEX color code.
     *
     * @apiParamExample {json} Request-Example:
     *   {
     *       "name":"Name",
     *       "description":"Description",
     *       "score":3
     *   }
     *
     * @apiPermission  manage-all-reputation-badge
     * @apiSuccess {json} Badge successfully created details
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Badge Update"
     *          "data" : [
     *                       "name":"Name",
     *                       "description":"Description",
     *                       "score":3
     *          ]
     *      }
     *
     * @apiError {json} Details of Fees couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There was am error. Please try again later",
     *
     *     }
     *
     * @apiDescription Badge Store
     */
    public function update(UpdateBadge $request, $id)
    {
        $badge = ReputationBadge::findOrFail($id);
        $badge->name = $request->name;
        $badge->description = $request->description;
        $badge->score = $request->score;
        $badge->icon = $request->icon;
        $badge->bg_color = $request->bg_color;
        $badge->icon_color = $request->icon_color;

        return Reply::loggedResponse($badge->save(),
            $badge,
            'update',
            'reputation',
            'reputationBadge');
    }

    /**
     * @api            {DELETE} /reputation/badge/:id
     * @apiName        DeleteReputationBadge
     * @apiGroup       Reputation
     * @apiPermission  manage-all-reputation-badge
     * @apiSuccess {json} Gives details of Badge which given by id
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "Badge deleted",
     *          "data": []
     *      }
     * @apiError {json} Details of Badge couldn't delete
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later"
     *     }
     * @apiDescription Delete Badge details by given badge id
     */
    public function destroy($id)
    {
        if (\Bouncer::cannot('manage-all-reputation-badge')) {
            return Reply::unAuthorized();
        }
        $badge = ReputationBadge::findOrFail($id);
        return Reply::loggedResponse($badge->delete(),
            $badge,
            'delete',
            'reputation',
            'reputationBadge');
    }
}
