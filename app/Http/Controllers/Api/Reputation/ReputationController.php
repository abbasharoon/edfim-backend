<?php

namespace App\Http\Controllers\Api\Reputation;

use App\Helpers\PermissionsHelper;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Reputation\StoreReputation;
use App\Reputation;
use App\User;
use App\UserFee;
use Bouncer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReputationController extends Controller
{
    /**
     * @api            {GET} /reputation Get Reputation
     * @apiName        GetReputationsList
     * @apiGroup       Reputation
     * @apiPermission  view-reputation,view-reputation,(student/parent)
     * @apiParam {Number} user_id Id of user for whom the reputation data should be fetched
     * @apiSuccess {Object[]} data An object containing all the returned data
     * @apiSuccess {Number} data.id  Id of the reputation in database
     * @apiSuccess {Number} data.user_id  Id of the user related to the reputation
     * @apiSuccess {Number} data.badge_id  Id of the badge related to the reputation
     * @apiSuccess {String} data.created_at String of date in YYYY-MM-DD HH:MM:SS format denoting reputation creation. Can be used to trace creation to the batch of student
     * @apiSuccess {Object[]} data.badge  An Object containing the badge details
     * @apiSuccess {Number} data.badge.id  Id of the badge in database
     * @apiSuccess {String} data.badge.name  Name of Badge
     * @apiSuccess {String} data.badge.description  Detail of Badge
     * @apiSuccess {String} data.badge.image  Image name to be used as icon fo the badge. May be null
     * @apiSuccess {String} data.badge.color  Color for the background of the Badge. Can be null as well. Hex format
     * @apiSuccess {Number} data.badge.score  Score of the badge.
     * @apiSuccess {Number} current_page Current Page number on pagination index
     * @apiSuccess {Number} from Start of number of received accounts
     * @apiSuccess {Number} to End of number of received accounts
     * @apiSuccess {Number} total Total Number of Accounts
     * @apiSuccess {Number} last_page Last page on pagination index
     * @apiSuccess {Number} per_page Number of records per page
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *           "status": true,
     *           "message": "",
     *           "data": {
     *               "current_page": 1,
     *               "data": [
     *               {
     *               "id": 1,
     *               "badge_id": 1,
     *               "user_id": 15,
     *               "created_at": "2018-04-30 22:14:53",
     *               "updated_at": "2018-04-30 22:14:53",
     *               "badge": {
     *                       "id": 1,
     *                       "name": "Watcher",
     *                       "description": "Bullying reporter and watcher of illegal activities in school",
     *                       "image": null,
     *                       "color": "#ffffff",
     *                       "score": 10,
     *                       "created_at": "2018-04-30 22:14:05",
     *                       "updated_at": "2018-04-30 22:14:05"
     *                   }
     *               }
     *               ],
     *       "current_page": 1,
     *       "last_page": 1,
     *       "per_page": 50,
     *       "from": 1,
     *       "to": 1,
     *       "total": 1
     *           }
     *      }
     * @apiDescription Get list of Badge
     */
    public function show($id, Request $request, PermissionsHelper $ph)
    {
        $user = User::find($id);
        if (Bouncer::can('view-all-reputation') ||
            (Bouncer::can('view-reputation') && $ph->teacherBatch($user->batch_id)) ||
            (Auth::user()->isA('student') && $id == Auth::id()) ||
            (Auth::user()->isA('parent') && $ph->childrenIds($id))) {
            $badgeUsers = Reputation::whereUserId($id);
            if ($request->exists('class_id')) {
                $badgeUsers = $badgeUsers->where('class_id', $request->class_id);
            }

            if ($request->exists('name')) {
                $badgeUsers = $badgeUsers->whereHas('badge', function ($query) use ($request) {
                    $query->where('name', 'like', '%' . $request->name . '%');
                });
            }
            if ($request->exists('score')) {
                $badgeUsers = $badgeUsers->whereHas('badge', function ($query) use ($request) {
                    $query->where('score', $request->score);
                });
            }


            if ($request->exists('badge_id')) {
                $badgeUsers = $badgeUsers->where('badge_id', $request->badge_id);
            }
            if ($request->exists('created_at')) {
                $badgeUsers = $badgeUsers->where('created_at', 'LIKE', $request->created_at . '%');
            }

            if (!$request->sort_column) {
                $request->sort_column = 'created_at';
                $request->sort_direction = 'desc';
            }
            if ($request->sort_direction === 'desc') {
                $badgeUsers->orderByDesc($request->sort_column);
            } else {
                $badgeUsers->orderBy($request->sort_column);
            }
            $badgeUsers = collect($badgeUsers->paginate($request->page_size))->toArray();
            $badgeUsers['stats'] = round(DB::table('reputation_badges')
                ->leftJoin('reputations', 'reputations.badge_id', '=', 'reputation_badges.id')
                ->where('reputations.user_id', $user->id)
                ->where('reputations.class_id', $request->exists('class_id') ? $request->class_id : $user->batch->class_id)
                ->avg('reputation_badges.score'), 2);
            return response()->json($badgeUsers);
        }
        return Reply::unAuthorized();
    }


    /**
     * @api            {post} /badge-user
     * @apiName        store Save Badge User data
     * @apiGroup       Reputation
     *
     * @apiParam {integer} badges_id Association Badges id.
     * @apiParam {integer} users_id Association Users id.
     *
     * @apiParamExample {json} Request-Example:
     *   {
     *       "token": "",
     *       "badges_id":3,
     *       "users_id":13
     *   }
     *
     * @apiPermission  manage-reputation
     * @apiSuccess {json} Badge User successfully created details
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "status": true,
     *           "message": "messages.reputationCreated",
     *           "data": {
     *               "badge_id": "1",
     *               "user_id": "15",
     *               "updated_at": "2018-04-30 22:14:53",
     *               "created_at": "2018-04-30 22:14:53",
     *               "id": 1
     *           }
     *       }
     *
     * @apiError {json} Details of Association couldn't save
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later",
     *
     *     }
     *
     * @apiDescription Association Store
     */
    public function store(StoreReputation $request)
    {

        $badgeUser = new Reputation;
        $student = User::findOrFail($request->user_id);
        $badgeUser->badge_id = $request->badge_id;
        $badgeUser->user_id = $request->user_id;
        $badgeUser->class_id = $student->batch->class_id;
        $badgeUser->detail = $request->detail;


        return Reply::loggedResponse($badgeUser->save()
            , $badgeUser,
            'create',
            'reputation',
            'reputation');
    }


    public function Update($id, Request $request)
    {

        $badgeUser = Reputation::findOrFail($id);
        $badgeUser->detail = $request->detail;

        return Reply::loggedResponse($badgeUser->save()
            , $badgeUser,
            'update',
            'reputation',
            'reputation');
    }


    /**
     * @api            {DELETE} /reputation/:id Delete Reputation
     * @apiName        DeleteReputation
     * @apiGroup       Reputation
     *
     * @apiPermission  manage-reputation
     * @apiSuccess {json} Gives details of Badge User which given by id
     *  * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "status": true,
     *          "message": "User reputation deleted",
     *          "data": []
     *      }
     * @apiError {json} Details of Badge User couldn't delete
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "message" : "There has a error. Please try again later"
     *     }
     * @apiDescription Delete user reputation for the given badge id.
     */
    public function destroy($id)
    {
        if (Bouncer::cannot('manage-all-reputation') && Bouncer::cannot('manage-reputation')) {
            return Reply::unAuthorized();
        }
        $badgeUser = Reputation::findOrFail($id);
        if ($badgeUser->badge_id == setting()->get('fine.reputation_badge_id')) {
            UserFee::destroy($badgeUser->detail);
        }
        return Reply::loggedResponse($badgeUser->delete()
            , $badgeUser,
            'delete',
            'reputation',
            'reputation');

    }
}
