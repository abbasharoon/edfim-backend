<?php

namespace App\Http\Controllers\Api\Reputation;

use App\Complaint;
use App\Helpers\Reply;
use App\Http\Controllers\Controller;
use App\Http\Requests\Reputation\StoreComplaint;
use Bouncer;
use Illuminate\Http\Request;

class ComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $complaints = Complaint::with(['complainant', 'complainee']);
        if (!$request->sort_column) {
            $request->sort_column = 'created_at';
            $request->sort_direction = 'desc';
        }
        if ($request->sort_direction === 'desc') {
            $complaints->orderByDesc($request->sort_column);
        } else {
            $complaints->orderBy($request->sort_column);
        }

        if (Bouncer::cannot('manage-complaint')) {
            $complaints = $complaints->where('user_id', auth()->id());
        }
        if ($request->exists('occured_at')) {
            $complaints = $complaints->whereBetween('occured_at', [$request->occured_at, $request->occured_at . ' 23:59:59']);
        }
        if ($request->exists('user_id')) {
            $complaints = $complaints->where('user_id', $request->user_id);
        }
        if ($request->exists('complainee')) {
            $complaints = $complaints->whereHas('complainee', function ($query) use ($request) {
                $query->where('id', $request->complainee);
            });
        }
        if ($request->exists('status')) {
            $complaints = $complaints->where('status', $request->status);
        }

        return response()->json($complaints->paginate($request->page_size));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreComplaint $request)
    {
        $complaint = new Complaint();
        $complaint->description = $request->description;
        $complaint->user_id = auth()->id();
        $complaint->occured_at = $request->occured_at;
        $complaint->save();
//        $complaint->complainee()->attach($request->complainee);
        return response(Reply::success('', $complaint));

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $complaint = Complaint::findOrFail($id);
        if (Bouncer::can('manage-complaint') || $complaint->user_id === auth()->id()) {
            if ($complaint->delete()) {
                return response()->json(Reply::success(__('messages.complaintDeleted')));
            }
            return response()->json(Reply::error(__('messages.complaintNotDeleted')));
        }
        return Reply::unAuthorized();
    }
}
