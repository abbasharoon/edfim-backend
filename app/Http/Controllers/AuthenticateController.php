<?php

namespace App\Http\Controllers;

use App\Helpers\Reply;
use App\TenantHostname;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;

class AuthenticateController extends Controller
{
    public function authenticate(Request $request)
    {
        // grab credentials from the request
        if (!$request->exists('password')) {
            $user = User::where('username', $request->username)->withoutGlobalScope('tenant_id')->first();
            if ($user) {
                $hostname = TenantHostname::whereTenantId($user->tenant_id)->first();
                if ($hostname) {
                    return response()->json(Reply::success('', ['hostname', $hostname->hostname]));
                }
            }
            return response()->json(Reply::error('Username/Email doesn\'t exists'));
        }
        $credentials = $request->only('username', 'password');
        $credentials['tenant_id'] = config('app.tenant')->tenant_id;
        try {
            if ($request->remember) {
                JWTFactory::setTTL(20160);

            }
            $token = JWTAuth::attempt($credentials);

            // attempt to verify the credentials and create a token for the user
            if (!$token) {
                $token = JWTAuth::attempt(['email' => $request->username, 'password' => $request->password, 'tenant_id' => config('app.tenant')->tenant_id]);
                if (!$token) {
                    return response()->json(['error' => 'invalid_credentials']);
                }
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token']);
        }

        // all good so return the token
        $user = Auth::user();
        $roles = DB::table('assigned_roles')
            ->where('assigned_roles.entity_id', Auth::id())
            ->where('assigned_roles.scope', config('app.tenant')->tenant_id)
            ->join('roles', 'assigned_roles.role_id', '=', 'roles.id')
            ->get();
        $abilities = Auth::user()->getAbilities();
        $children = '';
        if (Auth::user()->isNotA('student', 'parent')) {
            $classes = Auth::user()->classStaff;
            $batches = DB::table('batches')
                ->join('class_user', 'batches.class_id', '=', 'class_user.class_id')
                ->where('class_user.user_id', Auth::id())
                ->get();
            $subjects = Auth::user()->subject;
        } else {
            if (Auth::user()->isA('parent')) {
                $children = Auth::user()->children;
            }
            $classes = [];
            $batches = [];
            $subjects = [];
        }
        return response()->json(compact('token', 'user', 'roles', 'abilities', 'classes', 'batches', 'subjects', 'children'));
    }

    public function authenticateTenant(Request $request)
    {
        $tenantHostname = TenantHostname::where('hostname', strtolower($request->get('tenant')))->first();
        if ($tenantHostname === null) {
            return response()->json(['error' => 'unknown_tenant']);
        } else {
            return response()->json(['success' => 'valid_tenant']);
            /*$request->tenant = $tenantHostname;
            $request->tenant->hostname = strtolower($request->tenant->hostname);
            LandlordFacade::addTenant('tenant_id', $tenantHostname->tenant_id);*/
        }

    }


    public function getAuthUser(Request $request)
    {
        $user = JWTAuth::toUser($request->token);
        return response()->json(['result' => $user]);
    }
}
