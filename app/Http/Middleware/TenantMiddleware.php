<?php

namespace App\Http\Middleware;


use anlutro\LaravelSettings\Facade as Setting;
use App\TenantHostname;
use App\User;
use Closure;
use HipsterJazzbo\Landlord\Facades\Landlord;

class TenantMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $tenantHostname = TenantHostname::where('hostname', strtolower(parse_url($request->root(), PHP_URL_HOST)))->first();
        if ($tenantHostname === null) {
            return response()->json(['error' => 'unknown_tenant']);
        } else {
            $tenantHostname['mediaTag'] = $tenantHostname->id . 'media';
            config(['app.tenant' => $tenantHostname]);
            Landlord::addTenant('tenant_id', $tenantHostname->tenant_id);
            Landlord::applyTenantScopes(new User());
            // Set the tenant id to every request made
            $request->tenant = $tenantHostname;
            $request->tenant->hostname = strtolower($request->tenant->hostname);
            Setting::setExtraColumns(['tenant_id' => $tenantHostname->tenant_id]);
            \Bouncer::scope()->to($tenantHostname->tenant_id);
//            @TODO: Remove on next usage
            if (!setting('notification.sms.noticeboard')) {
                setting(['notification.sms.noticeboard' => 1])->save();
            }
            return $next($request);
        }

    }
}
