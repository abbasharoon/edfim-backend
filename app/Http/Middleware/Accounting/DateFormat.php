<?php

namespace App\Http\Middleware\Accounting;

use Carbon\Carbon;
use Closure;

class DateFormat
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (($request->method() == 'POST') || ($request->method() == 'PATCH')) {
            $fields = ['paid_at', 'due_at', 'billed_at', 'invoiced_at', 'started_at', 'ended_at'];

            foreach ($fields as $field) {
                $date = $request->get($field);

                if (empty($date)) {
                    continue;
                }

                $new_date = Carbon::parse($date)->format('Y-m-d H:i:s');

                $request->request->set($field, $new_date);
            }
        }

        return $next($request);
    }
}
