<?php

namespace App\Http\Middleware\Accounting;

use App\Utilities\Accounting\Overrider;
use Closure;

class LoadCurrencies
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $tenant_id = config('app.tenant_id')->tenant_id;

        if (empty($tenant_id)) {
            return $next($request);
        }

        Overrider::load('currencies');

        return $next($request);
    }
}
