<?php

namespace App\Http\Requests\Fee;

use App\Role;
use App\Rules\StudentUser;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreStudentInvoice extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Bouncer::can('manage-all-student-invoice');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'integer|in:0,1,2,3',
            'user_id' => ['required', 'integer', new StudentUser()],
            'fee' => 'nullable|array',
            'fee.*.fee_id' => ['required', 'integer',
                Rule::exists('fees', 'id')->where(function ($query) {
                    $query->whereTenantId(config('app.tenant')->tenant_id);
                    $query->whereNull('deleted_at');
                })],
            'fee.*.target_fee_id' => ['nullable', 'integer',
                Rule::exists('fees', 'id')->where(function ($query) {
                    $query->whereTenantId(config('app.tenant')->tenant_id);
                    $query->whereNull('deleted_at');
                })],
        ];
    }
}
