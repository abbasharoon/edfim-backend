<?php

namespace App\Http\Requests\Fee;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreFeeAssociation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Bouncer::can('manage-all-class-invoice');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        @TODO Update rules accordingly
        return [
            'class' => 'array',
            'class.add_fee' => 'array',
            'class.add_fee.*.class_id' => ['required', 'integer',
                Rule::exists('classes', 'id')->where(function ($q) {
                    $q->whereTenanId(config('app.tenant')->tenant_id);
                    $q->whereNull('delete_at');
                })],
            'class.add_fee.*.target_fee_id' => ['required', 'integer',
                Rule::exists('fees', 'id')->where(function ($q) {
                    $q->whereTenanId(config('app.tenant')->tenant_id);
                    $q->whereNull('delete_at');
                })],
            'class.remove_fee' => 'array',
            'class.remove_fee.*.class_id' => ['required', 'integer',
                Rule::exists('classes', 'id')->where(function ($q) {
                    $q->whereTenanId(config('app.tenant')->tenant_id);
                    $q->whereNull('delete_at');
                })],
            'student' => 'array',
            'student.add_fee' => 'array',
            'student.add_fee.*.class_id' => ['required', 'integer',
                Rule::exists('classes', 'id')->where(function ($q) {
                    $q->whereTenanId(config('app.tenant')->tenant_id);
                    $q->whereNull('delete_at');
                })],
            'student.add_fee.*.target_fee_id' => ['required', 'integer',
                Rule::exists('fees', 'id')->where(function ($q) {
                    $q->whereTenanId(config('app.tenant')->tenant_id);
                    $q->whereNull('delete_at');
                })],
            'student.remove_fee' => 'array',
            'student.remove_fee.*.class_id' => ['required', 'integer',
                Rule::exists('classes', 'id')->where(function ($q) {
                    $q->whereTenanId(config('app.tenant')->tenant_id);
                    $q->whereNull('delete_at');
                })],
        ];
    }
}
