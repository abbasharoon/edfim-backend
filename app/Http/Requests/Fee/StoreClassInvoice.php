<?php

namespace App\Http\Requests\Fee;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreClassInvoice extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Bouncer::can('manage-all-class-invoice');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'class_id' => ['required', 'integer',
                Rule::exists('classes', 'id')->where(function ($query) {
                    $query->whereTenantId(config('app.tenant')->tenant_id);
                    $query->whereNull('deleted_at');
                })],
            'schedule' => 'required_without:fee|integer|in:1,2,3,4,5,6,7',
            'fee' => 'nullable|array',
            'status' => 'integer|in:0,1,2,3',
            'fee.*.fee_id' => ['required', 'integer',
        Rule::exists('fees', 'id')->where(function ($query) {
            $query->whereTenantId(config('app.tenant')->tenant_id);
            $query->whereNull('deleted_at');
        })],
            'fee.*.target_fee_id' => ['nullable', 'integer',
        Rule::exists('fees', 'id')->where(function ($query) {
            $query->whereTenantId(config('app.tenant')->tenant_id);
            $query->whereNull('deleted_at');
        })],
        ];
    }
}
