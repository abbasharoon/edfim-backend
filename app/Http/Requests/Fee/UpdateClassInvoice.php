<?php

namespace App\Http\Requests\Fee;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateClassInvoice extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Bouncer::can('manage-all-class-invoice');

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'integer|in:0,1,2,3',
            'due_date' => 'date',
            'amount_paid' => 'integer',
            'invoice_id' => ['required', 'integer',
                Rule::exists('class_invoices', 'id')->where(function ($query) {
                    $query->whereTenantId(config('app.tenant')->tenant_id);
                    $query->whereNull('deleted_at');
//                    $query->whereStatus(1);
                }),
//                Rule::unique('user_invoices', 'class_invoice_id')->where(function ($query) {
//                    $query->whereTenantId(config('app.tenant')->tenant_id);
//                    $query->whereNull('deleted_at');
////                    $query->where('status', 2);
//                })
            ],
            'add_fee' => 'nullable|array',
            'add_fee.*.fee_id' => ['required', 'integer',
                Rule::exists('fees', 'id')->where(function ($query) {
                    $query->whereTenantId(config('app.tenant')->tenant_id);
                    $query->whereNull('deleted_at');
                })],
            'add_fee.*.target_fee_id' => ['nullable', 'integer',
                Rule::exists('fees', 'id')->where(function ($query) {
                    $query->whereTenantId(config('app.tenant')->tenant_id);
                    $query->whereNull('deleted_at');
                })],

            'remove_fee' => 'nullable|array',
            'remove_fee.*.fee_id' => ['required', 'integer',
                Rule::exists('fees', 'id')->where(function ($query) {
                    $query->whereTenantId(config('app.tenant')->tenant_id);
                    $query->whereNull('deleted_at');
                })],
        ];

    }
}
