<?php

namespace App\Http\Requests\Fee;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateFee extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Bouncer::can('manage-all-fee');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:256',
            'detail' => 'nullable|string|max:1024',
            'amount' => 'required|numeric',
            'schedule' => 'required|integer|max:7|min:0',
            'type' => 'required|integer|in:1,2',
//            'addition' => 'required|integer|in:0,1',
            'fee_category_id' => ['required_if:instant,false', 'nullable', 'integer',
                Rule::exists('fee_categories', 'id')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                }),
            ],
            'classes' => 'array',
            'classes.*' => 'array',
            'classes.*.class_id' => ['required', 'integer',
                Rule::exists('classes', 'id')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                    $query->whereNull('deleted_at');
                }),
            ],
            'classes.*.target_fee_id' => ['nullable', 'integer',
                Rule::exists('fees', 'id')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                    $query->whereNull('deleted_at');
                }),
            ]
        ];
    }

}
