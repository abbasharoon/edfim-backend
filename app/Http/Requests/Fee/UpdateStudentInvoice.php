<?php

namespace App\Http\Requests\Fee;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateStudentInvoice extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Bouncer::can('manage-all-student-invoice');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'invoice_id' => ['required', 'integer',
                Rule::exists('user_invoices', 'id')->where(function ($query) {
                    $query->whereTenantId(config('app.tenant')->tenant_id);
                    $query->whereNull('deleted_at');
//                    $query->whereStatus(1);
                })],
            'status' => 'integer|in:0,1,2,3',
            'due_date'=> 'date',
            'amount_paid' => 'integer',
            'add_fee' => 'nullable|array',
            'add_fee.*.fee_id' => ['required', 'integer',
                Rule::exists('fees', 'id')->where(function ($query) {
                    $query->whereTenantId(config('app.tenant')->tenant_id);
                    $query->whereNull('deleted_at');
                })],
            'add_fee.*.target_fee_id' => ['nullable', 'integer',
                Rule::exists('fees', 'id')->where(function ($query) {
                    $query->whereTenantId(config('app.tenant')->tenant_id);
                    $query->whereNull('deleted_at');
                })],

            'remove_fee' => 'nullable|array',
            'remove_fee.*.fee_id' => ['required', 'integer',
                Rule::exists('fees', 'id')->where(function ($query) {
                    $query->whereTenantId(config('app.tenant')->tenant_id);
                    $query->whereNull('deleted_at');
                })],
        ];
    }
}
