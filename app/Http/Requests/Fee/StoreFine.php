<?php

namespace App\Http\Requests\Fee;

use App\Rules\StudentUser;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreFine extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Bouncer::can('manage-fine');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => ['required', 'integer', new StudentUser()],
            'fee_id' => ['required', 'integer',
                Rule::exists('fees', 'id')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                    $query->where('fee_category_id', setting('fine.fee_category_id'));
                    $query->where('delete_at', null);
                })]
        ];
    }
}
