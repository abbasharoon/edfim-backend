<?php

namespace App\Http\Requests\Attendance;

use App\AttendanceAbsent;
use App\Helpers\PermissionsHelper;
use Bouncer;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateAttendanceAbsent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected $attendance;

    public function authorize(PermissionsHelper $ph)
    {
        $this->attendance = AttendanceAbsent::findOrFail(request()->route('absent'))->attendance;
        return (Bouncer::can('manage-all-attendance') ||
            (Bouncer::can('manage-attendance') && $ph->hasBatch($this->attendance->batch_id)));
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'required|integer|in:0,1',
            'reason_id' => ['required_if:status,1', 'integer',
                Rule::exists('absent_reasons', 'id')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })]
        ];
    }

}
