<?php

namespace App\Http\Requests\Attendance;

use App\Helpers\PermissionsHelper;
use Bouncer;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreAttendance extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(PermissionsHelper $ph)
    {
        return (Bouncer::can('manage-all-attendance') ||
            (Bouncer::can('manage-attendance') && $ph->hasBatch(request()->batch_id)));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => ['required', 'date'],
            'batch_id' => ['required', 'integer',
                Rule::exists('batches', 'id')->where(function ($query) {
                    $query->where('class_id', request()->class_id);
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                }),
            ],
            'attendance_register_id' => ['required', 'integer',
                Rule::exists('attendance_register', 'id')->where(function ($query) {
//                    $query->where(function ($q) {
//                        $q->where('class_id', request()->class_id);
//                        $q->orwhere('class_id', null);
//                    });
                    $query->where('batch_id', request()->batch_id);
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                }),
            ],
        ];
    }
}
