<?php

namespace App\Http\Requests\Attendance;

use App\Attendance;
use App\Helpers\PermissionsHelper;
use Bouncer;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateAttendance extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected $attendance;

    public function __construct(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $files = array(), array $server = array(), $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->attendance = Attendance::findOrFail(request()->route('attendance'));

    }

    public function authorize(PermissionsHelper $ph)
    {
        return (Bouncer::can('manage-all-attendance') ||
            (Bouncer::can('manage-attendance') && $ph->hasBatch($this->attendance->batch_id)));
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'date' => ['date',
//                Rule::unique('attendances', 'date')->where(function ($query) {
//                    $query->where('batch_id', request()->batch_id);
//                })->ignore(request()->route('attendance'))],
            'status' => 'in:0,1|integer'
        ];
    }
}
