<?php

namespace App\Http\Requests\Attendance;

use App\Attendance;
use App\Helpers\PermissionsHelper;
use App\Rules\StudentUser;
use Bouncer;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreAttendanceAbsent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected $attendance;

    public function __construct(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $files = array(), array $server = array(), $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->attendance = Attendance::findOrFail(request()->attendance_id);
    }

    public function authorize(PermissionsHelper $ph)
    {
        return (Bouncer::can('manage-all-attendance') ||
            (Bouncer::can('manage-attendance') && $ph->hasBatch($this->attendance->batch_id)));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $attendance = $this->attendance;
        return [
            'attendance_id' => ['required', 'integer',
                Rule::exists('attendances', 'id')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
//                    $query->where('status', '0'); //Attendance shouldn't be closed yet
                }),
            ],
//            'absent_students' => 'required|array',
            'user_id' => ['required', 'integer', 'distinct',
                new StudentUser($this->attendance->batch_id),
                Rule::unique('attendance_absents', 'user_id')->where(function ($query) use ($attendance) {
                    $query->where('attendance_id', $attendance->id);
                })],
            'reason_id' => ['nullable', 'integer',
                Rule::exists('absent_reasons', 'id')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })]
        ];
    }

    public function messages()
    {
        return [
            'user_id.unique' => 'Student is already marked absent for current attendance'
        ];
    }

}
