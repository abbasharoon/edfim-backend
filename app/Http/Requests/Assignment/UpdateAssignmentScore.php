<?php

namespace App\Http\Requests\Assignment;

use App\Assignment;
use App\AssignmentScore;
use App\Helpers\PermissionsHelper;
use Bouncer;
use Illuminate\Foundation\Http\FormRequest;

class UpdateAssignmentScore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected $assignment;

    public function __construct(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $files = array(), array $server = array(), $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->assignment = Assignment::findOrFail(request()->route('score'));
    }

    public function authorize(PermissionsHelper $ph)
    {
        return (Bouncer::can('manage-all-assignment') ||
            (Bouncer::can('manage-assignment') && $ph->hasBatch($this->assignment->batch_id)) ||
            (Bouncer::can('manage-subject-assignment') && $ph->hasSubject($this->assignment->subject_id)));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $assignment = $this->assignment;
        $maxScore = ($assignment->score == 0) ? 1 : $assignment->score;
        return [
            'score' => 'required|integer|min:0|max:' . $maxScore
        ];
    }
}
