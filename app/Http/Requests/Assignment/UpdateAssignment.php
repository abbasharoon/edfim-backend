<?php

namespace App\Http\Requests\Assignment;

use App\Assignment;
use App\Helpers\PermissionsHelper;
use App\Rules\StatusCheck;
use Bouncer;
use Illuminate\Foundation\Http\FormRequest;

class UpdateAssignment extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected $assignment;

    public function __construct(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $files = array(), array $server = array(), $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->assignment = Assignment::findOrFail(request()->route('assignment'));
    }

    public function authorize(PermissionsHelper $ph)
    {
        return (Bouncer::can('manage-all-assignment') ||
            (Bouncer::can('manage-assignment') && $ph->hasBatch($this->assignment->batch_id)) ||
            (Bouncer::can('manage-subject-assignment') && $ph->hasSubject($this->assignment->subject_id)));

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $assignment = $this->assignment;
        return [
            'name' => 'required_without:fill_status|string|max:256',
            'detail' => 'required_without:fill_status|string|max:1024',
            'due_date' => 'required_without:fill_status|date',
            'score' => 'required_without:fill_status|integer',
            'upload' => 'nullable|integer|max:5',
            'status' => ['required', 'integer', new StatusCheck($assignment->status)],
        ];
    }
}
