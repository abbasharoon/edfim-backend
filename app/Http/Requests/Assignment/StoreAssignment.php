<?php

namespace App\Http\Requests\Assignment;

use App\Helpers\PermissionsHelper;
use App\Rules\BatchSubject;
use Bouncer;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreAssignment extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize(PermissionsHelper $ph)
    {
        return (Bouncer::can('manage-all-assignment') ||
            (Bouncer::can('manage-assignment') && $ph->hasBatch(request()->batch_id)) ||
            (Bouncer::can('manage-subject-assignment') && $ph->hasSubject(request()->subject_id)));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:256',
            'detail' => 'nullable|string|max:1024',
            'batch_id' => ['required', 'integer',
                Rule::exists('batches', 'id')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                    $query->where('class_id', request()->class_id);
                    $query->where('deleted_at', null);
                }),
            ],
            'subject_id' => ['required', 'integer', new BatchSubject(request()->batch_id)],
            'due_date' => 'required|date',
            'score' => 'required|integer',
            'status' => 'required|in:0,1|integer',
            'upload' => 'nullable|integer|max:5',
        ];
    }
}
