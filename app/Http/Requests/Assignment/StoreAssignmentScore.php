<?php

namespace App\Http\Requests\Assignment;

use App\Assignment;
use App\Helpers\PermissionsHelper;
use App\Rules\StudentUser;
use Bouncer;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreAssignmentScore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected $assignment;

    public function __construct(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $files = array(), array $server = array(), $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->assignment = Assignment::findOrFail(request()->assignment_id);

    }


    public function authorize(PermissionsHelper $ph)
    {
        return (Bouncer::can('manage-all-assignment') ||
            (Bouncer::can('manage-assignment') && $ph->hasBatch($this->assignment->batch_id)) ||
            (Bouncer::can('manage-subject-assignment') && $ph->hasSubject($this->assignment->subject_id)));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $assignment = $this->assignment;
        $maxScore = ($assignment->score == 0) ? 1 : $assignment->score;
        return [
            'assignment_id' => ['required', 'integer',
                Rule::exists('assignments', 'id')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
//                    Where status must be either due or published
                    $query->whereNotIn('status', [0, 3]);
                })],
            'students_score' => 'nullable|array',
            'students_score.*.user_id' => ['required', 'integer', 'distinct',
                new StudentUser($assignment->batch_id),
                Rule::unique('assignment_score', 'user_id')->where(function ($query) use ($assignment) {
                    $query->where('assignment_id', $assignment->id);
                })
            ],
            'students_score.*.score' => 'required|integer|min:0|max:' . $maxScore
        ];
    }
}
