<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class StoreTimeTable extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Bouncer::can('manage-all-timetable');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'class_id' => 'required|Array',
            'class_id.*' => ['required', 'numeric',
                Rule::exists('classes', 'id')->where(function ($i) {
                    $i->where('tenant_id', config('app.tenant')->tenant_id);
                    $i->whereNull('deleted_at');
                })],
            'title' => 'string|required',
            'detail' => 'nullable|string|max:512',
            'end_date' => 'nullable|date',
            'start_date' => ['required', 'date'],
//            'start_date' => ['required', 'date', new UniqueTimetable($request->class_id, $request->end_date)],
        ];
    }
}
