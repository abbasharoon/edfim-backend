<?php

namespace App\Http\Requests\Subject;

use App\Rules\StaffUser;
use Illuminate\Foundation\Http\FormRequest;

class UpdateSubject extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Bouncer::can('manage-all-subject');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'user_id' => 'nullable|array',
            'user_id.*' => ['nullable', new StaffUser()],
        ];
    }

}
