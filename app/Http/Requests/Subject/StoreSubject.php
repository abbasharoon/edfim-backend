<?php

namespace App\Http\Requests\Subject;

use App\Role;
use App\Rules\StaffUser;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreSubject extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Bouncer::can('manage-all-subject');

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $roles = Role::where('name', '!=', 'student')
            ->where('name', '!=', 'parent')
            ->get()
            ->pluck('id');
        return [
            'name' => 'required|max:255',
            'class_id' => [
                'required', 'integer',
                Rule::exists('classes', 'id')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                }),
            ],
            'user_id' => 'array',
            'user_id.*' => ['required', 'distinct', new StaffUser()],
        ];
    }

}
