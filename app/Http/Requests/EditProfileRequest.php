<?php

namespace App\Http\Requests;

use App\Transformers\Accounting\Auth\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required_without_all:username,phone_no',
                Rule::unique('users')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })->ignore(request(auth()->id()))],
            'username' => ['string', 'required_without_all:email,phone_no', 'max:128',
                Rule::unique('users')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })->ignore(request(auth()->id()))],
            'phone_no' => ['required_without_all:username,email', 'numeric',
                Rule::unique('users')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })->ignore(request(auth()->id()))],
            'email_notification' => 'boolean',
            'sms_notification' => 'boolean',
            'web_notification' => 'boolean',
            'app_notification' => 'boolean',
            'new_password' => 'string',
            'confirm_password' => 'string|same:new_password',
        ];
    }
}
