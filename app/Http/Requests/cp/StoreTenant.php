<?php

namespace App\Http\Requests\cp;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreTenant extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'institutionName' => 'required|string|max:400',
            'institutionAddress' => 'required|string|max:600',
            'institutionPhone' => 'required|string|max:200',
//            'institutionStudents' => 'required|integer|min:100',
            'institutionHostname' => ['required','string','min:4', Rule::unique('tenant_hostnames','hostname')],
            'additionalSms' => 'nullable|integer',
            'subscriptionPlan' => 'required|integer|in:1,2,3',
            'paidFrom' => 'required|date',
            'paidTill' => 'required|date',
            'adminFirstName' => 'required|string|max:256',
            'adminLastName' => 'required|string|max:256',
            'adminPassword' => 'required|string|max:256',
            'adminConfirmPassword' => 'required|string|max:256',
            'adminEmail' => 'required|string|max:256',
            'adminPhone' => 'required|string|max:256',
        ];
    }
}
