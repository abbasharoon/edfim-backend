<?php

namespace App\Http\Requests\Noticeboard;

use Illuminate\Foundation\Http\FormRequest;

class StoreNotification extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'required|string|max:1024',
            'target_batches' => 'required_without_all:all,staff,parents,students|array',
            'target_users' => 'nullable|array|max:3',
            'staff' => 'required_without_all:all,target_batches,parents,students|array',
            'parents' => 'required_without_all:all,target_batches,staff,students|array',
            'students' => 'required_without_all:all,target_batches,parents,staff|array',
            'all' => 'required_without_all:target_batches,staff,parents,students'
        ];
    }
}
