<?php

namespace App\Http\Requests\Reputation;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreComplaint extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required|string|max:1024|min:126',
//            'complainee.*' => ['nullable', 'integer', Rule::unique('users')->where(function ($query) {
//                return $query->where('tenant_id', config('app.tenant')->tenant_id);
//            })],
            'occured_at' => 'required|date'
        ];
    }
}
