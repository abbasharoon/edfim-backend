<?php

namespace App\Http\Requests\Reputation;

use App\Role;
use App\Rules\StudentUser;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreReputation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Bouncer::can('manage-reputation');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $role = Role::where('name', 'student')->first();
        return [
            'badge_id' => ['required', 'integer',
                Rule::exists('reputation_badges', 'id')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                }),
            ],
            'user_id' => ['required', 'integer', new StudentUser()],
            'detail' => 'string|max:512',
        ];
    }


}
