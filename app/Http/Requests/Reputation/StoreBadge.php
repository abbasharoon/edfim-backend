<?php

namespace App\Http\Requests\Reputation;

use Illuminate\Foundation\Http\FormRequest;

class StoreBadge extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Bouncer::can('manage-all-reputation-badge');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:128',
            'description' => 'nullable|string|max:512',
            'score' => 'required|integer|min:-10|max:10',
            'icon' => 'required|string|max:126',
            'bg_color' => 'required|string|max:16',
            'icon_color' => 'required|string|max:16',
        ];
    }

}
