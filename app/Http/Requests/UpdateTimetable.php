<?php

namespace App\Http\Requests;

use App\Rules\UniqueTimetable;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UpdateTimetable extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Bouncer::can('manage-all-timetable');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        return [
            'class_id' => 'required|Array',
            'class_id.*' => 'required|numeric|exists:classes,id',
            'title' => 'string|required',
            'description' => 'string|max:500',
            'end_date' => 'nullable|date',
            'start_date' => ['required', 'date',
                new UniqueTimetable($request->class_id,
                    $request->end_date,
                    request()->route('timetable'))],
        ];
    }
}
