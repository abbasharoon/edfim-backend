<?php

namespace App\Http\Requests\Assessment;

use App\AssessmentExam;
use App\Helpers\PermissionsHelper;
use App\Rules\AssessmentStatus;
use App\Rules\Currency;
use Bouncer;
use Illuminate\Foundation\Http\FormRequest;

class UpdateAssessmentExam extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public $assessment;

    public function __construct(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $files = array(), array $server = array(), $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->assessment = AssessmentExam::findOrFail(request()->route('exam'));

    }

    public function authorize(PermissionsHelper $ph)
    {
        return (Bouncer::can('manage-all-exam') ||
            (Bouncer::can('manage-exam') && $ph->teacherBatch($this->assessment->batch_id)));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required_without:fill_status|string|max:256',
            'detail' => 'nullable|string|max:1024',
            'status' => ['required_with:fill_status', 'integer', new AssessmentStatus($this->assessment->status)],
            'schedule' => 'required_without:fill_status|integer|in:0,1,2,3,4,5,6,7',
        ];
    }
}
