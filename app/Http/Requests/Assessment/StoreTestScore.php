<?php

namespace App\Http\Requests\Assessment;

use App\AssessmentTest;
use App\Helpers\PermissionsHelper;
use App\Rules\StudentUser;
use Bouncer;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreTestScore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected $assessment;

    public function __construct(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $files = array(), array $server = array(), $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->assessment = AssessmentTest::findOrFail(request()->assessment_id);

    }

    public function authorize(PermissionsHelper $ph)
    {
        return (Bouncer::can('manage-all-test') ||
            (Bouncer::can('manage-test') && $ph->hasBatch($this->assessment->batch_id)) ||
            (Bouncer::can('manage-subject-test') && $ph->hasSubject($this->assessment->subject_id)));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $assessment = $this->assessment;
        return [
            'assessment_id' => ['required', 'integer',
                Rule::exists('assessment_tests', 'id')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
//                    Either due or published
                    $query->where('status', '!=', 3);
                })],
            'status' => 'nullable|boolean',
            'students_score' => 'required|array',
            'students_score.*.user_id' => ['required', 'integer', 'distinct', new StudentUser($assessment->batch_id)
                , Rule::unique('assessment_test_scores', 'user_id')->where(function ($query) use ($assessment) {
                    $query->where('assessment_test_id', $assessment->id);
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })
            ],
            'students_score.*.score' => 'required|integer|max:' . $assessment->score
        ];
    }
}
