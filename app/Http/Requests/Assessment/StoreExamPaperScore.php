<?php

namespace App\Http\Requests\Assessment;

use App\ExamPaper;
use App\Helpers\PermissionsHelper;
use App\Rules\StudentUser;
use Bouncer;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreExamPaperScore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected $paper;

    public function __construct(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $files = array(), array $server = array(), $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->paper = ExamPaper::findOrFail(request()->assessment_id);

    }

    public function authorize(PermissionsHelper $ph)
    {
        return (Bouncer::can('manage-all-paper') ||
            (Bouncer::can('manage-paper') && $ph->teacherClass($this->paper->assessmentExam->class_id)) ||
            (Bouncer::can('manage-subject-paper') && $ph->hasSubject($this->paper->subject_id)));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $assessment = $this->paper->assessmentExam;
        $paper = $this->paper;
        return [
            'assessment_id' => ['required', 'integer',
                Rule::exists('exam_papers', 'id')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
//                  Not cancelled
                    $query->where('status', '!=', 4);
                })],
            'status' => 'nullable|boolean',
            'students_score' => 'required|array',
            'students_score.*.user_id' => ['required', 'integer', 'distinct',
                new StudentUser($assessment->batch_id),
                Rule::unique('exam_paper_scores', 'user_id')->where(function ($query) use ($paper) {
                    $query->where('exam_paper_id', $paper->id);
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })
            ],
            'students_score.*.score' => 'required|integer|max:' . $paper->score
        ];
    }
}
