<?php

namespace App\Http\Requests\Assessment;

use App\AssessmentTest;
use App\AssessmentTestScore;
use App\Helpers\PermissionsHelper;
use Bouncer;
use Illuminate\Foundation\Http\FormRequest;

class UpdateTestScore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected $assessment;

    public function __construct(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $files = array(), array $server = array(), $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->assessment = AssessmentTest::findOrFail(request()->route('score'));

    }

    public function authorize(PermissionsHelper $ph)
    {
        return (Bouncer::can('manage-all-test') ||
            (Bouncer::can('manage-test') && $ph->hasBatch($this->assessment->batch_id)) ||
            (Bouncer::can('manage-subject-test') && $ph->hasSubject($this->assessment->subject_id)));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $assessment = $this->assessment;
        return [
            'score' => 'required|integer|min:0|max:' . $assessment->score
        ];
    }
}
