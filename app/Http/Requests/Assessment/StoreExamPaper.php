<?php

namespace App\Http\Requests\Assessment;

use App\AssessmentExam;
use App\Helpers\PermissionsHelper;
use Bouncer;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreExamPaper extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public $assessment;

    public function __construct(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $files = array(), array $server = array(), $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->assessment = AssessmentExam::whereIn('status', [0, 1, 2])->findOrFail(request()->assessment_exam_id);

    }

    public function authorize(PermissionsHelper $ph)
    {
        return (Bouncer::can('manage-all-paper') ||
            (Bouncer::can('manage-paper') && $ph->teacherBatch($this->assessment->batch_id)) ||
            (Bouncer::can('manage-subject-paper') && $ph->hasSubject(request('subject_id'))));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $assessment = $this->assessment;
        return [
            'name' => 'required|string|max:256',
            'detail' => 'nullable|string|max:1024',
            'due_date' => 'required|date',
            'score' => 'required|integer',
            'type' => 'nullable|string|max:64',
            'subject_id' => ['required', 'integer',
                Rule::exists('subjects', 'id')->where(function ($query) use ($assessment) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                    $query->where('class_id', $assessment->class_id);
                })],
        ];

    }
}
