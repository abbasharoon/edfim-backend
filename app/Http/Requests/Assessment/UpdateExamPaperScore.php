<?php

namespace App\Http\Requests\Assessment;

use App\ExamPaper;
use App\ExamPaperScore;
use App\Helpers\PermissionsHelper;
use Bouncer;
use Illuminate\Foundation\Http\FormRequest;

class UpdateExamPaperScore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected $paper;

    public function __construct(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $files = array(), array $server = array(), $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->paper = ExamPaper::findOrFail(request()->route('score'));

    }

    public function authorize(PermissionsHelper $ph)
    {
        return (Bouncer::can('manage-all-paper') ||
            (Bouncer::can('manage-paper') && $ph->teacherBatch($this->paper->assessmentExam->batch_id)) ||
            (Bouncer::can('manage-subject-paper') && $ph->hasSubject($this->paper->subject_id)));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $assessment = $this->paper;
        return [
            'score' => 'required|integer|min:0|max:' . $assessment->score
        ];
    }
}
