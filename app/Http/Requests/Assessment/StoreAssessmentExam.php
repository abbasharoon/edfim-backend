<?php

namespace App\Http\Requests\Assessment;

use App\Helpers\PermissionsHelper;
use Bouncer;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreAssessmentExam extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(PermissionsHelper $ph)
    {
        return (Bouncer::can('manage-all-exam') ||
            (Bouncer::can('manage-exam') && $ph->teacherBatch(request('batch_id'))));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => 'required|string|max:256',
            'detail' => 'required|string|max:1024',
            'schedule' => 'required|integer|in:0,1,2,3,4,5,6',
            'batch_id' => ['required', 'integer',
                Rule::exists('batches', 'id')->where(function ($query) {
                    $query->where('class_id', request()->class_id);
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })],

        ];

    }
}
