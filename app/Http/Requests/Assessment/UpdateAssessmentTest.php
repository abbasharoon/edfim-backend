<?php

namespace App\Http\Requests\Assessment;

use App\AssessmentTest;
use App\Helpers\PermissionsHelper;
use App\Rules\AssessmentStatus;
use Bouncer;
use Illuminate\Foundation\Http\FormRequest;

class UpdateAssessmentTest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function __construct(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $files = array(), array $server = array(), $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->assessment = AssessmentTest::findOrFail(request()->route('test'));

    }

    public function authorize(PermissionsHelper $ph)
    {
        return (Bouncer::can('manage-all-test') ||
            (Bouncer::can('manage-test') && $ph->hasBatch($this->assessment->batch_id)) ||
            (Bouncer::can('manage-subject-test') && $ph->hasSubject($this->assessment->subject_id)));
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $assessment = $this->assessment;
        return [
            'name' => 'required_without:fill_status|string|max:256',
            'detail' => 'nullable|string|max:1024',
            'status' => ['required_with:fill_status', 'integer', new AssessmentStatus($assessment->status)],
            'due_date' => 'required_without:fill_status|date',
            'score' => 'required_without:fill_status|integer',
            'schedule' => 'required_without:fill_status|integer|in:0,1,2,3,4,5,6,7',
            'type' => 'required_without:fill_status|string|max:64',
        ];
    }
}
