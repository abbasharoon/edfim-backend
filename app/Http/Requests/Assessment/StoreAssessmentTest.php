<?php

namespace App\Http\Requests\Assessment;

use App\Helpers\PermissionsHelper;
use App\Rules\BatchSubject;
use Bouncer;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreAssessmentTest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(PermissionsHelper $ph)
    {

        return (Bouncer::can('manage-all-test') ||
            (Bouncer::can('manage-test') && $ph->hasBatch(request('batch_id'))) ||
            (Bouncer::can('manage-subject-test') && $ph->hasSubject(request('subject_id'))));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:256',
            'detail' => 'nullable|string|max:1024',
            'status' => 'required|integer|in:0,1',
            'due_date' => 'required|date',
            'score' => 'required|integer',
            'schedule' => 'required|integer|in:0,1,2,3,4,5,6,7',
            'type' => 'required|string|max:64',
            'batch_id' => ['required', 'integer',
                Rule::exists('batches', 'id')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                    $query->where('class_id', request()->class_id);
                    $query->whereNull('deleted_at');
                })],
            'subject_id' => ['required', 'integer', new BatchSubject(request()->batch_id)],
        ];
    }
}
