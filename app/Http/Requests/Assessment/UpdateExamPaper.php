<?php

namespace App\Http\Requests\Assessment;

use App\ExamPaper;
use App\Helpers\PermissionsHelper;
use App\Rules\StatusCheck;
use Bouncer;
use Illuminate\Foundation\Http\FormRequest;

class UpdateExamPaper extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function __construct(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $files = array(), array $server = array(), $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $this->paper = ExamPaper::findOrFail(request()->route('paper'));
    }

    public function authorize(PermissionsHelper $ph)
    {
        return (Bouncer::can('manage-all-paper') ||
            (Bouncer::can('manage-paper') && $ph->teacherClass($this->paper->assessmentExam->class_id)) ||
            (Bouncer::can('manage-subject-paper') && $ph->hasSubject($this->paper->subject_id)));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $assessment = $this->paper;
        return [
            'name' => 'required|string|max:256',
            'detail' => 'nullable|string|max:1024',
//            'status' => ['required', 'integer', new StatusCheck($assessment->status)],
            'due_date' => 'required|date',
            'score' => 'required|integer',
            'type' => 'required|string|max:64',
        ];
    }
}
