<?php

namespace App\Http\Requests\Classes;

use App\Rules\StudentUser;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreBatchChange extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Bouncer::can('manage-all-student');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => ['required', 'integer', new StudentUser()],
            'to_batch_id' => ['required', 'integer',
                Rule::exists('batches', 'id')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                    $query->whereNull('deleted_at');
                })]
        ];
    }
}
