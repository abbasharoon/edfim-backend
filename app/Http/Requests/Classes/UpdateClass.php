<?php

namespace App\Http\Requests\Classes;

use Illuminate\Foundation\Http\FormRequest;

class UpdateClass extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Bouncer::can('manage-all-class');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
//            'user_ids'=>'array',
//            'user_ids.*' => ['integer',
//                    Rule::exists('users', 'id')->where(function ($query) {
//                        $query->where('tenant_id', config('app.tenant')->tenant_id);
//                    }),
//                ]
        ];
    }
}
