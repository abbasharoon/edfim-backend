<?php

namespace App\Http\Requests\Classes;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreClassChange extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Bouncer::can('manage-all-batch');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'batch_id' => ['required', 'integer',
                Rule::exists('batches', 'id')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                    $query->whereNull('deleted_at');
                })],
            'to_class_id' => ['required', 'integer',
                Rule::exists('classes', 'id')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                    $query->whereNull('deleted_at');
                })]
        ];
    }
}
