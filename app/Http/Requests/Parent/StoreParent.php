<?php

namespace App\Http\Requests\Parent;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreParent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Bouncer::can('manage-all-parent');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'first_name' => 'required|array',
            'first_name.*' => 'string|required|max:64',
            'last_name' => 'required|array',
            'last_name.*' => 'nullable|string|max:64',
            'middle_name' => 'nullable|array',
            'middle_name.*' => 'nullable|string|max:64',
            'email' => 'array',
            'email.*' => ['required_without_all:username.*,phone_no.*', 'nullable', 'email', 'max:512',
                Rule::unique('users', 'email')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })],
            'username' => 'nullable|array',
            'username.*' => ['required_without_all:email.*,phone_no.*', 'max:64',
                Rule::unique('users', 'username')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })],
            'phone_no' => 'required|array',
//            TODO: Implement Google Phone Lib library here
            'phone_no.*' => ['required_without_all:username.*,email.*', 'max:256',
                Rule::unique('users', 'phone_no')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })],
            'password' => 'array',
            'password.*' => 'nullable|string|max:256',
            'nic' => 'nullable|array',
            'nic.*' => 'nullable|string|max:256',
            'gender' => 'required|array',
            'gender.*' => 'required|integer|min:1|max:3',
            'dob' => 'required|array',
            'dob.*' => 'nullable|date',
            'picture_data' => 'required|array',
            'picture_data.*' => 'nullable|base64Image',
            'address' => 'required|array',
            'address.*' => 'nullable|string|max:64',
        ];

    }


}
