<?php

namespace App\Http\Requests\Parent;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateParent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Bouncer::can('manage-all-parent');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::findOrFail(request()->route('parent'));
        return [
            'first_name' => 'string|required|max:64',
            'last_name.*' => 'nullable|string|max:64',
            'middle_name' => 'nullable|string|max:64',
            'email' => ['required_without_all:username,phone_no', 'nullable', 'email', 'max:512',
                Rule::unique('users', 'email')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })->ignore($user->id)],
            'username' => ['required_without_all:email,phone_no', 'max:64',
                Rule::unique('users', 'username')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })->ignore($user->id)],
            'phone_no' => ['required_without_all:username,email', 'required', 'max:256',
                Rule::unique('users', 'phone_no')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })->ignore($user->id)],
            'nic' => 'nullable|string|max:256',
            'gender' => 'required|integer|min:1|max:3',
            'dob' => 'nullable|date',
            'picture_data' => 'nullable|base64Image',
            'address' => 'nullable|string|max:64',
        ];
    }
}
