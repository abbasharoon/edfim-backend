<?php

namespace App\Http\Requests\Accounting;

use Illuminate\Foundation\Http\FormRequest;

class AccountingRequest extends FormRequest
{

    /**
     * Set the company id to the request.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        // Get request data
        $data = $this->all();

        // Add active company id
        $data['tenant_id'] = config('app.tenant')->tenant_id;
        $data['currency_code'] = 'PKR';

        // Reset the request data
        $this->getInputSource()->replace($data);

        return parent::getValidatorInstance();
    }
}
