<?php

namespace App\Http\Requests\Accounting\Common;

use App\Http\Requests\Accounting\AccountingRequest;
use Illuminate\Validation\Rule;

class Item extends AccountingRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Check if store or update
        if ($this->getMethod() == 'PATCH' || $this->getMethod() == 'PUT') {
            $id = $this->item->getAttribute('id');
        } else {
            $id = null;
        }

        // Get company id
        $tenant_id = config('app.tenant')->tenant_id;

        return [
            'name' => 'required|string',
            'sku' => 'required|string|unique:accounting.items,NULL,' . $id . ',id,tenant_id,' . $tenant_id . ',deleted_at,NULL',
            'description' => 'nullable|string|max:512',
            'category_id' => 'nullable|integer',
            'quantity' => 'nullable|integer',
            'reorder_point' => 'nullable|integer',
            'image' => 'nullable|base64Image',

            'sales.amount' => 'required_with:sales.account_id|integer',
            'sales.description' => 'nullable|string|max:512',
            'sales.type' => 'required_with:sales.amount|boolean',
            'sales.tax_inclusive' => 'required_with:sales.amount|boolean',
            'sales.tax_id' => ['nullable', 'integer', Rule::exists('accounting.taxes', 'id')->where(function ($query) {
                $query->where('tenant_id', config('app.tenant')->tenant_id);
            })],
            'sales.account_id' => ['integer', 'required_with:sales.amount',
                Rule::exists('accounting.accounts', 'id')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })],

            'purchase.amount' => 'required_with:purchase.account_id|integer',
            'purchase.description' => 'nullable|string|max:512',
            'purchase.type' => 'required_with:purchase.amount|boolean',
            'purchase.tax_inclusive' => 'required_with:purchase.amount|boolean',
            'purchase.tax_id' => ['nullable', 'integer', Rule::exists('accounting.taxes', 'id')->where(function ($query) {
                $query->where('tenant_id', config('app.tenant')->tenant_id);
            })],
            'purchase.account_id' => ['integer', 'required_with:purchase.amount',
                Rule::exists('accounting.accounts', 'id')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })],
        ];
    }
}
