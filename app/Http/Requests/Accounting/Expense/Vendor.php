<?php

namespace App\Http\Requests\Accounting\Expense;

use App\Http\Requests\Accounting\AccountingRequest;

class Vendor extends AccountingRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $email = '';

        // Get company id
        $company_id = $this->request->get('tenant_ir');

        // Check if store or update
        if ($this->getMethod() == 'PATCH') {
            $id = $this->vendor->getAttribute('id');
        } else {
            $id = null;
        }

        if (!empty($this->request->get('email'))) {
            $email = 'email|unique:vendors,NULL,' . $id . ',id,tenant_id,' . $company_id . ',deleted_at,NULL';
        }

        return [
            'user_id' => 'nullable|integer',
            'name' => 'required|string',
            'email' => $email,
            'currency_code' => 'nullable|string|currency',
            'enabled' => 'integer|boolean',
        ];
    }
}
