<?php

namespace App\Http\Requests\Accounting\DoubleEntry;

use App\Http\Requests\Accounting\AccountingRequest;
use Illuminate\Validation\Rule;

class Account extends AccountingRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Get company id
        $tenant_id = $this->request->get('tenant_id');

        // Check if store or update
        if ($this->getMethod() == 'PATCH' || $this->getMethod() == 'PUT') {
            $id = $this->chart_of_account->getAttribute('id');
        } else {
            $id = null;
        }

        return [
            'name' => 'required|string',
            'description' => 'nullable|string|max:512',
            'parent_account_id' => ['nullable', 'integer', Rule::exists('accounting.accounts', 'id')->where(function ($query) {
                $query->where('tenant_id', config('app.tenant')->tenant_id);
            })],
            'number' => 'nullable|string|max:7|unique:accounting.accounts,NULL,' . $id . ',id,tenant_id,' . $tenant_id . ',deleted_at,NULL',
            'type_id' => ['required', 'integer', Rule::exists('accounting.account_types','id')->where(function ($query) {
                $query->where('tenant_id', config('app.tenant')->tenant_id);
            })],
            'tax_id' => ['nullable', 'integer', Rule::exists('accounting.taxes','id')->where(function ($query) {
                $query->where('tenant_id', config('app.tenant')->tenant_id);
            })]
        ];
    }
}
