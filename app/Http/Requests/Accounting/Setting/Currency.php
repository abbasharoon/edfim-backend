<?php

namespace App\Http\Requests\Accounting\Setting;

use App\Http\Requests\Accounting\AccountingRequest;

class Currency extends AccountingRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Check if store or update
        if ($this->getMethod() == 'PATCH' || $this->getMethod() == 'PUT') {
            $id = $this->currency->getAttribute('id');
        } else {
            $id = null;
        }

        // Get company id
        $tenant_id = config('app.tenant')->tenant_id;

        return [
            'name' => 'required|string',
            'code' => 'required|string|unique:accounting.currencies,NULL,' . $id . ',id,tenant_id,' . $tenant_id . ',deleted_at,NULL',
            'rate' => 'required',
            'enabled' => 'integer|boolean',
            'default_currency' => 'boolean',
            'symbol_first' => 'nullable|boolean',
        ];
    }
}
