<?php

namespace App\Http\Requests\Accounting\Setting;

use App\Http\Requests\Accounting\AccountingRequest;

class Tax extends AccountingRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'sales_rate' => 'nullable|min:0|max:100',
            'sales_account' => 'required_with:sales_rate|min:0|max:1',
            'purchase_rate' => 'nullable|min:0|max:100',
            'purchase_account' => 'required_with:purchase_rate|min:0|max:1',
            'type' => 'required|integer',
            'tax_agency_id' => 'required|integer|exists:accounting.tax_agencies,id,tenant_id,' . config('app.tenant')->tenant_id,
            'enabled' => 'integer|boolean',
        ];
    }
}
