<?php

namespace App\Http\Requests\Accounting\Income;

use App\Http\Requests\Accounting\AccountingRequest;
use App\Rules\Accounting\Currency;

class Customer extends AccountingRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $email = '';
        $required = '';

        // Get company id
        $tenant_id = config('app.tenant')->tenant_id;

        // Check if store or update
        if ($this->getMethod() == 'PATCH' || $this->getMethod() == 'PUT') {
            $id = $this->customer->getAttribute('id');
        } else {
            $id = null;
        }


        if (!empty($this->request->get('email'))) {
            $email = 'email|unique:accounting.customers,NULL,' . $id . ',id,tenant_id,' . $tenant_id . ',deleted_at,NULL';
        }

        return [
            'user_id' => 'nullable|integer',
            'name' => 'required|string',
            'email' => $email,
            'currency_code' => ['required', 'string', new Currency()],
            'enabled' => 'integer|boolean',
        ];
    }
}
