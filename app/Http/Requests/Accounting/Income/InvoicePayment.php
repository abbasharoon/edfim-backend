<?php

namespace App\Http\Requests\Accounting\Income;

use App\Http\Requests\Accounting\AccountingRequest;
use Carbon\Carbon;
use Illuminate\Validation\Rule;

class InvoicePayment extends AccountingRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'paid_at' => 'required|date_format:Y-m-d H:i:s',
            'amount' => 'required|amount',
//            'currency_code' => 'required|string|currency',
            'payment_method_id' => ['required', 'integer', Rule::exists('accounting.payment_methods', 'id')->where('tenant_id', config('app.tenant')->tenant_id)],
            'account_id' => ['required', 'integer', Rule::exists('accounting.accounts', 'id')->where('tenant_id', config('app.tenant')->tenant_id)],
//            'attachment' => 'nullable|mimes:jpeg,jpg,png,pdf',
        ];
    }

    public function withValidator($validator)
    {
        if ($validator->errors()->count()) {
            $paid_at = Carbon::parse($this->request->get('paid_at'))->format('Y-m-d');

            $this->request->set('paid_at', $paid_at);
        }
    }
}
