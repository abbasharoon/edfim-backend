<?php

namespace App\Http\Requests\Accounting\Income;

use App\Http\Requests\Accounting\AccountingRequest;
use App\Models\Accounting\Income\Invoice as InvoiceModel;
use App\Rules\Accounting\Currency;
use Carbon\Carbon;


class Invoice extends AccountingRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Check if store or update
        if ($this->getMethod() == 'PATCH' || $this->getMethod() == 'PUT') {
            $invoiceId = $this->invoice->getAttribute('id');
            $id = InvoiceModel::findOrFail($invoiceId);
            if ($id) {
                $id = $id->id;
            }
        } else {
            $id = null;
        }

        // Get company id
        $tenant_id = config('app.tenant')->tenant_id;

        return [
//            'invoice_number' => 'nullable|string|unique:invoices,NULL,' . $id . ',id,tenant_id,' . $tenant_id . ',deleted_at,NULL',
            'status' => 'required|integer|min:0|max:5',
            'invoiced_at' => 'required|date_format:Y-m-d H:i:s',
            'due_at' => 'required|date_format:Y-m-d H:i:s',
            'amount' => 'required',
            'item.*.name' => 'required|string',
            'item.*.quantity' => 'required',
            'item.*.price' => 'required|amount',
            'item.*.currency' => ['required', 'string', new Currency()],
            'currency_code' => ['required', 'string', new Currency()],
            'currency_rate' => 'required',
            'customer_id' => 'required|integer',
            'customer_name' => 'required|string',
            'category_id' => 'required|integer',
//            'attachment' => 'mimes:' . setting('general.file_types') . '|between:0,' . setting('general.file_size') * 1024,
        ];
    }

    public function withValidator($validator)
    {
        if ($validator->errors()->count()) {
            // Set date
            $invoiced_at = Carbon::parse($this->request->get('invoiced_at'))->format('Y-m-d');
            $due_at = Carbon::parse($this->request->get('due_at'))->format('Y-m-d');

            $this->request->set('invoiced_at', $invoiced_at);
            $this->request->set('due_at', $due_at);
        }
    }

    public function messages()
    {
        return [
            'item.*.name.required' => trans('validation.required', ['attribute' => mb_strtolower(trans('general.name'))]),
            'item.*.quantity.required' => trans('validation.required', ['attribute' => mb_strtolower(trans('invoices.quantity'))]),
            'item.*.price.required' => trans('validation.required', ['attribute' => mb_strtolower(trans('invoices.price'))]),
            'item.*.currency.required' => trans('validation.custom.invalid_currency'),
            'item.*.currency.string' => trans('validation.custom.invalid_currency'),
        ];
    }
}
