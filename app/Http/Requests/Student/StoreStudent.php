<?php

// namespace App\Http\Requests\Student;

// use Illuminate\Foundation\Http\FormRequest;
// use Illuminate\Validation\Rule;

namespace App\Http\Requests\Student;

use App\Role;
use App\Rules\ParentUser;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreStudent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Bouncer::can('manage-all-student');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $role = Role::where('name', 'parent')->first();
        return [
            'first_name' => 'required|array',
            'first_name.*' => 'string|required|max:64',
            'middle_name' => 'nullable|array',
            'middle_name.*' => 'string|max:64|nullable',
            'last_name' => 'required|array',
            'last_name.*' => 'nullable|string|max:64',
            'username' => 'required_without_all:email,phone_no|array',
            'username.*' => ['required_without_all:email.*,phone_no.*', 'max:64',
                Rule::unique('users', 'username')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })],
            'email' => 'required_without_all:username,phone_no|array',
            'email.*' => ['required_without_all:username.*,phone_no.*', 'nullable', 'email', 'max:512',
                Rule::unique('users', 'email')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })],
            'phone_no' => 'required_without_all:email,username|array',
            'phone_no.*' => ['required_without_all:username.*,email.*', 'nullable', 'max:256',
                Rule::unique('users', 'phone_no')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })],
            'password' => 'array',
            'password.*' => 'nullable|string|max:256',
            'parent_id' => 'required|array',
            'parent_id.*' => [
                'nullable', new ParentUser()
            ],
            'batch_id' => 'required|array',
            'batch_id .*' => [
                'nullable', 'integer',
                Rule::exists('batches', 'id')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                }),
            ],
            'roll_no' => 'required|array',
            'roll_no.*' => 'nullable|string|max:64',
            'gender' => 'required|array',
            'gender.*' => 'required|in:1,2,3',
            'dob' => 'required|array',
            'dob.*' => 'nullable|date',
            'address' => 'nullable|array',
            'address.*' => 'nullable|string|max:512',
            'picture_data' => 'nullable|array',
            'picture_data.*' => 'nullable|base64Image',
            'extra_fields' => 'nullable|array',
            'extra_fields.*' => 'nullable|string|max:1500',
        ];
    }


}
