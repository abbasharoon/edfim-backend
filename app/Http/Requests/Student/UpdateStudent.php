<?php

namespace App\Http\Requests\Student;

use App\Rules\ParentUser;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateStudent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Bouncer::can('manage-all-student');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::findOrFail(request()->route('student'));
        return [
            'first_name' => 'required|string|max:64',
            'middle_name' => 'nullable|string|max:64',
            'last_name.*' => 'nullable|string|max:64',
            'username' => ['required_without_all:email,phone_no', 'max:64',
                Rule::unique('users', 'username')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })->ignore($user->id)],
            'email' => ['required_without_all:username,phone_no', 'nullable', 'email', 'max:512',
                Rule::unique('users', 'email')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })->ignore($user->id)],
            'phone_no' => ['required_without_all:username,email', 'nullable', 'max:256',
                Rule::unique('users', 'phone_no')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })->ignore($user->id)],
            'batch_id' => [
                'nullable',
                Rule::exists('batches', 'id')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                }),
            ],
            'parent_id' => ['nullable', new ParentUser()],
            'gender' => 'required|numeric|in:1,2,3',
            'address' => 'nullable|string|max:256',
            'extra_fields' => 'nullable|string|max:1500',
            'dob' => 'nullable|date',
            'picture_data' => 'nullable|base64Image',
        ];
    }
}
