<?php

namespace App\Http\Requests\Classroom;

use App\Classroom;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class GetMeetingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (\Bouncer::can('manage-all-classroom') ||
            Classroom::findOrFail(request()->classroom_id)->whereHas('participant', function ($q) {
                $q->where('classroom_user.user_id', auth()->id());
            })->exists());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'classroom_id' => 'required|integer',
        ];
    }
}
