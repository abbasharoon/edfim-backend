<?php

namespace App\Http\Requests\Classroom;

use App\Classroom;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use \Bouncer;
class SaveMeeting extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Bouncer::can('manage-all-classroom') ||
            $classroom = Classroom::findOrFail(request()->classroom_id)->whereHas('moderator', function ($q) {
                $q->where('id', auth()->id());
            })->exists());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => 'required|string|max:126',
            'duration' => 'required|integer|max:30|min:5',
            'recorded' => 'nullable|boolean',
            'start_time' => 'required|date|after:'.strtotime('now'),
            'welcome_message' => 'nullable|string|max:5000',
            'classroom_id' => ['required', 'integer',
                Rule::exists('classrooms', 'id')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })],

        ];

    }
}
