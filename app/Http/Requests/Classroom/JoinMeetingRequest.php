<?php

namespace App\Http\Requests\classroom;

use App\Meeting;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class JoinMeetingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (\Bouncer::cannot('manage-all-classroom') || Meeting::findOrFail(request()->meeting_id)
                ->whereHas('classroom.participant', function ($q) {
                    $q->where('classroom_user.user_id', auth()->id());
                })->exists());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'meeting_id' => 'required|integer|exists:meetings'
        ];
    }
}
