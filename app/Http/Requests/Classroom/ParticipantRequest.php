<?php

namespace App\Http\Requests\Classroom;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ParticipantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Bouncer::can('manage-all-classroom');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'user_id' => ['required','integer',Rule::exists('users','id')->where(function($q){
//                $q->where('tenant_id', config('app.tenant')->tenant_id);
//            })],
            'classroom_id'  => ['required','integer',Rule::exists('classrooms','id')->where(function($q){
                $q->where('tenant_id', config('app.tenant')->tenant_id);
            })],
        ];
    }
}
