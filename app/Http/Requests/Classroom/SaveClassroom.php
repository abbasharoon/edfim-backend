<?php

namespace App\Http\Requests\Classroom;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use \Bouncer;

class SaveClassroom extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Bouncer::can('manage-all-classroom');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => 'required|string|max:126',
            'batch_id' => ['required', 'integer',
                Rule::exists('batches', 'id')->where(function ($query) {
                    $query->where('class_id', request()->class_id);
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })],
            'subject_id' => ['required', 'integer',
                Rule::exists('subjects', 'id')->where(function ($query) {
                    $query->where('class_id', request()->class_id);
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })],

        ];

    }
}
