<?php
// Departure 27/03/2018
namespace App\Http\Requests\Staff;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreStaff extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Bouncer::can('manage-all-staff');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userRole = auth()->user()->isA('system-admin');
        return [
            'first_name' => 'required|array',
            'first_name.*' => 'string|required',
            'last_name' => 'required|array',
            'last_name.*' => 'nullable|string',
            'middle_name' => 'nullable|array',
            'middle_name.*' => 'nullable|string',
            'email' => 'array',
            'email.*' => ['required_without_all:username.*,phone_no.*', 'nullable', 'email', 'max:512',
                Rule::unique('users', 'email')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })],
            'username' => 'nullable|array',
            'username.*' => ['nullable', 'required_without_all:email.*,phone_no.*', 'max:64',
                Rule::unique('users', 'username')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })],
            'phone_no' => 'required|array',
//            TODO: Implement Google Phone Lib library here
            'phone_no.*' => ['required_without_all:username.*,email.*', 'max:256',
                Rule::unique('users', 'phone_no')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })],
            'password' => 'array',
            'password.*' => 'nullable|string|max:256',
            'nic' => 'nullable|array',
            'nic.*' => 'nullable|string',
            'gender' => 'required|array',
            'gender.*' => 'required|integer|min:1|max:3',
            'dob' => 'array',
            'dob.*' => 'nullable|date',
            'role' => 'required|array',
            'role.*' => 'required|array',
            'role.*.*' => [
                'required', 'integer',
                Rule::exists('roles', 'id')->where(function ($query) use ($userRole) {
                    $query->where('scope', config('app.tenant')->tenant_id);
                    $query->where('name', '!=', 'parent');
                    $query->where('name', '!=', 'student');
                    if (!$userRole) {
                        $query->where('name', '!=', 'system-admin');
                    }
                }),
            ],
            'picture_data.*' => 'nullable|base64Image',
            'qualification' => 'array',
            'qualification.*' => 'nullable|string|max:512',
            'bio_data' => 'array',
            'bio_data.*' => 'nullable|string|max:512',
            'job_title' => 'array',
            'job_title.*' => 'nullable|string|max:64',
            'address' => 'array',
            'address.*' => 'nullable|string|max:64',
        ];
    }
}
