<?php

namespace App\Http\Requests\Staff;

use App\Role;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class UpdateStaff extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Bouncer::can('manage-all-staff');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userRole = auth()->user()->isA('system-admin');
        $forbiddenRoleIds = Role::whereIn('name', ['student', 'parent'])->get(['id']);
        $user = User::findOrFail(request()->route('staff'));
        return [
            'first_name' => 'string|required',
            'middle_name' => 'nullable|string',
            'last_name.*' => 'nullable|string',
            'email' => ['required_without_all:username,phone_no', 'nullable',
                Rule::unique('users')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })->ignore($user->id)],
            'username' => ['string', 'required_without_all:email,phone_no', 'max:128',
                Rule::unique('users')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })->ignore($user->id)],
            'phone_no' => ['required_without_all:username,email',
                Rule::unique('users')->where(function ($query) {
                    $query->where('tenant_id', config('app.tenant')->tenant_id);
                })->ignore($user->id)],
            'nic' => 'nullable|string:max:256',
            'gender' => 'required|in:1,2,3',
            'picture_data' => 'nullable|base64Image',
            'dob' => 'date',
            'qualification' => 'nullable|max:512',
            'bio_data' => 'nullable|max:512',
            'job_title' => 'nullable|max:64',
            'address' => 'nullable|max:64',
            'role_id' => 'array|required',
            'role_id.*' => [
                'required', 'integer',
                Rule::exists('roles', 'id')->where(function ($query) use ($userRole, $forbiddenRoleIds) {
                    $query->where('scope', config('app.tenant')->tenant_id);
                    $query->whereNotIn('name', $forbiddenRoleIds);
                    if (!$userRole) {
                        $query->where('name', '!=', 'system-admin');
                    }
                }),
            ],
        ];

    }

}
