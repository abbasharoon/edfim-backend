<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('base64Image', function ($attribute, $value, $params, $validator) {
            $img = base64_decode($value);
            $f = finfo_open();
            $mime = finfo_buffer($f, $img, FILEINFO_MIME_TYPE);

            if (strpos($mime, 'png') !== false ||
                strpos($mime, 'jpg') !== false ||
                strpos($mime, 'jpeg') !== false ||
                strpos($mime, 'bmp') !== false) {
                return true;
            }
            return false;
        });

        Validator::extend('amount', function ($attribute, $value, $parameters, $validator) use (&$amount) {
            $status = false;
            if ($value > 0) {
                $status = true;
            }
            $amount = $value;
            return $status;
        },
            trans('validation.custom.invalid_amount', ['attribute' => $amount])
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
