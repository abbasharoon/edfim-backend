<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserData extends Model
{
	/**
	 * The table associated with model
	 * @var string
	 */
    protected $table = 'users_data';
    protected $primaryKey = 'user_id';

    /**
     * Return the user basic information
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
    	return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Return the student basic information
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function student(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
