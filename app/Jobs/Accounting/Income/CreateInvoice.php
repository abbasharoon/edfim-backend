<?php

namespace App\Jobs\Accounting\Income;

use App\Events\Accounting\InvoiceCreated;
use App\Models\Accounting\Income\Invoice;
use App\Models\Accounting\Income\InvoiceHistory;
use App\Models\Accounting\Income\InvoiceTax;
use App\Models\Accounting\Income\InvoiceTotal;
use App\Traits\Accounting\Currencies;
use App\Traits\Accounting\DateTime;
use App\Traits\Accounting\Incomes;
use App\Traits\Accounting\Uploads;
use Illuminate\Foundation\Bus\Dispatchable;

class CreateInvoice
{
    use Currencies, DateTime, Dispatchable, Incomes, Uploads;

    protected $request;
    protected $invoice;

    /**
     * Create a new job instance.
     *
     * @param  $request
     */
    public function __construct($request, $invoice)
    {
        $this->request = $request;
        $this->invoice = $invoice;
    }

    /**
     * Execute the job.
     *
     * @return Invoice
     */
    public function handle()
    {


        // Upload attachment
        if ($this->request->file('attachment')) {
            $media = $this->getMedia($this->request->file('attachment'), 'invoices');

            $this->invoice->attachMedia($media, 'attachment');
        }

        $taxes = [];

        $tax_total = 0;
        $sub_total = 0;
        $discount_total = 0;
        $discount = $this->request['discount'];

        if ($this->request['taxes']) {
            foreach ($this->request->taxes as $tax) {
                $taxes[] = new InvoiceTax([
                    'tenant_id' => $this->request->tenant_id,
                    'tax_id' => $tax['tax_id'],
                    'name' => $tax['name'],
                    'rate' => $tax['rate'],
                    'taxable_amount' => (double)$tax['taxable_amount'],
                    'tax_amount' => $tax['tax_amount'],
                    'tax_rate' => $tax['tax_amount'],
                ]);
                $tax_total += $tax['tax_amount'];
            }
            $this->invoice->taxes()->saveMany($taxes);
        }
        if ($this->request['items']) {
            foreach ($this->request['items'] as $item) {
                dispatch(new CreateInvoiceItem($item, $this->invoice, $discount));
            }
        }


        // Apply discount to total
//        if ($discount) {
//            $s_discount = $this->request->amount * ($discount / 100);
//            $discount_total += $s_discount;
//            $s_total = $this->request - $s_discount;
//        }

//        $amount = $s_total + $tax_total;

//        $this->request['amount'] = money($amount, $this->request['currency_code'])->getAmount();

//        $this->invoice->update($this->request->input());

        // Add invoice totals
        $this->addTotals($this->invoice, $this->request, $taxes, $sub_total, $discount_total, $tax_total);

        // Add invoice history
        InvoiceHistory::create([
            'tenant_id' => session('tenant_id'),
            'invoice_id' => $this->invoice->id,
            'status_code' => 'draft',
            'notify' => 0,
            'description' => trans('messages.success.added', ['type' => $this->invoice->invoice_number]),
        ]);

        // Update next invoice number
//        $this->increaseNextInvoiceNumber();

        // Recurring
        $this->invoice->createRecurring();

        // Fire the event to make it extensible
        event(new InvoiceCreated($this->invoice));

        return $this->invoice;
    }

    protected function addTotals($invoice, $request, $taxes, $sub_total, $discount_total, $tax_total)
    {
        // Check if totals are in request, i.e. api
        if (!empty($request['totals'])) {
            $sort_order = 1;

            foreach ($request['totals'] as $total) {
                $total['invoice_id'] = $this->invoice->id;

                if (empty($total['sort_order'])) {
                    $total['sort_order'] = $sort_order;
                }

                InvoiceTotal::create($total);

                $sort_order++;
            }

            return;
        }

        $sort_order = 1;

        // Added invoice sub total
        InvoiceTotal::create([
            'tenant_id' => $request['tenant_id'],
            'invoice_id' => $this->invoice->id,
            'code' => 'sub_total',
            'name' => 'invoices.sub_total',
            'amount' => $sub_total,
            'sort_order' => $sort_order,
        ]);

        $sort_order++;

        // Added invoice discount
        if ($discount_total) {
            InvoiceTotal::create([
                'tenant_id' => $request['tenant_id'],
                'invoice_id' => $this->invoice->id,
                'code' => 'discount',
                'name' => 'invoices.discount',
                'amount' => $discount_total,
                'sort_order' => $sort_order,
            ]);

            // This is for total
            $sub_total = $sub_total - $discount_total;

            $sort_order++;
        }

        // Added invoice taxes
        if (isset($taxes)) {
            foreach ($taxes as $tax) {
                InvoiceTotal::create([
                    'tenant_id' => $request['tenant_id'],
                    'invoice_id' => $this->invoice->id,
                    'code' => 'tax',
                    'name' => $tax['name'],
                    'amount' => $tax['amount'],
                    'sort_order' => $sort_order,
                ]);

                $sort_order++;
            }
        }

        // Added invoice total
        InvoiceTotal::create([
            'tenant_id' => $request['tenant_id'],
            'invoice_id' => $this->invoice->id,
            'code' => 'total',
            'name' => 'invoices.total',
            'amount' => $sub_total + $tax_total,
            'sort_order' => $sort_order,
        ]);
    }
}
