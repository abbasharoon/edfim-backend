<?php

namespace App\Jobs\Accounting\Income;

use App\Models\Accounting\Income\InvoiceHistory;
use App\Models\Accounting\Income\InvoicePayment;
use Illuminate\Foundation\Bus\Dispatchable;

class CreateInvoicePayment
{
    use Dispatchable;


    protected $invoice;

    protected $invoice_payment;

    /**
     * Create a new job instance.
     *
     * @param  $request
     * @param  $invoice
     */
    public function __construct($request, $invoice, $invoice_payment)
    {
        $this->invoice = $invoice;
        $this->invoice_payment = $invoice_payment;
    }

    /**
     * Execute the job.
     *
     * @return InvoicePayment
     */
    public function handle()
    {
        $desc_amount = money((float)$this->invoice_payment->amount, (string)$this->invoice_payment->currency_code, true)->format();

        $history_data = [
            'tenant_id' => config('app.tenant')->tenant_id,
            'invoice_id' => $this->invoice_payment->invoice_id,
            'status_code' => $this->invoice->status,
            'notify' => '0',
            'description' => $desc_amount . ' ' . trans_choice('general.payments', 1),
        ];

        InvoiceHistory::create($history_data);

        return $this->invoice_payment;
    }
}