<?php

namespace App\Jobs\Accounting\Income;

use App\Events\Accounting\InvoiceUpdated;
use App\Models\Accounting\Common\Item;
use App\Models\Accounting\Income\Invoice;
use App\Models\Accounting\Income\InvoiceTax;
use App\Models\Accounting\Income\InvoiceTotal;
use App\Traits\Accounting\Currencies;
use App\Traits\Accounting\DateTime;
use App\Traits\Accounting\Incomes;
use App\Traits\Accounting\Uploads;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateInvoice
{
    use Currencies, DateTime, Dispatchable, Incomes, Uploads;

    protected $invoice;

    protected $request;

    /**
     * Create a new job instance.
     *
     * @param  $request
     */
    public function __construct($invoice, $request)
    {
        $this->invoice = $invoice;
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return Invoice
     */
    public function handle()
    {
        // Upload attachment
        if ($this->request->file('attachment')) {
            $media = $this->getMedia($this->request->file('attachment'), 'invoices');

            $this->invoice->attachMedia($media, 'attachment');
        }

        $taxes = [];

        $tax_total = 0;
        $sub_total = 0;
        $discount_total = 0;
        $discount = $this->request['discount'];

        $this->deleteRelationships($this->invoice, 'taxes');
        if ($this->request->taxes) {
            foreach ($this->request->taxes as $tax) {
                $taxes[] = new InvoiceTax([
                    'tenant_id' => $this->request->tenant_id,
                    'tax_id' => $tax['tax_id'],
                    'name' => $tax['name'],
                    'rate' => $tax['rate'],
                    'taxable_amount' => money($tax['taxable_amount'], $this->request['currency_code'])->getAmount(),
                    'tax_amount' => money($tax['tax_amount'], $this->request['currency_code'])->getAmount(),
                ]);
                $tax_total += $tax['tax_amount'];
            }
            $sub_total = $this->request->amount - $tax_total;
            $this->invoice->taxes()->saveMany($taxes);
        }

        $this->deleteRelationships($this->invoice, 'items');
        if ($this->request->items) {
            foreach ($this->request->items as $item) {
                if (empty($item->item_id)) {
                    continue;
                }

                $item_object = Item::findOrFail($item->item_id);

                $item_object->quantity += (double)$item->quantity;
                $item_object->save();
            }

            foreach ($this->request['items'] as $item) {
                dispatch(new CreateInvoiceItem($item, $this->invoice, $discount));
            }
        }


        // Apply discount to total
//        if ($discount) {
//            $s_discount = $s_total * ($discount / 100);
//            $discount_total += $s_discount;
//            $s_total = $s_total - $s_discount;
//        }

//        $amount = $s_total + $tax_total;

        $this->request['amount'] = money($this->request->amount, $this->request['currency_code'])->getAmount();

        $invoice_paid = $this->invoice->paid;

        unset($this->invoice->reconciled);

        if (($invoice_paid) && $this->request->amount > $invoice_paid) {
            $this->request['status'] = 2;
        }

        $this->invoice->update($this->request->input());

        // Delete previous invoice totals
        $this->deleteRelationships($this->invoice, 'totals');

        // Add invoice totals
//        $this->addTotals($this->invoice, $this->request, $taxes, $sub_total, $discount_total, $tax_total);

        // Recurring
        $this->invoice->updateRecurring();

        // Fire the event to make it extensible
        event(new InvoiceUpdated($this->invoice));

        return $this->invoice;
    }

    protected function addTotals($invoice, $request, $taxes, $sub_total, $discount_total, $tax_total)
    {
        // Check if totals are in request, i.e. api
        if (!empty($request['totals'])) {
            $sort_order = 1;

            foreach ($request['totals'] as $total) {
                $total['invoice_id'] = $invoice->id;

                if (empty($total['sort_order'])) {
                    $total['sort_order'] = $sort_order;
                }

                InvoiceTotal::create($total);

                $sort_order++;
            }

            return;
        }

        $sort_order = 1;

        // Added invoice sub total
        InvoiceTotal::create([
            'tenant_id' => $request['tenant_id'],
            'invoice_id' => $invoice->id,
            'code' => 'sub_total',
            'name' => 'invoices.sub_total',
            'amount' => $sub_total,
            'sort_order' => $sort_order,
        ]);

        $sort_order++;

        // Added invoice discount
        if ($discount_total) {
            InvoiceTotal::create([
                'tenant_id' => $request['tenant_id'],
                'invoice_id' => $invoice->id,
                'code' => 'discount',
                'name' => 'invoices.discount',
                'amount' => $discount_total,
                'sort_order' => $sort_order,
            ]);

            // This is for total
            $sub_total = $sub_total - $discount_total;

            $sort_order++;
        }

        // Added invoice taxes
        if (isset($taxes)) {
            foreach ($taxes as $tax) {
                InvoiceTotal::create([
                    'tenant_id' => $request['tenant_id'],
                    'invoice_id' => $invoice->id,
                    'code' => 'tax',
                    'name' => 'invoices.tax',
                    'amount' => $tax['amount'],
                    'sort_order' => $sort_order,
                ]);

                $sort_order++;
            }
        }

        // Added invoice total
        InvoiceTotal::create([
            'tenant_id' => $request['tenant_id'],
            'invoice_id' => $invoice->id,
            'code' => 'total',
            'name' => 'invoices.total',
            'amount' => $sub_total + $tax_total,
            'sort_order' => $sort_order,
        ]);
    }

    /**
     * Mass delete relationships with events being fired.
     *
     * @param  $model
     * @param  $relationships
     *
     * @return void
     */
    public function deleteRelationships($model, $relationships)
    {
        foreach ((array)$relationships as $relationship) {
            if (empty($model->$relationship)) {
                continue;
            }

            $items = $model->$relationship->all();

            if ($items instanceof Collection) {
                $items = $items->all();
            }

            foreach ((array)$items as $item) {
                $item->delete();
            }
        }
    }
}
