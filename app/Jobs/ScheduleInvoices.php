<?php

namespace App\Jobs;

use anlutro\LaravelSettings\Facade as SettingAuLurto;
use App\Classes;
use App\ClassInvoice;
use App\Helpers\InvoiceHelper;
use App\TenantHostname;
use App\User;
use App\UserInvoice;
use Carbon\Carbon;
use HipsterJazzbo\Landlord\Facades\Landlord;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;


class ScheduleInvoices implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $timeout = 500;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $classes = '';
    private $schedule = '';
    private $tenantId;

    public function __construct($tenantId, $schedule)
    {
        Landlord::removeTenant('tenant_id');
        $this->classes = Classes::where('tenant_id', $tenantId)->get();
        $this->schedule = $schedule;
        $this->tenantId = $tenantId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $schedule = $this->schedule;
        $tenantHostname = TenantHostname::where('tenant_id', $this->tenantId)->first();
        Landlord::addTenant('tenant_id', $this->tenantId);
        Landlord::applyTenantScopes(new User());
//        Landlord::applyTenantScopes(new ClassInvoice());
//        Landlord::applyTenantScopes(new UserInvoice());
        config(['app.tenant' => $tenantHostname]);
        SettingAuLurto::setExtraColumns(['tenant_id' => $this->tenantId]);
        $this->classes->each(function ($item, $key) use ($schedule) {
            $req = new \Illuminate\Http\Request();
            $req->schedule = $schedule;
            $req->class_id = $item['id'];
            $req->status = 1;
            $req->due_date = Carbon::now()->addDays(SettingAuLurto::get('schedule.due_days'))->toDateTimeString();
            InvoiceHelper::createClassInvoice($req);
//            Landlord::removeTenant('tenant_id');
        });
    }
}
