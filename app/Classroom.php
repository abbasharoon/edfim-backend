<?php

namespace App;

use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\Model;

class Classroom extends Model
{
    use BelongsToTenants;
    protected $hidden = ['tenant_id'];


    public function participant()
    {
        return $this->belongsToMany('App\User')->withPivot('role');
    }

    public function moderator()
    {
        return $this->belongsToMany('App\User')->wherePivot('role', '1');
    }


    public function meeting()
    {
        return $this->hasMany('App\Meeting');
    }

    public function classes()
    {
        return $this->belongsTo('App\Classes');
    }

    public function batch()
    {
        return $this->belongsTo('App\Batch');
    }

    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }


}
