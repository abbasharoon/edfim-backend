<tr>
    <td class="header">
        @if(setting('tenant.logo'))
            <img src="http://{{config('app.tenant')->hostname}}/storage/{{config('app.tenant')->hostname.
                    '/'.setting('tenant.logo')}}"/>
        @else
            <a href="{{ $url }}">
                {{setting('tenant.name')}}
            </a>
        @endif
    </td>
</tr>
