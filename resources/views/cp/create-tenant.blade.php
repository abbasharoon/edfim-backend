@extends('cp.main')

@section('title', 'Create Institution Account')

@section('content-title')
    New Institution
@endsection

@section('content')
    <script>
      let errors = <?php echo json_encode($errors->all()) ?>;
      console.log(errors);
    </script>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form action="/api/admin/create" method="POST">
                <div class="box box-warning">
                    <div class="box-header ui-sortable-handle">
                        <i class="fa fa-envelope"></i>

                        <h3 class="box-title">Institution Details</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            {{--<button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">--}}
                            {{--<i class="fa fa-times"></i></button>--}}
                        </div>
                        <!-- /. tools -->
                    </div>
                    <div class="box-body">
                        {{csrf_field()}}
                        <div class="form-group {{$errors->has('institutionName')?'has-error':''}}">
                            <input type="text" name="institutionName" class="form-control"
                                   placeholder="Institution Name">
                            @if($errors->has('institutionName'))
                                <span class="help-block">{{$errors->first('institutionName')}}</span>
                            @endif
                        </div>
                        <div class="form-group {{$errors->has('institutionAddress')?'has-error':''}}">
                            <input type="text" name="institutionAddress" class="form-control"
                                   placeholder="Institution Address">
                            @if($errors->has('institutionName'))
                                <span class="help-block">{{$errors->first('institutionName')}}</span>
                            @endif
                        </div>
                        <div class="form-group {{$errors->has('institutionPhone')?'has-error':''}}">
                            <input type="tel" name="institutionPhone" class="form-control"
                                   placeholder="Institution Phone">
                            @if($errors->has('institutionPhone'))
                                <span class="help-block">{{$errors->first('institutionPhone')}}</span>
                            @endif
                        </div>
                        <div class="form-group {{$errors->has('institutionStudents')?'has-error':''}}">
                            <input type="number" name="institutionStudents" class="form-control"
                                   placeholder="Students Count">
                            @if($errors->has('institutionStudents'))
                                <span class="help-block">{{$errors->first('institutionStudents')}}</span>
                            @endif
                        </div>
                        <div class="form-group {{$errors->has('additionalSms')?'has-error':''}}">
                            <input type="number" name="additionalSms" class="form-control"
                                   placeholder="Additional SMS (per month)">
                            @if($errors->has('additionalSms'))
                                <span class="help-block">{{$errors->first('additionalSms')}}</span>
                            @endif
                        </div>
                        <div class="input-group {{$errors->has('institutionHostname')?'has-error':''}}">
                            <label>Portal Url</label>
                            <input type="text" name="institutionHostname" class="form-control"
                                   placeholder="schoolName">
                            <span class="input-group-addon">.edfim.com</span>
                            @if($errors->has('institutionHostname'))
                                <span class="help-block">{{$errors->first('institutionHostname')}}</span>
                            @endif
                        </div>
                        <div class="form-group {{$errors->has('subscriptionPlan')?'has-error':''}}">
                            <label>Subscription Plan</label>
                            <select class="form-control" name="subscriptionPlan">
                                <option value="1">Basic</option>
                                <option value="2">Standard</option>
                                <option value="3">Premium</option>
                            </select>
                            @if($errors->has('subscriptionPlan'))
                                <span class="help-block">{{$errors->first('subscriptionPlan')}}</span>
                            @endif
                        </div>
                        <div class="form-group {{$errors->has('paidFrom')?'has-error':''}}">
                            <input type="date" name="paidFrom" class="form-control" placeholder="Paid From">
                            @if($errors->has('Paid From'))
                                <span class="help-block">{{$errors->first('Paid From')}}</span>
                            @endif
                        </div>
                        <div class="form-group {{$errors->has('paidTill')?'has-error':''}}">
                            <input type="date" name="paidTill" class="form-control" placeholder="Paid Till">
                            @if($errors->has('Paid Till'))
                                <span class="help-block">{{$errors->first('Paid Till')}}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="box box-info">
                    <div class="box-header ui-sortable-handle">
                        <i class="fa fa-envelope"></i>

                        <h3 class="box-title">Admin Account Details</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            {{--<button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">--}}
                            {{--<i class="fa fa-times"></i></button>--}}
                        </div>
                        <!-- /. tools -->
                    </div>
                    <div class="box-body">
                        <div class="form-group {{$errors->has('adminFirstName')?'has-error':''}}">
                            <input type="text" name="adminFirstName" class="form-control"
                                   placeholder="Admin First Name">
                            @if($errors->has('adminFirstName'))
                                <span class="help-block">{{$errors->first('adminFirstName')}}</span>
                            @endif
                        </div>
                        <div class="form-group {{$errors->has('adminLastName')?'has-error':''}}">
                            <input type="text" name="adminLastName" class="form-control" placeholder="Admin Last Name">
                            @if($errors->has('adminLastName'))
                                <span class="help-block">{{$errors->first('adminLastName')}}</span>
                            @endif
                        </div>
                        <div class="form-group {{$errors->has('adminUserName')?'has-error':''}}">
                            <input type="text" name="adminUserName" class="form-control" placeholder="Admin UserName">
                            @if($errors->has('adminUserName'))
                                <span class="help-block">{{$errors->first('adminUserName')}}</span>
                            @endif
                        </div>
                        <div class="form-group {{$errors->has('adminEmail')?'has-error':''}}">
                            <input type="text" name="adminEmail" class="form-control" placeholder="Admin Email">
                            @if($errors->has('adminEmail'))
                                <span class="help-block">{{$errors->first('adminEmail')}}</span>
                            @endif
                        </div>
                        <div class="form-group {{$errors->has('adminPhone')?'has-error':''}}">
                            <input type="text" name="adminPhone" class="form-control" placeholder="Admin Phone">
                            @if($errors->has('adminPhone'))
                                <span class="help-block">{{$errors->first('adminPhone')}}</span>
                            @endif
                        </div>
                        <div class="form-group {{$errors->has('adminPassword')?'has-error':''}}">
                            <input type="password" name="adminPassword" class="form-control"
                                   placeholder="Admin Password">
                            @if($errors->has('adminPassword'))
                                <span class="help-block">{{$errors->first('adminPassword')}}</span>
                            @endif
                        </div>
                        <div class="form-group {{$errors->has('adminConfirmPassword')?'has-error':''}}">
                            <input type="password" name="adminConfirmPassword" class="form-control"
                                   placeholder="Admin Confirm Password">
                            @if($errors->has('adminConfirmPassword'))
                                <span class="help-block">{{$errors->first('adminConfirmPassword')}}</span>
                            @endif
                        </div>
                    </div>
                    <div class="box-footer clearfix">
                        <button type="submit" class="pull-right btn btn-default" id="sendEmail">Send
                            <i class="fa fa-arrow-circle-right"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection