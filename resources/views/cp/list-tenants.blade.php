@extends('cp.main')

@section('title', 'Institutions Summary')

@section('content-title')
    Institutions Summary
@endsection

@section('content')
    <div class="col-12">
        <div class="box">
            <div class="box-header with-border">
                {{--<h3 class="box-title">Bordered Table</h3>--}}
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Hostname</th>
                        <th>Users</th>
                        <th>Sms</th>
                        <th>Users</th>
                        <th>Package</th>
                    </tr>
                    @foreach ($tenants as $tenant)
                        <tr>
                            <td>{{$tenant->id}}</td>
                            <td>{{$tenant->name}}</td>
                            <td>{{$tenant->tenantHostname[0]->hostname}}</td>
                            <td>
                                @foreach($tenant->tenantSettings as $k)
                                    @if($k['key']==='tenant.allowed_accounts')
                                        <div class="progress progress-xs">
                                            <div class="progress-bar progress-bar-danger"
                                                 style="width:{{($tenant->tenant_users_count/$k['value'])*100}}%"></div>
                                        </div>
                                        {{$tenant->tenant_users_count}}/{{$k['value']}}
                                    @endif
                                @endforeach
                            </td>
                            <td>
                                @foreach($tenant->tenantSettings as $k)
                                    @if($k['key']==='sms.quota')
                                        &nbsp;q:{{$k['value']}}
                                    @endif
                                    @if($k['key']==='sms.count')
                                        &nbsp;c:{{$k['value']}}
                                    @endif
                                @endforeach
                            </td>
                            <td>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
                                </div>
                            </td>
                            <td><span class="badge bg-red">55%</span></td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <!-- /.box-body -->
            {{ $tenants->links() }}
            {{--<div class="box-footer clearfix">--}}
            {{--<ul class="pagination pagination-sm no-margin pull-right">--}}
            {{--<li><a href="#">&laquo;</a></li>--}}
            {{--<li><a href="#">1</a></li>--}}
            {{--<li><a href="#">2</a></li>--}}
            {{--<li><a href="#">3</a></li>--}}
            {{--<li><a href="#">&raquo;</a></li>--}}
            {{--</ul>--}}
            {{--</div>--}}
        </div>
    </div>
@endsection