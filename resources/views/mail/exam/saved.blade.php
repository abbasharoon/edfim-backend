@component('mail::message')

#Hi {{$data['name']}},

Results for {{$data['exam']['name']}} has been declared. {{$data['addressBy']}} secured {{$data['score']['position']}} position in class.
Details of results are follow:

@component('mail::table')
|Subject|Paper          | Score |
|:-------|:------------- |-------------:|
@foreach($data['papers'] as $papers)
@php($subject=null)
@foreach($papers as $paper)
|@if(!$subject){{$paper->subject->name}}@endif&nbsp;|{{$paper->name}} | {{$data['score'][$paper->id]}}|
@php($subject=true)
@endforeach
@endforeach
|Total |       |{{$data['score']['total']}}       |
@endcomponent

Contact Administration if you have any question if you have any queries,<br>

Thanks,<br>
{{ setting('tenant.name') }}
@endcomponent
