@component('mail::message')
### Hi {{$data['name']}},

{{$data['addressBy']}} an invoice due to be paid by {{$data['due_data']}}.
You are hereby requested to pay the invoice on time to avoid any inconvenience.
For your reference, invoice number is &#35;{{$data['invoice_id']}} Detail of invoice is as follow.

 @component('mail::table')
| Fee           | Amount |
| :------------- |-------------:|
@foreach($data['invoices'] as $invoice)
|{{$invoice['name']}}|@if(isset($invoice['percentage_amount']))  {{'('.$invoice['percentage_amount'].')' }} @endif {{$invoice['amount']}} |
@if (isset($invoice['target_fee']))
@foreach($invoice['target_fee'] as $target)
|<small>&nbsp;&nbsp;&nbsp;&nbsp;{{$target['name']}}</small>|<small>@if(isset($target['percentage_amount']))  {{'('.$target['percentage_amount'].')'}} @endif {{$target['amount']}}</small>|
@endforeach
@endif
@endforeach
|<strong>Total</strong>      |<strong>{{$data['total']}}</strong>       |
@endcomponent

Contact Support if you have any queries,<br>
Thanks
{{ setting('general.name') }}
