<?php

return [
    'classCreated' => 'Class has been successfully created',
    'classNotCreated' => 'Sorry class can\'t be created',
    'classUpdated' => 'Class has been updated',
    'classDeleted' => 'Class has been successfully deleted',
    'timeTableCreated' => 'Schedule has been successfully stored',
    'timeTableNotCreated' => 'Schedule doesn\'t created',
    'timeTableUpdated' => 'Schedule has been updated',
    'timeTableNotUpdated' => 'Schedule doesn\'t updated',
    'timetableDeleted' => 'Schedule has been successfully deleted',
    'timetableNotDeleted' => 'An error occured while deleting the Timetable entry. Please try again',
    'bulkStudentCreated' => 'Students has been successfully created',
    'bulkStudentNotCreated' => 'Sorry students can\'t be created',
    'batchCreated' => 'Batch has been successfully created',
    'batchError' => 'There has a error. Please try again later',
    'batchUpdated' => 'Batch has been successfully updated',
    'batchDeleted' => 'Batch has been successfully deleted',
    'batchNoValidClass' => 'There has no valid class for this batch',
    'batchValid' => 'Please select a valid batch',
    'staffCreated' => 'Users has been successfully created',
    'staffNotCreated' => 'Users doesn\t successfully created',
    'staffUpdated' => 'User has been successfully updated',
    'staffNotUpdated' => 'User doesn\'t successfully updated',
    'staffDeleted' => 'User has been successfully deleted',
    'logBatchCreated' => 'Batch account created',
    'logBatchGet' => 'Batch account retrieved',
    'logBatchDeleted' => 'Batch account deleted',
    'logBatchUpdated' => 'Batch account updated',
    'logStudentCreated' => 'Student account created',
    'logTimetableCreated' => 'Time table created',
    'logTimetableDeleted' => 'Time table deleted',
    'logTimetableUpdated' => 'Time table updated',
    'logTimetableGet' => 'Time table retrieved',
    'logTimetableIndex' => 'Time table index',
    'logStaffDeleted' => 'Staff Account Deleted',
    'logStaffUpdated' => 'Staff Account Updated',
    'logStaffCreated' => 'New Staff Account(s) Created',
    'logClassesIndex' => 'Classes all get',
    'logClassesCreated' => 'Classes created',
    'logClassesGet' => 'Get class',
    'logClassesUpdate' => 'Class updated',
    'logClassesDeleted' => 'Class deleted',
    'logParentDeleted' => 'A Parent\'s account has been deleted',
    'logParentUpdated' => 'A Parent\'s account has been updated',
    'logParentCreated' => 'New Parents account created',
    'studentUpdated' => 'Student has been successfully updated',
    'studentNotUpdated' => 'Student doesn\'t updated',
    'studentDeleted' => 'Student has been successfully deleted',
    'bulkParentCreated' => 'Parents has been successfully created',
    'bulkParentNotCreated' => 'Sorry parents can\'t be created',
    'parentUpdated' => 'Parent has been successfully updated',
    'parentNotUpdated' => 'Parent doesn\t updated',
    'parentDeleted' => 'Parent has been successfully deleted',
    'feesCreated' => 'Fees created',
    'logFeesCreated' => 'Fees created',
    'feesUpdated' => 'Fees has been successfully updated',
    'logFeesUpdated' => 'Fees has been successfully updated',
    'feesDeleted' => 'Fees deleted',
    'logFeesDeleted' => 'Fees deleted',
    'classInvoiceCreated' => 'Class Invoice created',
    'logClassInvoiceCreated' => 'Class Invoice created',
    'studentInvoiceCreated' => 'Student Invoice created',
    'logStudentInvoiceCreated' => 'Student Invoice created',
    'subjectCreated' => 'Subject created',
    'logSubjectCreated' => 'Subject created',
    'subjectUpdated' => 'Subject updated',
    'logSubjectUpdated' => 'Subject updated',
    'subjectDeleted' => 'Subject deleted',
    'logSubjectDeleted' => 'Subject deleted',
    'settingUpdated' => 'Setting updated',

    'absentReasonDeleted' => 'Absent Reason Deleted',
    'logAbsentReasonDeleted' => 'Absent Reason Deleted',

    'absentReasonUpdated' => 'Absent Reason Updated',
    'absentReasonNotUpdated' => 'Absent Reason Updated',
    'logAbsentReasonUpdated' => 'Absent Reason Updated',

    'absentReasonCreated' => 'Absent Reason Created',
    'absentReasonNotCreated' => 'Absent Reason Not Created',
    'logAbsentReasonCreated' => 'Absent Reason Created',

    'attendanceCreated' => 'Attendance created',
    'attendanceNotCreated' => 'Attendance Not created',
    'logAttendanceCreated' => 'Attendance created',

    'attendanceAbsentCreated' => 'Attendance Absent created',
    'attendanceAbsentNotCreated' => 'Attendance Absent Not created',
    'logAttendanceAbsentCreated' => 'Attendance Absent created',

    'attendanceAbsentDeleted' => 'Attendance absent deleted',
    'attendanceAbsentNotDeleted' => 'Attendance Absent Not deleted',
    'logAttendanceAbsentDeleted' => 'Attendance Absent deleted',

    'attendanceUpdated' => 'Attendance updated',
    'attendanceNotUpdated' => 'Attendance Not Updated',
    'logAttendanceUpdated' => 'Attendance updated',

    'logBadgeCreated' => 'Badge created',
    'badgeCreated' => 'Badge created',

    'badgeDeleted' => 'Badge deleted',
    'logBadgeDeleted' => 'Badge deleted',


    'logAssignmentCreated' => 'Assignment Created',
    'attendanceClosed' => 'Attendance already closed',
    'assignmentCreated' => 'Assignment Successfully Created',
    'assignmentNotCreated' => 'Failed to create assignment',

    'logAssignmentUpdated' => 'Assignment Updated',
    'assignmentUpdated' => 'Assignment Successfully Updated',
    'assignmentNotUpdated' => 'Failed to updated assignment',

    'logAssignmentDeleted' => 'Assignment deleted',
    'assignmentDeleted' => 'Assignment Successfully Deleted',
    'assignmentNotDeleted' => 'Failed to create deleted',

    'logAssignmentScoreCreated' => 'Assignment Score Added',
    'assignmentScoreCreated' => 'Assignment Score Added',
    'assignmentScoreNotCreated' => 'Assignment Score Not Added',
    'assignmentScoreCancelled' => 'Assignment is cancelled. Score cannot be added',

    'logAssignmentScoreUpdated' => 'Assignment Score Updated',
    'assignmentScoreUpdated' => 'Assignment Score Updated',
    'assignmentScoreNotUpdated' => 'Assignment Score Not Updated',

    'assessmentTestCreated' => 'Assessment Successfully Created',
    'assessmentTestNotCreated' => 'Failed to create Assessment',
    'logAssessmentTestCreated' => 'Assessment Created',

    'assessmentTestUpdated' => 'Assessment Successfully Updated',
    'assessmentTestNotUpdated' => 'Failed to Updated Assessment',
    'logAssessmentTestUpdated' => 'Assessment Updated',

    'assessmentTestDeleted' => 'Assessment Successfully Deleted',
    'assessmentTestNotDeleted' => 'Failed to delete Assessment',
    'logAssessmentTestDeleted' => 'Assessment deleted',

    'assessmentExamCreated' => 'Assessment Successfully Created',
    'assessmentExamNotCreated' => 'Failed to Created Assessment',
    'logAssessmentExamCreated' => 'Assessment deleted',

    'assessmentExamUpdated' => 'Assessment Successfully Updated',
    'assessmentExamNotUpdated' => 'Failed to update Assessment',
    'logAssessmentExamUpdated' => 'Assessment updated',

    'assessmentExamDeleted' => 'Assessment Successfully Deleted',
    'assessmentExamNotDeleted' => 'Failed to delete Assessment',
    'logAssessmentExamDeleted' => 'Assessment deleted',


    'assessmentPaperCreated' => 'Paper Successfully Created',
    'assessmentPaperNotCreated' => 'Failed to Created Paper',
    'logAssessmentPaperCreated' => 'Paper deleted',

    'assessmentPaperUpdated' => 'Paper Successfully Updated',
    'assessmentPaperNotUpdated' => 'Failed to update Paper',
    'logAssessmentPaperUpdated' => 'Paper updated',

    'assessmentPaperDeleted' => 'Paper Successfully Deleted',
    'assessmentPaperNotDeleted' => 'Failed to delete Paper',
    'logAssessmentPaperDeleted' => 'Paper deleted',


    'feeCategoryDeletedCreated' => 'Fee Category Successfully Created',
    'feeCategoryDeletedNotCreated' => 'Failed to Created Fee Category',
    'logFeeCategoryDeletedCreated' => 'Paper deleted',

    'feeCategoryDeletedUpdated' => 'Fee Category Successfully Updated',
    'feeCategoryDeletedNotUpdated' => 'Failed to update Fee Category',
    'logFeeCategoryDeletedUpdated' => 'Fee Category updated',

    'feeCategoryDeletedDeleted' => 'Fee Category Successfully Deleted',
    'feeCategoryDeletedNotDeleted' => 'Failed to delete Fee Category',
    'logFeeCategoryDeletedDeleted' => 'Fee Category deleted',


    'success' => [
        'added' => ':type added!',
        'updated' => ':type updated!',
        'deleted' => ':type deleted!',
        'duplicated' => ':type duplicated!',
        'imported' => ':type imported!',
        'enabled' => ':type enabled!',
        'disabled' => ':type disabled!',
    ],

    'error' => [
        'over_payment' => 'The amount you entered passes the total: :amount',
        'not_user_company' => 'Error: You are not allowed to manage this company!',
        'customer' => 'Error: User not created! :name already uses this email address.',
        'no_file' => 'Error: No file selected!',
        'last_category' => 'Error: Can not delete the last :type category!',
        'invalid_token' => 'Error: The token entered is invalid!',
        'import_column' => 'Error: :message Sheet name: :sheet. Line number: :line.',
        'import_sheet' => 'Error: Sheet name is not valid. Please, check the sample file.',
    ],

    'warning' => [
        'deleted' => 'Warning: You are not allowed to delete <b>:name</b> because it has :text related.',
        'disabled' => 'Warning: You are not allowed to disable <b>:name</b> because it has :text related.',
        'disable_code' => 'Warning: You are not allowed to disable or change the currency of <b>:name</b> because it has :text related.',
    ],


];